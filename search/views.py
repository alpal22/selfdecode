from django.http import JsonResponse
from django.shortcuts import render
from haystack.backends import SQ
from haystack.query import SearchQuerySet

from chemical.models import Chemical, HealthEffect, Organism
from decodify.helpers import paginate
from experiment.models import Identification
from forum.models import Post
from genome.helpers import get_gene_scores, badges_for_gene
from genome.models import DiseaseTrait, GeneOntology
from genome.models import Gene
from genome.models import Snp


# possible filters for search view


aliases = [
    {'filter_string': 'chemical', 'name': 'Chemical'},
    {'filter_string': 'organism', 'name': 'Organism'},
    {'filter_string': 'gene', 'name': 'Gene'},
    {'filter_string': 'snp', 'name': 'Snp'},
    {'filter_string': 'disease', 'name': 'Disease'},
    {'filter_string': 'measurement', 'name': 'Measurement'},
    {'filter_string': 'trait', 'name': 'Trait'},
    {'filter_string': 'health_effect', 'name': 'Health effect'},
    {'filter_string': 'identification', 'name': 'Identification'},
    {'filter_string': 'molecular', 'name': 'Molecular function'},
    {'filter_string': 'biological', 'name': 'Biological process'},
    {'filter_string': 'forum', 'name': 'Forum'}
]


def search(request):
    # turn all of the separate objects into a big looped dict with type, name, and description
    query = request.GET.get('query')
    queryset = SearchQuerySet().all().using('fulltext').filter(
        SQ(text__exact=query)
        | SQ(description__contains=query)
        | SQ(title__contains=query)
        | SQ(body__contains=query)
    )

    filter_string = request.GET.get('filter', '')

    valid_aliases = []

    def model_filter(qs, filter_str):
        return {
            'chemical': qs.models(Chemical),
            'organism': qs.models(Organism),
            'gene': qs.models(Gene),
            'snp': qs.models(Snp),
            'disease': qs.filter(SQ(category__exact="Disease")).models(DiseaseTrait),
            'measurement': qs.filter(SQ(category__exact="Measurement")).models(DiseaseTrait),
            'trait': qs.filter(SQ(category__exact="Trait")).models(DiseaseTrait),
            'health_effect': qs.models(HealthEffect),
            'identification': qs.models(Identification),
            'molecular': qs.filter(SQ(type__exact="molecular-function")).models(GeneOntology),
            'biological': qs.filter(SQ(type__exact="biological-process")).models(GeneOntology),
            'forum': qs.models(Post)
        }.get(filter_str, qs)

    for item in aliases:
        if model_filter(queryset, item.get('filter_string')).count() > 0:
            valid_aliases.append(item)

    queryset = model_filter(queryset, filter_string)

    result_page = paginate(queryset, request.GET.get('page'))

    bad_genes = []
    contains_risk_allele = []
    gene_scores = []
    if request.user.is_authenticated():
        gene_ids = [item.pk for item in result_page.object_list if item.model_name == 'gene']
        if gene_ids:
            contains_risk_allele, bad_genes = badges_for_gene(request.user, gene_ids)

    return render(request, "search/search.html", {
        'page_obj': result_page,
        'found_items': queryset.count(),
        'bad_genes': bad_genes,
        'contains_risk_allele': contains_risk_allele,
        'search_items': valid_aliases,
        'gene_scores': gene_scores
    })


def global_autocomplete(request):
    query = request.GET.get('query', '')
    index = SearchQuerySet().all().using('default')
    queryset = index.filter(SQ(text__exact=query) | SQ(text__contains=query))[:20]
    result = {
        'query': query,
        'items': []
    }
    for item in queryset:
        label = item.model_name.title()
        slug = item.slug or item.text
        type_ = item.model_name
        if item.model_name == 'diseasetrait' and item.category == 'Disease':
            label = 'Disease'
        elif item.model_name == 'diseasetrait' and item.category == 'Trait':
            label = 'Trait'
        elif item.model_name == 'diseasetrait' and item.category == 'Measurement':
            label = 'Measurement'
        elif item.model_name == 'healtheffect':
            type_ = 'health-effect'
            label = 'Health Effect'
        elif item.model_name == 'tag':
            type_ = 'forum/tag'
            label = 'Tag'
        elif item.model_name == 'post':
            type_ = 'forum/post'
            label = 'Post'
        result['items'].append({
            'value': item.text,
            'label': label,
            'slug': slug,
            'type': type_
        })
    return JsonResponse(result)
