from django.conf.urls import url

from search import views

urlpatterns = [
    url(r'^search/$', views.search, name="main"),
    url(r'^search/autocomplete/$', views.global_autocomplete, name="autocomplete"),
]
