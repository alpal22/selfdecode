
(function () {
    $('[data-action="global-autocomplete"]').autocomplete({
        source: function(request, response){
            var $this = this.element;
            $.ajax({
                url: $this.data("url"),
                data: { query: request.term },
                dataType: 'json',
                success: function(data){
                    response(data.items);
                },
                error: function(){
                    response([{
                        label: "Error",
                        value: "Something went wrong..."
                    }]);
                }
            });
        },
        select: function(event, ui){
            var url = window.location.origin + '/' + ui.item.type + '/' + ui.item.slug;
            window.open(url, '_blank');
        }
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.label+ ':  ' + item.value + "</div>" ).appendTo( ul );
    };
})();
