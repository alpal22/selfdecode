CKEDITOR.plugins.add('variance', {
    icons: 'variance',
    init: function (editor) {
        editor.addCommand('varianceDialog', new CKEDITOR.dialogCommand('varianceDialog'));
        editor.ui.addButton('Variance', {
            label: 'Insert variance report shortcode',
            command: 'varianceDialog',
            toolbar: 'insert'
        });
        CKEDITOR.dialog.add('varianceDialog', function (editor) {
            return {
                title: 'Variance Report Shortcode',
                minWidth: 400,
                minHeight: 200,
                resizable: false,
                contents: [
                    {
                        id: 'tab-basic',
                        label: 'Basic Settings',
                        elements: [
                            {
                                type: 'select',
                                id: 'mafField',
                                items: [['0.5'], ['0.3'], ['0.1'], ['0.05'], ['0.025'], ['0.01'], ['0.005']],
                                label: 'Minor allele frequency',
                                'default': '0.5'
                            },
                            {
                                type: 'select',
                                id: 'reputationField',
                                items: [['all'], ['good'], ['bad'], ['neutral']],
                                label: 'Reputation',
                                'default': 'all'
                            },
                            {
                                type: 'select',
                                id: 'importanceField',
                                items: [['1'], ['2'], ['3'], ['4'], ['5']],
                                label: 'Importance',
                                'default': '2'
                            },
                            {
                                type: "hbox",
                                widths: ["25%", "75%"],
                                children: [
                                    {
                                        type: 'select',
                                        id: 'SearchObjectField',
                                        items: [['gene'], ['chemical'], ['disease']],
                                        label: 'Search Object',
                                        'default': 'gene'
                                    },
                                    {
                                        type: 'text',
                                        id: 'queryField',
                                        label: 'Query'
                                    }
                                ]
                            },
                            {
                                type: 'checkbox',
                                id: 'curatedContentField',
                                label: 'Search for SNPs with curated content only',
                                default: ''
                            }

                        ]
                    }
                ],
                onOk: function () {
                    var maf = this.getContentElement('tab-basic', 'mafField').getValue();
                    var rep = this.getContentElement('tab-basic', 'reputationField' ).getValue();
                    var imp = this.getContentElement('tab-basic', 'importanceField' ).getValue();
                    var searchObj = this.getContentElement('tab-basic', 'SearchObjectField' ).getValue();
                    var query = this.getContentElement('tab-basic', 'queryField' ).getValue();
                    var has_description = this.getContentElement('tab-basic', 'curatedContentField' ).getValue();
                    has_description = has_description ? 'on' : '';

                    var shortcode = '<div>[variance_report ' +
                        ' maf=' + maf +
                        ' reputation=' + rep +
                        ' importance=' + imp +
                        ' search_object=' + searchObj +
                        ' query=' + '"' + query + '"' +
                        ' has_description=' + has_description +
                        ' sbmt=true' +
                        ']</div>';

                    var html = CKEDITOR.dom.element.createFromHtml(shortcode, editor.document);
                    editor.insertElement(html);
                }
            }
        });
    }
});