
$(document).ready(function () {
    $.inlineNotification = function (message, type, closeable, timeout) {
        type = type || "info";
        timeout = timeout || 5000;
        closeable = closeable || true;

        if(closeable)
            message = '<a href="#" class="close" data-dismiss="alert">&times;</a>' + message;

        var $notify_container = $(".notifications-section");
        var $alert = $('<div class="alert alert-' + type + '">' + message + '</div>');
        $notify_container.css("margin-top", "30px").append($alert);

        setTimeout(function () {
            $alert.fadeOut(function () {
                $(this).remove();
                if($notify_container.find(".alert").length == 0)
                    $notify_container.css("margin-top", "0");
            });
        }, timeout);
    };

    $(document).on('click', '#clear-all-btn', function (event) {
        // event.preventDefault();
        $.get('/forum/clear-notifications/')
    });
});
