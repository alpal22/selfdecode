// simple string formatting "Test:{0}".format('test')
String.prototype.format = function () {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};

String.prototype.toTitle = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

$('.tooltip-scrollable').tooltip({
    trigger: 'manual'
});

function subscribe_tooltip() {
    $('.scrollable').tooltip({
    trigger: 'manual'
    }).on('mouseenter', function () {
        $('.scrollable').not(this).tooltip('hide');

        $(this).tooltip('show');
        $(this).next().on('mouseleave', function () {
            $(this).prev().tooltip('hide');
        });
    });
}

function resubscribe_tooltipsy(selector) {
    var tags = $(selector+',[data-subscribed="unsub"]').not('[title=""]');
    tags.tooltipsy();
    tags.each(function () {
        $(this).data('tooltipsy').readify();
        $(this).attr('data-subscribed', 'sub')
    });
}

function destroy_tooltipsy(selector) {
    if($(selector).data('tooltipsy')) {
        $(selector).each(function () {
            if( $(this).data('tooltipsy')) {
                console.log($(this).data('tooltipsy'));
                $(this).data('tooltipsy').destroy();
            }
        });
    }
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

subscribe_tooltip();
