(function () {
    function resubscribe_tooltipsy() {
        var tags = $('[data-tooltip="tooltipsy"]').not('[title=""]');
        tags.tooltipsy();
        tags.each(function () {
            $(this).data('tooltipsy').readify();
            $(this).attr('data-subscribed', 'sub')
        });
    }

    $('[data-action="load-more"]').each(function () {
        var $this = $(this);
        var api_url = $this.attr('data-url');
        var items = [];
        var loading_text = "Loading...";
        var initial_text = $this.html();
        $this.on('click', function () {
            var $tmpl = $($this.attr('data-tmpl'));
            var $container = $($this.parents('[data-plugin="collapsible"]'))
            var $output_container = $container.find('[data-container="list"]');

            $this.html(loading_text);
            $this.attr('disabled', true);

            $.get(api_url, function (response) {
                if(response.next) {
                    $this.html(initial_text);
                    $this.attr('disabled', false);
                    api_url = response.next;
                } else {
                    $this.remove();
                }
                items = items.concat(response.results);
                var node = $tmpl.tmpl({
                    response: response,
                    items: items,
                    app: window.app
                });
                $output_container.html(node);
            }).always(function () {
                resubscribe_tooltipsy();
            });
        });
    });

    $('[data-action="load-modal"]').each(function () {
        var $this = $(this);
        var api_url = $this.attr('data-url');
        var modal_title = $this.attr('data-title');
        $this.on('click', function () {
            var $tmpl = $($this.attr('data-tmpl'));
            var $container = $($this.attr('data-container'));
            var $output_container = $container.find('.modal-content');
            $.get(api_url, function (response) {
                var inner = $tmpl.tmpl({
                    response: response,
                    app: window.app,
                    modal_title: modal_title
                });
                $output_container.html(inner);
                $container.modal("show");
            }).always(function () {
                resubscribe_tooltipsy();
            });
        });
    });

    $('[data-plugin="collapsible"]').each(function () {
        var $this = $(this);
        var $target = $this.find('.panel-heading');
        $target.collapsible(function (promise) {
            promise.then(function () {
                $this.find('[data-action="load-more"]').trigger('click');
                $this.find('[data-action="old-load-more"]').trigger('click');
            });
        });
    });

    $.triggerAnchor().then(function ($this) {
        var $icon = $this.find('.fa');
        $icon.hasClass('fa-plus');
        $icon.removeClass("fa-plus").addClass("fa-minus");
        $this.next().slideToggle();
        $.scrollToElement($this);
        $this.parent().find('[data-action="load-more"]').trigger("click");
        $this.parent().find('[data-action="old-load-more"]').trigger("click");
    });
})();