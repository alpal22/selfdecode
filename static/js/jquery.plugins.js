
(function () {

    $.fn.collapsible = function (initialize) {
        $.each(this, function () {
            var obj = $.Deferred();
            initialize && initialize(obj);
            return (function () {
                this.on('click', function () {
                    var $this = $(this);
                    var target = $this.attr('data-target');
                    var $target = $(target || $this.next());
                    var $icon = $this.find('.fa');
                    if ($icon.hasClass('fa-plus')) {
                        $icon.removeClass("fa-plus").addClass("fa-minus");
                        window.location.hash = $this.attr('data-hash');
                    } else {
                        $icon.removeClass("fa-minus").addClass("fa-plus");
                    }
                    $target.slideToggle();
                    obj.resolve($this);
                    return false;
                });
            }).call($(this))
        });
    };

    $.triggerAnchor = function () {
        var hash = window.location.hash.slice(1);
        var $target = $('[data-hash="{0}"]'.format(hash));
        var result = $.Deferred();
        if ($target.length > 0)
            result.resolve($target);
        else
            result.reject();
        return result;
    };

    $.scrollToElement = function ($element) {
        $('html, body').animate({
            scrollTop: parseInt($element.offset().top - $(".top-navbar").height() - 10)
        }, 1000);
    };

    $.cookie = {
		set: function(name, value, days, sessionOnly) {
			var expires = "";

			if(sessionOnly)
				expires = "; expires=0"
			else if(days) {
				var date = new Date();
				date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
				expires = "; expires=" + date.toGMTString();
			}

			document.cookie = name + "=" + value + expires + "; path=/";
		},

		get: function(name, _default) {
			var nameEQ = name + "=";
			var ca = document.cookie.split(";");

			for(var i = 0; i < ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == " ") c = c.substring(1, c.length);
				if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
			}

			return _default || null;
		},

		remove: function(name) {
			this.set(name, "", -1);
		}
	}

})();
