from django.conf.urls import url

import fileapi.views

urlpatterns = [
    url(r'^sse/$', fileapi.views.Sse.as_view(), name="sse"),
    url(r'^upload/attempt/$', fileapi.views.upload_attempt, name="upload_attempt"),
    url(r'^upload/test_file_write/$', fileapi.views.test_file_write, name="test_file_write"),
    url(r'^upload/test_file_delete/$', fileapi.views.test_file_delete, name="test_file_delete"),

    url(r'^change_active_file/', fileapi.views.change_active_file, name="change_active_file"),
    url(r'^remove_file/', fileapi.views.remove_file, name="remove_file"),
    url(r'^rescan_file/', fileapi.views.rescan_file, name="rescan_file"),
    url(r'^process_uploaded_file/', fileapi.views.process_uploaded_file, name="process_uploaded_file"),
]
