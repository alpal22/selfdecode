from django.contrib import admin

# Register your models here.
from fileapi.forms import FileFormAdmin
from fileapi.models import UploadAttempt
from genome.models import File


@admin.register(UploadAttempt)
class UploadAttemptAdmin(admin.ModelAdmin):
    list_display = ("user", "message", "uploaded",)
    search_fields = ("user__email", "user__username",)
    readonly_fields = ("user", "created_at", "message",)
    fields = ("user", "message", "created_at",)
    ordering = ("-created_at",)

    def uploaded(self, inst):
        return inst.created_at.strftime("%c %Z")

    def has_add_permission(self, request):
        return False


@admin.register(File)
class FileAdmin(admin.ModelAdmin):
    form = FileFormAdmin
    list_display = ('id', 'file_name', 'original_name', 'provider', 'service', 'rescan_available', 'user_email', 'status', 'created_at',)
    list_filter = ("status", 'provider', 'service', 'rescan_available',)
    readonly_fields = ('id',)
    search_fields = ('user__email',)

    def user_email(self, inst):
        return inst.user.email
