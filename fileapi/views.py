import os.path
from time import sleep

import json

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse, StreamingHttpResponse
from django.shortcuts import redirect, render, get_object_or_404
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.views.generic import View

from decodify.decorators import disallow_demouser
from fileapi import tasks
from fileapi.models import UploadAttempt
from genome.decorators import subscription_required
from genome.models import File
from payment.helpers import get_paypal_form
from payment.models import Subscription, Plan, Coupon


@login_required
@disallow_demouser
@subscription_required(redirect_url="/checkout")
def uploader(request):
    subscription = Subscription.get_active(request.user)
    user_plan = None
    extra_price = None
    plans_qs = Plan.objects.order_by("price")
    if subscription:
        user_plan = subscription.plan
        extra_price = int(user_plan.price / 2) / 100
        plans_qs = plans_qs.exclude(pk=user_plan.pk)

    plans = {}
    for plan in plans_qs.all():
        coupon = Coupon.objects.active().filter(discount_code=request.GET.get("cdc"), supported_plans=plan).first()
        setattr(plan, 'coupon', coupon)
        plans[plan.plan_id] = plan

    return render(request, "genome/uploader.html", {
        'X23ANDME_CLIENT_ID': settings.X23ANDME_CLIENT_ID,
        'user_plan': user_plan,
        'extra_price': extra_price,
        'plans': plans,
        "STRIPE_API_KEY": settings.STRIPE_PUBLIC_KEY,
        "forms": {plan_id: get_paypal_form(request, plan_id, item).render() for plan_id, item in plans.items()},
    })


@require_POST
@login_required
@disallow_demouser
def change_active_file(request):
    file = get_object_or_404(File, user=request.user, pk=request.POST.get("file_id"))
    request.user.user_profile.active_file = file
    request.user.user_profile.save()
    return redirect(reverse("dashboard"))


@require_POST
@login_required
@disallow_demouser
def rescan_file(request):
    file = get_object_or_404(
        File,
        Q(status=File.FILE_STATUS_COMPLETED) | Q(status=File.FILE_STATUS_UPLOADED) | Q(status=File.FILE_STATUS_ERROR),
        user=request.user,
        pk=request.POST.get("file_id"),
        rescan_available=True
    )
    file.progress = 0
    file.status = File.FILE_STATUS_QUEUED
    file.status_message = None
    file.save()

    dashboard_uri = request.build_absolute_uri("/dashboard")

    if file.provider == File.PROVIDER_DIRECT_UPLOAD:
        tasks.process_genome_file.delay(request.user.id, dashboard_uri, file.pk)
    elif file.provider == File.PROVIDER_23ANDME_API:
        tasks.upload_23andme.delay(request.user.pk, code=None, file_pk=file.pk)

    return redirect("%s#section-my-files" % reverse("dashboard"))


class Sse(View):
    def get_status(self):
        user = self.request.user

        if self.request.GET.getlist('files[]'):
            qs = user.file_set.filter(pk__in=self.request.GET.getlist('files[]'))
        else:
            qs = user.file_set.filter(Q(status=File.FILE_STATUS_PROCESSING) | Q(status=File.FILE_STATUS_QUEUED))
        processed_files = qs.all()
        files_ids = [o.pk for o in processed_files]
        files = user.file_set.filter(
            Q(status=File.FILE_STATUS_PROCESSING)
            | Q(status=File.FILE_STATUS_QUEUED)
            | Q(status=File.FILE_STATUS_COMPLETED)
            | Q(status=File.FILE_STATUS_ERROR)
        ).filter(pk__in=files_ids)

        return self.send_message("file-progress", [
            {
                "file_id": file.pk,
                "progress": "%.0f" % file.progress,
                "status": file.status,
                "status_display": file.get_status_display(),
            } for file in files
        ])

    def dispatch(self, request, *args, **kwargs):
        self.request = request
        response = HttpResponse(self.get_status())
        return response

    def send_message(self, event, data):
        if isinstance(data, (dict, list,)):
            data = json.dumps(data)
        return """{"event": "%s", "data": %s}""" % (event, data,)


@login_required
@require_POST
@disallow_demouser
@csrf_exempt
def upload_attempt(request):
    useragent = request.META.get("HTTP_USER_AGENT")
    error_message = request.POST.get("error_message")
    action = request.POST.get("action")
    filename = request.POST.get("filename")
    message = request.POST.get("message")

    data = ""
    if useragent:
        data += "UserAgent: %s\n" % useragent
    if error_message:
        data += "ErrorMessage: %s\n" % error_message
    if action:
        data += "ActionButton: %s\n" % action
    if filename:
        data += "Filename: %s\n" % filename
    if message:
        data += "Message: %s\n" % message

    UploadAttempt.objects.create(
        user=request.user,
        message=data
    )
    return HttpResponse()


@login_required
@disallow_demouser
def uploader_23andme(request):
    if request.user.user_profile.is_file_uploads_quota_exceeded:
        raise PermissionError("File upload quota exceeded")
    code = request.GET.get("code")
    file = File.objects.create(
        file_name="23andMe_Connect_" + str(request.user.email),
        user=request.user,
        status=File.FILE_STATUS_QUEUED,
        provider=File.PROVIDER_23ANDME_API,
        service=File.SERVICE_23ANDME,
        rescan_available=True
    )
    if code:
        tasks.upload_23andme.delay(request.user.id, code, file.pk)
    else:
        tasks.upload_23andme.delay(request.user.pk, code=None, file_pk=file.pk)
    return redirect("%s#section-my-files" % reverse("dashboard"))


def _create_file(request):
    file = File.objects.filter(
        status=File.FILE_STATUS_QUEUED,
        file_name=request.GET["file_name"],
        user=request.user
    ).first()
    if file is None:
        full_name = request.GET["file_name"]
        filename, extension = os.path.splitext(full_name)
        file = File.objects.create(
            file_name=filename,
            original_name=full_name,
            hashed_name=full_name,
            user=request.user,
            status=File.FILE_STATUS_QUEUED,
            provider=File.PROVIDER_DIRECT_UPLOAD,
            rescan_available=True
        )
    request.user.user_profile.active_file = file
    request.user.user_profile.save()
    return file


@login_required
@disallow_demouser
@subscription_required()
@csrf_exempt
def process_uploaded_file(request):
    if request.user.user_profile.is_file_uploads_quota_exceeded:
        raise PermissionError("File upload quota exceeded")
    dashboard_uri = request.build_absolute_uri(reverse("dashboard"))
    file = _create_file(request)

    tasks.process_genome_file.delay(request.user.id, dashboard_uri, file.pk)

    return redirect("%s#section-my-files" % reverse("dashboard"))


@login_required
@disallow_demouser
def remove_file(request):
    file = get_object_or_404(File, user=request.user, pk=request.POST.get("file_id"))
    file.deleted_at = timezone.now()
    file.save()

    tasks.delete_uploaded_file.delay(request.user.pk, file.pk)

    return redirect(reverse("dashboard"))


static_dir = 'static/'


def test_file_write(request):
    filename = 'test.txt'
    context = "this is test contest for file"
    f = open(static_dir + filename, "w")
    f.write(context)
    f.close()
    return HttpResponse({"success": True})


def test_file_delete(request):
    filename = 'test.txt'
    os.remove(static_dir + filename)
    return HttpResponse({"success": True})
