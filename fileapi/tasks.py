import gzip
import io
import traceback
from io import BytesIO
from io import StringIO

import zipfile

import boto3
import json

import linecache
import magic
import numpy
import pandas as pd
import requests
import sys

from datetime import timedelta

import shutil
import vcf
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.utils import timezone
from django_pandas.io import read_frame
from itertools import islice

from genome.models import Snp, File, UserProfile, Gene, UserRsid, UserGeneReputation
from fileapi.api23andme import Api23AndMe
from genome.tasks import *


def get_s3_data(file_name):
    os.environ['S3_USE_SIGV4'] = S3_USE_SIGV4
    os.environ["AWS_ACCESS_KEY_ID"] = AWS_ACCESS_KEY_ID
    os.environ["AWS_SECRET_ACCESS_KEY"] = AWS_SECRET_ACCESS_KEY
    s3_client = boto3.client('s3')
    response = s3_client.get_object(Bucket=settings.S3_BUCKET, Key=str(file_name))
    data = response["Body"].read()
    return data


def get_s3_data_to_file(file_name):
    static_dir = 'static/'
    os.environ['S3_USE_SIGV4'] = S3_USE_SIGV4
    os.environ["AWS_ACCESS_KEY_ID"] = AWS_ACCESS_KEY_ID
    os.environ["AWS_SECRET_ACCESS_KEY"] = AWS_SECRET_ACCESS_KEY
    s3_client = boto3.client('s3')
    s3_client.download_file(settings.S3_BUCKET, str(file_name), static_dir + file_name)
    return static_dir + file_name


def check_genotype_style(genotype, snp):
    genotype = genotype.strip()
    if len(genotype) > 1 and genotype[0] != genotype[1]:
        return "heterozygous"
    elif len(genotype) > 1 and (genotype[0] == genotype[1]) and genotype[0] == snp.minor_allele:
        return "homozygous_minor"
    elif len(genotype) > 1 and (genotype[0] == genotype[1]) and genotype[0] != snp.minor_allele:
        return "homozygous_major"
    elif len(genotype) == 1 and genotype[0] != snp.minor_allele:
        return "hemizygous_major"
    elif len(genotype) == 1 and genotype[0] == snp.minor_allele:
        return "hemizygous_minor"
    elif genotype == 'II':
        return "double_insertion"
    elif genotype == 'DD':
        return "double_deletion"
    elif genotype == 'I':
        return "insertion"
    elif genotype == 'D':
        return "deletion"
    elif genotype == '--' or genotype == '-' or genotype == 'NC' or genotype == "":
        return "unknown"


# Unzip .gz and return io.StringIO object
def unzip_xgz(file_name='', *args, **kwargs):

    with gzip.open(file_name, 'rb') as f_in:
        with open(file_name + ".uncompressed", 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)

    return file_name + ".uncompressed"


def unzip_gz(*args, **kwargs):
    gfile = gzip.GzipFile(*args, **kwargs)
    zipped_buffer = StringIO()

    # Read gzipped file by chuncks until it finished with exception...
    try:
        while True:
            zipped_buffer.write(gfile.read1().decode('utf-8'))
    except OSError:
        pass

    zipped_buffer.seek(0)

    return zipped_buffer


# Unzip zipped file and return archive, unzipped file and name of unzipped file
def unzip_zip(file_name=''):
    archive = zipfile.ZipFile(file_name, 'r')
    name = archive.namelist()[0]
    file = archive.open(name)
    with open(file_name + ".uncompressed", 'wb') as f_out:
        f_out.write(file.read())
        f_out.close()
    return archive, file_name + ".uncompressed", name


# unzip supported zip file and return file like object
def unzip_any(buffer):
    mime_type = magic.from_buffer(buffer, mime=True)
    print("Mime type: %s" % mime_type)
    if mime_type == 'application/gzip':
        file = unzip_xgz(fileobj=BytesIO(buffer))
        archive = None
        file_name = None
    elif mime_type == 'application/x-gzip':
        file = unzip_xgz(fileobj=BytesIO(buffer))
        archive = None
        file_name = None
    elif mime_type == 'application/zip':
        archive, file, file_name = unzip_zip(buffer)
    else:
        raise Exception("Cannot unzip file with mime type: %s" % mime_type)
    return archive, file, file_name


def unzip_any_file(archive_file_name):
    mime_type = magic.from_file(archive_file_name, mime=True)
    print("Mime type: %s" % mime_type)
    if mime_type == 'application/gzip':
        file = unzip_xgz(file_name=archive_file_name)
        archive = None
        file_name = None
    elif mime_type == 'application/x-gzip':
        file = unzip_xgz(file_name=archive_file_name)
        archive = None
        file_name = None
    elif mime_type == 'application/zip':
        archive, file, file_name = unzip_zip(archive_file_name)
    else:
        raise Exception("Cannot unzip file with mime type: %s" % mime_type)
    return archive, file, file_name


def send_completed_email(dashboard_uri, user, file):
    send_mail("Your analysis is ready", """
    Your file was successfully processed. You can check details and generated reports following the link below:<br/>
    %s
    """ % dashboard_uri, "noreplay@selfdecode.com", [user.email])


def handle_errors(file):
    e_t, e_o, tb = sys.exc_info()
    f = tb.tb_frame
    line = linecache.getline(f.f_code.co_filename, tb.tb_lineno, f.f_globals)
    file.status = File.FILE_STATUS_ERROR
    file.status_message = "Error line: %s (%s: %s): '%s'" % (tb.tb_lineno, e_t, e_o, line.strip(),)
    file.save()
    traceback.print_exc()
    print(file.status_message)


def get_results(user_rsid, snp, file):
    zygosities = {
        'heterozygous': ('Heterozygous', '+/-', 'O',),
        'homozygous_major': ('Homozygous Major', '-/-', 'G',),
        'homozygous_minor': ('Homozygous Minor', '+/+', 'B',),
        'hemizygous_major': ('Hemizygous Major', '-', 'G',),
        'hemizygous_minor': ('Hemizygous Minor', '+', 'B',),
        'unknown': ('Unknown', '?/?', 'U',),
        'double_insertion': ('Double Insertion', 'I/I', 'O',),
        'double_deletion': ('Double Deletion', 'D/D', 'O',),
        'insertion': ('Single Insertion', 'I', 'O',),
        'deletion': ('Single Deletion', 'D', 'O',),
    }
    genotype_style_falls = {
        "heterozygous": "heterozygous_color",
        "insertion": "heterozygous_color",
        "deletion": "heterozygous_color",
        "double_insertion": "heterozygous_color",
        "double_deletion": "heterozygous_color",

        "homozygous_minor": "homozygous_minor_color",
        "hemizygous_minor": "homozygous_minor_color",
        "minor": "homozygous_minor_color",

        "homozygous_major": "homozygous_major_color",
        "hemizygous_major": "homozygous_major_color",
        "major": "homozygous_major_color",
    }
    colors = {
        'green': ('G', 'Good', 'success',),
        'yellow': ('O', 'Okay', 'warning',),
        'red': ('B', 'Bad', 'danger',),
        'gray': ('U', 'Unknown', 'default',),
    }
    color_field = genotype_style_falls[user_rsid.genotype_style]
    color = getattr(snp, color_field, "").strip()
    if color:
        reputation = colors.get(color)[0]
    else:
        reputation = zygosities.get(user_rsid.genotype_style)[2]
    return (
        reputation,
        user_rsid.genotype_style,
    )


def calculate_total_reputation(file):
    # MULTIPLIER - If reputation of the genotype is good (G), multiplier is 0,
    # if reputation is slightly bad, multiplier is 0.5, if reputation is bad (R), multiplier is 1
    mul = {
        "G": 0,
        "U": 0,
        "O": 0.5,
        "B": 1,
    }

    # The multipliers for all the different zygosities, i.e.heterozygous (one bad allele)
    # multiplies the score by 1, homozygous minor (two bad alleles) multiplies the score by 1.5
    z_mul = {
        'heterozygous': 1.2,
        'homozygous_major': 1,
        'major': 1,
        'homozygous_minor': 1.5,
        'minor': 1.5,
        'hemizygous_major': 1,
        'hemizygous_minor': 1.3,
        'unknown': 1,
        'double_insertion': 1.3,
        'double_deletion': 1.3,
        'insertion': 1.2,
        'deletion': 1.2,
    }

    # Spread amplifies the score, the higher the spread, the greater the score difference will be
    spread = 1.3

    print("Calculate genes reputation")
    rsids = file.related_rsid.values_list("rsid", flat=True).distinct()
    genes = Gene.objects.filter(snps__rsid__in=rsids).distinct().all()
    genes_count = genes.count()
    file.set_total_points(genes_count, latency=200)
    for gene in genes:
        total_reputation = 0
        for snp in gene.snps.all():
            user_rsid = file.related_rsid.filter(rsid=snp.rsid).first()
            if user_rsid is None:
                continue

            rep, zygosity = get_results(user_rsid, snp, file)

            if not rep or rep == "G" or rep == "U":
                continue

            importance = snp.importance

            weighted_reputation = importance * mul[rep]

            if rep == "B":
                weighted_reputation *= z_mul[zygosity]

            #  Now we apply the spread amplifier, we raise the score to the power of the spread number
            rep_square = pow(spread, weighted_reputation)
            total_reputation += rep_square
        try:
            UserGeneReputation.objects.create(gene=gene, file=file, score=total_reputation)
        except:
            pass
        file.update_progress()


def detect_service(file):
    # line = file.readline().decode()
    mime = magic.from_file(file)
    if mime == 'ASCII text, with very long lines':
        return File.SERVICE_VCF
    else:
        lines = list(islice(open(file), 0, 50))
        line = lines[0]
        if not isinstance(line, str):
            line = line.decode()
        text = " ".join([(l if isinstance(l, str) else l.decode()) for l in lines])

        if "23andMe" in line or "# rsid\tchromosome\tposition\tgenotype" in text:
            return File.SERVICE_23ANDME
        elif "AncestryDNA" in line:
            return File.SERVICE_ANCESTRY
        elif "Courtangen" in line or "rsid\tchromosome\tposition\tgenotype" in text:
            return File.SERVICE_COURTAGEN
        elif "RSID,CHROMOSOME,POSITION,RESULT" in line:
            return File.SERVICE_FAMILY_TREE
        return File.SERVICE_UNKNOWN


def get_data(user_id, file_pk):
    user = User.objects.get(id=user_id)
    obj = File.objects.get(pk=file_pk)
    file_name = obj.original_name

    try:
        data = get_s3_data(file_name)

        print("Unzipping file...")
        archive, file, name = unzip_any(data)
        name = name or file_name

        print("Detecting file service...")
        service = detect_service(file)

        if archive:
            file = archive.open(name)
        else:
            file.seek(0)

        obj.status = File.FILE_STATUS_PROCESSING
        obj.service = service
        obj.save()

        return archive, file, name, user, obj
    except:
        handle_errors(obj)
        return None


def get_data_to_file(user_id, file_pk):
    user = User.objects.get(id=user_id)
    obj = File.objects.get(pk=file_pk)
    file_name = obj.original_name
    static_dir = 'static/'

    try:
        print(static_dir + file_name)
        data = get_s3_data_to_file(file_name)
        print("finished writing to file")
        # zipped_file = open(static_dir + file_name, "w")
        # zipped_file.write(data)
        # zipped_file.close()
        print("Unzipping file...")
        archive, file, name = unzip_any_file(static_dir + file_name)
        print("Successfully unzipped")
        # name = name or file_name

        print("Detecting file service...")
        service = detect_service(file)
        #
        # if archive:
        #     file = archive.open(name)
        # else:
        #     file.seek(0)
        #
        obj.status = File.FILE_STATUS_PROCESSING
        obj.service = service
        obj.save()
        #
        return archive, file, name, user, obj
    except:
        handle_errors(obj)
        return None


def process_rsid_file(df, obj):
    p_total = len(df) + Gene.objects.count()
    obj.set_total_points(p_total, latency=100)
    print("Process file...")
    user_rsids = list(UserRsid.objects.filter(file=obj).values_list("rsid", flat=True))
    for index, item in df.iterrows():
        if not item[0] in user_rsids:
            snp = Snp.objects.filter(rsid=item[0]).first()
            if snp is not None:
                UserRsid.objects.create(
                    rsid=item[0],
                    chromosome=item[1],
                    position=item[2],
                    genotype=item[3],
                    file=obj,
                    genotype_style=check_genotype_style(item[3], snp)
                )
                user_rsids.append(item[0])
        obj.update_progress()


@app.task
def process_genome_file(user_id, dashboard_uri, file_pk):
    print("Start processing genome file...")
    services = {
        File.SERVICE_23ANDME: {"function": upload, "service": File.SERVICE_23ANDME},
        File.SERVICE_ANCESTRY: {"function": upload_ancestry, "service": File.SERVICE_ANCESTRY},
        File.SERVICE_COURTAGEN: {"function": upload_courtagen, "service": File.SERVICE_COURTAGEN},
        File.SERVICE_FAMILY_TREE: {"function": upload_family_tree, "service": File.SERVICE_FAMILY_TREE},
        File.SERVICE_VCF: {"function": upload_vcf, "service": File.SERVICE_VCF},
    }

    print("Download file from s3...")
    result = get_data_to_file(user_id, file_pk)
    if not result:
        return
    archive, file, name, user, obj = result
    try:
        if obj.service == File.SERVICE_UNKNOWN:
            line = file.readline().decode()
            raise Exception("Could not identify service, first line: %s" % line)
        print("Service detected as: %s" % obj.get_service_display())
        service = services.get(obj.service)
        fn = service.get("function")
        fn(archive, file, name, user, obj, dashboard_uri)

        obj.rescan_available = True
        obj.update_progress(100)
        send_completed_email(dashboard_uri, user, obj)
        os.remove(file)
        os.remove(file[:-13])

    except Exception:
        handle_errors(obj)


@app.task
def process_genome_downloaded_file(user_id, dashboard_uri, file_pk):
    print("Start processing genome file...")
    services = {
        File.SERVICE_23ANDME: {"function": upload, "service": File.SERVICE_23ANDME},
        File.SERVICE_ANCESTRY: {"function": upload_ancestry, "service": File.SERVICE_ANCESTRY},
        File.SERVICE_COURTAGEN: {"function": upload_courtagen, "service": File.SERVICE_COURTAGEN},
        File.SERVICE_FAMILY_TREE: {"function": upload_family_tree, "service": File.SERVICE_FAMILY_TREE},
        File.SERVICE_VCF: {"function": upload_vcf, "service": File.SERVICE_VCF},
    }

    print("Download file from s3...")
    result = get_data_to_file(user_id, file_pk)
    if not result:
        return
    archive, file, name, user, obj = result
    try:
        if obj.service == File.SERVICE_UNKNOWN:
            line = file.readline().decode()
            raise Exception("Could not identify service, first line: %s" % line)
        print("Service detected as: %s" % obj.get_service_display())
        service = services.get(obj.service)
        fn = service.get("function")
        fn(archive, file, name, user, obj, dashboard_uri)

        obj.rescan_available = True
        obj.update_progress(100)
        user.user_profile.file_uploads_count += 1
        user.user_profile.save()
        send_completed_email(dashboard_uri, user, obj)
    except Exception:
        handle_errors(obj)


def upload(archive, file, name, user, obj, dashboard_uri):
    print('tasks.upload Start processing file %s' % name)

    df = pd.read_csv(file, nrows=1, header=None)
    try:
        obj.sequenced_at = df.ix[0, 0][df.ix[0, 0].index(":") + 2:]
    except:
        pass

    if archive:
        file = archive.open(name)

    # Slice to first (Header) row
    for row in file:
        row = row.decode()
        if row.startswith('# rsid'):
            break

    df = pd.read_csv(file, header=0, delimiter="\t", dtype={"# rsid": str, "chromosome": str, "position": str, "genotype": str})  # combine the upload with adding headers to speed it up

    df.columns = ["rsid", "chromosome", "position", "genotype"]
    df2 = read_frame(Snp.objects.all())
    df3 = df.merge(df2, on="rsid")
    df3 = df3.drop_duplicates(['rsid']) # remove unneded emails to preveny timeout

    process_rsid_file(df3, obj)

    calculate_total_reputation(obj)

    print('tasks.upload Finished processing file %s' % name)


def upload_ancestry(archive, file, name, user, obj, dashboard_uri):
    try:
        print('tasks.upload_ancestry Start processing file %s' % name)

        df = pd.read_csv(file, header=0, comment='#', delimiter="\t", dtype={"rsid": str, "chromosome": str, "position": str, "allele1": str, "allele2": str}) #combine the upload with adding headers to speed it up

        df["genotype"] = df["allele1"] + df["allele2"]
        df = df[["rsid", "chromosome", "position", "genotype"]]
        df2 = read_frame(Snp.objects.all())
        df3 = df.merge(df2, on="rsid")
        df3 = df3.drop_duplicates(['rsid'])

        process_rsid_file(df3, obj)

        calculate_total_reputation(obj)

        print('tasks.upload_ancestry Finished processing file %s' % name)
    except Exception:
        handle_errors(obj)


def upload_courtagen(archive, file, name, user, obj, dashboard_uri):
    try:
        print('tasks.upload_courtagen Start processing file %s' % name)

        df = pd.read_csv(file, header=20, delimiter="\t", dtype={"# rsid": str, "chromosome": str, "position": str, "genotype": str})  # combine the upload with adding headers to speed it up
        df.columns = ["rsid", "chromosome", "position", "genotype"]
        df = df[["rsid", "chromosome", "position", "genotype"]]
        df2 = read_frame(Snp.objects.all())
        df3 = df.merge(df2, on="rsid")
        df3 = df3.drop_duplicates(['rsid'])

        process_rsid_file(df3, obj)

        calculate_total_reputation(obj)

        obj.update_progress(100)
        send_completed_email(dashboard_uri, user, obj)
        print('tasks.upload_courtagen Finished processing file %s' % name)
    except Exception:
        handle_errors(obj)


def upload_family_tree(archive, file, name, user, obj, dashboard_uri):
    try:
        print('tasks.upload_family_tree Start processing file %s' % name)

        df = pd.read_csv(file, engine='c', header=0, delimiter=',', dtype={"RSID": str, "CHROMOSOME": str, "POSITION": str, "RESULT": str})
        df.columns = ["rsid", "chromosome", "position", "genotype"]

        df2 = read_frame(Snp.objects.all())
        df3 = df.merge(df2, on="rsid")
        df3 = df3.drop_duplicates(['rsid'])

        process_rsid_file(df3, obj)

        calculate_total_reputation(obj)

        print('tasks.upload_family_tree Finished processing file %s' % name)
    except Exception:
        handle_errors(obj)


@app.task
def upload_23andme(user_id, code, file_pk):
    print("tasks.uploade_23andme api Start processing...")
    user = User.objects.get(id=user_id)
    obj = File.objects.get(pk=file_pk)
    try:
        user_profile_instance = UserProfile.objects.get(user=user)
        user_profile_instance.active_file = obj
        user_profile_instance.save()

        api = Api23AndMe()
        # Refresh token every 12 hours
        if user.user_profile.refresh_token \
                and user.user_profile.token_refresh_date <= timezone.now() - timedelta(hours=12):
            print("Refresh api token")
            access = api.get_refresh_token(user.user_profile.refresh_token)
        elif not user.user_profile.refresh_token:
            access = api.get_access_token(code)
            user.user_profile.token_refresh_date = timezone.now()
        else:
            access = {
                "access_token": user.user_profile.access_token,
                "refresh_token": user.user_profile.refresh_token,
            }
        access_token = access.get("access_token")
        refresh_token = access.get("refresh_token")
        api_user = api.get_user(access_token)
        print("Get User genomes...")
        genomes = api.get_genomes(access_token, api_user.get("profiles")[0].get("id"))

        user.user_profile.access_token = access_token
        user.user_profile.refresh_token = refresh_token
        user.user_profile.save()

        n = 2
        pair_list = [genomes.get("genome")[i:i+n] for i in range(0, len(genomes.get("genome")), n)]
        df = pd.DataFrame({"genotype": pair_list})
        del pair_list
        print("Get snps list")
        snps_res = api.get_snps_resources()

        df2 = pd.read_csv(snps_res, header=3, delimiter="\t")
        del snps_res
        df3 = df2.join(df)
        df3 = df3[["snp", "chromosome", "chromosome_position", "genotype"]] #filter this in order to decrease iterations and to decrease rows in memory
        df3 = df3[numpy.invert(df3["genotype"].str.contains("__"))]

        obj.status = File.FILE_STATUS_PROCESSING
        obj.save()

        process_rsid_file(df3, obj)
        del df3

        calculate_total_reputation(obj)

        obj.update_progress(100)
        print("Done processing 23AndMe Connect api")
    except:
        handle_errors(obj)


@app.task
def delete_uploaded_file(user_id, file_id):
    file = File.objects.get(user_id=user_id, pk=file_id, deleted_at__isnull=False)
    print('Removing file %s for user %s' % (file_id, user_id,))

    file.genes_to_look_at.clear()
    UserRsid.objects.filter(file=file).delete()

    file.delete()
    print('File fully removed')


def upload_vcf(archive, file, name, user, obj, dashboard_uri):
    try:
        print('tasks.upload_vcf Start processing file %s' % name)
        vcf_reader = vcf.Reader(open(file), 'r')
        user_rsids = list(UserRsid.objects.filter(file=obj).values_list("rsid", flat=True))
        # p_total = len(vcf_reader) + Gene.objects.count()
        obj.set_total_points(0, latency=100)
        print("Process file...")
        print(vcf_reader.samples)
        sample = vcf_reader.samples[0]
        for record in vcf_reader:
            if record.ID not in user_rsids:
                snp = Snp.objects.filter(rsid=record.ID).first()

                if snp is not None:
                    UserRsid.objects.create(
                        rsid=snp.rsid,
                        chromosome=record.CHROM,
                        position=record.POS,
                        genotype=record.genotype(sample).gt_bases,
                        file=obj,
                        genotype_style=check_genotype_style(record.genotype(sample).gt_bases, snp)
                    )
                    user_rsids.append(record.ID)
                    obj.update_progress()

    except Exception:
        handle_errors(obj)
