from django.conf.urls import url

from affiliate.views import *

urlpatterns = [
    url(r'^description/$', description, name="description"),
    url(r'^info/$', info, name="info"),
    url(r'^affiliate/$', affiliate, name="affiliate"),
    url(r'^register_affiliate', register_affiliate, name='register_affiliate')
]
