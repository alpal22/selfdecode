from django.contrib import admin

# Register your models here.
from django.db.models import Sum

from affiliate.forms import AffiliateProfileFormAdmin
from affiliate.models import AffiliateProfile, UserCommission
from genome.models import UserProfile


class AffiliatesInline(admin.TabularInline):
    model = UserProfile
    fk_name = "affiliated_from"
    readonly_fields = ("user_email", )
    fields = ("user_email", )
    extra = 0

    def user_email(self, obj):
        return obj.user.email


class AffiliateUserCommissionInline(admin.TabularInline):
    model = UserCommission
    fields = ("payed_amount", "created_at",)
    readonly_fields = ("created_at", )

    extra = 0


@admin.register(AffiliateProfile)
class AffiliateProfileAdmin(admin.ModelAdmin):
    list_display = ("user_profile", "paypal_email", "count_pending_commissions",)
    exclude = ("commissions_payed",)
    inlines = (AffiliateUserCommissionInline, AffiliatesInline,)
    form = AffiliateProfileFormAdmin

    def count_pending_commissions(self, obj):
        affiliates = obj.affiliates.all()
        total = 0
        for affiliate in affiliates:
            if affiliate.has_subscription():
                total += 10
        return total - int(obj.usercommission_set.aggregate(Sum("payed_amount")).get("payed_amount__sum") or 0)
