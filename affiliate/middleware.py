import datetime

from decodify import settings


class AffiliateCookieMiddleware(object):

    def process_request(self, request):

        if not request.COOKIES.get(settings.AFFILIATE_COOKIE_NAME):
            param = request.GET.get(settings.AFFILIATE_URL_PARAM, None)
            if param:
                request.COOKIES[settings.AFFILIATE_COOKIE_NAME] = param

    def process_response(self, request, response):
        param = request.GET.get(settings.AFFILIATE_URL_PARAM, None)

        if param:
            expires = datetime.datetime.now() + datetime.timedelta(days=365)
            response.set_cookie(settings.AFFILIATE_COOKIE_NAME, param, expires=expires)

        return response