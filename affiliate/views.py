import string
import random

from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, render_to_response

# Create your views here.
from django.template import RequestContext
from django.views.decorators.http import require_POST


from affiliate.forms import AffiliateRegistrationForm
from affiliate.models import AffiliateProfile
from decodify import settings


def description(request):
    return render(request, "affiliate/description.html")


def info(request):
    return render(request, "affiliate/info.html")


@login_required
def affiliate(request):
    if hasattr(request.user.user_profile, "affiliate_profile"):

        affiliate_profile = request.user.user_profile.affiliate_profile
        affiliate_url = "{absolute_url}?{param}={affiliate_code}".format(
            absolute_url=request.build_absolute_uri('/'),
            affiliate_code=affiliate_profile.affiliate_code,
            param=settings.AFFILIATE_URL_PARAM
        )
        return render(request, "affiliate/info.html", {
            "affiliate_profile": request.user.user_profile.affiliate_profile,
            "affiliate_url": affiliate_url
        })
    else:
        return render(request, "affiliate/description.html", {
            "user": request.user,
            "registration_form": AffiliateRegistrationForm,
        })


@require_POST
def register_affiliate(request):
    form = AffiliateRegistrationForm(request.POST)
    if form.is_valid():
        affiliate_profile = AffiliateProfile.objects.create(
            user_profile=request.user.user_profile,
            first_name=request.POST.get("first_name"),
            last_name=request.POST.get("last_name"),
            paypal_email=request.POST.get("paypal_email"),
            affiliate_code=''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(40))
        )
        return HttpResponseRedirect(reverse('affiliate:affiliate'))


def register_success(request):
    return render_to_response(
        'registration/success.html',
    )

