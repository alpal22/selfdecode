from django.db import models
import calendar
# Create your models here.
from django.db.models import Count, Sum


class AffiliateProfile(models.Model):
    user_profile = models.OneToOneField("genome.UserProfile", related_name="affiliate_profile")
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    paypal_email = models.CharField(max_length=255)
    affiliate_code = models.CharField(max_length=255)
    commissions_payed = models.IntegerField(default=0)

    def count_pending_commissions(self):
        affiliates = self.affiliates.all()
        total = 0
        for affiliate in affiliates:
            if affiliate.has_initial_subscription():
                total += 10
        return total - self.commissions_payed

    def total_payed(self):
        return self.usercommission_set.aggregate(Sum("payed_amount")).get("payed_amount__sum")

    def count_per_month_commissions(self):
        affiliates = self.affiliates.filter(
            user__subscriptions__isnull=False
        ).extra({
            'month': "Extract(month from created_at)",
            'year': "Extract(year from created_at)"
        }).values_list('month', 'year').annotate(Count('id'))
        response = []
        if affiliates.exists():
            response = [{"period": "%s %s" % (calendar.month_name[int(record[0])], int(record[1]),),
                         "amount": record[2] * 10
                         } for record in affiliates]

        return response


class UserCommission(models.Model):
    affiliate_profile = models.ForeignKey('AffiliateProfile')
    payed_amount = models.IntegerField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True,  null=True, blank=True)

