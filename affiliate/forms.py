from dal import autocomplete
from django import forms
from django.utils.translation import ugettext_lazy as _

from affiliate.models import AffiliateProfile


class AffiliateRegistrationForm(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=30)), label=_("First Name"))
    last_name = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=30)), label=_("Last Name"))
    paypal_email = forms.EmailField(widget=forms.TextInput(attrs=dict(required=True, max_length=30)), label=_("PayPal Email address"))


class AffiliateProfileFormAdmin(forms.ModelForm):
    class Meta:
        model = AffiliateProfile
        fields = '__all__'
        widgets = {
            "user_profile": autocomplete.ModelSelect2(url="genome_userprofile_autocomplete"),
        }
