from django.conf.urls import url

from social import views
from social import ajax_views

urlpatterns = [
    url(r'^account_access/$', views.account_access, name="account_access"),
    url(r'^user_authorization/$', views.user_authorization, name="user_authorization"),
    url(r'^handle-request/(?P<action>(\w+))/(?P<request_id>(\d+))',
        views.handle_request, name="handle_request"),

    # Ajax views
    url(r'^social-btn/$', ajax_views.social_btn_handler, name="social_btn"),
    url(r'^invite/$', ajax_views.send_invite, name='invite')
]
