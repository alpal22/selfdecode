from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.views.decorators.http import require_POST

from decodify.decorators import ajax_required, disallow_demouser
from social.helpers import send_email
from social.models import AccessRequest


@login_required
@disallow_demouser
@ajax_required
def social_btn_handler(request):
    receiver_id = request.POST.get('receiver', '')
    action = request.POST.get('action', '')
    receiver_instance = User.objects.filter(id=receiver_id).first()
    if not receiver_instance:
        return JsonResponse({'response': "There is no such user whom you are sending request"})
    if receiver_instance == request.user.id:
        return JsonResponse({'response': "You can't create request for yourself."})
    if action == 'compare':
        obj, created = AccessRequest.objects.get_or_create(
            sender=request.user,
            receiver_id=receiver_id,
            type='compare_genotype'
        )
        if created:
            send_email(
                'social/emails/compare_request.html',
                recipient_list=[receiver_instance.email],
                **{
                    'user': obj.sender.username,
                    'type': obj.type.replace("_", " "),
                    'page': "{}/social/account_access/".format(request.META.get('HTTP_ORIGIN'))
                }
            )

            return JsonResponse({'response': "You've sent request successfully. "
                                             "See your requests on Account access page."})
        else:
            return JsonResponse({'response': "You've already sent request to this user."})
    else:
        raise PermissionDenied()


@login_required
@require_POST
def send_invite(request):
    email = request.POST.get('email')
    if '+' in email:
        return JsonResponse({'response': 'You can not send a request on this address'})

    user_instance = User.objects.filter(email__iexact=email).first()

    to_mail = [email]

    if user_instance:
        receiver_id = user_instance.id
        access_request_item, created = AccessRequest.objects.get_or_create(
            sender=request.user,
            receiver_id=receiver_id,
            type='compare_genotype'
        )

        send_email(
            'social/emails/compare_request.html',
            recipient_list=to_mail,
            **{
                'user': access_request_item.sender,
                'type': access_request_item.type.replace("_", " "),
                'page': "{}/social/account_access/".format(request.META.get('HTTP_ORIGIN'))
            }
        )
        response = {"response": "Your invitation has been sent."}

    else:
        send_email(
            'social/emails/join_request.html',
            recipient_list=to_mail,
            **{
                'email': request.user.email,
                'page': "{}/accounts/signup/?invited={}".format(request.META.get('HTTP_ORIGIN'), request.user.id)
            }
        )
        response = {"response": "Your invitation has been sent."}

    return JsonResponse(response)
