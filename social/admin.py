from django.contrib import admin

from social.models import AccessRequest


@admin.register(AccessRequest)
class AccessRequestAdmin(admin.ModelAdmin):
    list_display = ("type", "sender", "receiver", "status", "created_at", "updated_at",)
