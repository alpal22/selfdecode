from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.core.exceptions import PermissionDenied

from decodify.decorators import disallow_demouser
from social.models import AccessRequest


@login_required
def user_authorization(request):
    """
        Onclick, if there is anythin selected in remove or authorize a user,
        then it actually gets posted. Otherwise the action url will change
    """
    if request.method == "POST" and request.user.is_authenticated():
        if request.POST.get("remove_auth_user"):
            request.user.user_profile.authorized_users.remove(User.objects.get(
                id=request.POST["remove_auth_user"]))
        else:
            try:
                request.user.user_profile.authorized_users.add(
                    User.objects.get(email=request.POST["add_auth_user"]))
            except:
                return HttpResponse("That email was invalid.")
    return render(request, "genome/profile.html")


@login_required
def account_access(request):
    received_requests = AccessRequest.objects.filter(receiver=request.user)
    sent_requests = AccessRequest.objects.filter(sender=request.user)
    context = {
        'received_requests': received_requests,
        'sent_requests': sent_requests,
    }
    return render(request, 'social/account_access.html', context)


@disallow_demouser
@login_required
def handle_request(request, action, request_id):
    access_request = get_object_or_404(AccessRequest, pk=request_id)
    if action == 'accept' and access_request.receiver == request.user:
        access_request.status = 'accepted'
        access_request.save()
        messages.info(request, "You have accepted request from user {}.".format(access_request.sender))
        return redirect('social:account_access')
    elif action == 'decline' and access_request.receiver == request.user:
        access_request.status = 'declined'
        access_request.save()
        messages.info(request, "You have declined request from user {}.".format(access_request.sender))
        return redirect('social:account_access')
    elif action == 'retry' and access_request.sender == request.user:
        access_request.status = 'pending'
        access_request.save()
        return redirect('social:account_access')
    elif action == 'delete' and access_request.sender == request.user:
        access_request.delete()
        messages.info(request, "You have deleted request to user {}.".format(access_request.receiver))
        return redirect('social:account_access')
    else:
        raise PermissionDenied()


