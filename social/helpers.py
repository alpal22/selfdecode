from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string


def send_email(template_name, subject='You have a new request', recipient_list=None, **kwargs):
    if recipient_list is None:
        recipient_list = []

    send_mail(
        subject=subject,
        message='',
        from_email=settings.EMAIL_FROM,
        recipient_list=recipient_list,
        fail_silently=False,
        html_message=render_to_string(template_name, kwargs)
    )
