$(function () {
    $('li[data-action]').on('click', function (evt) {
        evt.preventDefault();
        var data = {
            action: $(this).data('action'),
            receiver: $(this).data('receiver'),
            csrfmiddlewaretoken: getCookie('csrftoken')
        };
        $.post('/social/social-btn/', data)
            .always(function (data) {
                if (typeof data.response != 'undefined')
                    alert(data.response);
            });
    });

    $('#invite').on('click', function (evt) {
        evt.preventDefault();

        var data = {
            email: $('[name="user_email"]').val(),
            csrfmiddlewaretoken: getCookie('csrftoken')
        };

        $.post('/social/invite/', data).done(function (data) {
            alert(data.response);
            console.log(data);
        })
    })
});

