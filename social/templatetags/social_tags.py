from django import template

register = template.Library()


@register.inclusion_tag('social/partials/social-btn-tmpl.html', takes_context=True)
def social_button(context, size, receiver):
    return {
        'request': context['request'],
        'size': size,
        'receiver': receiver,
    }


@register.inclusion_tag('social/partials/generate-btn-tmpl.html')
def generate_btn(action, request_id):
    if action == 'delete' or action == 'decline':
        btn_color = 'danger'
    elif action == 'accept':
        btn_color = 'primary'
    else:
        btn_color = 'info'
    return {
        'btn_color': btn_color,
        'action': action,
        'request_id': request_id,
    }
