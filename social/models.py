import hashlib

from django.db import models
from django.contrib.auth.models import User

ACCESS_TYPE = (
    ('compare_genotype', 'Compare Genotype'),
    ('full_account_access', 'Full Account Access'),
)

STATUS_TYPE = (
    ('pending', 'Pending'),
    ('accepted', 'Accepted'),
    ('declined', 'Declined'),
)


class AccessRequest(models.Model):
    hash = models.CharField(max_length=255, blank=True, null=True)
    type = models.CharField(max_length=255, choices=ACCESS_TYPE, default='compare_genotype')
    sender = models.ForeignKey(User, related_name='sent_access_requests')
    receiver = models.ForeignKey(User, related_name='received_access_requests')
    status = models.CharField(max_length=255, choices=STATUS_TYPE, default='pending')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def save(self, *args, **kwargs):
        time = str(self.created_at).encode()
        email1 = self.sender.email.encode()
        email2 = self.receiver.email.encode()
        self.hash = hashlib.md5(email1 + email2 + time).hexdigest()
        super().save(*args, **kwargs)

    def __str__(self):
        return '{} request from: {} to {}'.format(self.type, self.sender, self.receiver)
