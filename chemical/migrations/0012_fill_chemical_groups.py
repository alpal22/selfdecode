# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-11-03 09:25
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('chemical', '0011_auto_20161103_0932'),
    ]

    operations = [
        migrations.RunSQL(
            """INSERT INTO chemical_chemicalgroup(name, slug) VALUES
                ('Natural treatments', 'natural-treatments'),
                ('Popular drugs', 'popular-drugs'),
                ('Toxins', 'toxins')"""
        )
    ]
