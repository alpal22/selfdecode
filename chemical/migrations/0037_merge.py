# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-01-26 13:41
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('chemical', '0036_auto_20170125_1300'),
        ('chemical', '0036_auto_20170118_1618'),
    ]

    operations = [
    ]
