# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-03-29 15:27
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('chemical', '0048_merge'),
        ('chemical', '0047_auto_20170328_0912'),
    ]

    operations = [
    ]
