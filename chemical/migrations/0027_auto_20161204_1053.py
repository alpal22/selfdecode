# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-12-04 10:53
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('chemical', '0027_auto_20161203_0923'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chemicalgeneinteraction',
            name='organism',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='chemical.Organism'),
        ),
    ]
