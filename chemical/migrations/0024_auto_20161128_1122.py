# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-11-28 11:22
from __future__ import unicode_literals

from django.db import migrations
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('chemical', '0023_auto_20161128_0927'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chemical',
            name='categories',
            field=mptt.fields.TreeManyToManyField(blank=True, related_name='chemicals', to='chemical.SubstanceCategory'),
        ),
    ]
