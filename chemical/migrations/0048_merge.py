# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-03-24 17:23
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('chemical', '0047_auto_20170324_0941'),
        ('chemical', '0047_auto_20170323_0401'),
    ]

    operations = [
    ]
