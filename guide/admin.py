from django.contrib import admin

# Register your models here.
from guide.models import Faq, InstructionalVideo


@admin.register(Faq)
class FaqAdmin(admin.ModelAdmin):
    list_display = ("question", "priority",)
    list_editable = ("priority",)
    search_fields = ("question",)


@admin.register(InstructionalVideo)
class InstructionalVideoAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "url_pattern", "video_url",)
    list_display_links = ("id", "title",)
    search_fields = ("title", "youtube_url",)

    def save_model(self, request, obj, form, change):
        obj.url_pattern = "/%s/" % obj.url_pattern.strip("/")
        res = super(InstructionalVideoAdmin, self).save_model(request, obj, form, change)
        return res
