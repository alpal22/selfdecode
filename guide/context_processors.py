from guide.models import InstructionalVideo


def instructional_videos(request):
    url = "/%s/" % request.path.strip("/")
    video_inst = None
    for video in InstructionalVideo.objects.all():
        if url.startswith(video.url_pattern):
            video_inst = video
            break
    return {
        "instructional_video": video_inst,
    }
