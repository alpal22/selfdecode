# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-01-25 16:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('guide', '0002_auto_20161209_1306'),
    ]

    operations = [
        migrations.CreateModel(
            name='InstructionalVideo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('url_pattern', models.CharField(db_index=True, max_length=255, unique=True)),
                ('video_url', models.CharField(max_length=255)),
            ],
        ),
    ]
