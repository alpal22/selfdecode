from ckeditor.fields import RichTextField
from django.db import models

# Create your models here.


class Faq(models.Model):
    question = models.CharField(max_length=500)
    answer = RichTextField(default="", null=True, blank=True)
    priority = models.IntegerField()

    class Meta:
        ordering = ("priority", "id",)


class InstructionalVideo(models.Model):
    title = models.CharField(max_length=255)
    url_pattern = models.CharField(max_length=255, unique=True, db_index=True)
    video_url = models.CharField(max_length=255)
