from django.shortcuts import render

# Create your views here.
from guide.models import Faq


def faqs(request):
    query_set = Faq.objects.all()

    return render(request, "guide/faqs.html", {
        "query_set": query_set
    })

