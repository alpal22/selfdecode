import ast

from dal import autocomplete
from django import forms

from payment.constants import SUPPORTED_PLANS
from payment.models import Transaction, Coupon


class TransactionFormAdmin(forms.ModelForm):
    class Meta:
        model = Transaction
        fields = '__all__'
        widgets = {
            "user": autocomplete.ModelSelect2(url="genome_user_autocomplete"),
            'provider': autocomplete.Select2(),
        }


class CouponFormAdmin(forms.ModelForm):
    class Meta:
        model = Coupon
        fields = '__all__'
