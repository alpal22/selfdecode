from django.conf.urls import url

from payment import views

urlpatterns = [
    url(r'^checkout/$', views.checkout, name="checkout"),
    url(r'^upgrade/$', views.upgrade_post, name="upgrade"),
    url(r'^cancel/$', views.cancel, name="cancel"),
    url(r'^stripe_webhook/$', views.stripe_webhook, name="stripe_webhook"),
    url(r'^purchase_history/$', views.purchase_history, name="history"),
]
