from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin
from django.contrib.admin import TabularInline
from django.utils import timezone

from payment.forms import TransactionFormAdmin, CouponFormAdmin
from payment.models import Transaction, Subscription, Coupon, Plan, PlanPermission, SubscriptionTransaction


@admin.register(Transaction)
class TransactionAdmin(ImportExportActionModelAdmin):
    form = TransactionFormAdmin
    search_fields = ('user__email',)
    readonly_fields = ('id',)
    list_display = ('user', 'transaction_type', 'created_at', 'traffic_source',)

    def traffic_source(self, obj):
        return obj.user.user_profile.traffic_source
    traffic_source.admin_order_field = 'user__user_profile__traffic_source'


@admin.register(SubscriptionTransaction)
class SubscriptionTransactionAdmin(admin.ModelAdmin):
    list_display = ("id", "tid", "display_amount", "subscription", "created_at",)
    list_display_links = ("id", "tid",)
    readonly_fields = ("created_at",)
    search_fields = ("tid", "subscription__sid", "subscription__user__email",)


@admin.register(PlanPermission)
class PlanPermissionAdmin(admin.ModelAdmin):
    list_display = ("id", "name",)
    list_display_links = ("id", "name",)
    prepopulated_fields = {"key": ("name",)}


@admin.register(Plan)
class PlanAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "plan_id", "price_formatted", "interval", "is_active", "created_at", "date_start", "date_end",)
    list_display_links = ("id", "name",)
    filter_horizontal = ("permissions",)
    search_fields = ("name",)
    ordering = ("date_start", "-date_end",)

    def get_queryset(self, request):
        return self.model._base_manager.all()

    def price_formatted(self, inst):
        return "$%0.2f" % (inst.price / 100)

    def is_active(self, inst):
        return inst.date_start <= timezone.now() < inst.date_end

    is_active.boolean = True

    price_formatted.short_description = "Price"


class SubscriptionTransactionInline(TabularInline):
    model = SubscriptionTransaction

    readonly_fields = ("tid", "display_amount", "created_at",)
    fields = ("tid", "display_amount", "created_at",)
    extra = 0

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ("id", "sid", "user_email", "provider", "date_started", "date_expires", "is_active", "canceled",)
    list_display_links = ("id", "sid",)
    list_select_related = ("user",)
    search_fields = ("user__email", "sid",)
    list_filter = ("provider", "plan", "canceled",)
    readonly_fields = ("coupon", "canceled", "date_canceled",)

    inlines = (SubscriptionTransactionInline,)

    def user_email(self, inst):
        return inst.user.email

    def is_active(self, inst):
        return inst.date_started <= timezone.now() < inst.date_expires

    is_active.boolean = True


@admin.register(Coupon)
class CouponAdmin(admin.ModelAdmin):
    form = CouponFormAdmin

    list_display = ("id", "discount_code", "discount", "created_at", "date_expires",)
    list_display_links = ("id", "discount_code",)
    filter_horizontal = ("supported_plans",)

    actions = None

    def get_fields(self, request, obj=None):
        fields = super(CouponAdmin, self).get_fields(request, obj)
        if obj:
            fields = list(set(fields) ^ set(['discount_value', 'discount_type']))
        return fields

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return list(set(self.get_fields(request) + ["discount"]))
        else:
            return ["old_supported_plans", "deleted_at"]

    def get_queryset(self, request):
        qs = self.model._base_manager
        qs = qs.filter(deleted_at__isnull=True)
        return qs

    def has_add_permission(self, request):
        return "/change/" not in request.get_full_path()

    def delete_model(self, request, obj):
        obj.deleted_at = timezone.now()
        obj.save()

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = {
            "show_save_as_new": False,
            "show_save_and_add_another": False,
            "show_save_and_continue": False,
            "show_save": False,
        }
        return super(CouponAdmin, self).change_view(request, object_id, form_url, extra_context)

    def discount(self, inst):
        return "%s%s" % (inst.discount_value, inst.get_discount_type_display(),)
