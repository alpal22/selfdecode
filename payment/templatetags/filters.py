from django import template
register = template.Library()


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@register.filter
def apply_coupon(plan, coupon):
    value = plan.price
    if not coupon:
        return value
    return coupon.get_discount(float(value))


@register.filter
def format_price(value):
    return ("%.2f" % (float(value) / 100)).rstrip("0").rstrip(".")
