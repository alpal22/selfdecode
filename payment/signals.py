import json

from datetime import timedelta

import dateutil.parser
import pytz
from allauth.account.utils import send_email_confirmation
from django.conf import settings
from django.utils import timezone
from paypal.standard.ipn.signals import valid_ipn_received

from django.dispatch import receiver
from django.contrib.auth.models import User

from payment.constants import PROVIDER_PAYPAL
from payment.models import Transaction, Coupon, Subscription, Plan


def paypal_subscr_payment(user, plan, paypal, discount_code):

    payment_date = paypal.payment_date

    Transaction.objects.create(
        price=plan.price / 100,
        user=user,
        provider=Transaction.PROVIDER_PAYPAL,
        date_start=payment_date,
        date_expires=timezone.now() + timedelta(days=plan.duration_days),
        subscription_id=paypal.subscr_id
    )

    coupon = None
    if discount_code:
        coupon = Coupon.objects.filter(discount_code=discount_code).first()

    subscription = Subscription.objects.filter(user=user, sid=paypal.subscr_id).first()

    if not subscription:
        subscription = Subscription.objects.create(
            user=user,
            coupon=coupon,
            provider=PROVIDER_PAYPAL,
            plan=plan,
            sid=paypal.subscr_id,
            customer_id=paypal.payer_id,
            date_started=payment_date,
            date_expires=payment_date + timedelta(days=plan.duration_days)
        )
    else:
        subscription.date_expires = timezone.now() + timedelta(days=plan.duration_days)
        subscription.save()

    subscription.add_payment(paypal.txn_id, int(paypal.mc_gross * 100), payment_date)

    # if not user.user_profile.is_email_verified():
    #     send_email_confirmation(None, user)


def paypal_subscr_cancel(user, plan, paypal):
    subscription = Subscription.objects.filter(user=user, sid=paypal.subscr_id).first()
    if subscription:
        subscription.cancel()

    transaction = user.user_profile.get_subscription_transaction()
    transaction.canceled = True
    transaction.save()


@receiver(valid_ipn_received)
def paypal_ipn_validated(sender, **kwargs):
    try:
        obj = json.loads(sender.custom)
        user_id = obj["user_id"]
        plan_id = obj["plan_id"]
        discount_code = obj.get("discount_code")
    except:
        user_id = sender.custom
        plan_id = 'monthly_19'
        discount_code = None
    try:
        user = User.objects.get(pk=user_id)
        plan = Plan.objects.get(plan_id=plan_id)
    except:
        invoice = sender.invoice
        args = invoice.split("-")
        user_id = args[2]
        plan_id = args[3]
        user = User.objects.get(pk=user_id)
        plan = Plan.objects.get(plan_id=plan_id)

    if sender.txn_type == 'subscr_payment':
        paypal_subscr_payment(user, plan, sender, discount_code)
    elif sender.txn_type == 'subscr_cancel':
        paypal_subscr_cancel(user, plan, sender)
