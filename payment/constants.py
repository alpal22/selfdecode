
PROVIDER_STRIPE = "stripe"
PROVIDER_PAYPAL = "paypal"
PROVIDERS = (
    (PROVIDER_STRIPE, "Stripe",),
    (PROVIDER_PAYPAL, "PayPal",),
)

SUPPORTED_PLANS = (
    ("monthly_19", "Recurring Individual Plan",),
    ("yearly_subscription_171", "Yearly subscription",),
)

COUPON_DISCOUNT_TYPE_DOLLAR = 0
COUPON_DISCOUNT_TYPE_PERCENTAGE = 1
COUPON_DISCOUNT_PERIOD_ONCE = 0
COUPON_DISCOUNT_PERIOD_ALL = 1

COUPON_DISCOUNT_PERIODS = (
    (COUPON_DISCOUNT_PERIOD_ONCE, "First Payment",),
    (COUPON_DISCOUNT_PERIOD_ALL, "All Payments",),
)

COUPON_DISCOUNT_TYPES = (
    (0, "$",),
    (1, "%",),
)

PLAN_INTERVAL_MONTH = "month"
PLAN_INTERVAL_YEAR = "year"

PLAN_INTERVALS = (
    (PLAN_INTERVAL_MONTH, "Monthly",),
    (PLAN_INTERVAL_YEAR, "Yearly",),
)
