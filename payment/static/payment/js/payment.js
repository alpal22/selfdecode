
(function () {
    $('[data-action="toggle"]').on('click', function () {
        var $this = $(this);
        var $target = $($this.attr('href'));

        if($target.length == 0)
            return true;

        $target.toggle();

        return false;
    });
})();
