import stripe
from django.conf import settings
from django.core.urlresolvers import reverse
import json

from paypal.standard.forms import PayPalPaymentsForm
from paypal.standard.ipn.models import PayPalIPN

from payment.constants import COUPON_DISCOUNT_PERIOD_ALL, PLAN_INTERVAL_YEAR
from payment.constants import COUPON_DISCOUNT_PERIOD_ONCE


def get_paypal_form(request, plan_id, plan):
    ipn_obj = PayPalIPN.objects.order_by('-id').first()

    paypal_extra_dict = {}
    discount_code = None

    base_price = plan.price / 100

    invoice = "invoice-%s-%s-%s" % ((ipn_obj.pk if ipn_obj else 0), request.user.pk, plan.pk)

    if plan.interval == PLAN_INTERVAL_YEAR:
        plan_duration = "Y"
    else:
        plan_duration = "M"

    if plan.coupon:
        invoice += "-%s" % plan.coupon.pk
        price = plan.coupon.get_discount(plan.price) / 100
        if plan.coupon.discount_period == COUPON_DISCOUNT_PERIOD_ONCE:
            paypal_extra_dict = {
                "a1": price,
                "p1": "1",
                "t1": plan_duration,
            }
        elif plan.coupon.discount_period == COUPON_DISCOUNT_PERIOD_ALL:
            paypal_extra_dict = {
                "a3": price
            }
        paypal_extra_dict.update({
            "on3": "Discount Coupon",
            "os3": plan.coupon.discount_code,
        })
        discount_code = plan.coupon.discount_code

    paypal_dict = {
        "cmd": "_xclick-subscriptions",
        "business": settings.PAYPAL_FACILITATOR_EMAIL,
        "a3": base_price,
        "p3": "1",  # duration of each unit (depends on unit)
        "t3": plan_duration,  # duration unit ("M for Month", "Y for Year")
        "src": "1",  # make payments recur
        "sra": "1",  # reattempt payment on payment error
        "no_note": "1",  # remove extra notes (optional)
        "item_name": plan.name,
        "invoice": invoice,
        "notify_url": request.build_absolute_uri(reverse('paypal-ipn')),
        "return_url": request.build_absolute_uri('/'),
        "cancel_return": request.build_absolute_uri('/'),
        "custom": json.dumps({
            "user_id": request.user.pk,
            "plan_id": plan_id,
            "discount_code": discount_code,
        }),
    }

    paypal_dict.update(paypal_extra_dict)

    form = PayPalPaymentsForm(initial=paypal_dict, button_type=PayPalPaymentsForm.SUBSCRIBE)
    return form


def get_stripe_plan(plan, auto_create=False):
    stripe_plan = None
    try:
        stripe_plan = stripe.Plan.retrieve(plan.plan_id)
    except stripe.error.InvalidRequestError:
        if auto_create:
            stripe_plan = stripe.Plan.create(
                amount=plan.price,
                interval=plan.interval,
                name=plan.name,
                currency="usd",
                id=plan.plan_id
            )
    return stripe_plan


def get_stripe_customer(user, token=None, auto_create=False):
    customer = None
    try:
        customer = stripe.Customer.retrieve(user.user_profile.stripe_customer_id)
    except stripe.error.InvalidRequestError:
        if auto_create:
            customer = stripe.Customer.create(source=token, email=user.email)
    return customer
