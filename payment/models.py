from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from payment.constants import PROVIDERS, COUPON_DISCOUNT_TYPE_DOLLAR,\
    COUPON_DISCOUNT_PERIODS, COUPON_DISCOUNT_TYPES, PLAN_INTERVALS, PLAN_INTERVAL_YEAR


class PlanPermission(models.Model):
    name = models.CharField(max_length=255)
    key = models.CharField(max_length=255, unique=True)

    icon_class = models.CharField(max_length=255, null=True, blank=True, help_text="Use any of http://fontawesome.io/icons/")

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("id",)


class PlanManager(models.Manager):
    def get_queryset(self):
        qs = super(PlanManager, self).get_queryset().filter(date_start__lte=timezone.now(), date_end__gt=timezone.now())
        return qs


class Plan(models.Model):
    name = models.CharField(max_length=255)
    plan_id = models.CharField(max_length=255, unique=True)
    price = models.IntegerField(verbose_name="Price(Cents)", default=0)
    interval = models.CharField(max_length=255, choices=PLAN_INTERVALS)

    permissions = models.ManyToManyField(PlanPermission, blank=True, related_name="plans")

    max_file_uploads = models.PositiveIntegerField(default=0, help_text="Set 0 for unlimited file uploads")

    created_at = models.DateTimeField(auto_now_add=True)
    date_start = models.DateTimeField(help_text="Date when current plan will be show up")
    date_end = models.DateTimeField(help_text="Date when current plan will be hidden")

    objects = PlanManager()

    def __str__(self):
        return self.name

    @property
    def duration_days(self):
        if self.interval == PLAN_INTERVAL_YEAR:
            return 365
        else:
            return 31


class Transaction(models.Model):
    PROVIDER_STRIPE = "stripe"
    PROVIDER_PAYPAL = "paypal"

    user = models.ForeignKey(User, related_name="related_transaction", null=True, default=0, blank=True)
    provider = models.CharField(max_length=64, choices=((PROVIDER_PAYPAL, "PayPal"), (PROVIDER_STRIPE, "Stripe"),), null=True)
    duration_in_days = models.IntegerField(null=True, blank=True)
    transaction_type = models.CharField(max_length=300, null=True, blank=True)
    price = models.DecimalField(max_digits=6, decimal_places=2, null=True, default=0, blank=True)
    subscription_id = models.CharField(max_length=255, null=True)
    canceled = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    date_start = models.DateTimeField(null=True)
    date_expires = models.DateTimeField(null=True)

    def __str__(self):
        return str(self.created_at) + " | " + str(self.transaction_type) + " | " + str(self.user)

    class Meta:
        db_table = "genome_transaction"


class CouponManager(models.Manager):
    def get_queryset(self):
        qs = super(CouponManager, self).get_queryset()
        return qs

    def active(self):
        return self.get_queryset().filter(deleted_at__isnull=True, date_expires__gte=timezone.now())


class Coupon(models.Model):
    discount_code = models.CharField(max_length=255)
    discount_value = models.FloatField(default=0, help_text="")
    discount_type = models.IntegerField(choices=COUPON_DISCOUNT_TYPES, default=0)
    discount_period = models.IntegerField(choices=COUPON_DISCOUNT_PERIODS, default=0)

    old_supported_plans = models.CharField(max_length=255, null=True)
    supported_plans = models.ManyToManyField(Plan, related_name="coupons")

    created_at = models.DateTimeField(auto_now_add=True)
    deleted_at = models.DateTimeField(null=True, blank=True)
    date_expires = models.DateTimeField()

    objects = CouponManager()

    @property
    def discount(self):
        if self.discount_type == COUPON_DISCOUNT_TYPE_DOLLAR:  # $
            return "%s%.0f" % (self.get_discount_type_display(), self.discount_value,)
        else:
            return "%.0f%s" % (self.discount_value, self.get_discount_type_display(),)

    def get_discount(self, cents):
        if self.discount_type == COUPON_DISCOUNT_TYPE_DOLLAR:  # $
            result = cents - (self.discount_value * 100)
        else:
            result = cents - cents * (self.discount_value / 100)
        return result

    def __str__(self):
        return "Coupon %s" % self.discount_code


class Subscription(models.Model):
    user = models.ForeignKey(User, related_name="subscriptions")
    coupon = models.ForeignKey(Coupon, null=True, blank=True, on_delete=models.SET_NULL, related_name="subscriptions")

    provider = models.CharField(max_length=64, choices=PROVIDERS)
    plan = models.ForeignKey(Plan, related_name="subscriptions")
    sid = models.CharField(max_length=255, verbose_name="Subscription ID")
    customer_id = models.CharField(max_length=255)
    canceled = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    date_started = models.DateTimeField()
    date_expires = models.DateTimeField()
    date_canceled = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.sid

    @classmethod
    def get_active(cls, user):
        return cls.objects.filter(
            user=user,
            date_started__lte=timezone.now(),
            date_expires__gt=timezone.now()
        ).first()

    def cancel(self):
        self.canceled = True
        self.date_canceled = timezone.now()
        self.save()

    def add_payment(self, transaction_id, amount, payment_date=None):
        if not SubscriptionTransaction.objects.filter(subscription=self, tid=transaction_id).exists():
            SubscriptionTransaction.objects.create(
                subscription=self,
                tid=transaction_id,
                amount=amount,
                date_paid=payment_date or timezone.now()
            )


class SubscriptionTransaction(models.Model):
    subscription = models.ForeignKey(Subscription, related_name="transactions")
    tid = models.CharField(max_length=255, verbose_name="Transaction ID")
    amount = models.IntegerField(default=0)

    date_paid = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)

    def display_amount(self):
        return '%.2f' % (self.amount / 100)
    display_amount.short_description = "Amount"

    display_amount = property(display_amount)

    def __str__(self):
        return self.tid
