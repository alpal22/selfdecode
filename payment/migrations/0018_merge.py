# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-04-18 16:01
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0016_merge'),
        ('payment', '0017_auto_20170410_0714'),
    ]

    operations = [
    ]
