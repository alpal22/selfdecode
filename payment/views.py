import json

import stripe
from datetime import datetime

from allauth.account.utils import send_email_confirmation
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from paypal.pro.helpers import PayPalWPP
from paypal.standard.ipn.models import PayPalIPN

from genome.models import UserProfile
from payment.constants import COUPON_DISCOUNT_PERIOD_ONCE, COUPON_DISCOUNT_PERIOD_ALL, COUPON_DISCOUNT_TYPE_DOLLAR, \
    COUPON_DISCOUNT_TYPE_PERCENTAGE, PROVIDER_STRIPE
from payment.helpers import get_paypal_form, get_stripe_plan, get_stripe_customer
from payment.models import Transaction, Coupon, Subscription, Plan

stripe.api_key = settings.STRIPE_API_KEY


def _cancel_stripe_subscription(request):
    user_profile = request.user.user_profile

    stripe_customer_id = user_profile.stripe_customer_id
    r = stripe.Customer.retrieve(stripe_customer_id)
    if len(r.subscriptions.data) > 0:
        subscription = stripe.Subscription.retrieve(r.subscriptions.data[0].id)
        subscription.delete()
    # Transaction.objects.create(transaction_type="plan_cancellation", price=0, user=request.user)
    transaction = user_profile.get_subscription_transaction()
    transaction.canceled = True
    transaction.save()

    subscription = Subscription.objects.filter(user=request.user, sid=r.subscriptions.data[0].id).first()
    if subscription:
        subscription.cancel()


def _cancel_paypal_subscription(request, transaction):
    obj = PayPalWPP(request)
    user = PayPalIPN.objects.filter(
        Q(custom=request.user.pk) | Q(subscr_id=transaction.subscription_id)
    ).order_by("-payment_date").first()
    if user is None:
        transaction.canceled = True
        transaction.save()
    else:
        obj.manangeRecurringPaymentsProfileStatus(params={
            'profileid': user.subscr_id,
            'payerid': user.payer_id,
            'action': "Cancel"
        })

        subscription = Subscription.objects.filter(user=request.user, sid=user.subscr_id).first()
        if subscription:
            subscription.cancel()


@csrf_exempt
def stripe_webhook(request):

    event_json = json.loads(request.body.decode("utf-8"))
    event = stripe.Event.retrieve(event_json["id"])

    user_profile = UserProfile.objects.filter(stripe_customer_id=event.data.object.customer).first()
    if not user_profile:
        return HttpResponse(200)
    user = user_profile.user

    if event.type == "invoice.payment_succeeded":
        transaction = user_profile.get_subscription_transaction()
        subscr = stripe.Subscription.retrieve(event.data.object.subscription)
        plan = Plan.objects.get(plan_id=subscr.plan.id)
        payment_date = datetime.fromtimestamp(event.data.object.date)
        if not transaction:
            Transaction.objects.create(
                user=request.user,
                price=plan.price / 100,
                provider=Transaction.PROVIDER_STRIPE,
                subscription_id=subscr["id"],
                date_start=datetime.fromtimestamp(subscr['current_period_start']),
                date_expires=datetime.fromtimestamp(subscr['current_period_end'])
            )

        discount_code = event.get("discount", {}).get("coupon", {}).get("id")

        coupon = Coupon.objects.filter(discount_code=discount_code).first()

        subscription = Subscription.objects.filter(user=user, sid=subscr['id']).first()

        if not subscription:
            subscription = Subscription.objects.create(
                user=user,
                coupon=coupon,
                provider=PROVIDER_STRIPE,
                plan=plan,
                sid=subscr['id'],
                customer_id=event.data.object.customer,
                date_started=datetime.fromtimestamp(subscr['current_period_start']),
                date_expires=datetime.fromtimestamp(subscr['current_period_end'])
            )
        else:
            subscription.date_expires = datetime.fromtimestamp(subscr['current_period_end'])
            subscription.save()

        subscription.add_payment(event.data.object.charge, event.data.object.amount_due, payment_date)

    return HttpResponse(200)


@login_required
def cancel(request):
    if request.method == "POST":
        # if request.user.user_profile.stripe_customer_id:
        transaction = request.user.user_profile.get_subscription_transaction()
        if transaction.provider == Transaction.PROVIDER_STRIPE or transaction.transaction_type == "monthly_individual_membership" or request.user.user_profile.stripe_customer_id:
            _cancel_stripe_subscription(request)
        elif transaction.provider == Transaction.PROVIDER_PAYPAL or transaction.transaction_type == "individual_membership":
            _cancel_paypal_subscription(request, transaction)
        else:
            transaction = request.user.user_profile.get_subscription_transaction()
            transaction.canceled = True
            transaction.save()
    return render(request, "payment/confirm_cancel.html")


@require_POST
@login_required
def upgrade_post(request):
    if request.user.user_profile.has_subscription():
        return redirect(reverse("dashboard"))

    plan_key = request.POST.get("plan")
    plan = Plan.objects.get(plan_id=plan_key)

    get_stripe_plan(plan, auto_create=True)
    customer = get_stripe_customer(request.user)


def checkout_post(request):
    token = request.POST['stripeToken']
    plan_key = request.POST.get("plan")
    # plan = settings.PAYMENTS_PLANS[plan_key]
    plan = Plan.objects.get(plan_id=plan_key)
    discount_code = request.POST.get("cdc")
    coupon = None
    if discount_code:
        coupon = Coupon.objects.active().filter(discount_code=request.POST.get("cdc"), supported_plans=plan).first()
    try:
        get_stripe_plan(plan, auto_create=True)
        customer = get_stripe_customer(request.user, token=token, auto_create=True)

        if coupon:
            try:
                stripe.Coupon.retrieve(coupon.discount_code)
            except stripe.error.InvalidRequestError:
                kwargs = {}
                if coupon.discount_period == COUPON_DISCOUNT_PERIOD_ONCE:
                    kwargs['duration'] = "once"
                elif coupon.discount_period == COUPON_DISCOUNT_PERIOD_ALL:
                    kwargs['duration'] = "forever"

                if coupon.discount_type == COUPON_DISCOUNT_TYPE_DOLLAR:
                    kwargs['amount_off'] = int(coupon.discount_value * 100)
                    kwargs['currency'] = "usd"
                elif coupon.discount_type == COUPON_DISCOUNT_TYPE_PERCENTAGE:
                    kwargs['percent_off'] = int(coupon.discount_value)

                stripe.Coupon.create(
                    id=coupon.discount_code,
                    **kwargs
                )

        kwargs = {}

        if coupon:
            kwargs['coupon'] = coupon.discount_code

        subscr = stripe.Subscription.create(
            customer=customer.id,
            plan=plan.plan_id,
            **kwargs
        )

        Transaction.objects.create(
            user=request.user,
            price=plan.price / 100,
            subscription_id=subscr["id"],
            provider=Transaction.PROVIDER_STRIPE,
            date_start=datetime.fromtimestamp(subscr['current_period_start']),
            date_expires=datetime.fromtimestamp(subscr['current_period_end'])
        )

        if not Subscription.objects.filter(user=request.user, sid=subscr['id'], canceled=False).exists():
            Subscription.objects.create(
                user=request.user,
                coupon=coupon,
                provider=PROVIDER_STRIPE,
                plan=plan,
                sid=subscr['id'],
                customer_id=customer.id,
                date_started=datetime.fromtimestamp(subscr['current_period_start']),
                date_expires=datetime.fromtimestamp(subscr['current_period_end'])
            )

        user_profile_instance = UserProfile.objects.get(user=request.user)
        user_profile_instance.stripe_customer_id = customer.id
        user_profile_instance.save()
    except stripe.CardError:
        return HttpResponse("Payment Declined")

    if not request.user.user_profile.is_email_verified():
        send_email_confirmation(request, request.user)

    return {
        "price": (plan.price / 100) if not coupon else (coupon.get_discount(float(plan.price)) / 100),
        "product": plan.name,
    }


@login_required
def checkout(request):
    if request.user.user_profile.has_subscription():
        return redirect(reverse("dashboard"))

    if request.method == "POST":
        data = checkout_post(request)
        return render(request, "payment/confirmation.html", data)

    plans = {}
    for plan in Plan.objects.order_by("price").all():
        coupon = Coupon.objects.active().filter(discount_code=request.GET.get("cdc"), supported_plans=plan).first()
        setattr(plan, 'coupon', coupon)
        plans[plan.plan_id] = plan

    data = {
        "STRIPE_API_KEY": settings.STRIPE_PUBLIC_KEY,
        "forms": {plan_id: get_paypal_form(request, plan_id, item).render() for plan_id, item in plans.items()},
        "plans": plans,
    }

    return render(request, "payment/checkout.html", data)


@login_required
def purchase_history(request):
    transactions = Transaction.objects.filter(user=request.user).all()
    return render(request, "payment/purchase_history.html", {
        "transactions": transactions,
    })
