from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db.models import Count
from django.db.models import F
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render
from django.template.defaultfilters import truncatechars
from django.utils.html import strip_tags

from chemical.facades import SubstanceCategoryFactory
from chemical.models import SubstanceCategory, ChemicalGeneInteraction, Chemical, ChemicalT3DBData
from decodify.aggregators import ArrayAgg
from decodify.decorators import ajax_required
from decodify.helpers import get_geo_location
from genome.decorators import subscription_required
from genome.decorators import uploaded_file_required
from genome.models import File, Snp, Gene
from genome.views import disease_risk_color_index
from recommendation.helpers import get_recommendations, get_bookmarked_recommendations


def _sort_genes(gene, selected):
    if gene['id'] in selected:
        return 10
    return 1


def _add_description(chemicals):
    ids = [chemical['id'] for chemical in chemicals]
    t3db = dict(ChemicalT3DBData.objects.filter(chemical__id__in=ids).values_list("chemical__id", "description"))
    for chemical in chemicals:
        chemical['description'] = t3db.get(chemical['id']) or chemical['definition']


@login_required
@subscription_required(redirect_url="/checkout")
@uploaded_file_required(redirect_url="/uploader")
def recommendations(request):
    beneficial_cat = SubstanceCategoryFactory.get(slug="beneficial-substances")
    natural_cat = SubstanceCategoryFactory.get(slug="natural-treatments")
    important_cat = SubstanceCategoryFactory.get(slug="important-natural-compounds")
    toxins_cat = SubstanceCategoryFactory.get(slug="toxins")

    if request.user.user_profile.active_file is not None and request.user.user_profile.active_file.status != File.FILE_STATUS_COMPLETED:
        return render(request, "genome/recommendations.html", {
            "file_not_ready": True,
        })

    b_genes = tuple(request.user.user_profile.active_file.genes_to_look_at.values_list("id", flat=True))[:50]

    location = get_geo_location(request)

    if location == "US":
        natural_qs = Q(affiliate_us__isnull=False)
        natural_qs_exclude = Q(affiliate_us__exact='')
    else:
        natural_qs = Q(affiliate_international__isnull=False)
        natural_qs_exclude = Q(affiliate_international__exact='')

    all_list = [
        {
            "title": "Natural Substance Recommendations",
            "query_set": get_recommendations(b_genes, natural_cat, filter_qs=natural_qs, exclude_qs=natural_qs_exclude, reverse_ordering=True)
        },
        {
            "title": "Beneficial Substances for Bookmarked SNPs",
            "query_set": get_bookmarked_recommendations(request.user, beneficial_cat),
        },
        {
            "title": "Important natural compounds",
            "query_set": get_recommendations(b_genes, important_cat, reverse_ordering=True)
        },
    ]

    toxins = get_recommendations(b_genes, toxins_cat, reverse_ordering=False)

    is_empty = False
    for item in all_list:
        is_empty |= len(item['query_set']) == 0

    is_empty |= len(toxins) == 0

    return render(request, "genome/recommendations.html", {
        "all_list": all_list,
        "toxins": toxins,
        "is_empty": is_empty,
    })


@ajax_required
def recommendations_by_filter(request):
    beneficial_cat = SubstanceCategoryFactory.get(slug="beneficial-substances")

    if request.is_ajax():
        if request.GET.getlist('genes[]'):
            genes = tuple(request.GET.getlist('genes[]'))
        elif request.GET.getlist('diseases[]'):
            if request.user.is_authenticated() and request.user.user_profile.active_file is not None:
                user_genotypes = Snp.objects.filter(
                    studies__diseasestraits__in=request.GET.getlist('diseases[]')
                ).extra(
                    select={
                        'user_genotype': 'genome_userrsid.genotype',
                        'genotype_style': 'genome_userrsid.genotype_style',
                    },
                    tables=['genome_userrsid'],
                    where=["genome_userrsid.rsid = genome_snp.rsid", "genome_userrsid.file_id = {}".format(request.user.user_profile.active_file.pk)],
                ).annotate(
                    Count("id"),
                    risk_alleles=ArrayAgg("studies__risk_allele"),
                ).prefetch_related("genes")

            user_genotypes = sorted(list(user_genotypes), key=lambda o: disease_risk_color_index(o.user_genotype, o.risk_alleles, o.minor_allele), reverse=True)

            bad_user_genes_for_disease = filter(lambda item: disease_risk_color_index(item.user_genotype, item.risk_alleles, item.genotype_style)[0] > 0, user_genotypes)
            genes = list(Gene.objects.filter(snps__in=bad_user_genes_for_disease).values_list("pk", flat=True))
        else:
            genes = None

        chems_list = get_recommendations(
            genes,
            beneficial_cat,
            values=('name', 'display_as', 'slug', 'id', 't3db_data__description', 'definition',)
        )

        _add_description(chems_list)

        result = {
            "chemicals": chems_list,
        }

        return JsonResponse(result, safe=False)


def recommendations_api(request, chemical=None):
    if request.user.is_authenticated() and request.GET.getlist('diseases[]'):
        user_genotypes = Snp.objects.filter(
            studies__diseasestraits__in=request.GET.getlist('diseases[]')
        ).extra(
            select={
                'user_genotype': 'genome_userrsid.genotype',
                'genotype_style': 'genome_userrsid.genotype_style',
            },
            tables=['genome_userrsid'],
            where=["genome_userrsid.rsid = genome_snp.rsid", "genome_userrsid.file_id = {}".format(request.user.user_profile.active_file.pk)],
        ).annotate(
            Count("id"),
            risk_alleles=ArrayAgg("studies__risk_allele"),
        ).prefetch_related("genes")

        user_genotypes = sorted(list(user_genotypes), key=lambda o: disease_risk_color_index(o.user_genotype, o.risk_alleles, o.minor_allele))

        bad_user_genes_for_disease = filter(lambda item: disease_risk_color_index(item.user_genotype, item.risk_alleles, item.minor_allele)[0] > 0, user_genotypes)

        genes = Gene.objects.filter(
            snps__in=bad_user_genes_for_disease,
            chemicalgeneinteraction__chemical__pk=chemical
        ).annotate(actions=ArrayAgg('chemicalgeneinteraction__actions__action'),).values('id', 'name', 'description_simple', 'function', 'exception_status', 'actions')[:30]
    else:
        genes = ChemicalGeneInteraction.objects.extra(
            tables=['chemical_chemicalgeneinteractionaction'],
            where=['chemical_chemicalgeneinteractionaction.interaction_id=chemical_chemicalgeneinteraction.id']
        ).values(
            'chemical_id',
            'gene_id',
            'gene__name',
        ).annotate(
            actions=ArrayAgg('actions__action'),
            name=F('gene__name'),
            id=F('gene_id'),
            description_simple=F('gene__description_simple'),
            function=F('gene__function'),
            exception_status=F('gene__exception_status'),
        ).filter(chemical_id=chemical)

    bookmarked_snps_ids = request.user.user_profile.bookmarked_snps.filter(studies__isnull=False).values_list('id', flat=True)

    bookmarked_genes = list(Gene.objects.filter(snps__pk__in=bookmarked_snps_ids).values_list('id', flat=True))

    genes = list(genes)
    selected_genes = [int(gene) for gene in request.GET.getlist('genes[]')]
    selected_and_bookmarked_genes = selected_genes + bookmarked_genes
    genes.sort(key=lambda gene: _sort_genes(gene, selected_and_bookmarked_genes), reverse=True)
    genes = genes[:40]

    chem = Chemical.objects.get(id=chemical)
    data = {
        'nodes': [{
            'name': chem.name.title(),
            'width': len(chem.name) * 10 + 15,
            'height': 30,
            'url': reverse('chemical:chemical', args=[chem.slug])
        }],
        'links': []
    }

    for index, gene in enumerate(genes):
        action = ''
        tooltip = truncatechars(strip_tags(gene['description_simple'] or gene['function'] or '').replace('[R]', '').replace('(R, R)', '').replace('(R)', ''), 300),
        if gene.get('actions'):
            chem_interactions = set(gene['actions'])
            chem_interactions.discard('affects')
            if chem_interactions is not None and len(chem_interactions) == 1:
                action = chem_interactions.pop()
            elif chem_interactions is not None and len(chem_interactions) == 2:
                action = "increasesdecreases"

        node = {
            'id': gene['id'],
            'name': gene['name'].upper(),
            'width': len(gene['name']) * 10 + 20,
            'height': 30,
            'url': reverse('gene', args=[gene['name'].lower()]),
            'selected': True if gene['id'] in selected_genes else False,
            'bookmarked': True if gene['id'] in bookmarked_genes else False,
            'tooltip': tooltip,
            'action': action,
            'exception_status': gene['exception_status']
        }
        data['nodes'].append(node)
        data['links'].append({"source": 0, "target": index + 1})
    return JsonResponse(data)
