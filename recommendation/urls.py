from django.conf.urls import url

from recommendation import views

urlpatterns = [
    url(r'^substance_explorer/$', views.recommendations, name="main"),
    url(r'^recommendations/by_filter/$', views.recommendations_by_filter, name="by_filter"),
    url(r'^recommendations/api/(?P<chemical>\d+)/$', views.recommendations_api, name="api"),
]
