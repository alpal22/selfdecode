from django.db import connection
from django.db.models import Count
from django.db.models import Q

from chemical.models import Chemical, ChemicalGeneInteraction
from genome.models import Gene


def _get_chemicals_by_gene_direction(genes, reverse=True):
    '''
    A new algorithm for getting chemicals on the recomendation page
    according to gene direction

    param genes should be a tuple of genes
    '''

    if not genes:
        return Chemical.objects.none(), {}

    cursor = connection.cursor()

    raw_query = '''
        SELECT chemical_id, SUM(cnt) AS fcnt from
        (SELECT id, chemical_id,
            CASE
            WHEN ( exception_status = 'increased' OR exception_status is Null) THEN
            cnt_inc - cnt_dec
            WHEN  exception_status = 'decreased' THEN
            cnt_dec - cnt_inc
            END AS cnt
        FROM (
            SELECT genome_gene.id as id, genome_gene.exception_status as exception_status, chemical_chemicalgeneinteraction.chemical_id AS chemical_id, count(interact_increase) AS cnt_inc, count(interact_decrease) AS cnt_dec
            FROM chemical_chemicalgeneinteraction
            INNER JOIN genome_gene on genome_gene.id = chemical_chemicalgeneinteraction.gene_id
            LEFT JOIN chemical_chemicalgeneinteractionaction as interact_increase on chemical_chemicalgeneinteraction.id = interact_increase.interaction_id and interact_increase.action = 'increases'
            LEFT JOIN chemical_chemicalgeneinteractionaction as interact_decrease on chemical_chemicalgeneinteraction.id = interact_decrease.interaction_id and interact_decrease.action = 'decreases'
            INNER JOIN chemical_chemical ON chemical_chemical.id = chemical_chemicalgeneinteraction.chemical_id
            WHERE "chemical_chemical"."recommendation_status" != 'disallow_everywhere' and gene_id IN %s
            GROUP BY chemical_id, genome_gene.id
        ) AS sub

        GROUP BY id, chemical_id, exception_status, cnt_inc, cnt_dec
        ORDER BY cnt ''' + ("DESC" if reverse else "ASC") + '''
        ) AS counts
        GROUP BY chemical_id
        ORDER BY fcnt ''' + ("DESC" if reverse else "ASC") + '''
    '''
    cursor.execute(raw_query, [tuple(genes)])
    raw_chem = cursor.fetchall()
    ids_cnt = dict(raw_chem)
    chem_ids = [id for id, _ in raw_chem]

    ids_to_sort = '''idx(array[{}], "chemical_chemical"."id") '''.format(','.join(str(i) for i in chem_ids))
    query_set = Chemical.objects.extra(select={'cust_ind': ids_to_sort}).filter(id__in=chem_ids).order_by('cust_ind').all()

    return query_set.annotate(Count("id")), ids_cnt


def add_scores(chems_list, chem_dict, reverse_ordering=None):
    if len(chems_list) > 0 and isinstance(chems_list[0], dict):
        def set_cout(o):
            obj["cnt"] = chem_dict.get(o['id'], 0)
    else:
        def set_cout(o):
            setattr(o, 'cnt', chem_dict.get(o.id, 0))

    for obj in chems_list:
        set_cout(obj)

    if reverse_ordering is not None:
        chems_list.sort(key=lambda x: x.cnt, reverse=reverse_ordering)


def get_recommendations(genes, cat, limit=5, exclude_qs=Q(), filter_qs=Q(), values=None,
                        reverse_ordering=True):
    query_set, chem_dict = _get_chemicals_by_gene_direction(genes, reverse_ordering)

    result = query_set.exclude(
        exclude_qs
    ).filter(
        filter_qs,
        categories__pk__in=list(cat.get_family().values_list("pk", flat=True))
    )

    if values:
        result = result.values(*values)
        order_key = lambda x: x['cnt']
    else:
        order_key = lambda x: x.cnt

    result = result.annotate(Count("id"))

    result = list(result[:limit])
    add_scores(result, chem_dict)

    result.sort(key=order_key, reverse=reverse_ordering)

    return result


def get_bookmarked_recommendations(user, cat):
    current_bookmarked_snps = user.user_profile.bookmarked_snps.filter(studies__isnull=False).all()
    bookmarked_genes = list(Gene.objects.filter(pk__in=current_bookmarked_snps.values_list("pk", flat=True)).values_list("pk", flat=True))

    bookmarked_query_set = ChemicalGeneInteraction.objects.filter(
        gene__pk__in=bookmarked_genes,
        chemical__recommendation_status__in=["default"]
    ).values_list('chemical', flat=True).annotate(c=Count('gene')).order_by('-c')

    bookmarked_ids = list(
        bookmarked_query_set.exclude(
            chemical__recommendation_status='disallow_everywhere'
        ).filter(
            chemical__categories__in=list(cat.get_family())
        )[:5]
    )

    bookmarked_recomendations = Chemical.objects.extra(
        select={"custom_ordering": "id=" + ("id=".join([str(v) + "," for v in bookmarked_ids])).strip(",")},
        order_by=["-custom_ordering"]
    ).exclude(
        recommendation_status='disallow_everywhere'
    ).filter(
        id__in=bookmarked_ids
    ).all()

    bookmarked_recomendations = list(bookmarked_recomendations)

    _, chem_dict = _get_chemicals_by_gene_direction(bookmarked_genes)
    add_scores(bookmarked_recomendations, chem_dict, reverse_ordering=True)

    return bookmarked_recomendations
