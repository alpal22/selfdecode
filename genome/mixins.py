from decimal import Decimal
from django.views.generic.list import ListView
from django.http import JsonResponse
from django.forms.models import model_to_dict
from django.db import models
from haystack.query import SearchQuerySet


class AutocompleteSlugCatMixin(ListView):

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            query = request.GET.get('query', '')
            # import pdb; pdb.set_trace()
            queryset = SearchQuerySet().models(self.model).filter(category=self.category).autocomplete(text__contains=query)[:10]
            data = {
                'query': query,
                'type': self.slug,
                'slug': [item.slug for item in queryset],
                'suggestions': [item.text for item in queryset]
            }
            return JsonResponse(data, safe=False)
        return super().get(request, *args, **kwargs)


class AutocompleteSlugMixin(ListView):

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            query = request.GET.get('query', '')
            # import pdb; pdb.set_trace()
            queryset = SearchQuerySet().models(self.model).autocomplete(text__contains=query)[:10]
            data = {
                'query': query,
                'type': self.slug,
                'slug': [item.slug for item in queryset],
                'suggestions': [item.text for item in queryset]
            }
            return JsonResponse(data, safe=False)
        return super().get(request, *args, **kwargs)


class AutocompleteSlugTypeMixin(ListView):

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            query = request.GET.get('query', '')
            # import pdb; pdb.set_trace()
            queryset = SearchQuerySet().models(self.model).filter(type=self.type).autocomplete(text__contains=query)[:10]
            data = {
                'query': query,
                'type': self.type,
                'slug': [item.slug for item in queryset],
                'suggestions': [item.text for item in queryset]
            }
            return JsonResponse(data, safe=False)
        return super().get(request, *args, **kwargs)


class ModelDiffMixin(models.Model):
    """
    A model mixin that tracks model fields' values and provide useful API
    to know what fields have been changed.

    The main value is to allow simply changing the values and then saving the
    object only if it is "really changed"
    """

    def __init__(self, *args, **kwargs):
        super(ModelDiffMixin, self).__init__(*args, **kwargs)
        self.__initial = self._dict

    @property
    def diff(self):
        d1 = self.__initial
        d2 = self._dict
        # original version did not deal with floating point rounding (decimal(9,6) in database)
        #  diffs = [(k, (v, d2[k])) for k, v in d1.items() if v != d2[k]]
        diffs = {}
        for k, v1 in d1.items():
            v2 = d2[k]
            if isinstance(v1, Decimal):
                v1 = float(v1)
            if isinstance(v2, Decimal):
                v2 = float(v2)
            elif isinstance(v2, float) or isinstance(v1, float):
                # CAUTION: we assume the field is stored in db with 5 or more digits of precision
                # should really get the field definition and find the number of digits
                change = self.is_float_changed(v1, v2)
            else:
                change = v1 != v2
            if change:
                diffs[k] = (v1, v2)

        return dict(diffs)

    def is_float_changed(self, v1, v2):
        ''' Compare two floating point or decimal values to the proper precision
        Default precision is 5 digits
        Override this method if all fload/decimal fields have fewer digits in database
        '''
        return abs(round(v1 - v2, 5)) > 0.00001

    @property
    def has_changed(self):
        return bool(self.diff)

    @property
    def changed_fields(self):
        return self.diff.keys()

    def get_field_diff(self, field_name):
        """
        Returns a diff for field if it's changed and None otherwise.
        """
        return self.diff.get(field_name, None)

    def save(self, *args, **kwargs):
        """
        Saves model and set initial state.
        """
        super(ModelDiffMixin, self).save(*args, **kwargs)
        self.__initial = self._dict

    @property
    def _dict(self):
        return model_to_dict(self, fields=[field.name for field in
                                           self._meta.fields])

    class Meta:
        abstract = True
