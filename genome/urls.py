from django.conf import settings
from django.conf.urls.static import static
from django.shortcuts import redirect

import fileapi.views
from genome import ajax_views
from . import reports
from genome import autocomplete_views
from . import views
from django.conf.urls import url
from django.views.generic import TemplateView
from dal import autocomplete
from .models import BloodMarker

urlpatterns = [
    url(r'^profile/', views.profile, name="profile"),
    url(r'^try/', views.try_demo, name="try-demo"),
    url(r'^snp/(?P<rsid>([A-Za-z0-9_-]+))/', views.snp, name="snp"),
    url(r'^page/disclaimer/$', TemplateView.as_view(template_name='genome/global-disclaimer.html'), name="disclaimer"),
    url(r'^page/(?P<slug>([A-Za-z0-9_-]+))/', views.page, name="page"),
    url(r'^$', views.home_page, name="home_page"),
    url(r'^$', TemplateView.as_view(template_name='genome/home_page.html')),
    url(r'^snp_explorer/$', reports.variance_report, name="variance"),
    url(r'^potentially_bad_snps/$', reports.snps_to_look_at, name="snps_to_look_at"),
    url(r'^get_report_pdf_page/$', reports.get_report_pdf_page, name="get_snps_report_pdf_page"),
    url(r'^info/(?P<id>\d+)/', views.info, name="info"),
    url(r'^fixes/(?P<id>\d+)/', views.fixes, name="fixes"),
    url(r'^symptoms_conditions/', views.symptoms_conditions, name="symptoms_conditions"),
    url(r'^gene_packs/', views.gene_packs, name="gene_packs"),
    url(r'^gene_pack/add/$', views.create_gene_pack, name="create-gene-pack"),
    url(r'^gene_pack/(?P<id>\d+)/change/$', views.edit_gene_pack, name="edit-gene-pack"),
    url(r'^gene_pack/(?P<id>\d+)/delete/$', views.delete_gene_pack, name="delete-gene-pack"),
    url(r'^like_gene_pack_api/(?P<pack_id>\d+)/$', views.like_gene_pack_api, name="like-gene-pack"),
    url(r'^dislike_gene_pack_api/(?P<pack_id>\d+)/$', views.dislike_gene_pack_api, name="dislike-gene-pack"),
    url(r'^gene_ajax/(?P<gene_pk>(.+))/(?P<interaction_type>(.+))/', ajax_views.gene_ajax, name="gene_ajax"),
    url(r'^gene_experiment_ajax/(?P<gene>.+)/', views.gene_experiment_ajax, name="gene_experiment_ajax"),
    url(r'^gene_disease_ajax/(?P<gene>.+)/', views.gene_disease_ajax, name="gene_disease_ajax"),

    url(r'^gene/(?P<slug>.+)/pathway/(?P<kegg_id>\d+)/', views.gene_pathway_redirect, name="gene_pathway_redirect"),
    url(r'^gene/(?P<slug>.+)/pathway/(?P<kegg_id>.+)/', views.gene_pathway, name="gene_pathway_details"),

    url(r'^pathway/(?P<pathway_id>.+)/', views.pathway, name="pathway_details"),
    url(r'^pathway_chemical_ajax/(?P<pathway_id>.+)/', ajax_views.pathway_chemical_ajax, name="pathway_chemical_ajax"),
    url(r'^gene/(?P<gene>(.+))/', views.gene, name="gene"),

    url(r'^dashboard/$', views.dashboard, name="dashboard"),
    url(r'^potentially_bad_and_rare_snps/$', reports.my_rare_snps, name="rare"),
    url(r'^potentially_bad_genes/', views.genes_to_look_at, name="genes_to_look_at"),
    url(r'^my_important_snps/$', reports.my_important_snps, name="my_important_snps"),
    url(r'^potential_fixes/', views.my_favorable_options, name="my_favorable_options"),
    url(r'^fix/(?P<gene>([A-Za-z0-9_-]+))/', views.fix, name="fix"),
    url(r'^description/(?P<gene>(.+))/$', views.description, name="description"),
    url(r'^start/', views.start, name="start"),
    url(r'^s3/', views.s3, name="s3"),
    url(r'^sign_s3/', views.sign_s3, name="sign_s3"),
    url(r'^list_gene_pack/(?P<id>\d+)/$', reports.list_gene_pack, name="list_gene_pack"),
    url(r'^snp_pack/(?P<id>\d+)/category/(?P<category_id>\d+)/$', views.snp_pack, name="snp_pack"),
    url(r'^snp_pack/(?P<id>\d+)/', views.snp_pack, name="snp_pack"),
    url(r'^substance/(?P<substance>([A-Za-z0-9_-]+))/', views.substance, name="substance"),
    url(r'^admin_gene/(?P<gene>(.+))/$', views.admin_gene, name="admin_gene"),
    url(r'^quick_refund/', views.quick_refund, name="quick_refund"),
    url(r'^snp_aggregator/', views.snp_packs, name="snp_packs"),
    url(r'^marketing_scripts/', views.marketing_scripts, name="marketing_scripts"),
    url(r'^add_bookmark_snp/$', views.add_bookmark_snp, name="add_bookmark_snp"),
    url(r'^my_bookmarked_snps/$', reports.bookmarked_snps, name="bookmarked_snps"),
    url(r'^post/FAQs/$', lambda request: redirect("guide:faqs")),
    url(r'^post/(?P<slug>([A-Za-z0-9_-]+))/', views.post, name="post"),
    url(r'^choose_user/', views.choose_user, name="choose_user"),
    url(r'^blood_markers/', views.blood_markers, name="blood_markers"),
    url(r'^snp_autocomplete/', autocomplete.Select2QuerySetView.as_view(model=BloodMarker), name="snp_autocomplete"),
    url(r'^genediseasetraits/(?P<gene>(.+))/$', views.genediseasetrait_list, name="genediseasetraits"),
    url(r'^disease_genes_ajax/(?P<slug>[a-zA_Z0-9-_]+)/', views.disease_genes_ajax, name="disease_genes_ajax"),
    url(r'^disease_chemicals_ajax/(?P<slug>[a-zA_Z0-9-_]+)/', views.disease_chemicals_ajax, name="disease_chemicals_ajax"),
    url(r'^disease_go_ajax/(?P<slug>[a-zA_Z0-9-_]+)/', ajax_views.disease_go_ajax, name="disease_go_ajax"),
    url(r'^disease_pathways_ajax/(?P<slug>[a-zA_Z0-9-_]+)/', ajax_views.disease_pathways_ajax, name="disease_pathways_ajax"),
    url(r'^diseasetrait/(?P<slug>[a-zA_Z0-9-_]+)/$', views.diseasetrait_redirect, name="diseasetrait"),
    url(r'^disease/(?P<slug>[a-zA_Z0-9-_]+)/disease_in_hierarchy/$', ajax_views.disease_in_hierarchy, name="disease_in_hierarchy"),
    url(r'^disease/(?P<slug>[a-zA_Z0-9-_]+)/$', views.diseasetrait, name="disease"),
    url(r'^trait/(?P<slug>[a-zA_Z0-9-_]+)/$', views.diseasetrait, name="trait"),
    url(r'^measurement/(?P<slug>[a-zA_Z0-9-_]+)/$', views.diseasetrait, name="measurement"),
    url(r'^diseases-list-api/$', views.diseases_list_api, name="diseases-list-api"),
    url(r'^diseasetrait-symptom-ajax/(?P<slug>[a-zA_Z0-9-_]+)/$',
        ajax_views.DiseaseSymptomAjaxListView.as_view(), name="diseasetrait_symptoms_ajax"),
    url(r'symptom-diseasetraits-ajax/(?P<slug>[a-zA_Z0-9-_]+)/$',
        ajax_views.SymptomDiseasesAjaxListView.as_view(), name='symptom_diseasetraits_ajax'),
    url(r'symptom/(?P<slug>[a-zA_Z0-9-_]+)/$', views.SymptomDetailView.as_view(), name='symptom'),

    url(r'^biological-process/(?P<go_id>.+)/$', views.gene_ontology, name="biological-process"),
    url(r'^molecular-function/(?P<go_id>.+)/$', views.gene_ontology, name="molecular-function"),
    url(r'^geneontology/(?P<go_id>.+)/$', views.gene_ontology_redirect, name="geneontology"),

    url(r'^uploader/', fileapi.views.uploader, name="uploader"),
    url(r'^uploader_23andme/', fileapi.views.uploader_23andme, name="uploader_23andme"),

    url(r'^genome/gene_autocomplete$', autocomplete_views.GeneAutocomplete.as_view(), name="genome_gene_autocomplete"),
    url(r'^genome/diseasetrait_autocomplete$', autocomplete_views.DiseaseTraitAutocomplete.as_view(), name="genome_diseasetrait_autocomplete"),
    url(r'^genome/disease_symptom', autocomplete_views.DiseaseSymptomAutocomplete.as_view(), name="genome_disease_symptom"),
    url(r'^genome/disease_hierarchy_autocomplete$', autocomplete_views.DiseaseHierarchyAutocomplete.as_view(), name="genome_disease_hierarchy_autocomplete"),
    url(r'^genome/diseasesynonym_autocomplete$', autocomplete_views.DiseaseSynonymAutocomplete.as_view(), name="genome_diseasesynonym_autocomplete"),
    url(r'^genome/diseasevariant_autocomplete$', autocomplete_views.DiseaseVariantAutocomplete.as_view(), name="genome_diseasevariant_autocomplete"),
    url(r'^genome/factors_autocomplete$', autocomplete_views.FactorsAutocomplete.as_view(), name="genome_factors_autocomplete"),
    url(r'^genome/categories_autocomplete$', autocomplete_views.CategoryAutocomplete.as_view(), name="genome_categories_autocomplete"),
    url(r'^genome/user_autocomplete$', autocomplete_views.UserAutocomplete.as_view(), name="genome_user_autocomplete"),
    url(r'^genome/userprofile_autocomplete$', autocomplete_views.UserProfileAutocomplete.as_view(), name="genome_userprofile_autocomplete"),
    url(r'^genome/snp_autocomplete$', autocomplete_views.SnpAutocomplete.as_view(), name="genome_snp_autocomplete"),
    url(r'^genome/symptom_autocomplete$', autocomplete_views.SymptomAutocomplete.as_view(), name="genome_symptom_autocomplete"),
    url(r'^genome/condition_autocomplete$', autocomplete_views.ConditionAutocomplete.as_view(), name="genome_condition_autocomplete"),
    url(r'^genome/file_autocomplete$', autocomplete_views.FileAutocomplete.as_view(), name="genome_file_autocomplete"),
    url(r'^genome/genekeyword_autocomplete$', autocomplete_views.GeneKeywordAutocomplete.as_view(), name="genome_genekeyword_autocomplete"),
    url(r'^genome/genepathway_autocomplete$', autocomplete_views.GenePathwayAutocomplete.as_view(), name="genome_genepathway_autocomplete"),
    url(r'^genome/disease_exceptions_autocomplete$', autocomplete_views.DiseaseExceptionsPathwayAutocomplete.as_view(), name="disease_exceptions_autocomplete"),
    url(r'^genome/gene_ontology_autocomplete', autocomplete_views.GeneOntologyAutocomplete.as_view(), name="gene_ontology_autocomplete"),
    url(r'^genome/pathway_autocomplete$', autocomplete_views.PathwayAutocomplete.as_view(), name="pathway_autocomplete"),
    url(r'^genes-list-api/$', views.genes_list_api, name="genes-list-api"),

    url(r'^genome/get_disease_trait_modal_body/(?P<rsid>([A-Za-z0-9_-]+))/(?P<user_name>(.+))/', views.get_disease_trait_modal_body,
        name="get_disease_trait_modal_body"),
    url(r'^genome/get_substance_interaction_modal_body/(?P<gene>(.+))/', views.get_substance_interaction_modal_body,
        name="get_substance_interaction_modal_body"),

    url(r'^disease-tree/$', views.disease_tree, name="disease-tree"),
    url(r'^disease-tree-ajax/(?P<node_id>\d+)/$', views.disease_tree_ajax, name="disease-tree-ajax"),

    # url(r'^diseasetrait-load-more/$', views.diseasetrait_load_more, name="diseasetrait-load-more"),
    url(r'^diseasetest/(?P<disease>(.+))/$', views.genediseasetrait_list),

    url(r'^autocomplete/diseases/$', views.autocomplete_diseases, name="ajax-autocomplete-diseases"),

    url(r'^bad-genes-api/$', views.bad_genes_api, name="bad-genes-api"),

    url(r'^gene_packs_ajax/$', ajax_views.GenePacksAjaxListView.as_view()),
    url(r'^report-autocomplete/$', ajax_views.report_autocomplete, name="report-autocomplete"),

    url(r'^resend_confirmation_email/$', views.resend_confirmation_email, name="resend-confirmation-email"),

    url(r'^compare_reports/(?P<compare_type>(.+))/(?P<comparator>(.+))/$',
        reports.CompareReports.as_view(), name='compare_reports')
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
