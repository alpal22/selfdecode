from chemical.models import ChemicalGeneInteraction


def _gene_chemical_interactions(request, query_set, interaction_type, extra):
    query = """SELECT "chemical_chemicalgeneinteraction"."id",
                        "chemical_chemicalgeneinteraction"."gene_id",
                        "chemical_chemicalgeneinteraction"."chemical_id",
                        "chemical_chemicalgeneinteraction"."organism_id",
                        "chemical_chemicalgeneinteraction"."interaction",
                        "chemical_chemicalgeneinteraction"."pub_med_ids",
                        "chemical_chemicalgeneinteraction"."amount",
                        COUNT(DISTINCT "chemical_chemicalgeneinteractionaction"."action") AS "exists_opposite",
                        ARRAY_AGG(DISTINCT "chemical_chemicalgeneinteractiontype"."name") AS "actions_merged",
                        ((COUNT("chemical_chemicalgeneinteractionaction"."action") + "chemical_chemicalgeneinteraction"."amount") - 1) AS "total",
                        "chemical_chemical"."id",
                        "chemical_chemical"."name",
                        "chemical_chemical"."slug",
                        "chemical_chemical"."synonyms",
                        "chemical_chemical"."associated_from",
                        "chemical_chemical"."recommendation_status",
                        "chemical_chemical"."category_associated_from",
                        "chemical_chemical"."chemical_number",
                        "chemical_chemical"."parent_chemical_numbers",
                        "chemical_chemical"."cas_rn",
                        "chemical_chemical"."definition",
                        "chemical_chemical"."drug_bank_ids",
                        "chemical_chemical"."foodb_id",
                        "chemical_chemical"."name_scientific",
                        "chemical_chemical"."itis_id",
                        "chemical_chemical"."wikipedia_id",
                        "chemical_chemical"."picture_file_name",
                        "chemical_chemical"."picture_content_type",
                        "chemical_chemical"."picture_file_size",
                        "chemical_chemical"."t3db_id",
                        "chemical_chemical"."inchi_identifier",
                        "chemical_chemical"."inchi_key",
                        "chemical_chemical"."hmdb_id",
                        "chemical_chemical"."pubchem_Compound_ID",
                        "chemical_chemical"."chembl_id",
                        "chemical_chemical"."chem_spider_id",
                        "chemical_chemical"."kegg_compound_id",
                        "chemical_chemical"."uni_prot_id",
                        "chemical_chemical"."omim_id",
                        "chemical_chemical"."chebi_id",
                        "chemical_chemical"."bio_cyc_id",
                        "chemical_chemical"."ctd_id",
                        "chemical_chemical"."stitch_di",
                        "chemical_chemical"."pdb_id",
                        "chemical_chemical"."actor_id",
                        "chemical_organism"."id",
                        "chemical_organism"."latin_name",
                        "chemical_organism"."english_name"
                    FROM "chemical_chemicalgeneinteraction"
                        INNER JOIN "chemical_chemicalgeneinteractionaction" ON ("chemical_chemicalgeneinteraction"."id" = "chemical_chemicalgeneinteractionaction"."interaction_id")
                        INNER JOIN "chemical_chemicalgeneinteractionaction" T4 ON ("chemical_chemicalgeneinteraction"."id" = T4."interaction_id")
                        INNER JOIN "chemical_chemicalgeneinteractiontype" ON (T4."interaction_type_id" = "chemical_chemicalgeneinteractiontype"."id")
                        INNER JOIN "chemical_chemical" ON ("chemical_chemicalgeneinteraction"."chemical_id" = "chemical_chemical"."id")
                        %(extra_join)s
                        LEFT OUTER JOIN "chemical_organism" ON ("chemical_chemicalgeneinteraction"."organism_id" = "chemical_organism"."id")
                        WHERE ("chemical_chemicalgeneinteraction"."gene_id" = %(gene_id)s
                            AND "chemical_chemicalgeneinteractionaction"."action" IN ('decreases', 'increases')
                            AND T4."action" = '%(extra_type_filter)s'
                            %(extra_where)s
                        )
                        GROUP BY "chemical_chemicalgeneinteraction"."id", "chemical_chemical"."id", "chemical_organism"."id"
                        ORDER BY "total" DESC """ % {
        "extra_join": extra.get("join"),
        "extra_where": extra.get("where"),
        "gene_id": query_set.pk,
        "extra_type_filter": interaction_type
    }

    decrease_v2 = ChemicalGeneInteraction.objects.raw(query)

    return list(decrease_v2)
