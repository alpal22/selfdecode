

function drawGraph(api, cola, tab) {

    var width = 900,
        height = 500;

    d3.select(".chem-graph svg").remove();

    var tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function (d) {
            if(d.tooltip == '') {
                return '<div style="font-size:12px; width:200px; line-height:1.5">No info</div>'
            }
            else {
                return '<div style="font-size:12px; width:200px; line-height:1.5">' + d.tooltip + '</div>';
            }
        });

    var svg = d3.select(".chem-graph").append("svg")
        .attr("width", width)
        .attr("height", height).call(tip);


    d3.json(api, function (error, graph) {
        $('#loader').empty();

        cola
            .nodes(graph.nodes)
            .links(graph.links)
            .start();

        var link = svg.selectAll(".link")
            .data(graph.links)
            .enter().append("line")
            .attr("class", "link");

        var node = svg.selectAll(".node")
            .data(graph.nodes)
            .enter()
            .append("rect")
            .attr("class", "node")
            .attr("width", function (d) {
                return d.width;
            })
            .attr("height", function (d) {
                return d.height;
            })
            .attr("rx", 7).attr("ry", 5)
            .attr("bookmarked", function (d) {
                if (tab == "bookmarked")
                    return d.bookmarked;
            })
            .attr("selected", function (d) {
                if (tab == "selected")
                    return d.selected;
            })
            .call(cola.drag);

        var label = svg.selectAll(".label")
            .data(graph.nodes)
            .enter()
            .append("text")
            .attr("class", "label")
            .text(function (d) {
                return d.name
            })
            .on('mouseover', tip.show)
            .on('mouseout', tip.hide)
            .call(cola.drag);

        var arrow = svg.selectAll(".arrow")
            .data(graph.nodes)
            .enter()
            .append("foreignObject")
            .attr('width', 20)
            .attr('height', 20);

        arrow
            .append("xhtml:i")
            .attr("class", function (d) {
                if (d.action == 'increases')
                    return 'fa fa-long-arrow-up'
                else if (d.action == 'decreases')
                    return 'fa fa-long-arrow-down'
                else if (d.action == 'increasesdecreases')
                    return 'fa fa-arrows-v'
                else
                    return ''
            })
            .attr("style", function (d) {
                if (d.exception_status == 'decreased' && d.action == 'decreases')
                    return "color: #3eafdb; font-size: 16px";
                else if (d.exception_status == 'increased' && d.action == 'increases')
                    return "color: #3eafdb; font-size: 16px";
                else if (d.exception_status == 'increased' && d.action == 'decreases')
                    return "color: rgb(158, 2, 2); font-size: 16px";
                else if (d.exception_status == 'decreased' && d.action == 'increases')
                    return "color: rgb(158, 2, 2); font-size: 16px";
                else
                    return "color: white; font-size: 16px";
            })
            .call(cola.drag);

        svg.select(".node")
            .style("fill", "#3eafdb");

        svg.select("text")
            .on('mouseover', tip.hide)
            .on('mouseout', tip.hide);

        svg.select("foreignObject i")
            .style("display", "none");

        cola.on("tick", function () {
            link.attr("x1", function (d) {
                return d.source.x;
            })
                .attr("y1", function (d) {
                    return d.source.y;
                })
                .attr("x2", function (d) {
                    return d.target.x;
                })
                .attr("y2", function (d) {
                    return d.target.y;
                });

            node.attr("x", function (d) {
                return d.x - d.width / 2;
            })
                .attr("y", function (d) {
                    return d.y - d.height / 2;
                });

            label.attr("x", function (d) {
                return d.x;
            })
                .attr("y", function (d) {
                    var h = this.getBBox().height;
                    return d.y + h / 4;
                });

            arrow.attr("x", function (d) {
                return d.x - d.width / 2.3
            })
                .attr("y", function (d) {
                    return d.y + -d.height / 2.5
                });

        });

        var link_func = function (d, i) {
            if (d.url.includes('chemical')) {
                return;
            }
            if (d3.event.defaultPrevented) {
                return;
            }
            window.open(d.url);
        };

        node.on("click", link_func);
        label.on("click", link_func);

    });
}