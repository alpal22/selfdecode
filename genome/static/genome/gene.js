$(document).ready(function () {
    var pages = {
        "increase": 1,
        "decrease": 1
    };
    var experiment = {
        "increase": {
            page: 1,
            visible: false,
            category: "disease"
        },
        "decrease": {
            page: 1,
            visible: false,
            category: "disease"
        }
    };
    var already_visible = {};

    function load_on_expand(hash_id) {
        hash_id = hash_id.replace('#', '');
        if (!already_visible[hash_id] && hash_id == "all-ways-to-increase-gene") {
            $('.load-more-btn[data-type="increase"]').trigger('click');
        }
        if (!already_visible[hash_id] && hash_id == "all-ways-to-decrease-gene") {
            $('.load-more-btn[data-type="decrease"]').trigger('click');
        }
        already_visible[hash_id] = true;
    }

    var filters = {};
    $('.load-more-btn').on('click', function (e, extra) {
        var $this = $(this);
        $this.data("oldtext", $this.text());
        $this.text("Loading...");
        $this.attr("disabled", true);

        if (extra)
            filters = extra;

        var url = $this.data("url") + "?page=" + pages[$this.data("type")];
        if (filters.category === "all")
            filters.category = null;
        else if (filters.category)
            url += "&category=" + filters.category;

        $.get(url).success(function (data) {
            var paginator = data.paginator;

            if (!paginator.has_next)
                $this.hide();
            else
                $this.show();

            $("#" + $this.data("type") + "-reaction tbody").append(data.data);
            $('[data-toggle="tooltip"]').tooltip();

            pages[$this.data("type")] = paginator.next_page_number;
        }).always(function () {
            $this.text($this.data("oldtext"));
            $this.attr("disabled", false);
        });

        return false;
    });
    $('.more-experiments').on('click', function () {
        var $this = $(this);
        experiment[$this.data("type")].visible = true;
        $this.data("oldtext", $this.text());
        $this.text("Loading...");
        $this.attr("disabled", true);
        var url = $this.data("url") + "?page=" + experiment[$this.data("type")].page;
        if (experiment[$this.data("type")].category)
            url += "&category=" + experiment[$this.data("type")].category;

        if ($this.data("type"))
            url += "&type=" + $this.data("type");

        $.get(url).success(function (response) {
            var paginator = response.paginator;
            if (!paginator.has_next)
                $this.hide();
            else
                $this.show();
            experiment[$this.data("type")].page = paginator.next_page_number;
            $("#" + $this.data("type") + "-experiment tbody").append(response.data)
            $('[data-toggle="tooltip"]').tooltip();
        }).always(function () {
            $this.text($this.data("oldtext"));
            $this.attr("disabled", false);
        });
    });

    $(".category-filter").on('change', function () {
        var $this = $(this);
        var $target = $("#" + $this.data("attribute"));
        $target.find("tbody").empty();
        var $btn = $target.find(".load-more-btn");
        pages[$this.data("type")] = 1;
        $btn.trigger("click", {category: $this.val()});
        return false;
    });
    $(".experiment-filter").on('change', function () {
        var $this = $(this);
        var $target = $("#" + $this.data("attribute"));
        $target.find("tbody").empty();
        var $btn = $target.find(".more-experiments");
        experiment[$this.data("type")].page = 1;
        experiment[$this.data("type")].category = $this.val();
        $btn.trigger("click");
        return false;
    });


    var disease_page = 1;
    $('#more-diseases').on('click', function () {
        window.disease_visible = true;
        var $this = $(this);
        $this.data("oldtext", $this.text());
        $this.text("Loading...");
        $this.attr("disabled", true);
        var url = $this.data("url") + "?page=" + disease_page;
        $.get(url).success(function (response) {
            var paginator = response.paginator;
            if (!paginator.has_next)
                $this.hide();
            else
                $this.show();
            disease_page = paginator.next_page_number;
            $("#disease-interaction-table").append(response.data)
        }).always(function () {
            $this.text($this.data("oldtext"));
            $this.attr("disabled", false);
        });
    });

    var hash = window.location.hash;
    if (!experiment.increase.visible && hash == "#experiments_increase") {
        $('#increase-experiment .more-experiments').trigger('click');
    }
    if (!experiment.decrease.visible && hash == "#experiments_decrease") {
        $('#decrease-experiment .more-experiments').trigger('click');
    }
    if (!window.disease_visible && hash == "#gene-disease-interactions") {
        $('#more-diseases').trigger('click');
    }

    window.disease_visible = false;

    if (hash) {
        var $this = $(hash);
        if ($this.length > 0) {
            var $icon = $this.find('.fa');
            $icon.hasClass('fa-plus');
            $icon.removeClass("fa-plus").addClass("fa-minus");
            $this.next().slideToggle();
            scrollToPosition($this);
            load_on_expand(hash);
            if(Highcharts && Highcharts.charts && Highcharts.charts.length > 0)
                Highcharts.charts[0].reflow();
        }
    }

    $('.panel-toggle .panel-heading').on('click', function () {
        var $this = $(this);
        var $icon = $this.find('.fa');
        var hash_id = $this.attr('id');
        if ($icon.hasClass('fa-plus')) {
            $icon.removeClass("fa-plus").addClass("fa-minus");
            window.location.hash = hash_id;
            scrollToPosition($this);
        }
        else
            $icon.removeClass("fa-minus").addClass("fa-plus");
        if (!experiment.increase.visible && hash_id == "experiments_increase") {
            $('#increase-experiment .more-experiments').trigger('click');
        }
        if (!experiment.decrease.visible && hash_id == "experiments_decrease") {
            $('#decrease-experiment .more-experiments').trigger('click');
        }
        if (!window.disease_visible && hash_id == "gene-disease-interactions") {
            $('#more-diseases').trigger('click');
        }

        load_on_expand(hash_id);

        $this.next().slideToggle();
        Highcharts.charts[0].reflow();
    });

});

resubscribe_tooltipsy('.hashtip');
$('.substances-hashtip').tooltipsy();

function scrollToPosition(elem) {
    $('html, body').animate({
        scrollTop: parseInt(elem.offset().top - $(".top-navbar").height() - 10)
    }, 1000);
}