(function () {
    var already_visible = {};
    var disease_page_num = 1; // for top 10 genes block
    var disease_counter = 1;
    var experiment_page = 1;
    var disease_page = 1;
    var symptom_page_num = 1;

    var pages = {
        "my_snp": 1,
        "my_snp_sub_diseases": 1,
        "all_snps": 1,
        "biological-process": 1,
        "molecular-function": 1
    };

    var pathway_page = 1;
    var width = 900,
        height = 500;

    function scrollToPosition(elem) {
        $('html, body').animate({
            scrollTop: parseInt(elem.offset().top - $(".top-navbar").height() - 10)
        }, 1000);
    }

    function load_more($btn) {
        if(!already_visible.disease_hierarchy && $btn.attr('data-hash') == 'disease-hierarchy') {
            load_hierarchy();
        }
    }

    $('.panel-toggle .panel-heading[data-hash="disease-hierarchy"]').on('click', function () {
        var $this = $(this);
        var $icon = $this.find('.fa');
        if ($icon.hasClass('fa-plus')){
            $icon.removeClass("fa-plus").addClass("fa-minus");
            window.location.hash = '#' + $this.attr('data-hash');
            scrollToPosition($this);
        }
        else
            $icon.removeClass("fa-minus").addClass("fa-plus");

        load_more($this);

        $this.next().slideToggle();
    });

    var hash = window.location.hash;

    $('.chem-btn').click(function (event) {
        event.preventDefault();

        $('#my-modal').modal('show');

        var img = document.createElement("img");
        img.setAttribute("src", "https://s3-us-west-1.amazonaws.com/decodify/assets/default.gif");
        img.setAttribute("style", "position:relative; left:45%; width:60px;");

        $('#loader').append(img);

        $(".modal-header").html($(this).attr('data-title'));
        var colaapi = cola.d3adaptor()
            .linkDistance(80)
            .avoidOverlaps(true)
            .size([width, height]);
        drawGraph($(this).attr('data-url'), colaapi);
    });


    $(".category-filter").change(function (e) {
        e.cancelBubble = true;
        e.preventDefault();
        var category = $(this).val();
        var listId = $(this).data().attribute;
        if (category == 'all') {
            $("#" + listId + " li").show()
        }
        else {
            $("#" + listId + ' li').hide();
            $("#" + listId + ' li[data-attribute*="' + category + '"]').show();
        }
        return false;
    });

    function unique(arr) {
        var result = [];

        nextInput:
            for (var i = 0; i < arr.length; i++) {
                var str = arr[i];
                for (var j = 0; j < result.length; j++) {
                    if (result[j] == str) continue nextInput;
                }
                result.push(str);
            }

        return result;
    }

    function disease_risk(genotype, risk_alleles) {
        if (risk_alleles.length == 0)
            return '';
        grouped_alleles = unique(risk_alleles);

        if (grouped_alleles.length == 1) {
            allele = grouped_alleles[0];
            if ((genotype.split(allele).length - 1) == 2) {
                return '<i class="fa fa-exclamation-circle text-danger" aria-hidden="true"></i>';
            } else if ((genotype.split(allele).length - 1) == 1) {
                return '<i class="fa fa-exclamation-circle text-warning" aria-hidden="true"></i>';
            } else {
                return '<i class="fa fa-exclamation-circle text-success" aria-hidden="true"></i>';
            }
        } else if (grouped_alleles.length == 2) {
            var allele1 = grouped_alleles[0];
            var allele2 = grouped_alleles[1];
            if ((genotype.split(allele1).length - 1) || (genotype.split(allele2).length - 1)) {
                return '<i class="fa fa-exclamation-circle text-warning" aria-hidden="true"></i>';
            } else {
                return '<i class="fa fa-exclamation-circle text-success" aria-hidden="true"></i>';
            }
        } else {
            return ' ';
        }
    }

    $('.disease-panel').on('click', function () {
    var $this = $(this);
    var $icon = $this.find('.fa');
    if ($icon.hasClass('fa-plus')) {
        $icon.removeClass("fa-plus").addClass("fa-minus");
        window.location.hash = '#' + $this.attr('id');
        scrollToPosition($this);
    }
    else
        $icon.removeClass("fa-minus").addClass("fa-plus");
    load_hierarchy();

    $this.next().slideToggle();
});


    function load_hierarchy(){
        var url = 'disease_in_hierarchy/';
         $.get(url).success(function (response) {
            $("#disease-hierarchy").siblings('.panel-body').append(response.data);
            already_visible.disease_hierarchy = true;
        });
    }

    $(".user-snp-load-more").click(function() {
        var el = $(this);
        var query = '?page=' + pages[el.data("type")] + "&page_type=" + el.data("type");

        $(this).text('Loading...').attr("disabled", "disabled");
        $.ajax({
            url: query,
            success: function(page) {
                el.text('Load more').removeAttr("disabled");
                var paginator = page.paginator;

                if (page.object_list.length == 0) {
                    el.closest(".panel").find(".my_snp_table").html("<p>" + app.config.AJAX_NOINFO_MESSAGE + "</p>");
                } else if (pages[el.data("type")] == 1) {
                    el.closest(".panel").find(".my_snp_table").html('<table class="table table-bordered" cellspacing="0" width="100%">' +
                        '<thead>' +
                        '<tr>' +
                        '<th>Genes</th>' +
                        '<th>SNP</th>' +
                        '<th>Your Genotype</th>' +
                        '<th>Risk Alleles</th>' +
                        ((page.object_list.length > 0 && page.object_list[0].disease) ? '<th>Disease Subcategory</th>' : '') +
                        '</tr>' +
                        '</thead>' +
                        '<tbody class="">' +
                        '</tbody>' +
                        '</table>');
                }

                pages[el.data("type")] = paginator.next_page_number;
                $.each(page.object_list, function(index, item) {
                    var count = item.user_genotype.split(item.minor_allele).length - 1;

                    if (count == 2){
                        var user_genotype = '<span class="badge badge-danger"> ' + item.user_genotype + '</span>';
                    } else if (count == 1) {
                        var user_genotype = '<span class="badge badge-warning"> ' + item.user_genotype + '</span>';
                    } else {
                        var user_genotype = '<span class="badge badge-success"> ' + item.user_genotype + '</span>';
                    }
                    var genes_list = '';

                    for (var i = 0; i < item.genes.length; i++) {
                        var gene = item.genes[i];

                        genes_list += '<a href="/gene/{0}" class="hashtip" data-subscribed="unsub" title="{1}">{2}</a>'.format(gene.slug, gene.title, gene.name.toUpperCase());
                        if(gene.is_bad_gene) {
                            genes_list += '<i class="fa fa-exclamation-triangle text-warning hashtip" ' +
                                'aria-hidden="true" ' +
                                'data-toggle="tooltip" ' +
                                'title="Potentially Bad Gene"></i>';
                        }
                        if(gene.contains_risk_allele) {
                            genes_list += '<i class="fa fa-exclamation-circle text-danger hashtip" ' +
                                'aria-hidden="true" ' +
                                'data-toggle="tooltip" ' +
                                'title="Contains Risk Alleles"></i>';
                        }
                        if ( i != item.genes.length - 1){
                            genes_list += ', ';
                        }
                    };

                    var bookmarkSnpButton = '<button\
                            data-add-url="/add_bookmark_snp/"\
                            data-remove-url="/bookmarked_snps/"\
                            data-pk="{0}"\
                            data-rsid="{1}"\
                            data-action="bookmark-snp"\
                            class="btn btn-default btn-sm btn-rounded-sm modal-button">\
                        <i class="fa fa-bookmark"></i>\
                    </button>'.format(item.id, item.rsid);

                    var icon = disease_risk(item.user_genotype, item.risk_alleles);
                    var row = '<tr> <td> <span>' + genes_list + '</span> </td> <td> <span><a href="/snp/' + item.rsid + '/">  ' + item.rsid + ' </a>'  + bookmarkSnpButton + ' </span> </td> <td> <span>' + user_genotype + '</span> </td> <td> <span>' + item.risk_alleles.join(", ") + ' ' + icon  + '</span> </td>' + (item.disease ? "<td>" + item.disease + "</td>" : "") + ' </tr>' ;

                    el.closest(".panel").find(".my_snp_table tbody").append(row);

                });

                if (!paginator.has_next) {
                    el.remove();
                }

                $('[data-toggle="tooltip"]').tooltip();
                resubscribe_tooltipsy('.hashtip');
            }
        });
    });

    $("#user-other-load-more").click(function () {
        var query = '?page=' + pages.all_snps + "&page_type=other";
        $(this).text('Loading...').attr("disabled", "disabled");
        $.ajax({
            url: query,
            success: function (page) {

                $("#user-other-load-more").text('Load more').removeAttr("disabled");
                var paginator = page.paginator;
                if (!paginator.has_next) {
                    $('#user-other-load-more').remove();
                }
                pages.all_snps = paginator.next_page_number;

                $.each(page.object_list, function (index, item) {

                    var genes_list = '';
                    for (var i = 0; i < item.genes.length; i++) {
                        genes_list += '<a href="/gene/' + item.genes[i].slug + '/">' + item.genes[i].name.toUpperCase() + '</a>';
                        if (i != item.genes.length - 1) {
                            genes_list += ', ';
                        }
                    }
                    var row = '<tr> <td> <span>' + genes_list + '</span> </td> <td> <span><a href="/snp/' + item.rsid + '/">' + item.rsid + '</a></span> </td><td> <span>' + item.risk_alleles.join(", ") + '</span> </td> </tr>';
                    $('#my_other_table').append(row);
                });
            }
        });
    });

    $('#top-disease-toggle').click(function () {
        if(!window.disease_visible) {
            $('#top-disease-genes-load-more').trigger('click');
        }
    });


    $('#top-disease-genes-load-more').click(function (event) {
        window.disease_visible = true;
        event.stopPropagation();
        var disease = $.parseJSON($(this).attr('data-value')).disease;
        var url = '/top-disease-genes/{0}/?page={1}'.format(disease, disease_page_num);
        disease_page_num += 1;
        $(this).text('Loading...').attr('disabled', 'disabled');
        function check_bad_genes() {
            if (typeof window.bad_genes !== 'undefined') {
                $.get(url).success(function (response) {
                    if (!response['has_next']) {
                        $('#top-disease-genes-load-more').remove()
                    }
                    response.request_data.forEach(function (item) {
                        var gene_name = item.gene__name.toUpperCase();
                        var link_tag = '<a href="/gene/{0}/">{0}</a>'.format(gene_name);
                        var exclamation = ~$.inArray(item.gene__id, window.bad_genes) ?
                            '<i class="fa fa-exclamation-circle text-danger" aria-hidden="true" ' +
                            'data-toggle="tooltip" title="Potentially problematic gene"></i>' : '';
                        var output_tag = '<p>{0}. {1} ({2}) {3}</p>'.format(disease_counter,
                            link_tag, item.inference, exclamation);

                        disease_counter += 1;
                        $('#data-container').append(output_tag)
                    });

                }).always(function () {
                    $('#top-disease-genes-load-more').text('Load more').attr("disabled", false);
                });
            } else {
                setTimeout(check_bad_genes, 100);
            }
        };
        check_bad_genes();
    });

    $('#more-chemicals').on('click', function () {
        window.experiments_visible = true;
        var $this = $(this);
        $this.data("oldtext", $this.text());
        $this.text("Loading...");
        $this.attr("disabled", true);
        var url = $this.data("url") + "?page=" + experiment_page;
        $.get(url).success(function (response) {
            var paginator = response.paginator;
            if (!paginator.has_next)
                $this.hide();
            else
                $this.show();
            experiment_page = paginator.next_page_number;
            var $table = $("#chemical-interaction-table");
            if(response.data.trim()) {
                $table.append(response.data);
                $table.parents("table").show();
            } else {
                $table.parents("table").replaceWith('<p>' + app.config.AJAX_NOINFO_MESSAGE + '</p>');
            }
        }).always(function () {
            $this.text($this.data("oldtext"));
            $this.attr("disabled", false);
        });
    });

    $('#more-genes').on('click', function () {
        window.disease_visible = true;
        var $this = $(this);
        $this.data("oldtext", $this.text());
        $this.text("Loading...");
        $this.attr("disabled", true);
        var url = $this.data("url") + "?page=" + disease_page;

        function check_bad_genes() {
                $.get(url).success(function (response) {
                    var paginator = response.paginator;
                    if (!paginator.has_next)
                        $this.hide();
                    else
                        $this.show();
                    disease_page = paginator.next_page_number;
                    var $table = $("#gene-interaction-table");
                    if(response.data.trim()) {
                        $table.append(response.data);
                        $table.parents("table").show();
                    } else {
                        $table.parents("table").replaceWith('<p>' + app.config.AJAX_NOINFO_MESSAGE + '</p>');
                    }

                    $table.find("[data-gene_id]:not(.has-exclamation)").each(function () {
                        var $this = $(this);
                        var gene_id = parseInt($this.data("gene_id"));
                        $this.addClass("has-exclamation");
                    });


                }).always(function () {
                    $this.text($this.data("oldtext"));
                    $this.attr("disabled", false);
                    resubscribe_tooltipsy('.hashtip');
                });
        }
        resubscribe_tooltipsy('.hashtip');
        check_bad_genes();
    });

     $('.disease-go-load-more').on('click', function () {
        var $this = $(this);
        $this.data("oldtext", $this.text());
        $this.text("Loading...");
        $this.attr("disabled", true);
        var url = $this.data("url") + "?page=" + pages[$this.data("type")] + "&go_type=" + $this.data("type");
        $.get(url).success(function (response) {
            var paginator = response.paginator;
            if (!paginator.has_next)
                $this.hide();
            else
                $this.show();
            pages[$this.data("type")] = paginator.next_page_number;
            $this.parent().siblings(".disease-go-ul").append(response.data)
        }).always(function () {
            $this.text($this.data("oldtext"));
            $this.attr("disabled", false);
        });
    });

     $('#more-pathways').on('click', function () {
        var $this = $(this);
        $this.data("oldtext", $this.text());
        $this.text("Loading...");
        $this.attr("disabled", true);
        var url = $this.data("url") + "?page=" + pathway_page;
        $.get(url).success(function (response) {
            var paginator = response.paginator;
            if (!paginator.has_next)
                $this.hide();
            else
                $this.show();
            pathway_page = paginator.next_page_number;

            var $table = $("#pathway-interaction-table");
            if(response.data.trim()) {
                $table.append(response.data);
                $table.parents("table").show();
            } else {
                $table.parents("table").replaceWith('<p>' + app.config.AJAX_NOINFO_MESSAGE + '</p>');
            }

        }).always(function () {
            $this.text($this.data("oldtext"));
            $this.attr("disabled", false);
        });
    });

    $('#disease-symptoms-load-more').on('click', function () {

        var url = '{0}?page={1}'.format($(this).attr('data-url'), symptom_page_num);
        symptom_page_num += 1;
        $(this).text('Loading...').attr('disabled', 'disabled');

        $.get(url).success(function (response) {
            if (!response.has_next) {
                $('#disease-symptoms-load-more').remove()
            }
            if (response.rendered_data == '') {
                $('#disease-symptom-ul').replaceWith("<p>" + app.config.AJAX_NOINFO_MESSAGE + "</p>");
            }
            $('#disease-symptom-ul').append(response.rendered_data)
        }).always(function () {
            $('#disease-symptoms-load-more').text('Load more').attr("disabled", false);
        });
    });

    $.triggerAnchor().then(function ($this) {
        load_more($this);
    });
})();
