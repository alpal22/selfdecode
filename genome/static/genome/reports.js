/**
 * Created by developer on 12/25/16.
 */
(function () {
    var $report_modal = $('#report-ajax-modal');

    function preloader() {
        var img = document.createElement("img");
        var div = document.createElement("div");
        img.setAttribute("src", "https://s3-us-west-1.amazonaws.com/decodify/assets/default.gif");
        img.setAttribute("style", "position:fixed;top:50%;margin-left:-30px;margin-top:-30px;left:50%;width:60px;");
        div.setAttribute("style", "position: fixed;top: 0;left:0;right:0;bottom:0;z-index: 90000;background: rgba(255,255,255,0.8);");
        $(div).append(img);
        $report_modal.find(".modal-body").append(div);
    }

    function bookmark_handler(fn) {
        return function () {
            var $this = $(this);
            $this.attr("disabled", true);
            $this.find('.fa').removeClass('fa-bookmark').addClass('fa-spinner').addClass('fa-spin');

            var result = fn.call(this);

            result.always(function () {
                $this.attr("disabled", false);
                $this.find('.fa').addClass('fa-bookmark').removeClass('fa-spinner').removeClass('fa-spin');
            }).error(function () {
                $this.removeClass("btn-success");
                $this.removeClass("btn-default");
                $this.addClass("btn-danger");
                $this.attr("title", "Something went wrong...");
                $this.attr("disabled", true);
            });
            return result;
        };
    }

    $('[data-action="get-modal-content"]').on("click", function () {
        var $this = $(this);

        $report_modal.find(".modal-title").text($this.data("head"));
        var data_genotype = $this.data("genotype");
        console.log(data_genotype);
        if(data_genotype) {
            $report_modal.find(".genotype-text").text("My Genotype: " + $this.data("genotype"));
        }
        else {
            $report_modal.find(".genotype-text").text("");
        }
        $report_modal.modal("show");

        preloader();

        $.get($this.data("url")).success(function (data) {
            $report_modal.find(".modal-body").html(data)
        });
        return false;
    });

    $(document).on("click", '[data-action="remove-bookmarked-snp"]', bookmark_handler(function () {
        var $this = $(this);
        return $.get($this.data("url"), {snp_id: $this.data("pk")}).success(function () {
            $this.parent().parent().css("background", "#cfcfcf");
            $this.parent().find("*").remove();
            $this.attr("data-action", "bookmark-snp");
        });
    }));

    $(document).on("click", '[data-action="bookmark-snps"]', bookmark_handler(function () {
        var $this = $(this);
        var ids = $this.attr("data-ids").split(',');
        return $.get($this.data("add-url"), {snp_ids: ids}, function() {
            $this.removeClass("btn-default");
            $this.addClass("btn-success");
            $this.attr("data-action", "unbookmark-snps");
        });
    }));

    $(document).on("click", '[data-action="bookmark-snp"]', bookmark_handler(function () {
        var $this = $(this);
        Intercom('trackEvent', 'bookmark snp', {snp: $this.data("rsid")});
        return $.get($this.data("add-url"), {snp_id: $this.data("pk")}, function() {
            $this.removeClass("btn-default");
            $this.addClass("btn-success");
            $this.attr("data-action", "unbookmark-snp");
        });
    }));

    $(document).on("click", '[data-action="unbookmark-snp"]', bookmark_handler(function () {
        var $this = $(this);
        return $.get($this.data("remove-url"), {snp_id: $this.data("pk")}, function() {
            $this.removeClass("btn-success");
            $this.addClass("btn-default");
            $this.attr("data-action", "bookmark-snp");
        });
    }));

    $('[data-action="unbookmark-gene-snps"]').on("click", bookmark_handler(function () {
        var $this = $(this);
        return $.get($this.data("url"), {gene_id: $this.data("pk")}).success(function () {
            $this.parent().parent().css("background", "#cfcfcf");
            $this.parent().find("*").remove();
        });
    }));

    $('[data-action="bookmark-gene-snps"]').on("click", bookmark_handler(function () {
        var $this = $(this);
        return $.get($this.data("url"), {gene_id: $this.data("pk")}, function() {
            $this.removeClass("btn-default");
            $this.addClass("btn-success");
        });
    }));

    $('[data-action="gene-modal"]').on("click", function () {
        var $this = $(this);
        $report_modal.find(".modal-title").text($this.data("title"));
        $report_modal.find(".genotype-text").empty();
        $report_modal.modal("show");

        preloader();

        $.get($this.data("url"), function(data) {
            $report_modal.find(".modal-body").html(data);
        });
    });

    $(document).on("shown.bs.tab", '[data-toggle="tab"]', function (e) {
        var $this = $(e.target);
        var $class = $this.data('cls');
        var $nav_tab = $this.parents(".nav-tabs");
        $nav_tab.removeClass("border-bottom-tab-blue");
        $nav_tab.removeClass("border-bottom-tab-red");
        $nav_tab.addClass($class);
    });


    $('[data-action="gene-page"]').on("click", function () {
       var $this = $(this);
       var gene_url = $this.attr("data-url");
       window.open(gene_url)
    });

    $('[data-toggle="tooltip"]').tooltip();

    resubscribe_tooltipsy('.hashtip');
})();
