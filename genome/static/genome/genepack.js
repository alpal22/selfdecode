function GenePackHandler() {

    this.gene_packs_pages = {
        my_packs_page_num: 1,
        upvoted_page_num: 1,
        not_upvoted_page_num: 1
    };

    this.conditions = {
        MY_GENE_PACKS_CONDITION: 'my',
        UPVOTED_CONDITION: 'gt',
        NOT_UPVOTED_CONDITION: 'eq'
    };

    this.data_root = {
        FAVORITE_DATA_ROOT: '#favorite-container',
        UPVOTED_DATA_ROOT: '#upvoted-container',
        NOT_UPVOTED_DATA_ROOT: '#not-upvoted-container'
    };

    this.endpoint_url_template = '/gene_packs_ajax/?page={0}&condition={1}';

    this.ajax_instances = function () {
        return {
            FAVORITE_AJAX_INSTANCE: this.ajax_pack_instance(this.urls().FAVORITE_PACKS_URL),
            UPVOTED_AJAX_INSTANCE: this.ajax_pack_instance(this.urls().UPVOTED_PACKS_URL),
            NOT_UPVOTED_AJAX_INSTANCE: this.ajax_pack_instance(this.urls().NOT_UPVOTED_PACKS_URL)
        };
    };

    this.urls = function () {
        return {
            FAVORITE_PACKS_URL: this.build_url(this.gene_packs_pages.my_packs_page_num,
                this.conditions.MY_GENE_PACKS_CONDITION),

            UPVOTED_PACKS_URL: this.build_url(this.gene_packs_pages.upvoted_page_num,
                this.conditions.UPVOTED_CONDITION),

            NOT_UPVOTED_PACKS_URL: this.build_url(this.gene_packs_pages.not_upvoted_page_num,
                this.conditions.NOT_UPVOTED_CONDITION)
        };
    };

    this.build_url = function (page_num, cond) {
        return this.endpoint_url_template.format(page_num, cond)
    };

    this.ajax_pack_instance = function (url) {
        return $.ajax({
            url: url,
            dataType: 'json'
        })
    }

}

var handler = new GenePackHandler();

var button_selector = {
    FAVORITE_BUTTON: '#favorite-packs-load-more',
    UPVOTED_BUTTON: '#upvoted-packs-load-more',
    NOT_UPVOTED_BUTTON: '#not-upvoted-packs-load-more'
};

function append_data(root_id, data) {
    $(root_id).append(data)
}

function remove_button(selector, has_next) {
    if (!has_next) {
        $(selector).remove()
    }
}

function preload_data() {

    var fav_instance = handler.ajax_pack_instance(handler.urls().FAVORITE_PACKS_URL);
    var upvoted_instance = handler.ajax_pack_instance(handler.urls().UPVOTED_PACKS_URL);
    var not_upvoted_instance = handler.ajax_pack_instance(handler.urls().NOT_UPVOTED_PACKS_URL);

    $.when(fav_instance, upvoted_instance, not_upvoted_instance).then(
        function (favorite, upvoted, not_upvoted) {

            var fav_response = favorite[0];
            var upv_response = upvoted[0];
            var not_upv_response = not_upvoted[0];

            append_data(handler.data_root.FAVORITE_DATA_ROOT, fav_response.rendered_data);
            append_data(handler.data_root.UPVOTED_DATA_ROOT, upv_response.rendered_data);
            append_data(handler.data_root.NOT_UPVOTED_DATA_ROOT, not_upv_response.rendered_data);

            remove_button(button_selector.FAVORITE_BUTTON, fav_response.has_next);
            remove_button(button_selector.UPVOTED_BUTTON, upv_response.has_next);
            remove_button(button_selector.UPVOTED_BUTTON, not_upv_response.has_next);

        })
}

$('#favorite-packs-load-more').on('click', function () {

    $(this).text('Loading...').attr('disabled', 'disabled');

    handler.gene_packs_pages.my_packs_page_num += 1;
    var ajax_instance = handler.ajax_pack_instance(handler.urls().FAVORITE_PACKS_URL);

    ajax_instance.done(function (response) {

        remove_button(button_selector.FAVORITE_BUTTON, response['has_next']);
        append_data(handler.data_root.FAVORITE_DATA_ROOT, response.rendered_data)

    }).always(function () {

        $(button_selector.FAVORITE_BUTTON).text('Load more').attr("disabled", false);
    });

});

$('#upvoted-packs-load-more').on('click', function () {

    $(this).text('Loading...').attr('disabled', 'disabled');

    handler.gene_packs_pages.upvoted_page_num += 1;
    var ajax_instance = handler.ajax_pack_instance(handler.urls().UPVOTED_PACKS_URL);

    ajax_instance.done(function (response) {

        remove_button(button_selector.UPVOTED_BUTTON, response['has_next']);
        append_data(handler.data_root.UPVOTED_DATA_ROOT, response.rendered_data)

    }).always(function () {
        $('#upvoted-packs-load-more').text('Load more').attr("disabled", false);
    });

});

$('#not-upvoted-packs-load-more').on('click', function () {
    $(this).text('Loading...').attr('disabled', 'disabled');

    handler.gene_packs_pages.not_upvoted_page_num += 1;
    var ajax_instance = handler.ajax_pack_instance(handler.urls().NOT_UPVOTED_PACKS_URL);

    ajax_instance.done(function (response) {

        remove_button(button_selector.NOT_UPVOTED_BUTTON, response['has_next']);
        append_data(handler.data_root.NOT_UPVOTED_DATA_ROOT, response.rendered_data);

    }).always(function () {

        $('#not-upvoted-packs-load-more').text('Load more').attr("disabled", false);
    });

});


preload_data();


$(document).on('click', '.fa-heart-o', function () {
    var elem = $(this);
    var pack_id = elem.attr('data-pack-id');
    var url = '/like_gene_pack_api/' + pack_id + '/';
    $.get(url, function () {
        elem.removeClass('fa-heart-o');
        elem.addClass('fa-heart');
        var counter = elem.next().text();
        elem.next().text(+counter + 1);
    }).fail(function (error) {
        console.log(error)
    });
});

$(document).on('click', '.fa-heart', function () {
    var elem = $(this);
    var pack_id = elem.attr('data-pack-id');
    var url = '/dislike_gene_pack_api/' + pack_id + '/';
    $.get(url, function () {
        elem.removeClass('fa-heart');
        elem.addClass('fa-heart-o');
        var counter = elem.next().text();
        elem.next().text(+counter - 1);
    }).fail(function (error) {
        console.log(error)
    });
});