$(document).ready(function(){
        destroy_tooltipsy('.panel-hashtip');
        resubscribe_tooltipsy('.panel-hashtip');

        var selected_diseases = [];
        var selected_genes = [];
        var tpl_toxins = $('#tpl-toxins').html();
        var is_ajax_working_disease = 0;
        var is_ajax_working_gene = 0;
        var width = 900,
            height = 500;

        $('table.genes').append('<p>Select genes to see beneficial substances</p>');
        $('table.diseases').append('<p>Select diseases to see beneficial substances</p>');

        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        });

        function get_content(tab){
            if(tab == 'genes')
                $('select.genes').attr("disabled", true).trigger("chosen:updated");
            if(tab == 'diseases')
                $('select.diseases').attr("disabled", true).trigger("chosen:updated");

            if ((tab == 'genes' && is_ajax_working_gene) || (tab == 'diseases' && is_ajax_working_disease))
                return false;

            $('table.' + tab + ' tr').remove();
            $('table.' + tab + ' p').remove();
            var img = document.createElement("img");
            img.setAttribute("src", "https://s3-us-west-1.amazonaws.com/decodify/assets/default.gif");
            img.setAttribute("style", "position:relative; left:45%; width:30px;");
            $('.tab-' + tab).find("img").remove();
            $('.tab-' + tab).append(img);
            if (tab == 'genes')
                is_ajax_working_gene = 1;
            else
                is_ajax_working_disease = 1;
            if (tab == 'genes'){
                var request_obj = {'genes': selected_genes};
            } else if (tab == 'diseases'){
                var request_obj = {'diseases': selected_diseases};
            }
            $.get('/recommendations/by_filter/', request_obj, function(data){
                var rows = '';
                $('.tab-' + tab + ' img').remove();
                if ($.isEmptyObject(data)){
                    $('table.' + tab).append('<p>Select ' + tab + ' to see beneficial substances</p>');
                } else {
                    if (data.chemicals.length > 0){
                        data.chemicals.forEach(function(item){
                            var selected_genes_to_string = '?';
                            selected_genes.map(function (gene) {
                                selected_genes_to_string += 'genes%5B%5D=' + gene + '&';
                            });
                            var rows = tpl_toxins
                            .replace('{chem_name}', item.name)
                            .replace('{count}', item.cnt)
                            .replace('{data_url}', "/recommendations/api/" + item.id + selected_genes_to_string)
                            .replace('{data_tab}', tab == 'genes' ? 'selected' : '')
                            .replace('{data_title}', item.t3db_data__description || item.definition)
                            .replace('{chem_url}', "/chemical/" + item.slug + '/');
                            $('table.' + tab).append(rows);
                        });
                    }else {
                        $('table.' + tab).append('<p>No chemicals found</p>');
                    };
                };
            }).always(function() {
                $('.ajax-hashtip').tooltipsy();
                if (tab == 'genes') {
                    is_ajax_working_gene = 0;
                    $('select.genes').attr("disabled", false).trigger("chosen:updated");
                }
                else {
                    is_ajax_working_disease = 0;
                    $('select.diseases').attr("disabled", false).trigger("chosen:updated");
                }
            });
        };

        $('select.diseases').ajaxChosen({
            dataType: 'json',
            type: 'GET',
            url:'/diseases-list-api/',
        }, {
                processItems: function(data){ return data.results; },
                useAjax: true,
                generateUrl: function(q){ return '/diseases-list-api/' },
                minLength: 2
        }, {
            disable_search_threshold: 10,
            width: "100%"
        });

        $('select.genes').ajaxChosen({
            dataType: 'json',
            type: 'GET',
            url:'/genes-list-api/',
        }, {
                processItems: function(data){ return data.results; },
                useAjax: true,
                generateUrl: function(q){ return '/genes-list-api/' },
                minLength: 2
        }, {
            disable_search_threshold: 10,
            width: "100%"
        });

        $('select.chemical').ajaxChosen({
            dataType: 'json',
            type: 'GET',
            url:'/chemicals-list-api/',
        }, {
                processItems: function(data){ return data.results; },
                useAjax: true,
                generateUrl: function(q){ return '/chemicals-list-api/' },
                minLength: 2
        }, {
            width: "72%",
            max_selected_options: 1
        });


        $('.panel-toggle .panel-heading').on('click', function () {
            var $this = $(this);
            var $icon = $this.find('.fa');
            if($icon.hasClass('fa-plus'))
                $icon.removeClass("fa-plus").addClass("fa-minus");
            else
                $icon.removeClass("fa-minus").addClass("fa-plus");
            $this.next().slideToggle();
        });

        $('.panel-toggle .panel-heading').on('expand', function () {
            var $this = $(this);
            var $icon = $this.find('.fa');
            if($icon.hasClass('fa-plus')){
                $icon.removeClass("fa-plus").addClass("fa-minus");
                $this.next().slideToggle();
            };
        });


        $('.chosen-select.diseases').on('change', function(evt, obj) {
            if (obj.selected){
                selected_diseases.push(obj.selected);
                $('.bg-diseases').trigger('expand');
                get_content('diseases');
            };
            if (obj.deselected){
                index = selected_diseases.indexOf(obj.deselected);
                if (~index){
                    selected_diseases.splice(index, 1);
                };
                get_content('diseases');

                if (selected_diseases.length == 0) {
                    selected_genes = [];
                    get_content('diseases');
                }
            };
          });

        $('.chosen-select.genes').on('change', function(evt, obj) {
            if (obj.selected){
                selected_genes.push(obj.selected);
                $('.bg-genes').trigger('expand');
                get_content('genes');
            };
            if (obj.deselected){
                index = selected_genes.indexOf(obj.deselected);
                if (~index){
                    selected_genes.splice(index, 1);
                };
                get_content('genes');
            };
          });

        $('.chosen-select.chemical').on('change', function(evt, obj) {
            if (obj.selected){
                var chem_button = $('#tpl-btn').html();
                chem_button = chem_button
                    .replace('{data_url}', "/recommendations/api/" + obj.selected);
                $('div.chem-button button').remove();
                $('div.chem-button').append(chem_button);
            } else {
                $('div.chem-button button').remove();
            }
          });

        $(document).on('click', '.chem-btn', function(event){
            event.preventDefault();

            $('#my-modal').modal('show');
            var img = document.createElement("img");
            img.setAttribute("src", "https://s3-us-west-1.amazonaws.com/decodify/assets/default.gif");
            img.setAttribute("style", "position:relative; left:45%; width:60px;");

            $('#loader').html(img);

            $(".modal-header").html($(this).attr('data-title'));
            var colaapi = cola.d3adaptor()
                .linkDistance(80)
                .avoidOverlaps(true)
                .size([width, height]);
            drawGraph($(this).attr('data-url'), colaapi, $(this).attr('data-tab'));
        });

    });
