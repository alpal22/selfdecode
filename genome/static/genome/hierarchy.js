$('.disease-panel').on('click', function () {
    var $this = $(this);
    var $icon = $this.find('.fa');
    if ($icon.hasClass('fa-plus')) {
        $icon.removeClass("fa-plus").addClass("fa-minus");
        window.location.hash = '#' + $this.attr('id');
        scrollToPosition($this);
    }
    else
        $icon.removeClass("fa-minus").addClass("fa-plus");
    load_hierarchy();

    $this.next().slideToggle();
});


function load_hierarchy(){
    var url = 'disease_in_hierarchy/'
    $.get(url).success(function (response) {
        $("#disease-hierarchy").siblings('.panel-body').append(response.data)
    })
}



    <ul class="list-unstyled">
    <li>
        <div class="panel panel-info panel-heading" id="disease-hierarchy">
            <span><i class="fa fa-minus"></i> </span> <a
                href="{% url "disease" slug=disease_in_hierarchy.root.disease.slug %}" target="_blank"
                onclick="event.stopPropagation()">{{ disease_in_hierarchy.root.disease.name|capfirst }}</a>
        </div>
        <ul class="child-container">
            {% include 'genome/partials/diseasetrait_tree_node.html' with nodes=disease_in_hierarchy.root.get_children %}
        </ul>
    </li>

</ul>