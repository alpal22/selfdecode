// $(document).ready(function () {
//     $(".load-children").on('click', function () {
//
//     })
// });

$(document).on('click', '.load-children', function () {
    var url = "/disease-tree-ajax/" + $(this).closest('.panel').attr("id") + "/";
    var el = $(this);
    $.get(url).success(function (data) {
        el.removeClass("load-children");
        el.addClass("hide-children");
        el.find("i").removeClass("fa-plus");
        el.find("i").addClass("fa-minus");

        el.siblings('.child-container').first().append(data.data);
    });
    console.log(this)
});

$(document).on('click', '.hide-children', function() {
    var el = $(this);
    var child_ul = el.siblings('.child-container').first();
    el.removeClass("hide-children");
    el.addClass("load-children");
    el.find("i").removeClass("fa-minus");
    el.find("i").addClass("fa-plus");
    child_ul.empty()
});