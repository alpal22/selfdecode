$(document).ready(function () {

    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRFToken", $.cookie.get('csrftoken'));
        }
    });

    function set_rating(instance, dec_count, inc_count, user_rating) {

        instance.find('.inc_counter').html(inc_count);
        instance.find('.dec_counter').html(dec_count);

        if(user_rating == 1) {
            instance.find('.up').addClass('green-pointer');
        }
        else if (user_rating == -1) {
            instance.find('.down').addClass('red-pointer');
        }
        else {
            instance.find('.up').removeClass('green-pointer');
            instance.find('.down').removeClass('red-pointer');
        }
    }

    var gene_id = $('[data-action="voting"]').attr('data-object-id');

    var request_data = [
        {'genome.gene': [gene_id]},
    ];

    $.post('/single/rating/', JSON.stringify({'objects': request_data})).done(function(data) {
        var res = data.response.gene[0].response;
        var item = $('[data-action="voting"]');
        set_rating(item, res.dec_count, res.inc_count, res.user_rating);
    });

    function post_rating(rating) {
        var obj_id = this.data('object-id');
        var request_data = {
            'obj': this.data('content-type'),
            'obj_label': this.data('label'),
            'obj_id': obj_id,
            'user_rating': rating
        };
        var item = $(this);

        $.post('/single/vote/', request_data).done(function (data) {
            set_rating(item, data.dec_count, data.inc_count, data.user_rating);
        });
    }


    $('[data-action="voting"]').each(function () {
        var voting = $(this);
        var $up = $(this).find('.up');
        var $down = $(this).find('.down');

        $up.click(function () {
            $down.removeClass('red-pointer');
            post_rating.call(voting, 1);
            $up.addClass('green-pointer');
        });

        $down.click(function () {
            $up.removeClass('green-pointer');
            post_rating.call(voting, -1);
            $down.addClass('red-pointer');
        });
    });

});
