(function () {

    var pages = {
        'symptom_diseases_page': 1
    };


    var anchor = {
        '#symptom-diseases-toggle': '#symptom-diseases-load-more'
    };

    $('#symptom-diseases-load-more').on('click', function () {
        var url = '{0}?page={1}'.format($(this).attr('data-url'), pages.symptom_diseases_page);
        pages.symptom_diseases_page += 1;
        $(this).text('Loading...').attr('disabled', 'disabled');

        $.get(url).success(function (response) {
            if (!response.has_next) {
                $('#symptom-diseases-load-more').remove()
            }
            if(pages.symptom_diseases_page == 2 && response.rendered_data == '') {
                $('#symptom-diseases-ul').append('No info')
            }
            $('#symptom-diseases-ul').append(response.rendered_data)
        }).always(function () {
            $('#symptom-diseases-load-more').text('Load more').attr("disabled", false);
        });

    });


    function subscribe_toggles() {
        Object.keys(anchor).forEach(function (key) {
            $(key).one('click', function () {
                $(anchor[key]).trigger('click')
            })
        });
    }

    subscribe_toggles();

    function hashchecker() {
        var hash = window.location.hash;
        $(anchor[hash]).trigger('click');
    }

    hashchecker();


    function scrollToPosition(elem) {
        $('html, body').animate({
            scrollTop: parseInt(elem.offset().top - $(".top-navbar").height() - 10)
        }, 1000);
    }

    $('.panel-toggle .panel-heading').on('click', function () {
        var $this = $(this);
        var $icon = $this.find('.fa');
        if ($icon.hasClass('fa-plus')){
            $icon.removeClass("fa-plus").addClass("fa-minus");
            window.location.hash = '#' + $this.attr('id');
            scrollToPosition($this);
        }
        else {
            $icon.removeClass("fa-minus").addClass("fa-plus");
        }

        hashchecker();
        $this.next().slideToggle();
    });


})();