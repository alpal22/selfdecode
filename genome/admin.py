from dal import autocomplete
from django.utils.safestring import mark_safe
from django.contrib import admin
from django.core.urlresolvers import reverse
from django.contrib.admin import SimpleListFilter
from django.contrib.admin import widgets
from django.core.urlresolvers import reverse
from django.db import models
from django.forms import BaseInlineFormSet
from django.utils.safestring import mark_safe
from django_mptt_admin.admin import DjangoMpttAdmin
from mptt.admin import DraggableMPTTAdmin
from import_export.admin import ImportExportActionModelAdmin
from log.helpers import log_recent_changes
from .models import DiseaseTrait, PopulationGenome, GeneKeyword, UniprotData, GenePathway, SnpStudy, SnpGenes, \
    DiseaseHierarchy, DiseaseGOAssociations, GeneOntology, GeneDiseasePathwayInteraction, Pathology, DiseaseVariant, \
    DiseaseSymptomInteraction, DiseaseSymptom, ModelFieldChanges
from genome.forms import SnpForm, GeneFormAdmin, GenePackFormAdmin, SnpAlleleFormAdmin, \
    UserProfileFormAdmin, SiteLoginFormAdmin, PopulationGenomeFormAdmin, MarketingEventFormAdmin, DiseaseTraitAdminForm,\
    GeneChemicalInteractionSubForm, SnpStudyAdminForm, DiseaseHierarchyAdminForm, PathologyForm, DiseaseVariantForm, \
    DiseaseSymptomInteractionForm
from chemical.models import ChemicalGeneInteraction, ChemicalGeneInteractionAction
from .models import Snp
from .models import UserProfile
from .models import Page
from .models import Gene, Category, Condition, GenePack, SnpAllele, User, SiteLogin, MarketingEvent, Post, \
    KeywordLink, Fix, TranscriptionFactor
from hijack_admin.admin import HijackUserAdminMixin
from import_export.admin import ImportExportActionModelAdmin


class FieldsLoggerMixin(admin.ModelAdmin):
    """ Remembers fields changes """
    def save_form(self, request, form, change):
        self.changed_data = form.changed_data
        return super(FieldsLoggerMixin, self).save_form(request, form, change)

    def log_change(self, request, object, message):
        log_recent_changes(request.user, object, message, self.changed_data)


class GenePackAdmin(admin.ModelAdmin):
    form = GenePackFormAdmin
    list_display = ("id", "name", "price", "pack_type", "genes_list", "created_at",)

    def genes_list(self, inst):
        return ", ".join([gene.name for gene in inst.genes.all()])


class UserProfileAdmin(ImportExportActionModelAdmin):
    form = UserProfileFormAdmin
    list_display = ("user",)
    search_fields = ("user__email",)


class PopulationGenomeInline(admin.TabularInline):
    model = PopulationGenome
    extra = 0


class GeneralSnpAlleleInline(admin.StackedInline):
    model = SnpAllele
    extra = 0
    exclude = ('score_minor', 'score_major', 'score_hetero', 'fix_hetero', 'fix_major', 'fix_minor',)

    def get_queryset(self, request):
        queryset = super(GeneralSnpAlleleInline, self).get_queryset(request).filter(category__name="General")
        return queryset


@admin.register(Snp)
class SnpAdmin(FieldsLoggerMixin):
    form = SnpForm
    search_fields = ('rsid', 'name',)
    list_display = ('id', 'rsid', 'minor_allele', 'minor_allele_frequency', 'genes_list', 'view_page',)
    list_display_links = ('id', 'rsid',)
    list_filter = ("minor_allele", "red_flag",)
    inlines = (GeneralSnpAlleleInline, PopulationGenomeInline,)

    formfield_overrides = {
        models.ForeignKey: {'widget': autocomplete.ModelSelect2()},
    }

    def get_queryset(self, request):
        qs = super(SnpAdmin, self).get_queryset(request)
        qs = qs.prefetch_related("genes")
        return qs

    def genes_list(self, obj):
        return ", ".join([item.name for item in obj.genes.all()])

    genes_list.short_description = "Genes"

    def diseasestraits_list(self, obj):
        return ", ".join([item.name for item in obj.diseasestraits.all()])

    diseasestraits_list.short_description = "Diseases/Traits"

    def get_form(self, request, obj=None, **kwargs):  # get rid of the real field eventually
        self.exclude = ("description_simple",)
        form = super(SnpAdmin, self).get_form(request, obj, **kwargs)
        return form

    def view_page(self, inst):
        return mark_safe('<a href="%s">View Page</a>' % (
            reverse("snp", kwargs={"rsid": inst.rsid,}),
        ))

    def save_related(self, request, form, formsets, change):
        genes = form.cleaned_data.get("genes", [])
        del form.cleaned_data['genes']

        if change:
            form.instance.related_genes.all().delete()
            for gene in genes:
                SnpGenes.objects.create(snp=form.instance, gene=gene)

        return super(SnpAdmin, self).save_related(request, form, formsets, change)


@admin.register(SnpStudy)
class SnpStudyAdmin(admin.ModelAdmin):
    form = SnpStudyAdminForm
    list_display = ("id", "pubmed_id", "journal", "journal_published_at", "view_snp",
                    "risk_allele", "risk_allele_frequency",
                    "p_value", "odds_ratio", "view_page",)
    search_fields = ("snp__rsid",)
    list_filter = ("risk_allele",)

    def get_queryset(self, request):
        qs = super(SnpStudyAdmin, self).get_queryset(request)
        qs = qs.select_related("snp")
        return qs

    def view_snp(self, inst):
        return mark_safe('<a href="%s">%s</a>' % (
            reverse("admin:genome_snp_change", args=(inst.snp.pk,)),
            inst.snp,
        ))

    view_snp.short_description = "SNP"
    view_snp.admin_order_field = "snp"

    def view_page(self, inst):
        return mark_safe('<a href="%s">View Page</a>' % (
            reverse("snp", kwargs={"rsid": inst.snp.rsid,}),
        ))


class UniprotInline(admin.TabularInline):
    model = UniprotData
    extra = 0
    ordering = ("category",)
    verbose_name = "Uniprot Values"
    verbose_name_plural = "Uniprot Values"
    exclude = ("chemical",)


class ChemicalGeneInteractionFormSet(BaseInlineFormSet):
    def get_queryset(self):
        qs = super(ChemicalGeneInteractionFormSet, self).get_queryset()
        return qs[:10]


class ChemicalGeneInteractionInline(admin.TabularInline):
    def actions_callable(self, inst):
        return ", ".join(["%s %s" % (o.action, o.interaction_type,) for o in inst.actions.all()])
    actions_callable.short_description = "Actions"

    def edit_callable(self, inst):
        return '<a href="/joes_office/chemical/chemicalgeneinteraction/%s/change/">Edit</a>' % inst.pk \
            if inst.pk is not None else ""#resolve("admin:chemical_chemicalgeneinteraction_change", inst.pk)

    template = 'admin/tabular.html'
    edit_callable.short_description = ""
    edit_callable.allow_tags = True

    model = ChemicalGeneInteraction
    form = GeneChemicalInteractionSubForm
    formset = ChemicalGeneInteractionFormSet
    extra = 0
    readonly_fields = ("actions_callable", "edit_callable",)


class GeneAdmin(FieldsLoggerMixin):
    form = GeneFormAdmin
    search_fields = ("name", "symbol")
    list_display = ("id", "name", "symbol")
    exclude = ("synonyms",)

    inlines = [UniprotInline, ChemicalGeneInteractionInline]

    ordering = ("name",)

    formfield_overrides = {
        models.ManyToManyField: {"widget": widgets.FilteredSelectMultiple("", is_stacked=False)}
    }



@admin.register(GenePathway)
class GenePathwayAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "kegg_id", 'link',)
    search_fields = ("name",)

    def link(self, obj):
        return '<a href="http://www.genome.jp/kegg-bin/show_pathway?hsa%s" target="_blank">Show</a>' % obj.kegg_id
    link.allow_tags = True


class UserAdmin(ImportExportActionModelAdmin, HijackUserAdminMixin):
    search_fields = ('email',)
    readonly_fields = ('id',)
    list_display = ('username', 'hijack_field',)


class SnpAlleleAdmin(admin.ModelAdmin):
    form = SnpAlleleFormAdmin
    save_as = True
    exclude = ('fix_minor', 'fix_major', 'fix_hetero',)
    search_fields = ('snp__rsid', 'category__name',)
    list_display = ('snp', 'category',)
    list_filter = ("category",)

    formfield_overrides = {
        models.ForeignKey: {'widget': autocomplete.ModelSelect2()},
    }


class SiteLoginAdmin(admin.ModelAdmin):
    form = SiteLoginFormAdmin
    list_display = ('user', 'created_at',)


@admin.register(MarketingEvent)
class MarketingEventAdmin(admin.ModelAdmin):
    form = MarketingEventFormAdmin
    list_display = ('name', 'created_at', 'script_name_a', 'script_name_b',)


class UserRsidAdmin(admin.ModelAdmin):
    search_fields = ('rsid',)
    readonly_fields = ('id',)


class KeywordLinkAdmin(ImportExportActionModelAdmin):
    list_display = ('keyword', 'link')
    search_fields = ('keyword', 'link')


class TranscriptionFactorAdmin(admin.ModelAdmin):
    filter_horizontal = ("fixes",)
    list_display = ("id", "slug", "name", "ref_url",)
    list_display_links = ("id", "slug",)
    search_fields = ("name",)


class FixAdmin(admin.ModelAdmin):
    list_display = ('name',)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ("name",)
    search_fields = ("name",)


class DiseaseTraitParentOnly(SimpleListFilter):
    title = '"Is Alias"'
    parameter_name = "is_alias"

    def lookups(self, request, model_admin):
        return (
            (1, "Yes"),
            (0, "No"),
        )

    def queryset(self, request, queryset):
        if self.value() is None:
            return queryset
        queryset = queryset.exclude(parent__isnull=bool(int(self.value())))
        return queryset


@admin.register(DiseaseTrait)
class DiseaseTraitAdmin(FieldsLoggerMixin):
    form = DiseaseTraitAdminForm
    prepopulated_fields = {"slug": ("name",)}
    list_display = ("id", "name", "parent", "category", "slug", "view_page")
    list_display_links = ("id", "name",)
    list_filter = (DiseaseTraitParentOnly, "category",)
    search_fields = ("name",)
    ordering = ("name",)

    def view_page(self, inst):
        return mark_safe('<a href="%s" target="_blank" style="white-space: nowrap;">View Page</a>' % reverse("diseasetrait", kwargs={"slug": inst.slug}))


@admin.register(PopulationGenome)
class PopulationGenomeAdmin(admin.ModelAdmin):
    form = PopulationGenomeFormAdmin
    list_display = ("id", "title", "abbr", "snp",)
    list_filter = ("title",)
    search_fields = ("snp__name",)


@admin.register(GeneKeyword)
class GeneKeywordAdmin(admin.ModelAdmin):
    list_display = ("id", "slug", "name",)
    search_fields = ("slug", "name",)
    list_display_links = ("id", "slug",)


@admin.register(Condition)
class ConditionAdmin(admin.ModelAdmin):
    filter_horizontal = ("gene_packs",)


@admin.register(DiseaseHierarchy)
class DiseaseHierarchyAdmin(DjangoMpttAdmin):
    tree_auto_open = False
    tree_load_on_demand = 1
    trigger_save_after_move = True

    # Autoescape the tree data; default is True
    autoescape = True

    # useContextMenu option for the tree; default is False
    use_context_menu = True

    # define which field of the model should be the label for tree items
    item_label_field_name = None
    form = DiseaseHierarchyAdminForm


@admin.register(DiseaseGOAssociations)
class DiseaseGOAssociationsAdmin(admin.ModelAdmin):
    list_display = ("disease", "gene_ontology",)
    search_fields = ("disease", "gene_ontology",)
    list_display_links = ("disease",)


@admin.register(GeneOntology)
class GeneOntologyAdmin(admin.ModelAdmin):
    list_display = ("go_name", "go_id", "type",)
    search_fields = ("go_name", "go_id",)
    list_filter = ("type",)
    list_display_links = ("go_name", "go_id",)


@admin.register(GeneDiseasePathwayInteraction)
class GeneDiseasePathwayInteractionAdmin(admin.ModelAdmin):
    list_display = ("disease", "pathway", "gene_symbol",)
    search_fields = ("disease",)


@admin.register(DiseaseVariant)
class DiseaseVariantAdmin(admin.ModelAdmin):
    form = DiseaseVariantForm
    list_display = ("disease", "rsid", "gene_symbol", "risk_allele")
    search_fields = ("rsid",)


@admin.register(DiseaseSymptomInteraction)
class DiseaseSymptomInteractionAdmin(admin.ModelAdmin):
    form = DiseaseSymptomInteractionForm
    list_display = ("disease", "symptom", "frequency",)
    search_fields = ("disease",)


@admin.register(DiseaseSymptom)
class DiseaseSymptomAdmin(admin.ModelAdmin):
    list_display = ("name", "hpo_id",)
    search_fields = ("name",)


@admin.register(Pathology)
class PathologyAdmin(admin.ModelAdmin):
    form = PathologyForm
    list_display = ("gene", "name")
    # search_fields = ("gene",)
# @admin.register(Symptom)
# class SymptomAdmin(admin.ModelAdmin):
#     filter_horizontal = ("gene_packs",)


# Register your models here.
admin.site.unregister(User)
admin.site.register(Post)
admin.site.register(TranscriptionFactor, TranscriptionFactorAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(Fix, FixAdmin)
admin.site.register(SnpAllele, SnpAlleleAdmin)
admin.site.register(Gene, GeneAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Page)
admin.site.register(Category, CategoryAdmin)
admin.site.register(SiteLogin, SiteLoginAdmin)
admin.site.register(GenePack, GenePackAdmin)
admin.site.register(KeywordLink, KeywordLinkAdmin)
