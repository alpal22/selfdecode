from django.contrib.contenttypes.models import ContentType

from django.forms import MultipleChoiceField

from django.db import connection
from sqlalchemy import column
from sqlalchemy.sql import label

from genome.models import Snp
from genome.models import Gene
from django.db import connection

VALID_GENE_SCORE = 2.0


class SNPMultipleChoiceField(MultipleChoiceField):

    def to_python(self, value):
        value = super(SNPMultipleChoiceField, self).to_python(value=value)
        return set(map(int, value)) if len(value) > 0 else "{}"

    def valid_value(self, value):
        if value == "{" or value == "}":
            return True
        return super(SNPMultipleChoiceField, self).valid_value(value=value)



def get_related_instance(value):
    model, id = value.split(":")
    object_type = ContentType.objects.filter(model=model).first()
    return object_type.get_object_for_this_type(id=id)


def get__sqla_session():
    """
    Getting Session object of SqlAlchemy

    :return: SqlAlchemy Session object
    """
    from aldjemy.core import get_engine
    engine = get_engine()
    from sqlalchemy.orm import sessionmaker
    _Session = sessionmaker(bind=engine)
    return _Session()


class ResultObject(object):
    """
    Mapper for SqlAlchemy queries

    """
    def __init__(self, **entries):
        self.__dict__.update(entries)


def compile_sqla_statement(statement):
    """
    Compiles SqlAlchemy statement

    :param statement: SqlAlchemy statement
    :return: str: Compiled Sql query
    """
    from sqlalchemy.dialects import postgresql
    return str(statement.compile(dialect=postgresql.dialect(),
               compile_kwargs={"literal_binds": True}))


def get_potentially_problematic_genes(user, gene_ids=None, only_affected_snps=None):

    if gene_ids:
        ids_in = ', '.join(map(str, gene_ids))
        query_with_genes = """
            SELECT gene_id as id, "risk_allele_color_index"(risk_alleles, user_genotype, minor_allele, genotype_exceptions)  FROM (
                SELECT genome_snp.id,
                      (genome_userrsid.genotype) AS "user_genotype",
                      (genome_userrsid.genotype_style) AS "genotype_style",
                      COUNT("genome_snp"."id") AS "id__count",
                      ARRAY_AGG("genome_snpstudy"."risk_allele") AS "risk_alleles",
                      genome_snp.minor_allele,
                      genome_snp_genes.gene_id,
                      genotype_exceptions::text[]
                FROM "genome_snp"
                JOIN "genome_snp_genes" ON ("genome_snp"."id" = "genome_snp_genes"."snp_id" )
                LEFT OUTER JOIN "genome_snpstudy" ON ("genome_snp"."id" = "genome_snpstudy"."snp_id") , "genome_userrsid"
                WHERE (genome_userrsid.rsid = genome_snp.rsid) AND (genome_userrsid.file_id = {file_id}) AND gene_id in ({gene_ids})
                GROUP BY genome_snp_genes.gene_id,
                      (genome_userrsid.genotype),
                      (genome_userrsid.genotype_style),
                      "genome_snp"."id",
                      genome_snp.rsid
                ORDER BY "genome_snp"."rsid" ASC

            ) as G
            WHERE "risk_allele_color_index"(risk_alleles, user_genotype, minor_allele, genotype_exceptions) is not NULL
            ORDER BY "risk_allele_color_index"(risk_alleles, user_genotype, minor_allele, genotype_exceptions) DESC
            """.format(file_id=user.user_profile.active_file.pk, gene_ids=ids_in)
        query_set = Snp.objects.raw(query_with_genes)
    else:
        no_genes_query = """
        SELECT id, "risk_allele_color_index"(risk_alleles, user_genotype, minor_allele, genotype_exceptions), rsid  FROM (
            SELECT genome_snp.id, (genome_userrsid.genotype) AS "user_genotype",
                  (genome_userrsid.genotype_style) AS "genotype_style",
                  COUNT("genome_snp"."id") AS "id__count",
                  ARRAY_AGG("genome_snpstudy"."risk_allele") AS "risk_alleles",
                  genome_snp.minor_allele,
                  genome_snp.rsid,
                  genotype_exceptions::text[]
            FROM "genome_snp"
            LEFT OUTER JOIN "genome_snpstudy" ON ("genome_snp"."id" = "genome_snpstudy"."snp_id") , "genome_userrsid"
            WHERE (genome_userrsid.rsid = genome_snp.rsid) AND (genome_userrsid.file_id = {file_id})
            GROUP BY (genome_userrsid.genotype),
                  (genome_userrsid.genotype_style),
                  "genome_snp"."id",
                  genome_snp.rsid
            ORDER BY "genome_snp"."rsid" ASC

        ) as G
        WHERE "risk_allele_color_index"(risk_alleles, user_genotype, minor_allele, genotype_exceptions) is not NULL
        ORDER BY "risk_allele_color_index"(risk_alleles, user_genotype, minor_allele, genotype_exceptions) DESC
        """.format(file_id=user.user_profile.active_file.pk)
        query_set = Snp.objects.raw(no_genes_query)

    if only_affected_snps:
        list_query = [str(item.id) for item in list(query_set)
                      if item.risk_allele_color_index[2] == 'bad']
    else:
        list_query = [str(item.id) for item in list(query_set)
                      if item.risk_allele_color_index[0] > '0'
                      or item.risk_allele_color_index[2] == 'bad']

    return list_query


def execute_sqla_query(query):
    """
    Executes SqlAlchemy query with django cursor. We had to...

    :param query: SqlAlchemy query

    :return: list of ResultObject
    """
    raw_query = compile_sqla_statement(query.statement)
    with connection.cursor() as cursor:
        cursor.execute(raw_query)
        columns = [item.get('name') for item in query.column_descriptions]
        result_list = [
            ResultObject(**dict(zip(columns, row)))
            for row in cursor.fetchall()
        ]

    return result_list

def query_count(query):
    """
    print(self.cur.rowcount)

    :param query: SqlAlchemy query

    :return: int: Row count
    """
    raw_query = compile_sqla_statement(query.statement)

    with connection.cursor() as cursor:
        cursor.execute('SELECT COUNT(t.*) from ({0}) as t'.format(raw_query))
        return cursor.fetchone()[0]


def paginate_sqla_statement(query, page_num, per_page=50):
    """
    Paginates SqlAlchemy query

    :param query: SqlAlchemy query
    :param page_num: Number of needed page
    :param per_page: Items per page

    :return:  list: ResultObject list
    """
    total_count = query_count(query)
    page_num = int(page_num) if page_num else 1

    page_info = {
        'has_next': total_count > (page_num*per_page),
        'next_page_number': page_num + 1,
        'has_previous': page_num > 1,
        'previous_page_number': page_num - 1,
        'total_count': total_count,
        'num_pages': int(total_count / 50),
        'number': page_num
    }

    offset = (page_num - 1) * per_page
    paginated_query = query.offset(offset).limit(per_page)

    return execute_sqla_query(paginated_query), ResultObject(**page_info)

def get_gene_scores(gene_ids, file_id):
    """
    Calculates scores for specified gene ids

    :param gene_ids: List of Gene ids
    :param file_id: Current User File id
    :return: ResultObject: gene_id, gene_score
    """
    query = Gene.sa.query(
        Gene.sa.id,
        label('gene_score', column(
            """get_gene_score(genome_gene.id,{0})""".format(file_id), is_literal=True
        ))
    ).filter(Gene.sa.id.in_(gene_ids))

    result = execute_sqla_query(query)
    return {item.id: item.gene_score for item in result}


def get_bad_genes(user, gene_ids):
    active_file = user.user_profile.active_file
    bad_genes = active_file.genes_to_look_at.filter(
        pk__in=gene_ids
    ).values_list('id', flat=True)

    gene_scores = get_gene_scores(gene_ids, active_file.id)

    # gene id is valid if Score is greater than 2.0
    valid_ids = [
        str(key) if gene_scores[key] > VALID_GENE_SCORE else None for key in gene_scores.keys()
    ]

    # intersection with bad_genes
    gene_intersection = list(set([str(item) for item in bad_genes]) & set(valid_ids))

    return gene_intersection


def badges_for_gene(user, gene_ids):
    """
    Evaluates whether Gene is in List of Bad Snps or Contains Risk allele
    :param user: User instance
    :param gene_ids: List of gene ids
    :return:
    """

    contains_risk_allele = get_potentially_problematic_genes(user, gene_ids)
    bad_genes = get_bad_genes(user, gene_ids)

    return contains_risk_allele, bad_genes


def query_count(query):
    """
    print(self.cur.rowcount)

    :param query: SqlAlchemy query

    :return: int: Row count
    """
    raw_query = compile_sqla_statement(query.statement)

    with connection.cursor() as cursor:
        cursor.execute('SELECT COUNT(t.*) from ({0}) as t'.format(raw_query))
        return cursor.fetchone()[0]


def paginate_sqla_statement(query, page_num, per_page=50):

    total_count = query_count(query)
    page_num = int(page_num) if page_num else 1

    page_info = {
        'has_next': total_count > (page_num*per_page),
        'next_page_number': page_num + 1,
        'has_previous': page_num > 1,
        'previous_page_number': page_num - 1,
        'total_count': total_count,
        'num_pages': int(total_count / 50),
        'number': page_num
    }

    offset = (page_num - 1) * per_page
    paginated_query = query.offset(offset).limit(per_page)

    return execute_sqla_query(paginated_query), ResultObject(**page_info)
