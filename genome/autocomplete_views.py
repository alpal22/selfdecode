from dal import autocomplete
from django.contrib.auth.models import User
from django.db.models import Q

from analysis.models import Symptom
from genome.models import Snp, Gene, DiseaseTrait, TranscriptionFactor, Category, Condition, File, GeneKeyword, \
    GenePathway, DiseaseHierarchy, DiseaseHierarchyProxy, DiseaseSynonym, GeneOntology, DiseaseSymptom, Pathway, \
    DiseaseVariant, UserProfile


class SnpAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Snp.objects.none()

        qs = Snp.objects.all()

        if self.q:
            qs = qs.filter(rsid__istartswith=self.q)

        return qs


class GeneAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Gene.objects.none()

        qs = Gene.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs


class DiseaseTraitAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return DiseaseTrait.objects.none()

        qs = DiseaseTrait.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs


class FactorsAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return TranscriptionFactor.objects.none()

        qs = TranscriptionFactor.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs


class CategoryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Category.objects.none()

        qs = Category.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs


class UserAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return User.objects.none()

        qs = User.objects.all()

        if self.q:
            qs = qs.filter(username__istartswith=self.q)

        return qs


class UserProfileAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return UserProfile.objects.none()

        qs = UserProfile.objects.all()

        if self.q:
            qs = qs.filter(user__username__istartswith=self.q)

        return qs


class ConditionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Condition.objects.none()

        qs = Condition.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs


class FileAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return File.objects.none()

        qs = File.objects.all()

        if self.q:
            qs = qs.filter(file_name__istartswith=self.q)

        return qs


class GeneKeywordAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return GeneKeyword.objects.none()

        qs = GeneKeyword.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs


class GenePathwayAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return GenePathway.objects.none()

        qs = GenePathway.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs


class DiseaseExceptionsPathwayAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return DiseaseTrait.objects.none()

        qs = DiseaseTrait.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs


class DiseaseHierarchyAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return DiseaseHierarchyProxy.objects.none()

        qs = DiseaseHierarchyProxy.objects.all()

        if self.q:
            qs = qs.filter(Q(disease__name__istartswith=self.q) | Q(disease__name__istartswith=self.q))
        print(str(qs.query))
        return qs.order_by('tree_id', 'lft')


class SymptomAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Symptom.objects.none()

        qs = Symptom.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs


class DiseaseSynonymAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return DiseaseSynonym.objects.none()

        qs = DiseaseSynonym.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs


class PathwayAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Pathway.objects.none()

        qs = Pathway.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs


class GeneOntologyAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return GeneOntology.objects.none()

        qs = GeneOntology.objects.all()

        if self.q:
            qs = qs.filter(go_name__istartswith=self.q)

        return qs


class DiseaseSymptomAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return DiseaseSymptom.objects.none()

        qs = DiseaseSymptom.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs


class DiseaseVariantAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return DiseaseVariant.objects.none()

        qs = DiseaseVariant.objects.all()

        if self.q:
            qs = qs.filter(disease__name__istartswith=self.q)

        return qs
