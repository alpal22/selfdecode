var background = window.document.createElement("div")
background.setAttribute("style","position:fixed;background:rgba(255, 255, 255, 1);top:0;left:0;right:0;bottom:0;z-index:40000;display:none;border-top:6px solid #7aba3a;");
background.innerHTML = '<div class="exit-popup" style="float:right;font-size:2em;padding:5px 10px;cursor:pointer;color:#5c5e60;">x</div>'
window.document.body.appendChild(background)

var foreground = window.document.createElement("div")
foreground.setAttribute("style","margin: 16% auto;text-align:center;position:relative;top:0;width:787px;background-size:100%;padding:3%;");
foreground.innerHTML = '<h1 class="text-center" style="color:#5c5e60;margin-bottom:5px;font-size:2.2em; letter-spacing:0.01em;">Take your Health to the Next Level</h1><h3 class="text-center" style="margin-top:0px;color:#5c5e60; letter-spacing:0.01em;">Get Genetic Health Insights You Might Have Missed!</h3><button id="consult-button" class="btn btn-success" style="font-size:1.4em;font-weight:500px;"><i class="fa fa-cubes"></i> Consult with an Expert</button>'

background.appendChild(foreground)

jQuery(".exit-popup").click(function(){
  background.setAttribute("style", "display:none;")
  foreground.setAttribute("style", "display:none;")
  jQuery(".exit-popup").hide()
})

if (jQuery(window).width() > 800) {
  jQuery(background).fadeIn()
}

jQuery("#consult-button").click(function(){

  jQuery.ajax({
    url: /marketing_scripts/,
    data: {event_name:"consult_id00003",variant:"a"},
    success: function() { window.location.href="https://www.selfdecode.com/page/consults/"
    }
  });

})