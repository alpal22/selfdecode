from dal_select2_queryset_sequence.widgets import QuerySetSequenceSelect2
from django import forms
from django.contrib import admin
import django.contrib.admin.forms
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from dal import autocomplete

from mptt.forms import TreeNodeChoiceField, MPTTAdminForm

from .models import BloodMarker, Snp, Gene, GenePack, DiseaseTrait, SnpStudy, DiseaseHierarchy, Pathology, \
    DiseaseVariant, DiseaseSymptomInteraction, Pathway
from chemical.models import ChemicalGeneInteraction
from genome.helpers import SNPMultipleChoiceField


def email_unique(user, value):
    if value in User.objects.exclude(pk=user.pk).values_list('email', flat=True):
        raise ValidationError("Email is already in use by another user.")


def username_unique(user, value):
    if value in User.objects.exclude(pk=user.pk).values_list('email', flat=True):
        raise ValidationError("Username is already in use by another user.")


class NameForm(forms.ModelForm):

    def clean_email(self):
        value = self.cleaned_data.get("email")
        email_unique(self.instance, value)
        return value

    def clean_username(self):
        value = self.cleaned_data.get("username")
        username_unique(self.instance, value)
        return value

    class Meta:
        model = get_user_model()
        fields = ['first_name', 'last_name', 'email']


SNP_GENOTYPE_EXCEPTIONS = (
    ("1", "Heterozygous"),
    ("2", "Homozygous minor"),
)


class SnpForm(forms.ModelForm):
    genes = forms.ModelMultipleChoiceField(queryset=Gene.objects.all(), widget=autocomplete.ModelSelect2Multiple(url="genome_gene_autocomplete"))
    genotype_exceptions = SNPMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), choices=SNP_GENOTYPE_EXCEPTIONS, required=False)

    class Meta:
        model = Snp
        fields = '__all__'
        widgets = {
            "genes": autocomplete.ModelSelect2Multiple(url="genome_gene_autocomplete"),
            "diseasestraits": autocomplete.ModelSelect2Multiple(url="genome_diseasetrait_autocomplete"),
            "disease_variant": autocomplete.ModelSelect2Multiple(url="genome_diseasevariant_autocomplete"),
        }


class GeneFormAdmin(forms.ModelForm):
    # global_pathways = forms.ModelMultipleChoiceField(queryset=Pathway.objects.all(), widget=autocomplete.ModelSelect2Multiple(url="pathway_autocomplete"))

    class Meta:
        model = Gene
        fields = '__all__'
        widgets = {
            "transcription_factors": autocomplete.ModelSelect2Multiple(url="genome_factors_autocomplete"),
            "keywords": autocomplete.ModelSelect2Multiple(url="genome_genekeyword_autocomplete"),
            "pathways": autocomplete.ModelSelect2Multiple(url="genome_genepathway_autocomplete"),
            # "global_pathways": autocomplete.ModelSelect2Multiple(url="pathway_autocomplete"),
            "disease_exceptions": autocomplete.ModelSelect2Multiple(url="disease_exceptions_autocomplete"),
            "go": autocomplete.ModelSelect2Multiple(url="gene_ontology_autocomplete")
        }


class GenePackForm(forms.ModelForm):

    def clean_genes(self):
        data = self.cleaned_data.get("genes")
        if not len(data):
            raise forms.ValidationError("Fill up genes field")
        return data

    def clean_name(self):
        data = self.cleaned_data.get("name")
        if len(data) < 2:
            raise forms.ValidationError("Name should be longer than 2 symbols")
        if len(data) > 30:
            raise forms.ValidationError("The name can not be longer than 30 chars")
        return data

    class Meta:
        model = GenePack
        fields = ['name', 'genes', 'author']


class GenePackFormAdmin(forms.ModelForm):

    class Meta:
        model = GenePack
        fields = '__all__'
        widgets = {
            "genes": autocomplete.ModelSelect2Multiple(url="genome_gene_autocomplete"),
            "categories": autocomplete.ModelSelect2Multiple(url="genome_categories_autocomplete"),
            "likes": autocomplete.ModelSelect2Multiple(url="genome_user_autocomplete"),
        }


class SnpAlleleFormAdmin(forms.ModelForm):
    class Meta:
        model = GenePack
        fields = '__all__'
        widgets = {
            "snp": autocomplete.ModelSelect2(url="genome_snp_autocomplete"),
            "category": autocomplete.ModelSelect2(url="genome_categories_autocomplete"),
        }


class UserProfileFormAdmin(forms.ModelForm):
    class Meta:
        model = GenePack
        fields = '__all__'
        widgets = {
            "user": autocomplete.ModelSelect2(url="genome_user_autocomplete"),
            "gene": autocomplete.ModelSelect2Multiple(url="genome_gene_autocomplete"),
            "symptoms": autocomplete.ModelSelect2Multiple(url="genome_symptom_autocomplete"),
            "conditions": autocomplete.ModelSelect2Multiple(url="genome_condition_autocomplete"),
            "bookmarked_snps": autocomplete.ModelSelect2Multiple(url="genome_snp_autocomplete"),
            "authorized_users": autocomplete.ModelSelect2Multiple(url="genome_user_autocomplete"),
            "active_file": autocomplete.ModelSelect2(url="genome_file_autocomplete"),
        }


class DiseaseTraitAdminForm(admin.forms.forms.ModelForm):
    aliases = admin.forms.forms.ModelMultipleChoiceField(
        queryset=DiseaseTrait.objects,
        required=False,
        widget=autocomplete.ModelSelect2Multiple(url="genome_diseasetrait_autocomplete")
    )

    def __init__(self, *args, **kwargs):
        if "instance" in kwargs:
            self.base_fields["aliases"].initial = [obj.pk for obj in self.Meta.model.objects.filter(parent=kwargs["instance"])]
        super(DiseaseTraitAdminForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        self.Meta.model.objects.filter(parent=self.instance).update(parent=None)
        for obj in self.cleaned_data['aliases']:
            obj.parent = self.instance
            obj.save()

        return super(DiseaseTraitAdminForm, self).save(commit=commit)

    class Meta:
        model = DiseaseTrait
        exclude = ("parent",)
        widgets = {
            "category": autocomplete.ModelSelect2(),
            "ctd_synonyms": autocomplete.ModelSelect2Multiple(url="genome_diseasesynonym_autocomplete"),
        }


class SiteLoginFormAdmin(forms.ModelForm):
    class Meta:
        model = GenePack
        fields = '__all__'
        widgets = {
            "user": autocomplete.ModelSelect2(url="genome_user_autocomplete"),
        }


class PopulationGenomeFormAdmin(forms.ModelForm):
    class Meta:
        model = GenePack
        fields = '__all__'
        widgets = {
            "snp": autocomplete.ModelSelect2(url="genome_snp_autocomplete"),
        }


class MarketingEventFormAdmin(forms.ModelForm):
    class Meta:
        model = GenePack
        fields = '__all__'
        widgets = {
            "users_shown": autocomplete.ModelSelect2Multiple(url="genome_user_autocomplete"),
        }


class GeneChemicalInteractionSubForm(forms.ModelForm):
    class Meta:
        model = ChemicalGeneInteraction
        fields = ("chemical", "organism", "amount",)
        widgets = {
            'chemical': autocomplete.ModelSelect2(url='chemical:chemical-autocomplete'),
            'organism': autocomplete.ModelSelect2(url='chemical:organism-autocomplete'),
        }


class SnpStudyAdminForm(forms.ModelForm):
    class Meta:
        model = SnpStudy
        fields = '__all__'
        widgets = {
            "snp": autocomplete.ModelSelect2(url="genome_snp_autocomplete"),
            "diseasestraits": autocomplete.ModelSelect2Multiple(url="genome_diseasetrait_autocomplete"),
        }


class DiseaseHierarchyAdminForm(MPTTAdminForm):
    class Meta:
        model = DiseaseHierarchy
        fields = ('parent', 'disease', )
        widgets = {
            "parent": autocomplete.ModelSelect2(url="genome_disease_hierarchy_autocomplete"),
            "disease": autocomplete.ModelSelect2(url="genome_diseasetrait_autocomplete")
        }


class PathologyForm(forms.ModelForm):
    class Meta:
        model = Pathology
        fields = ('__all__')
        widgets = {
            "gene": autocomplete.ModelSelect2(url="genome_gene_autocomplete"),
        }


class DiseaseVariantForm(forms.ModelForm):
    class Meta:
        model = DiseaseVariant
        fields = ('__all__')
        widgets = {
            "gene": autocomplete.ModelSelect2(url="genome_gene_autocomplete"),
            "disease": autocomplete.ModelSelect2(url="genome_diseasetrait_autocomplete")
        }


class DiseaseSymptomInteractionForm(forms.ModelForm):
    class Meta:
        model = DiseaseSymptomInteraction
        fields = ('__all__')
        widgets = {
            "symptom": autocomplete.ModelSelect2(url="genome_disease_symptom"),
            "disease": autocomplete.ModelSelect2(url="genome_diseasetrait_autocomplete")
        }

