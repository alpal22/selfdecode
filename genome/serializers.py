from django.core.serializers.json import DjangoJSONEncoder
from django.utils.html import strip_tags
from django.utils.safestring import mark_safe


class PageObjSerializer(DjangoJSONEncoder):
    def default(self, page):
        if page.has_next():
            has_next = True
            next_page_number = page.next_page_number()
        else:
            next_page_number = page.paginator.num_pages
            has_next = False
        result = {
            'paginator': {
                'has_next': has_next,
                'next_page_number': next_page_number,
            },
            'object_list': []
        }
        if page:
            for snp in page.object_list:
                genotype_style = getattr(snp, 'genotype_style', '')
                genotype = getattr(snp, 'genotype', '')
                user_genotype = getattr(snp, 'user_genotype', '')
                disease = getattr(snp, 'disease', '').capitalize()

                result['object_list'].append({
                    'genes': [
                        {
                             "name": gene.name,
                             "slug": gene.slug,
                             'is_bad_gene': str(gene.id) in page.bad_genes,
                             'contains_risk_allele': str(gene.id) in page.contains_risk_allele,
                             'title': mark_safe(strip_tags(
                                 gene.description_simple or gene.function or ""
                             ))
                        } for gene in snp.genes.all()
                    ],
                    'rsid': snp.rsid,
                    'id': snp.id,
                    'minor_allele': snp.minor_allele,
                    'genotype_style': genotype_style or '',
                    'genotype': genotype or '',
                    'user_genotype': user_genotype or '',
                    'risk_alleles': snp.risk_alleles,
                    'disease': disease,
                })

        return result


class InteractionSerializer(DjangoJSONEncoder):
    def default(self, page):
        if page.has_next():
            has_next = True
            next_page_number = page.next_page_number()
        else:
            next_page_number = page.paginator.num_pages
            has_next = False
        result = {
            'paginator': {
                'has_next': has_next,
                'next_page_number': next_page_number,
            },
            'object_list': []
        }
        if page:
            for item in page.object_list:
                result['object_list'].append({
                    'disease': item.get('diseasetrait__name'),
                    'disease_slug': item.get('diseasetrait__slug'),
                    'inference_score': item.get('max_inference_score'),
                    'pub_med_ids': item.get('pubmed_ids', []),
                    'chemicals': item.get('chemicals', []),
                })
            return result


class GeneInteractsDiseaseSerializer(DjangoJSONEncoder):
    def default(self, page):
        if page.has_next():
            has_next = True
            next_page_number = page.next_page_number()
        else:
            next_page_number = page.paginator.num_pages
            has_next = False
        result = {
            'paginator': {
                'has_next': has_next,
                'next_page_number': next_page_number,
            },
            'object_list': []
        }
        if page:
            for action in page.object_list:
                result['object_list'].append({
                    "chemical": {
                        "name": action.interaction.chemical.display_name,
                        "slug": action.interaction.chemical.slug,
                    },
                    "action": {
                        "name": action.get_action_display(),
                        "type": action.interaction_type.name,
                        "description": action.interaction.interaction,
                    },
                    "organism": (
                        action.interaction.organism.english_name if action.interaction.organism.english_name else action.interaction.organism.latin_name
                    ) if action.interaction.organism else None,
                    "pids": action.interaction.pub_med_ids_as_list(),
                    "categories": list(action.interaction.chemical.categories.all().values("name")),
                })
            return result
        return super().default(page)


class ExperimentSerializer(DjangoJSONEncoder):
    def default(self, page):
        if page.has_next():
            has_next = True
            next_page_number = page.next_page_number()
        else:
            next_page_number = page.paginator.num_pages
            has_next = False
        result = {
            'paginator': {
                'has_next': has_next,
                'next_page_number': next_page_number,
            },
            'object_list': []
        }
        if page:
            for analytic in page.object_list:
                result['object_list'].append({
                    'organism': {
                        'english_name': analytic.configuration.identification.organism.english_name or '',
                        'latin_name': analytic.configuration.identification.organism.latin_name or '',
                        'name': analytic.configuration.identification.organism_name or '',


                    },
                    'log2fold': analytic.log2fold,
                    'gene': {
                        'ref': analytic.gene.name or '',
                        "name": analytic.gene_name
                    },
                    'configuration': {
                        'name': analytic.configuration.name,
                    },
                    'identification': {
                        'ref': analytic.configuration.identification.slug,
                        'factor_name': analytic.configuration.identification.factor_name,
                        'title': analytic.configuration.identification.title
                    }
                })
            return result
        return super().default(page)


class DiseaseAutocompleteSerializer(DjangoJSONEncoder):
    def default(self, query_set):
        result = {
            "suggestions": []
        }

        for item in query_set:
            result["suggestions"].append({
                "value": item.name.capitalize(),
                "data": item.pk,
            })

        return result


class SnpSubDiseasesSerializer(DjangoJSONEncoder):
    def default(self, page):
        if page.has_next():
            has_next = True
            next_page_number = page.next_page_number()
        else:
            next_page_number = page.paginator.num_pages
            has_next = False
        result = {
            'paginator': {
                'has_next': has_next,
                'next_page_number': next_page_number,
            },
            'object_list': []
        }
        if page:
            for snp in page.object_list:
                genotype_style = getattr(snp[0], 'genotype_style', '')
                genotype = getattr(snp[0], 'genotype', '')
                user_genotype = getattr(snp[0], 'user_genotype', '')
                disease = getattr(snp[0], 'disease', '').capitalize()

                result['object_list'].append({
                    'genes': [
                        {'name': gene.name,
                         'slug': gene.slug,
                         'is_bad_gene': str(gene.id) in page.bad_genes,
                         'contains_risk_allele': str(gene.id) in page.contains_risk_allele,
                         'title': mark_safe(strip_tags(
                             gene.description_simple or gene.function or ""
                         ))
                         } for gene in snp[0].genes.all()],
                    'rsid': snp[0].rsid,
                    'minor_allele': snp[0].minor_allele,
                    'genotype_style': genotype_style or '',
                    'genotype': genotype or '',
                    'user_genotype': user_genotype or '',
                    'risk_alleles': snp[0].risk_alleles,
                    'disease': disease
                })

        return result
