from haystack import indexes

from forum.models import Post, Tag
from genome.models import Gene, Snp, DiseaseTrait, GeneOntology
from chemical.models import Chemical, HealthEffect, Organism
from experiment.models import Identification


class GeneIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(model_attr='name', document=True)
    slug = indexes.CharField(model_attr='slug')

    def get_model(self):
        return Gene


class SnpIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(model_attr='rsid', document=True)

    def get_model(self):
        return Snp


class DiseaseTraitIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(model_attr='name', document=True)
    category = indexes.CharField(model_attr='category')
    slug = indexes.CharField(model_attr='slug')

    def get_model(self):
        return DiseaseTrait


class ChemicalIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(model_attr='name', document=True)
    slug = indexes.CharField(model_attr='slug')

    def get_model(self):
        return Chemical


class OrganismIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(use_template=True, document=True)
    slug = indexes.CharField(model_attr='slug')
    latin_name = indexes.CharField(model_attr='latin_name')
    english_name = indexes.CharField(model_attr='english_name')
    scientific_name_full = indexes.CharField(model_attr='scientific_name_full')
    scientific_name = indexes.CharField(model_attr='scientific_name')
    common_name = indexes.CharField(model_attr='common_name')
    chinese_name = indexes.CharField(model_attr='chinese_name')

    def get_model(self):
        return Organism


class HealthEffectIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(model_attr='name', document=True)
    slug = indexes.CharField(model_attr='slug')

    def get_model(self):
        return HealthEffect


class IdentificationIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(model_attr='name', document=True)
    slug = indexes.CharField(model_attr='slug')

    def get_model(self):
        return Identification


class GeneOntologyIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(model_attr='go_name', document=True)
    type = indexes.CharField(model_attr='type')
    slug = indexes.CharField(model_attr='go_id')

    def get_model(self):
        return GeneOntology


class PostIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(model_attr='title', document=True)
    slug = indexes.CharField(model_attr='slug')
    body = indexes.CharField(model_attr='body')

    def get_model(self):
        return Post


class ForumTagIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(model_attr='name', document=True)
    slug = indexes.CharField(model_attr='slug')

    def get_model(self):
        return Tag


class GeneFullTextIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(model_attr='name', document=True)
    slug = indexes.CharField(model_attr='slug')
    description = indexes.CharField(model_attr='description_advanced', boost=0.8)
    synonyms = indexes.MultiValueField(boost=0.9)

    def get_model(self):
        return Gene

    def prepare_synonyms(self, obj):
        return [synonym.name for synonym in obj.synonyms.all()]


class SnpFullTextIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, model_attr='rsid')
    description = indexes.CharField(model_attr='description_advanced', default='')
    genes = indexes.MultiValueField()

    def get_model(self):
        return Snp

    def prepare_genes(self, obj):
        return [gene.name for gene in obj.genes.all()]


class DiseaseTraitFullTextIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, model_attr='name')
    category = indexes.CharField(model_attr='category')
    slug = indexes.CharField(model_attr='slug')
    description = indexes.CharField(model_attr='definition', default='', boost=0.8)

    def get_model(self):
        return DiseaseTrait


class ChemicalFullTextIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, model_attr='name')
    slug = indexes.CharField(model_attr='slug')
    description = indexes.CharField(model_attr='definition', default='', boost=0.8)
    synonyms = indexes.CharField(model_attr='synonyms', default='', boost=0.9)

    def get_model(self):
        return Chemical


class HealthEffectFullTextIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, model_attr='name')
    slug = indexes.CharField(model_attr='slug')
    description = indexes.CharField(model_attr='description', default='', boost=0.8)

    def get_model(self):
        return HealthEffect


class IdentificationFullTextIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, model_attr='name')
    title = indexes.CharField(model_attr='title', default='')
    slug = indexes.CharField(model_attr='slug')

    def get_model(self):
        return Identification
