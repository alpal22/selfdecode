from django import template
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.contrib.admin.models import LogEntry, CHANGE
from django.db.models import Max
from django.template.loader import render_to_string

register = template.Library()


@register.simple_tag(takes_context=True)
def usage_history(context):
    ids = LogEntry.objects.filter(action_flag=CHANGE, content_type__model__in=(
        'snp', 'gene', 'diseasetrait', 'chemical', 'genepack', 'post'
    )).values(
        "object_id"
    ).annotate(
        max_id=Max('id')
    ).order_by('-max_id').values_list('max_id', flat=True)[:40]

    query_set = LogEntry.objects.filter(id__in=list(ids)).order_by('-action_time')

    entities = []

    for obj in query_set:
        try:
            inst = obj.get_edited_object()
            # Magic... set name field from one of this attrs...
            fields = ("rsid", "name", "title",)
            for field in fields:
                if hasattr(inst, field):
                    setattr(inst, "name", getattr(inst, field))

            setattr(obj, "edited_obj", inst)
        except:
            setattr(obj, "edited_obj", None)

        entities.append(obj)

    result = render_to_string("genome/_usage_history.html", {
        'query_set': entities,
        'user': context.request.user
    })

    return result
