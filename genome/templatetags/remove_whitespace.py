from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()



@register.filter(name='remove_whitespace')
def remove_whitespace(string):
    return string


@register.filter
@stringfilter
def remove_no_genes(string):
    if string.startswith('~'):
        return False
    return True