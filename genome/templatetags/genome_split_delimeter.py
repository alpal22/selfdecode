from django import template
register = template.Library()


@register.simple_tag()
def split_string(target, character='|'):
    result = target.split(character)
    output = {
        'gene_name': result[0],
        'gene_slug': result[1],
        'gene_score': result[3],
    }
    # concatenate gene id
    if result[2]:
        output.update({'gene_id': int(result[2])})

    return output
