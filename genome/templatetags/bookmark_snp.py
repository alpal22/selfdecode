from django import template

register = template.Library()


@register.inclusion_tag('genome/partials/bookmark_snp_button.html', takes_context=True)
def bookmark_snp_button(context, snp_id, snp_rsid):
    return {
        'snp_id': snp_id,
        'snp_rsid': snp_rsid,
        'user': context['user'],
        'request': context['request'],
    }