from django import template
register = template.Library()



@register.filter(name='remove_dunder')
def remove_dunder(string):
    try:
        string = string[string.index("__")+2:]
    except: 
        string = string
    return string
