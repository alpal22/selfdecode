from django import template
register = template.Library()


@register.filter
def replace_underscores(target):
    return target.replace('_', ' ')
