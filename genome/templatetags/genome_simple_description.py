from django import template
from django.utils.html import strip_tags
from django.utils.safestring import mark_safe

from genome.models import Gene

register = template.Library()


@register.simple_tag(takes_context=True)
def simple_description_by_gene(context, gene):  # gene may be gene instance or id
    genes = context.get("genes", {})

    if type(gene) == str:
        gene_item = genes.get(gene)
    else:
        gene_item = gene

    if not gene_item:
        return ""

    if type(gene) == dict:
        description_simple = gene_item.get('gene__description_simple')
        function = gene_item.get('gene__function')
    else:
        description_simple = gene_item.description_simple
        function = gene_item.function

    return mark_safe(strip_tags(description_simple or function or ""))
