from django import template
from django.core.urlresolvers import reverse

register = template.Library()


@register.simple_tag(name='resolve_url')
def resolve_url(type, object):
    if not object:
        return ""
    if type == 'gene':
        return reverse('gene', args=[object.name])
    elif type == 'chemical':
        return reverse('chemical:chemical', args=[object.slug])
    elif type == 'diseasetrait' and object.category.name == "Trait":
        return reverse('trait', args=[object.slug])
    elif type == 'diseasetrait' and object.category.name == "Measurement":
        return reverse('measurement', args=[object.slug])
    elif type == 'diseasetrait' and object.category.name == "Disease":
        return reverse('disease', args=[object.slug])
    elif type == 'snp':
        return reverse('snp', args=[object.rsid])
    elif type == 'genepack':
        return reverse('list_gene_pack', args=[object.pk])
    elif type == 'post':
        return reverse('forum:post', args=[object.slug])
    elif type == 'identification':
        return reverse('experiment:details', args=[object.slug])
    elif type == 'healtheffect':
        return reverse('chemical:health-effect', args=[object.slug])
    else:
        return ''


@register.simple_tag(takes_context=True)
def contains_url(context, *args):
    request = context.get("request")

    current_url = request.path

    for route in args:
        if current_url == reverse(route):
            return True

    return False
