import re

from django import template
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter
def percentages(value, base=1.0):
    if not value:
        return value
    try:
        value = float(value)
    except ValueError:
        return value
    base = float(base)
    value = (value * 100.0) / base
    return "%.2f%%" % value


@register.filter
def mul(value, by=1.0):
    try:
        value = float(value)
    except:
        return value
    by = float(by)
    return value * by


@register.filter(name="format")
def fmt(value, tpl):
    try:
        value = float(value)
    except:
        return value
    return tpl.format(value)


@register.filter
def get_page(paginator, page):
    return paginator.page(page)


re1 = re.compile(r"e([-+\d]+)")
re2 = re.compile(r"([-+]){1}[0]{1,}")


@register.filter(name="format_pvalue")
def format_pvalue(x):
    if not x:
        return x
    x = float(x)
    cropped = "%.2e" % x
    formatted = re1.sub("*10<sup>\g<1></sup>", cropped)
    cleaned = re2.sub("\g<1>", formatted)
    return mark_safe(cleaned)


@register.filter
def is_ambiguous_snp(snp):
    if isinstance(snp, dict):
        if snp.get("ambiguity_code") in ("W", "S",):
            return True
    elif snp.ambiguity_code in ("W", "S",):
        return True
    return False


@register.simple_tag
def disease_risk(genotype, risk_alleles):
    '''
    Compares user genotype and disease risk alleles and estimates the risk of disease
    '''
    grouped_alleles = set(risk_alleles)

    if len(grouped_alleles) == 1:
        allele = grouped_alleles.pop()
        if genotype.count(allele) == 2:
            return mark_safe('<i class="fa fa-exclamation-circle text-danger " aria-hidden="true" data-toggle="tooltip" title="Contains Risk Alleles"></i>')
        elif genotype.count(allele) == 1:
            return mark_safe('<i class="fa fa-exclamation-circle text-warning" aria-hidden="true"></i>')
        else:
            return mark_safe('<i class="fa fa-exclamation-circle text-success" aria-hidden="true"></i>')
    elif len(grouped_alleles) == 2:
        allele1 = grouped_alleles.pop()
        allele2 = grouped_alleles.pop()
        if genotype.count(allele1) or genotype.count(allele2):

            return mark_safe('<i class="fa fa-exclamation-circle text-warning" aria-hidden="true"></i>')
        else:
            return mark_safe('<i class="fa fa-exclamation-circle text-success" aria-hidden="true"></i>')
    else:
        return ''


@register.filter
def format_protein_name(name):
    rec_name_regex = re.compile('^[^(]*')  # get everything before (
    alternative_names_regex = re.compile('\((.*?)\)')  # get inside ( )

    rec_name = rec_name_regex.findall(name)[0]
    alt_names = list(filter(lambda x: 'EC ' not in x, alternative_names_regex.findall(name)))

    names = {
        'rec_name': rec_name.strip(),
        'alt_names': alt_names
    }

    if not alt_names:
        return names

    short_name = min(alt_names, key=len)
    if short_name == short_name.upper():
        names['short_name'] = short_name
        alt_names.remove(short_name)

    return names


@register.simple_tag(takes_context=True)
def build_request_string(context, pagination_page_num):
    request = context.get('request')

    params = request.GET.copy()
    if params.get('page'):
        del params['page']

    final_string = '?page={0}'.format(pagination_page_num)

    for param in params.keys():
        final_string += '&{0}={1}'.format(param, params.get(param))

    return final_string
