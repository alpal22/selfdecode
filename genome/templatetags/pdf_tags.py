from django import template
from django.core.urlresolvers import reverse

register = template.Library()


@register.simple_tag(takes_context=True)
def abs_link(context, reverse_name, *args):
    request = context.get("request")
    if not request:
        return reverse(reverse_name, args=args)
    return request.build_absolute_uri(reverse(reverse_name, args=args))
