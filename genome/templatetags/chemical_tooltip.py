from django import template
from django.utils.html import strip_tags
from django.utils.safestring import mark_safe

from chemical.models import Chemical, ChemicalT3DBData
from genome.models import Gene

register = template.Library()


@register.simple_tag
def chemical_tooltip(chemical_pk):
    chemical_item = Chemical.objects.filter(pk=chemical_pk).first()

    description = ''
    definition = ''

    try:
        definition = chemical_item.definition
        description = chemical_item.t3db_data.get(chemical_id=chemical_item.id).description
    except:
        pass

    if description:
        return mark_safe(strip_tags(description))

    return mark_safe(strip_tags(definition))

