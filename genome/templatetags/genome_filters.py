from django import template
from django.db.models import F
from django.db.models.aggregates import Count
from django.utils.safestring import mark_safe

from chemical.models import ChemicalGeneInteractionAction, SubstanceCategory
from genome.models import Gene, Snp
from genome.management.commands.uniprot_import import chunks
from django.forms.models import model_to_dict
register = template.Library()


@register.filter
def count_chars(text, ch=''):
    if not ch or not text:
        return 0
    return text.count(ch)


@register.filter
def gene_interactions(gene, action):
    interactions = ChemicalGeneInteractionAction.objects.filter(
        interaction__gene__name=gene, action=action
    ).exclude(
        interaction__chemical__categories__in=list(SubstanceCategory.objects.filter(
            slug="obscure_chemicals").first().get_family())).select_related(
        "interaction", "interaction__chemical", "interaction__chemical__slug",
        "interaction_type", "interaction__amount", "interaction__chemical__t3db_data__description",
        "interaction__chemical__definition"
    ).annotate(total=Count('action') + F('interaction__amount') - 1).order_by('-total').values(
        'interaction__gene__name', 'interaction__chemical__name', 'interaction__chemical__display_as',
        'interaction__chemical__slug', 'total', 'interaction__chemical__t3db_data__description',
        'interaction__chemical__definition'
        )

    result = list(interactions.all())
    used = []
    items = []
    for item in result:
        if item.get("interaction__chemical__name") in used:
            continue
        used.append(item.get("interaction__chemical__name"))
        items.append(item)
    return items


@register.filter
def to_list(iterator):
    return list(iterator)


@register.filter
def page_range(page):
    offset = 5
    start_page = page.number - offset
    end_page = page.number + offset
    if start_page <= 1:
        start_page = 1
    if end_page > page.paginator.num_pages:
        end_page = page.paginator.num_pages + 1
    return range(start_page, end_page)


@register.filter
def chunk_queryset(queryset, num_genes_on_page):
    return chunks(queryset, num_genes_on_page // 4)


@register.filter
def has_at_list_one_subchild(query_set, sub_set):
    for set in query_set.all():
        if model_to_dict(set)[sub_set]:
            return True
    return False


@register.filter
def can_make_conclusion(query_set):
    for set in query_set.all():
        if set.diseasestraits.exists() and set.risk_allele and set.ci_text:
            return True
    return False


@register.filter
def split(string, delimiter):
    return string.split(delimiter)


@register.filter
def uniq(lst):
    return list(set(lst))


@register.simple_tag
def resolve_name(obj):
    if obj.__class__.__name__ == 'Gene':
        return obj.name.upper()
    elif obj.__class__.__name__ == 'Chemical':
        return obj.name.capitalize()
    elif obj.__class__.__name__ == 'DiseaseTrait':
        return obj.name.capitalize()
    elif obj.__class__.__name__ == 'HealthEffect':
        return obj.name.capitalize()
    elif obj.__class__.__name__ == 'Snp':
        return obj.rsid
    elif obj.__class__.__name__ == 'Identification':
        return obj.name


@register.filter('get_value_from_dict')
def get_value_from_dict(dict_data, key):
    """
    usage example {{ your_dict|get_value_from_dict:your_key }}
    """
    if key:
        return float(dict_data.get(int(key)))
