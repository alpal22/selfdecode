from django import template


register = template.Library()


@register.filter(name='get_grade')
def get_grade(value):
    return value


@register.filter
def get_genotype(item):
    if item["genotype_style"] == "heterozygous":
        if item["description_hetero"] != "":
            item["score"] = item["score_hetero"]
            item["description"] = item["description_hetero"]
        item["color"] = item["heterozygous_color"]
    elif item["genotype_style"] == "homozygous_major":
        if item["description_major"] != "":
            item["score"] = item["score_major"]
            item["description"] = item["description_major"]
        item["color"] = item["homozygous_major_color"]
    elif item["genotype_style"] == "homozygous_minor":
        if item["description_minor"] != "":
            item["score"] = item["score_minor"]
            item["description"] = item["description_minor"]
        item["color"] = item["homozygous_minor_color"]
    elif item["genotype_style"] == "major":
        item["color"] = item["homozygous_major_color"]
    elif item["genotype_style"] == "minor":
        item["color"] = item["homozygous_minor_color"]

    item["blue_grade_label"] = (item["blue_grade_label"] or "").format(name=item["category"].lower())
    item["orange_grade_label"] = (item["orange_grade_label"] or "").format(name=item["category"].lower())
    item["red_grade_label"] = (item["red_grade_label"] or "").format(name=item["category"].lower())

    return item

