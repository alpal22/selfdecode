from django import template
from django.conf import settings

register = template.Library()


@register.filter
def is_demouser(user):
    if user.email == settings.DEMO_USER_EMAIL:
        return True
    return False

