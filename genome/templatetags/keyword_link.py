from django import template
register = template.Library()
import re
from genome.models import KeywordLink
from functools import reduce


dict_key = {}

for item in KeywordLink.objects.all().values_list():
    for item2 in item[1].split(','):
        if item2 != "":
            dict_key[re.sub('[?+]', '',item2.strip())] = item[2]


@register.filter(name='keyword_link')
def keyword_link(value):    


    def linker(x):
        return '<a target="_blank" href="' + dict_key[x.group()] + '"/>' + str(x.group()) + '</a>'
    

    pattern = re.compile(r'(?<=[ \b\>])(' + '|'.join(dict_key.keys()) + r')(?=[<\. ])')
    result = pattern.sub(linker, value)
    return result
