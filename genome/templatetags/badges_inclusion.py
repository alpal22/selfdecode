from django import template

register = template.Library()


@register.inclusion_tag('exclamation_badges.html')
def show_badges(gene, bad_genes, contains_risk_allele):

    gene_score = None
    gene_id = gene
    if not isinstance(gene, (str, int,)):
        gene_score = getattr(gene, "gene_score", 0)
        gene_id = str(gene.id)
    else:
        gene_id = str(gene_id)

    return {
        'gene_id': gene_id,
        'gene_score': gene_score,
        'bad_genes': bad_genes,
        'contains_risk_allele': contains_risk_allele
    }
