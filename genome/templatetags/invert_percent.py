from django import template
register = template.Library()



@register.filter(name='invert_percent')
def invert_percent(num):
    return 1 - float(num)
