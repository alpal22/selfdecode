import re
from itertools import groupby
from genome.models import Gene
from django import template
from django.utils.html import strip_tags
from django.utils.safestring import mark_safe, mark_for_escaping

from decodify.aggregators import JsonAgg
from genome.models import Snp, PopulationGenome
from genome.templatetags.genome_filters import count_chars

register = template.Library()


@register.simple_tag
def combined_risk_alleles(studies, genotype):
    alleles = groupby(sorted(studies, key=lambda v: str(v['f1'])), key=lambda v: v['f1'])
    genotype_alleles = list(genotype)
    result = ""
    for allele, study_list in alleles:
        if allele in genotype_alleles:
            study_list = list(study_list)
            for study in study_list:
                result += allele if allele else "?"
                result += "="
                if study.get("f3"):
                    result += "{f3}".format(f3=study.get('f3').capitalize())
                else:
                    result += "?"
                result += "<br>"
    if result:
        info_item = """&nbsp;<span class="fa fa-info-circle"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    data-html="true"
                                    title="{title}">
                             </span>""".format(title=result)
    else:
        info_item = ''

    return mark_safe(info_item)


@register.simple_tag
def sort_genotype(value, minor_allele):
    if not value or not minor_allele or minor_allele not in value:
        return value
    if value.count(minor_allele) == 2:
        return value
    alleles = list("%s%s" % (value, minor_allele))
    return "".join(reversed(sorted(set(alleles), key=alleles.count)))


@register.simple_tag
def genotype_badge(genotype, minor_allele, value):
    cnt = count_chars(genotype, minor_allele)
    if cnt == 0:
        return mark_safe('<span class="badge badge-success">%s</span>' % value) if value != '--' else ''
    elif cnt == 1:
        value = sort_genotype(value, minor_allele)
        return mark_safe('<span class="badge badge-warning">%s</span>' % value) if value != '--' else ''
    elif cnt >= 2:
        return mark_safe('<span class="badge badge-danger">%s</span>' % value) if value != '--' else ''


def prepare_pack_score(item):
    score = 0
    if item.user_genotype_style == "heterozygous":
        # if item.description_hetero != "":
        score = item.score_hetero
    elif item.user_genotype_style == "homozygous_major":
        # if item.description_major != "":
        score = item.score_major
    elif item.user_genotype_style == "homozygous_minor":
        # if item.description_minor != "":
        score = item.score_minor
    return score


def count_gene_pack_score(data, categories_count):
    not_empty = 0
    score = 0.0
    for item in data:
        if prepare_pack_score(item):
            not_empty += 1
        score += prepare_pack_score(item)
    return score/categories_count


@register.simple_tag
def gene_pack_score(data, categories_count):
    score = count_gene_pack_score(data, categories_count)
    if score > 2:
        response = "<span style='font-size:1.1em;margin-left:4px;' class='badge badge-success'>  A +</span>"
    elif score > 1.5:
        response = "<span style='font-size:1.1em;margin-left:4px;' class='badge badge-success'>  A -</span>"
    elif score > 1:
        response = "<span style='font-size:1.1em;margin-left:4px;' class='badge badge-success'>  B +</span>"
    elif score > 0.5:
        response = "<span style='font-size:1.1em;margin-left:4px;' class='badge badge-success'>  B -</span>"
    elif score > 0:
        response = "<span style='font-size:1.1em;margin-left:4px;' class='badge badge-danger'>  C +</span>"
    elif score > -0.5:
        response = "<span style='font-size:1.1em;margin-left:4px;' class='badge badge-danger'>  C -</span>"
    elif score > -1:
        response = "<span style='font-size:1.1em;margin-left:4px;' class='badge badge-danger'>  D +</span>"
    elif score > -4:
        response = "<span style='font-size:1.1em;margin-left:4px;' class='badge badge-danger'>  D -</span>"
    else:
        response = "<span style='font-size:1.1em;margin-left:4px;' class='badge badge-danger'>  F</span>"
    return mark_safe(response)


@register.simple_tag
def genotype_frequency(genotype, snp_id, raw_frequency=False):
    population_genotype = PopulationGenome.objects.filter(
        snp__pk=snp_id, abbr__iexact="ALL"
    ).first()
    if not population_genotype or len(genotype) < 2:
        return ""
    alleles = population_genotype.group2.split(";")
    v1 = "|".join(sorted(genotype))
    v2 = "|".join(reversed(sorted(genotype)))
    alleles = list(filter(lambda v: v1 in v or v2 in v, alleles)) + [":"]
    frequency = "".join(alleles).split(":")[1]
    if not frequency:
        return ""
    if raw_frequency:
        return float(frequency)
    return "%.0f%%" % (float(frequency) * 100)


@register.simple_tag
def genotype_with_frequency(genotype, snp_id, minor_allele, value):
    _genotype = genotype_badge(genotype, minor_allele, value)
    _frequency = genotype_frequency(genotype, int(snp_id))

    if _frequency:
        return mark_safe("%s = %s" % (_genotype, _frequency,))
    else:
        return mark_safe(_genotype)


@register.simple_tag
def genotype_with_frequency_snp_search(rsid, user):
    if not user.is_authenticated():
        return ""
    snp = Snp.objects.filter(
        rsid=rsid
    ).prefetch_related(
        "studies", "genes"
    ).extra(
        select={
            "user_genotype": "genome_userrsid.genotype",
            "user_genotype_style": "genome_userrsid.genotype_style",
            "gene_name": "genome_gene.name",
        },
        tables=["genome_userrsid"],
        where=[
            "genome_snp.rsid = genome_userrsid.rsid",
            "genome_userrsid.file_id = %s" % user.user_profile.active_file_id,
        ]
    ).annotate(
        snpstudies=JsonAgg(
            "studies__risk_allele", "studies__risk_allele_frequency",
            "studies__diseasestraits__name", "studies__p_value",
            distinct=True
        ),
    ).order_by("genes__name").first()
    if not snp:
        return ""
    genotype_and_frequency = genotype_with_frequency(snp.user_genotype, snp.pk, snp.minor_allele, snp.user_genotype)
    tooltips = combined_risk_alleles(snp.snpstudies, snp.user_genotype)
    return genotype_and_frequency + tooltips


@register.simple_tag
def risk_disease_tooltip(genotype, risk_allele, disease_name):
    cnt = count_chars(genotype, risk_allele)
    cls = ""
    if cnt == 0:
        cls = " text-success"
    elif cnt == 1:
        cls = " text-warning"
    elif cnt >= 2:
        cls = " text-danger"
    tpl = '<span class="fa fa-info-circle{cls}" data-toggle="tooltip" data-placement="top" title="{disease}"></span>'
    return mark_safe(tpl.format(cls=cls, disease=disease_name))


@register.simple_tag(takes_context=True)
def allele_flag(context, snp, user_genotype=None):

    if not isinstance(snp, dict):
        snp = snp.__dict__

    user = context['user']
    red_flag = (isinstance(snp, dict) and snp.get('red_flag', False)) or (isinstance(snp, Snp) and snp.red_flag)
    studies = snp.get("snpstudies", [])
    risk_alleles = [study["f1"] for study in studies]
    counts = {allele: risk_alleles.count(allele) for allele in risk_alleles}
    common_allele_count = counts.get(snp.get("minor_allele"), 0)
    total_allele_count = sum(counts.values())
    show_red_flag = (common_allele_count / total_allele_count) > 0.5
    minor_allele = snp.get("minor_allele")

    user_genotype = user_genotype if user_genotype else snp.get('user_genotype', None)

    if not user_genotype:
        auto_flag = False
    else:
        auto_flag = show_red_flag and user_genotype.count(minor_allele or "") == 2

    if red_flag and auto_flag:
        title = "Auto and manually flagged"
    elif auto_flag:
        title = "Auto flagged"
    elif red_flag:
        title = "Manually flagged"

    if not user.is_staff:
        title = ""

    if red_flag or auto_flag:
        tpl = '<span class="fa fa-flag text-danger flag-icon-centered" data-toggle="tooltip" data-placement="top" title="%s"></span>' % title
        return mark_safe(tpl)
    return ""


@register.simple_tag
def gene_chemical_highchart_categories(top_interactions, gene=None, chemical=None):
    result = []
    for interaction in top_interactions:
        icon = ''

        action = interaction.get("action", [])
        exception_status = gene.exception_status if gene else interaction.get('gene__exception_status')

        slug = interaction.get('chemical__slug', interaction.get('gene__slug', ''))
        name = (interaction.get('chemical__display_as') if interaction.get('chemical__display_as') is not None else interaction.get('chemical__name', interaction.get('gene__name', ''))).upper()
        description_simple = interaction.get('gene__description_simple', '').replace('\r\n', '<br>').replace('\'', '')
        description_simple = mark_safe(strip_tags(description_simple))

        function = interaction.get('gene__function', '')
        if isinstance(function, type(None)):
            function = ''
        function = function.replace('\r\n', '<br>').replace('\'', '')
        function = mark_safe(strip_tags(function))

        url = "/chemical/{slug}" if gene else "/gene/{slug}"
        url = url.format(slug=slug)

        if 'increases' in action and 'decreases' in action:
            icon = '<span class="fa fa-arrows-v"></span>'
        elif 'increases' in action:
            if exception_status == "increased":
                color = "text-info"
            elif exception_status == "decreased":
                color = "text-danger"
            else:
                color = ''
            icon = '<span class="fa fa-long-arrow-up %s"></span>' % color
        elif 'decreases' in action:
            if exception_status == "decreased":
                color = "text-info"
            elif exception_status == "increased":
                color = "text-danger"
            else:
                color = ''
            icon = '<span class="fa fa-long-arrow-down %s"></span>' % color
        result.append(
            """'<a class="x-axis-label hashtip" href="{url}" data-palcement="top" data-toggle="tooltip" title="{title}">{name}</a>&nbsp;{icon}'""".format(
                url=url,
                name=name.replace("\'", ""),
                icon=icon,
                title=description_simple or function
            )
        )
    return mark_safe(",".join(result))


def _color_index(item):
    if not item.genotype:
        return -1
    return count_chars(item.genotype, item.minor_allele)


@register.filter
def sort_snps(items):
    """Sort snps by color on genes to look at page"""
    return list(sorted(items, key=_color_index, reverse=True))


@register.simple_tag
def get_summary(query_set):
    fields = [
        "definition", "ctd_definition", "medlineplus_summary", "nih_rare_diseases_summary",
        "ghr_summary", "malacards_summary", "wikipedia_summary",
    ]

    for field in fields:
        summary = getattr(query_set, field, None)
        if summary:
            return mark_safe(summary)


regex = re.compile(r'\[.*\]')


@register.simple_tag
def get_beta_value(study):
    """
    Return Beta value
    :type study: SnpStudy
    :rtype: unicode
    """
    return regex.sub(str(study.odds_ratio), study.ci_text)


@register.simple_tag
def risk_allele_concat(bad_allele, gwas, disease_variant):
    gwas_alleles = [record.risk_allele for record in gwas]
    if bad_allele:
        gwas_alleles += [bad_allele]
    if disease_variant:
        gwas_alleles += [disease_variant]
    gwas_alleles = list(filter(lambda v: v, gwas_alleles))
    return ", ".join(gwas_alleles)


@register.simple_tag
def diseasetrait_score(score, inf_score):
    if not score and inf_score:
        return inf_score
    elif not inf_score and score:
        return score
    elif inf_score and score:
        return inf_score * score

    return ''


@register.simple_tag
def get_gene_score(items):
    """
    Calculate gene score using new algorithm
    :type items: list<Snp>
    :rtype: float
    """
    sum = 0.0
    counter = 0
    for item in items:
        if item.genotype:
            snp = Snp.objects.filter(rsid=item.rsid).values("pk").first()
            freq = genotype_frequency(item.genotype, snp, raw_frequency=True)
            if freq:
                sum += (1 / freq) * item.importance
                counter += 1
    return round(sum / (counter * 2.5), 2)
