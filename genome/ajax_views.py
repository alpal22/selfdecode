from itertools import chain

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Count
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.template import Context
from django.template.loader import get_template, render_to_string
from django.views.generic import ListView

from decodify.aggregators import JsonAgg
from genome.helpers import get_gene_scores, get_potentially_problematic_genes, badges_for_gene
from genome.models import DiseaseGOAssociations, DiseaseTrait, Gene, DiseaseSymptomInteraction, \
    GenePack, Pathway, GeneDiseasePathwayInteraction, DiseaseHierarchy
from haystack.query import SearchQuerySet
from chemical.models import SubstanceCategory, Chemical
from genome.raw_sql_queries import _gene_chemical_interactions


def disease_go_ajax(request, slug):
    query_set = get_object_or_404(DiseaseTrait, slug=slug)
    template = get_template('genome/partials/table_bodies/disease_gene_ontology.html')

    interactions = DiseaseGOAssociations.objects.filter(disease=query_set, gene_ontology__type=request.GET.get("go_type"))

    page = request.GET.get("page")
    paginator = Paginator(interactions, 10)
    try:
        current_page = paginator.page(page)
    except PageNotAnInteger:
        current_page = paginator.page(1)
    except EmptyPage:
        current_page = paginator.page(paginator.num_pages)

    interactions = current_page.object_list

    if current_page.has_next():
        has_next = True
        next_page_number = current_page.next_page_number()
    else:
        has_next = False
        next_page_number = current_page.paginator.num_pages
    context = Context({
        "interactions": interactions,
        "link": request.GET.get("go_type")
    })

    return JsonResponse({
        'paginator': {
            'has_next': has_next,
            'next_page_number': next_page_number,
        },
        'data': template.render(context)
    })


def disease_pathways_ajax(request, slug):
    query_set = get_object_or_404(DiseaseTrait.objects.prefetch_related(
        "studies", "studies__snp"
    ), slug=slug)
    disease_pathway_interactions = GeneDiseasePathwayInteraction.objects.filter(
        disease=query_set,
        gene__isnull=False
    ).values(
        "gene__name", "gene__slug", "gene__id"
    ).annotate(
        pathways=JsonAgg("pathway__pathway_name", "pathway__pathway_id"),
        gene_agg=Count("gene__name")
    )
    page = request.GET.get("page")
    paginator = Paginator(disease_pathway_interactions, 20)
    try:
        current_page = paginator.page(page)
    except PageNotAnInteger:
        current_page = paginator.page(2)
    except EmptyPage:
        current_page = paginator.page(paginator.num_pages)

    interactions = current_page.object_list
    bad_genes = []
    contains_risk_allele = []
    gene_scores = []
    all_interact_genes = {interaction['gene__id'] for interaction in interactions}
    if request.user.is_authenticated():
        contains_risk_allele, bad_genes = badges_for_gene(request.user, all_interact_genes)

    template = get_template('genome/partials/table_bodies/disease_pathway_interaction.html')
    if current_page.has_next():
        has_next = True
        next_page_number = current_page.next_page_number()
    else:
        has_next = False
        next_page_number = current_page.paginator.num_pages
    context = Context({
        "interactions": interactions,
        "bad_genes": bad_genes,
        "contains_risk_allele": contains_risk_allele,
        "gene_scores": gene_scores,
    })

    return JsonResponse({
        'paginator': {
            'has_next': has_next,
            'next_page_number': next_page_number,
        },
        'data': template.render(context),
    })


def gene_ajax(request, gene_pk, interaction_type):
    query_set = get_object_or_404(Gene, pk=gene_pk)
    interactions = []
    display_action = ""

    extra = {
        "join": '',
        "where": ''
    }

    if request.GET.get("category"):
        cat_root = SubstanceCategory.objects.filter(pk=request.GET.get("category")).first().get_family().values_list("pk")
        cat_list = list(chain.from_iterable(cat_root))
        extra = {
            "join": 'INNER JOIN "chemical_chemical_categories" ON ("chemical_chemical_categories"."chemical_id" = "chemical_chemical"."id")',
            "where": 'AND "chemical_chemical_categories".substancecategory_id in (%s)' % ", ".join([str(record) for record in cat_list])
        }

    if interaction_type == "increase":
        interactions = _gene_chemical_interactions(request, query_set, "increases", extra)
        display_action = "Increases"
    elif interaction_type == "decrease":
        interactions = _gene_chemical_interactions(request, query_set, "decreases", extra)
        display_action = "Decreases"
    page = request.GET.get("page")

    paginator = Paginator(interactions, 10)
    try:
        current_page = paginator.page(page)
    except PageNotAnInteger:
        current_page = paginator.page(2)
    except EmptyPage:
        current_page = paginator.page(paginator.num_pages)
    # print(current_page)
    template = get_template('genome/partials/table_bodies/gene_chemical.html')
    if current_page.has_next():
        has_next = True
        next_page_number = current_page.next_page_number()
    else:
        has_next = False
        next_page_number = current_page.paginator.num_pages
    context = Context({
        "interactions": current_page.object_list,
        "display_action": display_action
    })
    return JsonResponse({
        'paginator': {
            'has_next': has_next,
            'next_page_number': next_page_number,
        },
        'data': template.render(context)
    })


def disease_in_hierarchy(request, slug):
    query_set = get_object_or_404(DiseaseTrait.objects.prefetch_related(
        "studies", "studies__snp"
    ), slug=slug)
    hierarchy = DiseaseHierarchy.objects.filter(disease=query_set).first()
    structure = {}
    if hierarchy:
        structure["current"] = hierarchy
        structure["root"] = hierarchy.get_root()
        structure["parents"] = hierarchy.get_ancestors()
    template = get_template("genome/partials/disease_in_hierarchy.html")
    context = Context({
        "disease_in_hierarchy": structure,
    })
    return JsonResponse({
        'data': template.render(context)
    })


class AjaxMixin(object):

    paginate_by = 10

    def dispatch(self, request, *args, **kwargs):
        return super(AjaxMixin, self).dispatch(request, *args, **kwargs)

    def get_context_data(self):
        paginator = Paginator(self.query_set, self.paginate_by)

        page_number = int(self.request.GET.get('page', '1'))

        try:
            results = paginator.page(page_number)
        except EmptyPage:
            results = paginator.page(paginator.num_pages)

        context = {
            'has_next': results.has_next(),
            'request_data': list(results.object_list),
        }

        return context

    def get(self, request, *args, **kwargs):

        self.query_set = self.get_queryset()

        response = self.get_context_data()
        return JsonResponse(response, safe=False)


class GenePacksAjaxListView(AjaxMixin, ListView):
    paginate_by = 8
    template_name = "genome/partials/gene_pack_list.html"

    def get_context_data(self):
        context = super(GenePacksAjaxListView, self).get_context_data()

        result = render_to_string(self.template_name, context, request=self.request)
        context.pop('request_data')
        context.update({'rendered_data': result})

        return context

    def get_queryset(self):
        query = GenePack.objects.annotate(total_likes=Count('likes')) \
            .exclude(pack_type='sponsored')\
            .order_by('-total_likes')

        condition = self.request.GET.get('condition')
        if condition == 'gt':
            query = query.filter(total_likes__gt=0)
        elif condition == 'eq':
            query = query.filter(total_likes=0)
        elif condition == 'my':
            query = self.request.user.liked_gene_packs.get_queryset()

        return query


# Symptoms on diseasetrait page
class DiseaseSymptomAjaxListView(AjaxMixin, ListView):
    paginate_by = 10
    template_name = "genome/partials/disease_symptom_list.html"

    def get_context_data(self):
        context = super(DiseaseSymptomAjaxListView, self).get_context_data()

        result = render_to_string(self.template_name, context, request=self.request)
        context.pop('request_data')
        context.update({'rendered_data': result})

        return context

    def get_queryset(self):
        slug = self.kwargs.get('slug')

        query_set = DiseaseSymptomInteraction.objects.filter(
            disease__slug__iexact=slug).values('symptom__name', 'symptom__slug')

        return query_set


# Diseases on symptom page
class SymptomDiseasesAjaxListView(AjaxMixin, ListView):
    paginate_by = 10
    template_name = "genome/partials/symptom_diseases_list.html"

    def get_context_data(self):
        context = super(SymptomDiseasesAjaxListView, self).get_context_data()

        result = render_to_string(self.template_name, context, request=self.request)
        context.pop('request_data')
        context.update({'rendered_data': result})

        return context

    def get_queryset(self):
        slug = self.kwargs.get('slug')

        query_set = DiseaseSymptomInteraction.objects.filter(
            symptom__slug__iexact=slug).values('disease__name', 'disease__slug').distinct()

        return query_set


def pathway_chemical_ajax(request, pathway_id):
    query_set = get_object_or_404(Pathway, pathway_id=pathway_id)
    chemical_list = query_set.chemicalpathway_set.all()
    template = get_template('genome/partials/table_bodies/pathway_chemical.html')

    page = request.GET.get("page")
    paginator = Paginator(chemical_list, 20)
    try:
        current_page = paginator.page(page)
    except PageNotAnInteger:
        current_page = paginator.page(1)
    except EmptyPage:
        current_page = paginator.page(paginator.num_pages)

    chemical_list = current_page.object_list

    if current_page.has_next():
        has_next = True
        next_page_number = current_page.next_page_number()
    else:
        has_next = False
        next_page_number = current_page.paginator.num_pages
    context = Context({
        "chemical_list": chemical_list,
    })

    return JsonResponse({
        'paginator': {
            'has_next': has_next,
            'next_page_number': next_page_number,
        },
        'data': template.render(context)
    })


def report_autocomplete(request):
    query = request.GET.get('query', '')
    search_object = request.GET.get('search_object', '')

    max_results = 10

    index = SearchQuerySet().all().using('default')

    if search_object == 'gene':
        queryset = index.models(Gene).filter(text__contains=query)
    elif search_object == 'disease':
        queryset = index.models(DiseaseTrait).filter(
            text__contains=query,
            category="Disease"
        )
    elif search_object == 'chemical':
        queryset = index.models(Chemical).filter(text__contains=query)
    else:
        queryset = SearchQuerySet().none()

    results = [item.text for item in queryset[:max_results]]
    data = {
        'query': query,
        "suggestions": results
    }
    return JsonResponse(data, safe=False)
