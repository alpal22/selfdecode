from __future__ import absolute_import
import django
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'decodify.settings')
django.setup()

import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'decodify.settings')

# set the default Django settings module for the 'celery' program.

from django.conf import settings  # noqa

app = Celery('decodify')

S3_USE_SIGV4 = 'True'
# AWS_ACCESS_KEY_ID = 'AKIAIM4ZUBWXUKSKBJUA'
# AWS_SECRET_ACCESS_KEY = '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'

AWS_ACCESS_KEY_ID = settings.AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY = settings.AWS_SECRET_ACCESS_KEY

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')

app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.update(BROKER_URL=settings.CELERY_BROKER_URL, CELERYD_TASK_SOFT_TIME_LIMIT=7200)  # For localhost

app.conf.CELERY_TIMEZONE = 'UTC'
