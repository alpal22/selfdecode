from django.apps import AppConfig


class GenomeConfig(AppConfig):
    name = 'genome'

    def ready(self):
        import genome.signals
