from django.conf import settings


def intercom(request):
    return {'INTERCOM_APP_ID': settings.INTERCOM_APP_ID}
