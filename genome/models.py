import stripe
from django.conf import settings
from django.contrib.admin.models import LogEntry
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.contrib.auth.models import User
from django.db.models import Count
from django.db.models import F
from django.db.models import Q
from django.utils import timezone
from allauth.account.signals import user_signed_up
from django.dispatch import receiver
from ckeditor.fields import RichTextField
from django.contrib.auth.signals import user_logged_in
from datetime import timedelta, datetime
from django.core.urlresolvers import reverse

from django_extensions.db.fields import AutoSlugField
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel

from affiliate.models import AffiliateProfile
from genome.mixins import ModelDiffMixin
from genericm2m.models import RelatedObjectsDescriptor
from social.models import AccessRequest
from payment.constants import PROVIDER_STRIPE
from payment.models import Transaction, Subscription, Plan


class Fix(models.Model):
    name = models.CharField(max_length=200, null=True, blank=True)
    description = RichTextField(default="",null=True, blank=True)
    image_name = models.CharField(max_length=200, null=True, blank=True)


class TranscriptionFactor(models.Model):
    name = models.CharField(max_length=200, null=False, blank=False, default="")
    slug = models.SlugField(max_length=200, null=False, blank=False, default="")
    ref_url = models.CharField(max_length=255, null=True, blank=True)
    fixes = models.ManyToManyField(Fix, related_name="fixes_transcription_factor", blank=True)

    def __str__(self):
        return str(self.name)


class Synonym(models.Model):
    name = models.CharField(max_length=500)
    slug = AutoSlugField(populate_from="name", unique=True)

    def __str__(self):
        return self.name

EXCEPTION_STATUS = (
        ('increased', 'Increased'),
        ('decreased', 'Decreased')
    )


class Gene(models.Model):
    form = "GeneForm"
    name = models.CharField(max_length=500, null=True, blank=True)
    slug = models.SlugField(max_length=500, null=True, blank=True)
    description_advanced = RichTextField(default="", null=True, blank=True)
    description_simple = RichTextField(default="", null=True, blank=True)
    fix_advanced = RichTextField(default="", null=True, blank=True)
    fix_simple = RichTextField(default="", null=True, blank=True)
    creator = models.CharField(max_length=500, blank=True, null=True)
    transcription_factors = models.ManyToManyField(TranscriptionFactor,related_name="gene_transcription_factors", blank=True)
    diseasetraits = models.ManyToManyField("DiseaseTrait", through="GeneDiseaseTrait", related_name="genes", blank=True)
    function = models.TextField(null=True, blank=True)
    tissue_specificity = models.TextField(null=True, blank=True)
    induction = models.TextField(null=True, blank=True)
    miscellaneous = models.TextField(null=True, blank=True)
    developmental_stage = models.TextField(null=True, blank=True)
    caution = models.TextField(null=True, blank=True)
    enzyme_regulation = models.TextField(null=True, blank=True)
    cofactor = models.TextField(null=True, blank=True)
    keywords = models.ManyToManyField("GeneKeyword", related_name="genes", blank=True)
    pathways = models.ManyToManyField("GenePathway", related_name="genes", blank=True)
    global_pathways = models.ManyToManyField("Pathway", related_name="genes", blank=True, through="GeneDiseasePathwayInteraction")
    symbol = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.TextField(default="", null=True, blank=True)
    disgenet_geneid = models.CharField(max_length=255, null=True, blank=True)
    synonyms = models.ManyToManyField(Synonym, blank=True)
    disease_exceptions = models.ManyToManyField("DiseaseTrait", related_name="genes_exceptions", blank=True)
    exception_status = models.CharField(choices=EXCEPTION_STATUS, max_length=9, null=True, blank=True)
    ctd_id = models.CharField(max_length=255, null=True, blank=True)
    ctd_alt_gene_ids = models.TextField(default='', blank=True)
    ctd_bio_grid_ids = models.TextField(default='', blank=True)
    ctd_pharm_gkb_ids = models.TextField(default='', blank=True)
    ctd_uniprot_ids = models.TextField(default='', blank=True)

    protein_names = models.TextField(default='', blank=True)
    gene_encoded_by = models.TextField(default='', blank=True)
    natural_variant = models.TextField(default='', blank=True)
    length = models.TextField(default='', blank=True)
    polymorphysm = models.TextField(default='', blank=True)
    ec_number = models.TextField(default='', blank=True)
    catalytic_activity = models.TextField(default='', blank=True)
    kinetics = models.TextField(default='', blank=True)
    active_site = models.TextField(default='', blank=True)
    binding_site = models.TextField(default='', blank=True)
    metal_binding = models.TextField(default='', blank=True)
    dna_binding = models.TextField(default='', blank=True)
    nucleotide_binding = models.TextField(default='', blank=True)
    site = models.TextField(default='', blank=True)
    subunit_structure = models.TextField(default='', blank=True)
    go = models.ManyToManyField("GeneOntology", blank=True)
    pathology_allergenic = models.TextField(default='', blank=True)
    uniprot_biotechnological_use = models.TextField(default='', blank=True)
    uniprot_disruption_phenotype = models.TextField(default='', blank=True)
    uniprot_disease = models.TextField(default='', blank=True)
    uniprot_pharmacological_use = models.TextField(default='', blank=True)
    uniprot_toxic_dose = models.TextField(default='', blank=True)
    uniprot_subcellular_location = models.TextField(default='', blank=True)
    uniprot_peptide = models.TextField(default='', blank=True)
    uniprot_lipidation = models.TextField(default='', blank=True)
    uniprot_disulfide_bond = models.TextField(default='', blank=True)
    uniprot_zinc_finger = models.TextField(default='', blank=True)
    uniprot_protein_families = models.TextField(default='', blank=True)
    uniprot_domain = models.TextField(default='', blank=True)
    uniprot_motif = models.TextField(default='', blank=True)
    accessions_ensembl = models.TextField(default='', blank=True)
    accessions_pdb = models.TextField(default='', blank=True)
    accession_disgenet = models.TextField(default='', blank=True)
    accession_kegg = models.TextField(default='', blank=True)
    accession_ctd = models.TextField(default='', blank=True)
    accession_intac = models.TextField(default='', blank=True)
    accession_genewiki = models.TextField(default='', blank=True)

    ghr_function = RichTextField(default='', null=True, blank=True)

    def get_top_interactions(self):
        interactions = self.chemicalgeneinteraction_set.all().annotate(
            total=Count('actions') + F('amount') -1).order_by('-total').values('gene__name', 'interaction', 'total')[:10]
        return interactions

    def __str__(self):
        return str(self.name)

    def get_absolute_url(self):
        return reverse('gene', args=[str(self.name)])

    class Meta:
        ordering = ['name']


class Pathology(models.Model):
    gene = models.ForeignKey(Gene)
    name = models.CharField(max_length=255)
    # slug = AutoSlugField(populate_from="name", max_length=255)
    summary = RichTextField(default='', null=True, blank=True)

    def __str__(self):
        return self.name


class GeneKeyword(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class GenePathway(models.Model):
    name = models.CharField(max_length=255)
    kegg_id = models.CharField(max_length=255, unique=True)
    html = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class UniprotCategory(models.Model):
    name = models.CharField(max_length=255)
    slug = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class UniprotData(models.Model):
    category = models.ForeignKey(UniprotCategory)
    gene = models.ForeignKey(Gene, related_name="uniprot_data")
    value = models.CharField(max_length=255)
    chemical = models.ForeignKey("chemical.Chemical", related_name="uniprot", null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.value


class File(models.Model):
    FILE_STATUS_UPLOADED = "uploaded"
    FILE_STATUS_QUEUED = "queued"
    FILE_STATUS_PROCESSING = "processing"
    FILE_STATUS_ERROR = "error"
    FILE_STATUS_COMPLETED = "completed"
    FILE_STATUS_CHOICES = (
        (FILE_STATUS_UPLOADED, 'Uploaded',),
        (FILE_STATUS_QUEUED, 'Queued',),
        (FILE_STATUS_PROCESSING, 'Processing',),
        (FILE_STATUS_ERROR, 'Error',),
        (FILE_STATUS_COMPLETED, 'Completed',),
    )

    PROVIDER_UNKNOWN = 0
    PROVIDER_DIRECT_UPLOAD = 1
    PROVIDER_23ANDME_API = 2
    PROVIDER_CHOICES = (
        (PROVIDER_UNKNOWN, "Unknown"),
        (PROVIDER_DIRECT_UPLOAD, "Direct upload"),
        (PROVIDER_23ANDME_API, "23AndMe Api"),
    )

    SERVICE_UNKNOWN = 0
    SERVICE_23ANDME = 1
    SERVICE_ANCESTRY = 2
    SERVICE_COURTAGEN = 3
    SERVICE_FAMILY_TREE = 4
    SERVICE_VCF = 5
    SERVICE_CHOICES = (
        (SERVICE_UNKNOWN, "Unknown"),
        (SERVICE_23ANDME, "23AndMe"),
        (SERVICE_ANCESTRY, "Ancestry"),
        (SERVICE_COURTAGEN, "Courtagen"),
        (SERVICE_FAMILY_TREE, "Family Tree"),
        (SERVICE_VCF, "VCF"),
    )

    FILE_TYPE_CHOICES = (
        (0, "Default"),
        (1, "Demo"),
    )

    file_name = models.CharField(max_length=500)
    original_name = models.CharField(max_length=255, null=True)
    hashed_name = models.CharField(max_length=255, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    sequenced_at = models.CharField(max_length=500, blank=True, null=True) # grab from the first line starting at : 
    user = models.ForeignKey(User)
    genes_to_look_at = models.ManyToManyField(Gene, blank=True, through="UserGeneReputation")

    provider = models.IntegerField(choices=PROVIDER_CHOICES, default=0)
    file_type = models.IntegerField(choices=FILE_TYPE_CHOICES, default=0)
    service = models.IntegerField(choices=SERVICE_CHOICES, default=0)
    rescan_available = models.BooleanField(default=False)

    status = models.CharField(max_length=255, choices=FILE_STATUS_CHOICES, null=True, default=FILE_STATUS_UPLOADED)
    progress = models.FloatField(default=0, blank=True, null=True)
    status_message = models.TextField(blank=True, null=True)

    deleted_at = models.DateTimeField(null=True, blank=True)

    def __init__(self, *args, **kwargs):
        self.progress_total_count = 0
        self.progress_total_index = 0
        self.progress_total = 0
        self.progress_delta = 0
        super(File, self).__init__(*args, **kwargs)

    def set_total_points(self, points, latency=100):
        self.progress_total_count = points if points > 0 else 1
        self.progress_delta = (100 - self.progress_total) / self.progress_total_count
        self.latency = latency

    def update_progress(self, val=None):
        self.progress_total_index += 1
        self.progress_total += self.progress_delta
        if val:
            self.progress_total = val
        if self.progress_total_index % self.latency == 0 or val is not None:
            self.progress = self.progress_total
            if self.progress_total >= 100:
                self.status = self.FILE_STATUS_COMPLETED
            self.save()

    @property
    def is_demofile(self):
        return self.file_type == 1

    def __str__(self):
        return str(self.file_name)


class MarketingEvent(models.Model):
    name = models.CharField(max_length=500, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    users_shown = models.ManyToManyField(User, related_name="marketing_event", blank=True)
    script_name_a = models.CharField(max_length=500, null=True, blank=True)
    script_name_b = models.CharField(max_length=500, null=True, blank=True)
    views_a = models.IntegerField(default=0)
    views_b = models.IntegerField(default=0)
    conversions_a = models.IntegerField(default=0)
    conversions_b = models.IntegerField(default=0)


class UserRsid(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    file = models.ForeignKey(File, related_name="related_rsid")
    rsid = models.CharField(max_length=500)
    chromosome = models.CharField(max_length=500)
    position = models.CharField(max_length=500)
    genotype = models.CharField(max_length=500) #When the genotype is homozygous minor, show as something to look at
    genotype_style = models.CharField(max_length=500, blank=True, null=True)
    def __str__(self):
        return str(self.file)


class SnpStudy(models.Model):
    snp = models.ForeignKey("Snp", null=False, blank=False, related_name="studies")
    pubmed_id = models.IntegerField()
    first_author = models.CharField(max_length=255, blank=True, null=True)
    journal = models.CharField(max_length=255, blank=True, null=True)
    journal_published_at = models.DateField(null=True, blank=True)
    study = models.TextField(blank=True, null=True)
    initial_sample_size = models.TextField(blank=True, null=True)
    replication_sample_size = models.TextField(blank=True, null=True)
    risk_allele = models.CharField(max_length=1, null=True, blank=True)
    risk_allele_frequency = models.FloatField(null=True, blank=True)
    context = models.TextField(blank=True, null=True)
    p_value = models.FloatField(blank=True, null=True)
    p_value_mlog = models.FloatField(blank=True, null=True)
    p_value_text = models.CharField(max_length=255, blank=True, null=True)
    region = models.CharField(max_length=255, blank=True, null=True)
    snp_id_current = models.IntegerField(null=True, blank=True)
    intergenic = models.IntegerField(null=True, blank=True)
    strongest_snp_risk_allele = models.TextField(null=True, blank=True)
    odds_ratio = models.FloatField(null=True, blank=True, default=0.0)
    ci_text = models.CharField(max_length=255, null=True, blank=True)
    accession = models.CharField(max_length=255, null=True, blank=True)
    diseasestraits = models.ManyToManyField("DiseaseTrait", related_name="studies", verbose_name="Diseases/Traits", blank=True)
    platform_qc = models.CharField(max_length=255, null=True, blank=True)
    cnv = models.CharField(max_length=32, null=True, blank=True)

    class Meta:
        verbose_name = "Snp Study"
        verbose_name_plural = "Snp Studies"


class Snp(models.Model): #each snp has a reputation of either good, maybe good, bad, maybe bad
    rsid = models.CharField(max_length=500, blank=True, null=True)
    name = models.CharField(max_length=500, blank=True, null=True)
    code = models.CharField(max_length=500, blank=True, null=True)
    description_advanced = RichTextField(verbose_name="description", default="", blank=True, null=True) #from wp_posts. #summary and advanced for snps are contained in wp_postmeta
    description_simple = RichTextField(default="", blank=True, null=True) #from pods_snp summary
    importance = models.IntegerField(default=2, blank=True, null=True)
    genes = models.ManyToManyField(Gene, related_name="snps", through="SnpGenes")
    alleles = models.CharField(max_length=16, blank=True, null=True)
    ancestral_allele = models.CharField(max_length=500, blank=True, null=True)
    ambiguity_code = models.CharField(max_length=1, null=True, blank=True)
    minor_allele = models.CharField(max_length=1, blank=True, null=True, verbose_name="Minor Allele")
    minor_allele_frequency = models.FloatField(blank=True, null=True, verbose_name="Minor Allele Frequency")
    major_allele = models.CharField(max_length=1, blank=True, null=True, verbose_name="Major Allele")
    major_allele_frequency = models.FloatField(blank=True, null=True, verbose_name="Major Allele Frequency")
    bad_allele = models.CharField(max_length=1, blank=True, null=True)
    category = models.CharField(max_length=500, blank=True, null=True)
    genotype = models.CharField(max_length=500, blank=True, null=True)
    chromosome = models.CharField(max_length=500, blank=True, null=True)
    position = models.CharField(max_length=500, blank=True, null=True)
    locus = models.CharField(max_length=500, blank=True, null=True)
    allele_orientation = models.CharField(max_length=500, blank=True, null=True)
    heterozygous_color = models.CharField(max_length=500)
    homozygous_major_color = models.CharField(max_length=500)
    homozygous_minor_color = models.CharField(max_length=500)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    red_flag = models.BooleanField(default=False, blank=True)
    genotype_exceptions = models.CharField(max_length=255, blank=True, null=True, default="{}")
    chr_id = models.CharField(max_length=255, null=True, blank=True)
    chr_pos = models.CharField(max_length=255, null=True, blank=True)
    snp_id_current = models.CharField(max_length=255, null=True, blank=True)
    gene_ids = models.CharField(max_length=255, null=True, blank=True)
    upstream_gene_id = models.IntegerField(null=True, blank=True)
    downstream_gene_id = models.IntegerField(null=True, blank=True)
    upstream_gene_distance = models.IntegerField(null=True, blank=True)
    downstream_gene_distance = models.IntegerField(null=True, blank=True)
    merged = models.IntegerField(null=True, blank=True)

    disease_variant = models.ManyToManyField("DiseaseVariant", blank=True)

    def __str__(self):
        return str(self.rsid)

    def get_absolute_url(self):
        return reverse('snp', args=[str(self.rsid)])

    class Meta:
        ordering = ['rsid']


class SnpGenes(models.Model):
    snp = models.ForeignKey(Snp, on_delete=models.CASCADE, related_name="related_genes")
    gene = models.ForeignKey(Gene, on_delete=models.CASCADE, related_name="related_snps")
    excluded = models.BooleanField(default=False)

    class Meta:
        db_table = "genome_snp_genes"


class Category(models.Model):
    name = models.CharField(max_length=500, null=True, blank=True)
    blue_grade_label = models.CharField(max_length=255, null=True, blank=True)
    orange_grade_label = models.CharField(max_length=255, null=True, blank=True)
    red_grade_label = models.CharField(max_length=255, null=True, blank=True, help_text="You can use {name} keyword to inject a name of category")

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name_plural = "Categories"


class SnpAllele(models.Model):
    #created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    snp = models.ForeignKey(Snp, related_name="related_snp_allele", blank=True)
    description_minor = RichTextField(null=True, blank=True)
    description_hetero = RichTextField(null=True, blank=True)
    description_major = RichTextField(null=True, blank=True)
    score_minor = models.FloatField(default=0, max_length=10)
    score_major = models.FloatField(default=0, max_length=10)
    score_hetero = models.FloatField(default=0, max_length=10)
    fix_minor = RichTextField(null=True, blank=True)
    fix_major = RichTextField(null=True, blank=True)
    fix_hetero = RichTextField(null=True, blank=True)
    category = models.ForeignKey(Category, null=True, blank=True, related_name="snp_alleles")
    def __str__(self):
        return str(self.snp)


# Make the genes belong to the genepack and then after a user installs it, they will be able to click instead of where "uninstall" is rn to see a 
# report of the snps relevant to the group of genes they installed
# 3rd parties will create their own gene packs where they add some snps to genes and some genes to a gene pack with their own 
# explanations. In the database 
class GenePack(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    genes = models.ManyToManyField(Gene, related_name='related_gene_pack', blank=True) #must add related name to all many to many
    name = models.CharField(max_length=500, blank=True, null=True)
    description_simple = RichTextField(default="", blank=True, null=True)
    creator = models.CharField(max_length=500, blank=True, null=True, default="main")
    price = models.CharField(max_length=200, blank=True, null=True, default=0)
    pack_type = models.CharField(max_length=200, blank=True, null=True, default="gene_pack")
    description_advanced = RichTextField(default="", blank=True, null=True)
    categories = models.ManyToManyField(Category, blank=True, related_name="gene_packs")
    coming_soon = models.CharField(max_length=200, blank=True, null=True)
    owner = models.ForeignKey(User, related_name='gene_packs', default=1)
    likes = models.ManyToManyField(User, related_name='liked_gene_packs', blank=True)
    author = models.CharField(max_length=500, blank=True, null=True)

    def __str__(self):
        return str(self.name)


class Condition(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    name = models.CharField(max_length=500, null=True, blank=True)
    gene_packs = models.ManyToManyField(GenePack, related_name="condition", blank=True)
    category = models.CharField(max_length=500, blank=True,null=True)
    def __str__(self):
        return str(self.name)

# class Symptom(models.Model):
#     created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
#     name = models.CharField(max_length=500, blank=True,null=True)
#     gene_packs = models.ManyToManyField(GenePack, related_name="reverse_symptom", blank=True)
#     category = models.CharField(max_length=500, blank=True,null=True)
#     def __str__(self):
#         return str(self.name)


class Substance(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    name = models.CharField(max_length=500, blank=True, null=True)
    slug = models.CharField(max_length=500, blank=True, null=True)
    description = RichTextField(null=True, blank=True)

    def __str__(self):
        return str(self.name)


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name="user_profile")
    active_file = models.ForeignKey(File, blank=True, null=True, on_delete=models.DO_NOTHING)
    stripe_customer_id = models.CharField(max_length=500, blank=True, null=True)
    points = models.IntegerField(default=0)
    gene_packs = models.ManyToManyField(GenePack, related_name="gene_pack_user_profile", blank=True)
    gene = models.ManyToManyField(Gene, related_name="gene_user_profile", blank=True)
    symptoms = models.ManyToManyField("analysis.Symptom", related_name="user_profile_symptoms", blank=True)
    conditions = models.ManyToManyField(Condition, related_name="user_profile_conditions", blank=True)
    authorized_users = models.ManyToManyField(User, related_name="authorized_users", blank=True)
    traffic_source = models.CharField(max_length=500, blank=True, null=True)
    bookmarked_snps = models.ManyToManyField(Snp, related_name="bookmarked_snps", blank=True)
    related_objects = RelatedObjectsDescriptor()

    affiliated_from = models.ForeignKey("affiliate.AffiliateProfile", null=True, blank=True, related_name="affiliates")
    file_uploads_count = models.PositiveIntegerField(default=0, null=True)

    # 23AndMe Api
    access_token = models.CharField(max_length=255, null=True, blank=True)
    refresh_token = models.CharField(max_length=255, null=True, blank=True)
    token_refresh_date = models.DateTimeField(null=True, blank=True)

    @property
    def is_file_uploads_quota_exceeded(self):
        subscription = Subscription.get_active(self.user)
        if subscription:
            return (subscription.plan.max_file_uploads == 0) \
                   or (self.file_uploads_count >= subscription.plan.max_file_uploads)
        return False

    def get_subscription_transaction(self):
        transaction = (
            Transaction.objects.filter(
                models.Q(transaction_type="individual_membership")  # PayPal subscription
                | models.Q(transaction_type="monthly_individual_membership")  # Stripe subscription
            ).extra(where=[
                """created_at >= TIMESTAMP '%s' - duration_in_days * INTERVAL '1 day'""" % timezone.now().utcnow().strftime("%Y-%m-%d %H:%M:%S")
            ]) | Transaction.objects.filter(provider__isnull=False, date_expires__gte=timezone.now())
        ).filter(user=self.user).order_by('-created_at').first()

        if not transaction:  # Allow 1 year from registration for users with special transaction types
            transaction = Transaction.objects.select_related("user").filter(
                Q(transaction_type='individual_membership', user__date_joined__lt="2016-09-01")
                | Q(transaction_type='single') | Q(transaction_type='yearly_membership_60')
            ).filter(
                user=self.user,
                user__date_joined__gte=timezone.now() - timedelta(days=365)
            ).order_by('-created_at').first()

        if not transaction and self.stripe_customer_id:
            subscription = self.get_subscription()
            if subscription:
                return subscription
            stripe.api_key = settings.STRIPE_API_KEY
            customer = stripe.Customer.retrieve(self.stripe_customer_id)
            try:
                subscr = stripe.Subscription.retrieve(customer.subscriptions.data[0].id)
                plan = Plan.objects.filter(plan_id=subscr.plan.id).first()
            except:
                self.stripe_customer_id = None
                self.save()
                return None
            if subscr:
                created = datetime.fromtimestamp(subscr['current_period_start'])
                end = datetime.fromtimestamp(subscr['current_period_end'])
                transaction = Transaction.objects.create(
                    date_start=created,
                    user=self.user,
                    price=plan.price,
                    provider=Transaction.PROVIDER_STRIPE,
                    subscription_id=subscr["id"],
                    date_expires=end
                )

                Subscription.objects.create(
                    user=self.user,
                    provider=PROVIDER_STRIPE,
                    plan=plan,
                    sid=subscr['id'],
                    customer_id=self.stripe_customer_id,
                    date_started=datetime.fromtimestamp(subscr['current_period_start']),
                    date_expires=datetime.fromtimestamp(subscr['current_period_end'])
                )

        return transaction

    def get_cancellation_transaction(self):
        transaction = self.get_subscription_transaction()

        subscription = self.get_subscription()
        if subscription:
            return subscription.canceled

        cancellation = Transaction.objects.filter(
            user=self.user,
            transaction_type="plan_cancellation",
            created_at__gte=transaction.created_at
        ).order_by('-created_at').first()

        return transaction if transaction.canceled else cancellation

    def has_subscription(self):
        transaction = self.get_subscription_transaction()

        if not transaction:
            subscription = self.get_subscription()
            return subscription is not None
        return transaction is not None

    def get_subscription(self):
        return Subscription.get_active(self.user)

    def has_cancellation(self):
        transaction = self.get_cancellation_transaction()

        return transaction is not None

    def can_subscribe(self):
        return not self.has_subscription()

    def is_email_verified(self):
        return self.user.emailaddress_set.filter(verified=True, email=self.user.email).exists()

    def has_free_subscription(self):
        tr = self.get_subscription_transaction()
        return tr.transaction_type == "a_signup"

    def has_initial_subscription(self):
        transaction = Transaction.objects.filter(user=self.user).order_by('created_at').first()
        if not transaction:
            return False
        return transaction

    def __str__(self):
        return str(self.user)


class SiteLogin(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.user)


@receiver(user_signed_up)
def new_user_signup(sender, **kwargs):
    request_instance = kwargs.get('request')
    affiliate_cookie = request_instance.COOKIES.get(settings.AFFILIATE_COOKIE_NAME)
    traffic_source_cookie = request_instance.COOKIES.get('traffic_source')

    if traffic_source_cookie and affiliate_cookie:
        traffic_source_field = '{0}&{2}={1}'.format(
            traffic_source_cookie, affiliate_cookie, settings.AFFILIATE_URL_PARAM
        )
    elif traffic_source_cookie:
        traffic_source_field = traffic_source_cookie
    elif affiliate_cookie:
        traffic_source_field = '?{0}={1}'.format(settings.AFFILIATE_URL_PARAM, affiliate_cookie)
    else:
        traffic_source_field = None

    p = UserProfile(user=kwargs['user'], traffic_source=traffic_source_field)
    p.active_file = File.objects.filter(file_type=1).first()

    if affiliate_cookie:
        affiliate_profile = AffiliateProfile.objects.filter(
            affiliate_code=affiliate_cookie
        ).first()
        p.affiliated_from = affiliate_profile

    p.save()
    for item in Gene.objects.filter(related_gene_pack__id=1):
        p.gene.add(item)
    p.save()

    if kwargs.get('request'):
        if kwargs.get('request').COOKIES.get('invited', '').isnumeric():
            sender = int(kwargs.get('request').COOKIES.get('invited'))
            AccessRequest.objects.create(sender_id=sender, receiver=kwargs['user'])

    if (kwargs['user'].email in paid_users) or ("testcambria" in kwargs['user'].email):
        print("paid!")
        Transaction.objects.create(user=kwargs['user'], price=10, transaction_type="single",
                                   duration_in_days=365)


@receiver(user_logged_in)
def user_login(sender, **kwargs):
    SiteLogin.objects.create(user=kwargs['user'])


class Page(models.Model):
    slug = models.CharField(max_length=500)
    title = models.CharField(max_length=500, default="Decodify Your Genome")
    content = models.TextField(default="")
    description = models.TextField(default="", blank=True)
    def __str__(self):
        return str(self.slug)

class Post(models.Model):
    slug = models.CharField(max_length=500)
    title = models.CharField(max_length=500, default="Decodify Your Genome")
    content = RichTextField(default="", null=True, blank=True)
    description = models.TextField(default="", blank=True)
    def __str__(self):
        return str(self.slug)


class KeywordLink(models.Model):
    keyword = models.CharField(max_length=500, blank=True, null=True)
    link = models.CharField(max_length=500, blank=True, null=True)
    def __str__(self):
        return str(self.keyword)


class BloodMarker(models.Model): # so far we are putting the alternative spellings inside the name in brackets
    name = models.CharField(max_length=500, blank=True, null=True)
    unit_main = models.CharField(max_length=500, blank=True, null=True)
    unit = models.CharField(max_length=500, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return str(self.name)


# class UserBloodMarker(models.Model): # so far we are putting the alternative spellings inside the name in brackets
#     name = models.CharField(max_length=500, blank=True, null=True)
#     unit_main = models.CharField(max_length=500, blank=True, null=True)
#     unit = models.CharField(max_length=500, blank=True, null=True)
#     created_at = models.DateTimeField(auto_now_add=True)
#     def __str__(self):
#         return str(self.name)


class DiseaseSymptom(models.Model):
    name = models.CharField(max_length=255)
    slug = AutoSlugField(populate_from='name', max_length=255)
    hpo_id = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name


class DiseaseTraitCategory(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Disease/Trait Category"
        verbose_name_plural = "Diseases/Traits Categories"


class DiseaseSynonym(models.Model):
    name = models.CharField(max_length=500)
    slug = AutoSlugField(populate_from='name')

    def __str__(self):
        return self.name


class DiseaseTrait(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)
    slug = models.SlugField(max_length=255, null=True, blank=True)
    parent = models.ForeignKey("DiseaseTrait", null=True, blank=True)
    ref_url = models.CharField(max_length=255, null=True, blank=True)
    category = models.ForeignKey(DiseaseTraitCategory, blank=True, null=True, related_name="diseasestraits")
    categories = models.ManyToManyField(DiseaseTraitCategory, blank=True)
    disgenet_diseaseid = models.CharField(max_length=255, null=True, blank=True)
    definition = RichTextField(null=True, blank=True)

    medlineplus_summary = models.TextField(null=True, blank=True)
    nih_rare_diseases_summary = models.TextField(null=True, blank=True)
    malacards_summary = models.TextField(null=True, blank=True)
    uniprotkb_summary = models.TextField(null=True, blank=True)
    disease_ontology_summary = models.TextField(null=True, blank=True)
    cdc_summary = models.TextField(null=True, blank=True)
    wikipedia_summary = models.TextField(null=True, blank=True)
    ghr_summary = models.TextField(null=True, blank=True)
    omim_summary = models.TextField(null=True, blank=True)
    annotation_score = models.IntegerField(default=0)
    accession_malacards = models.CharField(max_length=255, null=True, blank=True)
    disease_ontology = models.CharField(max_length=255, null=True, blank=True)
    icd10 = models.CharField(max_length=255, null=True, blank=True)
    icd9cm = models.CharField(max_length=255, null=True, blank=True)
    snomed_ct = models.CharField(max_length=255, null=True, blank=True)
    mesh = models.CharField(max_length=255, null=True, blank=True)
    ncit = models.CharField(max_length=255, null=True, blank=True)
    umsl = models.CharField(max_length=255, null=True, blank=True)
    omim = models.CharField(max_length=255, null=True, blank=True)
    orphanet = models.CharField(max_length=255, null=True, blank=True)
    icd10_via_orphanet = models.CharField(max_length=255, null=True, blank=True)
    mesh_via_orphanet = models.CharField(max_length=255, null=True, blank=True)
    umls_via_orphanet = models.CharField(max_length=255, null=True, blank=True)
    medgen = models.CharField(max_length=255, null=True, blank=True)

    ebi_uri = models.CharField(max_length=255, null=True, blank=True)

    ctd_name = models.CharField(max_length=500, null=True, blank=True)
    ctd_id = models.CharField(max_length=500, null=True, blank=True)
    ctd_alt_id = models.CharField(max_length=500, null=True, blank=True)
    ctd_definition = models.TextField(default='', null=True, blank=True)
    ctd_parent_ids = models.TextField(default='', null=True, blank=True)
    ctd_tree_numbers = models.TextField(default='', null=True, blank=True)
    ctd_parent_tree_numbers = models.TextField(default='', null=True, blank=True)
    ctd_synonyms = models.ManyToManyField("DiseaseSynonym")
    ctd_slim_mapping = models.TextField(default='', null=True, blank=True)

    is_mapped = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Disease/Trait"
        verbose_name_plural = "Diseases/Traits"


class DiseaseHierarchy(MPTTModel):
    disease = models.ForeignKey(DiseaseTrait)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)

    def __str__(self):
        return self.disease.name


class DiseaseHierarchyProxy(DiseaseHierarchy):
    def __str__(self):
        return str("-->".join([record.disease.name for record in self.get_ancestors()] + [self.disease.name]))

    class Meta:
        proxy = True


class Pathway(models.Model):
    pathway_name = models.TextField(default='')
    pathway_id = models.CharField(max_length=512, null=True, blank=True)
    html = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.pathway_name


class GeneDiseasePathwayInteraction(models.Model):
    disease = models.ForeignKey("DiseaseTrait", null=True, blank=True)
    pathway = models.ForeignKey("Pathway", null=True, blank=True)
    gene_symbol = models.CharField(max_length=255, null=True, blank=True)
    gene = models.ForeignKey("Gene", null=True, blank=True)


class PopulationGenome(models.Model):
    snp = models.ForeignKey(Snp, related_name="population_genomes")
    title = models.CharField(max_length=255)
    abbr = models.CharField(max_length=255)
    group = models.CharField(max_length=255)
    group2 = models.CharField(max_length=255)
    homozygous_minor = models.CharField(max_length=255, null=True, blank=True)
    homozygous_major = models.CharField(max_length=255, null=True, blank=True)
    heterozygous = models.CharField(max_length=255, null=True, blank=True)
    homozygous_minor_freq = models.FloatField(null=True, blank=True)
    homozygous_major_freq = models.FloatField(null=True, blank=True)
    heterozygous_freq = models.FloatField(null=True, blank=True)



    def allele_parsed(self):
        data = [ar.split(':') for ar in self.group2.split(';')]
        if len(data[0]) == 1:
            return self
        hetero = list(filter(lambda v: len(set(v[0].split('|'))) > 1, data))
        major_minor = list(filter(lambda v: len(set(v[0].split('|'))) == 1, data))
        major, minor = major_minor if len(major_minor) == 2 else (major_minor[0], ['', 0],)
        if float(major[1]) < float(minor[1]):
            minor, major = major, minor

        self.Maj = major
        self.Min = minor
        self.Het = hetero[0] if len(hetero) > 0 else ['', 0]

        return self


class DiseaseSymptomInteraction(models.Model):
    disease = models.ForeignKey("DiseaseTrait", null=True, blank=True)
    symptom = models.ForeignKey("DiseaseSymptom", null=True, blank=True)
    frequency = models.CharField(max_length=255, null=True, blank=True)


class GeneDiseaseTrait(models.Model):
    gene = models.ForeignKey(Gene, on_delete=models.CASCADE, db_index=True)
    diseasetrait = models.ForeignKey(DiseaseTrait, on_delete=models.CASCADE, db_index=True)
    description = models.TextField(null=True, blank=True)
    sources = models.CharField(max_length=512, null=True, blank=True)
    score = models.FloatField(null=True, blank=True, default=0)
    nofpmids = models.IntegerField(null=True, blank=True)
    nofsnps = models.IntegerField(null=True, blank=True)
    inference_chemical = models.ForeignKey("chemical.Chemical", null=True, blank=True)
    direct_evidence = models.TextField(default='')
    inference_score = models.FloatField(null=True, blank=True, default=0)
    omim_ids = models.TextField(default='')
    pub_med_ids = models.TextField(default='')
    combined_score = models.FloatField(null=True, blank=True, default=0, help_text='Score calculated by inference_score*score')


class UserGeneReputation(models.Model):
    gene = models.ForeignKey(Gene, on_delete=models.CASCADE)
    file = models.ForeignKey(File, on_delete=models.CASCADE)
    score = models.FloatField(default=0)
    disease_risk_score = models.FloatField(default=0)

    @property
    def score_symbol(self):
        bad_weight = 3.7
        good_weight = 1.8
        if not self.score:
            return "U"
        elif self.score >= bad_weight:
            return "B"
        elif good_weight <= self.score < bad_weight:
            return "O"
        else:
            return "G"

    def __str__(self):
        return "%s <-> %s %s" % (self.file.user.username, self.gene.name, self.score,)

    class Meta:
        db_table = "genome_file_genes_to_look_at"


class GeneOntology(models.Model):
    TYPES = (
        ("cellular-component", "Cellular Component",),
        ("molecular-function", "Molecular Function",),
        ("biological-process", "Biological Process",),
    )
    go_id = models.CharField(max_length=255)
    go_name = models.CharField(max_length=512)
    slug = AutoSlugField(populate_from="go_name", max_length=512)
    type = models.CharField(choices=TYPES, max_length=255)

    def __str__(self):
        return self.go_name


class DiseaseGOAssociations(models.Model):
    disease = models.ForeignKey(DiseaseTrait)
    gene_ontology = models.ForeignKey(GeneOntology)
    inference_gene = models.TextField(default='')
    inference_gene_qty = models.IntegerField(default=0)


class ModelFieldChanges(models.Model):
    log_entry = models.OneToOneField(LogEntry, on_delete=models.CASCADE)
    changed_fields = ArrayField(
        models.CharField(max_length=255, blank=True)
    )


class DiseaseVariant(models.Model):
    disease = models.ForeignKey("DiseaseTrait", null=True, blank=True)
    rsid = models.CharField(max_length=255, null=True, blank=True)
    gene_symbol = models.CharField(max_length=255, null=True, blank=True)
    gene = models.ForeignKey("Gene", null=True, blank=True)
    variant = models.CharField(max_length=255, null=True, blank=True)
    significance = models.CharField(max_length=512, null=True, blank=True)
    risk_allele = models.CharField(max_length=255, null=True, blank=True)
    assembly = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.disease.name + ' at ' + self.rsid


paid_users = ['0wn3dbyg0d@gmail.com',
 '22rom93023@gmail.com',
 'ben_dufort@hotmail.co.uk',
 '4r4r@gmail.com',
 '2nataliepate@gmail.com',
 '4trecartin@gmail.com',
 '9e65b6c2@opayq.com',
 '9needles@gmail.com',
 'A.Leonard90@gmail.com',
 'atkinsonethan@yahoo.com',
 'abberoberts@gmail.com',
 'absolutspice@yahoo.com',
 'ac1chuck@aol.com',
 'acavanagh2@bigpond.com',
 'claire.daniel@gmail.com',
 'adrien.maistre@gadz.org',
 'aedrumm@gmail.com',
 'ahmo@tsn.cc',
 'david@escapetunnel.co.uk',
 'ailish789@gmail.com',
 'aimeyc@aol.com',
 'ajohnsonfpmh_apn@yahoo.com',
 'ajulson@suddenlink.net',
 'AkumaJDM@gmail.com',
 'alanna.johnson@gmail.com',
 'albjuv@gmail.com',
 'aleixcc10@hotmail.com',
 'alensw11@gmail.com',
 'alensworsoff@hotmail.com',
 'alexander.eriksson@gmx.com',
 'aleksandr.pashuk@xpansa.com',
 'xaxbxc@gmail.com',
 'Aliciaannmahle@gmail.com',
 'alissaw8@gmail.com',
 'alkaline322@hotmail.com',
 'mlr7569@gmail.com',
 'pythonscriptfun@gmail.com',
 'alyson.klein@ntlworld.com',
 'amalagalib@gmail.com',
 'amandameansangel@yahoo.com',
 'ashley.merten@gmail.com',
 'amy.glenney@gmail.com',
 'amydouglass@gmail.com',
 'ana@ancla.net',
 'andrealimb@hotmail.com',
 'andrewswant@gmail.com',
 'andy44@gmx.net',
 'angela.jo.good@gmail.com',
 'anncmohler@gmail.com',
 'donnamobley@me.com',
 'apd117@gmail.com',
 'armand.amico@gmail.com',
 'arseniogarcia71@yahoo.com',
 'arthur@workofart.org',
 'arvidc@mac.com',
 'ash.nouveau@gmail.com',
 'athenaze@hotmail.com',
 'august4@verizon.net',
 'aune333@yahoo.com',
 'avechr@gmail.com',
 'awscottiwilson1@live.co.uk',
 'annesuro@gmail.com',
 'baitsai89@yahoo.com',
 'salvia420@gmail.com',
 'bkfogg@vt.edu',
 'henrik.eckermann87@gmail.com',
 'bkostyk@shaw.ca',
 'blifebyshawn@gmail.com',
 'blongrie@yahoo.com',
 'bethward@cox.net',
 'bolalbi65@hotmail.com',
 'bonnie.thompson@ymail.com',
 'boonpeng33@gmail.com',
 'bpkillian@gmail.com',
 'bqc6slt@ptd.net',
 'kgrabanica@gmx.de',
 'brett@brettborders.net',
 'brettmckay@gmail.com',
 'brian.robert.joyce@gmail.com',
 'brooks.noble@gmail.com',
 'bryantru@gmail.com',
 'bryce_sellers@outlook.com',
 'caleb.helbling@gmail.com',
 'camppeony@msn.com',
 'carcinlee@aol.com',
 'cninja27@gmail.com',
 'carltalbot@bigpond.com',
 'cbellina64@aol.com',
 'cdhaden@gmail.com',
 'cecil.bonnie@gmail.com',
 'chabbah@aol.com',
 'chacahua@mac.com',
 'chana0418@gmail.com',
 'derivado@hotmail.com',
 'cjhood71@gmail.com',
 'chris.cbk@runbox.com',
 'chris.tsui5@gmail.com',
 'chris@lotny.com',
 'jdsmihu@msn.com',
 'christinahood@yahoo.com',
 'Christinehort@aol.com',
 'christophergconroy@gmail.com',
 'chropsahl@gmail.com',
 'chryssy@gmail.com',
 'cia415@gmail.com',
 'cindylapoint@gmail.com',
 'circlingbird@gmail.com',
 'cksgrl@aol.com',
 'clamasta@gmail.com',
 'clm392@gmail.com',
 'cloudlin123321@gmail.com',
 'clscheffler@aol.com',
 'cmekmchal@yahoo.com',
 'cmitchell451@gmail.com',
 'cogniquity@gmail.com',
 'colintconway@gmail.com',
 'colorado4bjh@gmail.com',
 'stapletonc@gmail.com',
 'jamiedemeo@gmail.com',
 'matt@aminoman.com',
 'salewski@tesco.net',
 'coriwyn@yahoo.com',
 'obbie2gs@gmail.com',
 'cpittm12@yahoo.com',
 'crakmuffin@hotmail.com',
 'neelie.crooke@gmail.com',
 'chris@cshutchinson.com',
 'csloan@gmail.com',
 'cwhollister@gmail.com',
 'dabfotocreative@dabfoto.com',
 'damianxavier1@gmail.com',
 'dana.c.howell@gmail.com',
 'daniel.joyner923@gmail.com',
 'daniel.labbe@btinternet.com',
 'daniel9212@hotmail.de',
 'darlingswe@hotmail.com',
 'darrenequick@gmail.com',
 'dars.box@gmail.com',
 'datasink@gmail.com',
 'dave.cartwright@gmail.com',
 'david.spittel@gmail.com',
 'davisjs3@aol.com',
 'deborahlee@jeffnet.org',
 'debra.shultz@gmail.com',
 'debwot@gmail.com',
 'dedougan@mail.com',
 'deedeedumbo@gmail.com',
 'deleena1212@gmail.com',
 'Austin_mcf@hotmail.com',
 'horsebox@gmx.com',
 'denis.gelrud@gmail.com',
 'denis.odinokov@gmail.com',
 'denise@varnadoe.net',
 'denny.robinson@gmail.com',
 'derrond@posc-az.com',
 'deszie@gmail.com',
 'dewalker@trentu.ca',
 'diannachairez@yahoo.com',
 'diddens.bras@hccnet.nl',
 'dinovids@hotmail.com',
 'dl.cartwright@comcast.net',
 'dnyoto@gmail.com',
 'dnyoto@hotmail.com',
 'docno@mthitx.com',
 'doggykittydr@gmail.com',
 'dominictamlynbere@yahoo.co.uk',
 'donna3658@comcast.net',
 'donnasfoodlab@gmail.com',
 'doublejoy97@yahoo.com',
 'drlum@fhihealthcare.com',
 'drstickler@gmail.com',
 'd.sola@archangelaerospace.com',
 'dtnikkihansen@gmail.com',
 'dublin14@gmail.com',
 'duffyme5@msu.edu',
 'dynamicality@yahoo.com',
 'dzevad@multicom.ba',
 'dziwota@mthfr-genetics.co.uk',
 'dzmcfatridge@gmail.com',
 'emiliebalkman@gmail.com',
 'eblair123@gmail.com',
 'bleken@gmail.com',
 'jennifersardam@gmail.com',
 'einbinder.kelly@gmail.com',
 'eirinnocuinn@gmail.com',
 'eliz.cardenas@yahoo.com',
 'elyonn@yahoo.com.mx',
 'matvey.ezhov@gmail.com',
 'emma_chase@yahoo.com',
 'erika.moyer@gmail.com',
 'erikalovesemail@gmail.com',
 'erindaab@aol.com',
 'esc.designs@gmail.com',
 'euro2512@gmail.com',
 'ewlkfmw@gmail.com',
 'extraordinarypigeon@yahoo.com',
 'faridaro@yahoo.com',
 'favere@gmail.com',
 'fentonb@fastmail.fm',
 'fernacular@gmail.com',
 'filosoof@gmail.com',
 'fionapulskamp@gmail.com',
 'fkennard22@gmail.com',
 'florianvh@gmail.com',
 'frederikemilhanfgarn@hotmail.co.uk',
 'fredley33@gmail.com',
 'frogleap8@aol.com',
 'redtigerlion@icloud.com',
 'frogwarrior33@gmail.com',
 'fru.lindahl@gmail.com',
 'garystratton@gmail.com',
 'GB50NOLA@cox.net',
 'gbrooks1222@gmail.com',
 'geezman07@aol.com',
 'geneaum@hotmail.com',
 'george.results@gmail.com',
 'gertrudis2008@hotmail.com',
 'getbrandonchase@gmail.com',
 'ghoff1@gmail.com',
 'guy@gia.lu',
 'ginniselle@gmail.com',
 'gmanlove@gmail.com',
 'gmbeav1@yahoo.com',
 'gmstrack@gmail.com',
 'gleb.nazarkin@gmail.com',
 'gogote@live.com',
 'samschulman@gmail.com',
 'gregcooper95@me.com',
 'gtutenuit@yahoo.fr',
 'guybrown1000@gmail.com',
 'guy@icanvas.com',
 'halyna.merzhyyevska@yahoo.com',
 'christoph.hanazeder@gmail.com',
 'spjohn@hal-pc.org',
 'harryhagaman@gmail.com',
 'hart@nutregy.com.au',
 'haugstedmikael@gmail.com',
 'haydenh123@hotmail.com',
 'head2329@aol.com',
 'heather.spear@verizon.net',
 'helanham@yahoo.com',
 'helendellan@gmail.com',
 'henrikeckermann87@gmail.com',
 'henrystewart278@gmail.com',
 'heps@seanet.com',
 'hilmer99@hotmail.com',
 'hmk23@hotmail.com',
 'holly.kibbe@yahoo.com',
 'hoppypuppy@yahoo.com',
 'hsuhrbur@catlover.com',
 'hutchpj@yahoo.com',
 'hypolyd@gmail.com',
 'ice320@aol.com',
 'ilovecheerios@hotmail.co.uk',
 'imajennonline@gmail.com',
 'ingrid.carlson@mail.rrdsb.com',
 'internship@internship.com',
 'Isak.wahl@gmail.com',
 'ivanko.sroka@gmail.com',
 'ives.matthew@gmail.com',
 'j.ryan.baldwin@gmail.com',
 'jack@jackwebbheller.com',
 'jackmccoy01@gmail.com',
 'james.wilkes@me.com',
 'chrischard10@gmail.com',
 'jamieboc@comcast.net',
 'jamram@gmail.com',
 'janasynekherd@aol.com',
 'jane702@yahoo.com',
 'jasmin.baron@gmail.com',
 'jawortham8@yahoo.com',
 'jcahill100791@gmail.com',
 'jcohen@decodify.me',
 'jd.sroll@centrum.cz',
 'jeanine@novowellnessforlife.com',
 'jeldridge18@gmail.com',
 'jennymcdonald@msn.com',
 'jeni_mccormick@hotmail.com',
 'jenngaluska@gmail.com',
 'jennifer.j.peterson@hotmail.com',
 'jennyeden17@gmail.com',
 'jeremyezerzer@gmail.com',
 'jgilldrafting@msn.com',
 'jhouston@tutanota.de',
 'jiehuang001@hotmail.com',
 'jigsawdesign@mac.com',
 'jilldenvermfr@gmail.com',
 'jimmy.yono@gmail.com',
 'jjkinz@gmail.com',
 'visionarymind111@gmail.com',
 'jodieleighroper@gmail.com',
 'jodyborrelli@gmail.com',
 'joel@leestrategies.net',
 'beeaware@hotmail.com',
 'john.henderson57@gmail.com',
 'jondank@sympatico.ca',
 'jcohen@selfhacked.com',
 'josh.finlay@yahoo.com',
 'joyce_paquin@yahoo.com',
 'jpmaguire2@gmail.com',
 'jptoes@optonline.net',
 'Jonatannordlander@hotmail.com',
 'judy.crothers@gmail.com',
 'juraj@bednar.sk',
 'justinalexawright14@gmail.com',
 'justinmrotek@gmail.com',
 'james.baber@gmail.com',
 'jweaver77@me.com',
 'jbw1974@gmail.com',
 'kadicso@yahoo.com',
 'kaila360@aol.com',
 'karen.l.connelly@gmail.com',
 'karlamessier@gmail.com',
 'kategillies8@hotmail.com',
 'katheegibbs@me.com',
 'kathryn_cunnyngham@yahoo.com',
 'kbohmova@gmail.com',
 'keith@calvinchristian.org',
 'keithskarma@yahoo.com',
 'kellycolby2@gmail.com',
 'kenny2219@gmail.com',
 'kentwhite1@gmail.com',
 'kerridaly@yahoo.com',
 'kerstinbaer@hotmail.com',
 'kevelsonr@yahoo.com',
 'khalistanton@gmail.com',
 'khonshuh@gmail.com',
 'kiel.dowlin@gmail.com',
 'kimcahill61@gmail.com',
 'yesaroseisaroseisarose@gmail.com',
 'kirstenhazen19@gmail.com',
 'kmac@daystarphoto.biz',
 'knculver@gmail.com',
 'knlterhaar@comcast.net',
 'knlterhaar@gmail.com',
 'kokejavito@gmail.com',
 'kpotter64@icloud.com',
 'kpsreading@gmail.com',
 'kpsshopping@gmail.com',
 'krfesle@gmail.com',
 'kristinadkremer@gmail.com',
 'KROSEMCCULLY@GMAIL.COM',
 'kukukajoo@yahoo.com',
 'kwm453@yahoo.com',
 'kypros.kyprianou@gmail.com',
 'alex.gierczyk@gmail.com',
 'laleger@gmail.com',
 'Lara.Stiegler@gmail.com',
 'LaraC234@yahoo.com',
 'luke@lukeatch.com',
 'laurel6123@gmail.com',
 'LawrenceAusten@yahoo.com',
 'lellenh29@gmail.com',
 'leohalepli@gmail.com',
 'leon.gutierrez@ally.com',
 'lesliebatts@yahoo.com',
 'lesliemccue@gmail.com',
 'lettertoclare@yahoo.com',
 'leymarie.frederic@wanadoo.fr',
 'lidiakerr@gmail.com',
 'lilystarling.ncmt@gmail.com',
 'lindavegasims@aol.com',
 'linnettek@hotmail.com',
 'leovonp@gmail.com',
 'lisa.soyland@gmail.com',
 'lisa.w.zimmerman@gmail.com',
 'lisakh0616@yahoo.com',
 'robinlynk@verizon.net',
 'lizziefitch@gmail.com',
 'llheinsohn@yahoo.com',
 'lokzo5royals@hotmail.com',
 'lordsofchaosrj@gmail.com',
 'lottish@gmail.com',
 'louisashworth@gmail.com',
 'loweryr@gmail.com',
 'lowry@glgcanada.com',
 'lrussellgm@gmail.com',
 'lterhaar@preshomes.org',
 'lucien.burke@uvm.edu',
 'ludwigvongrubenheimer@gmail.com',
 'luigibmth@gmail.com',
 'lukehastorun@gmail.com',
 'lynnweingarten@gmail.com',
 'lyssahvanbaalen@gmail.com',
 'm5335eby@sbcglobal.net',
 'ma_crowder@yahoo.com',
 'mac_nally@hotmail.com',
 'mackeycrystal@hotmail.com',
 'maggiemnew@yahoo.com',
 'magnetismo2000-ramon@yahoo.es',
 'maidmarian2262@sbcglobal.net',
 'mail@dabfoto.com',
 'mamapotter@icloud.com',
 'maneki613@gmail.com',
 'marchesi03@tpg.com.au',
 'marika.o@gmail.com',
 'mark@arcieromiller.com',
 'marshath@gmail.com',
 'marychosullivansanford@gmail.com',
 'matt_k_001@hotmail.com',
 'mouna.attarha@gmail.com',
 'matthew_keener@yahoo.com',
 'matthew.green419@gmail.com',
 'matthew70@hotmail.com',
 'matthewiregier@gmail.com',
 'matthewresinger@gmail.com',
 'mattias@isingetorpet.se',
 'mattknee@gmail.com',
 'matus.pagac@gmail.com',
 'medical@fluxsoft.com',
 'mbarber78@live.com',
 'caitlin.mccombs@gmail.com',
 'mcculloch@engineer.com',
 'michael.s.mcdonald23@gmail.com',
 'Markus.Lausen@web.de',
 'mjcress16@yahoo.com',
 'mcsissy@gmail.com',
 'me@benjohnson.name',
 'megos777@hotmail.com',
 'melissa.roglitzwalker@gmail.com',
 'mfrank43@gmail.com',
 'matty@3scan.com',
 'mgbidart@hotmail.com',
 'mmaloof56@gmail.com',
 'mgiwanicki@gmail.com',
 'michael@storific.com',
 'wbh.health@gmail.com',
 'michelleannbeckett@gmail.com',
 'mikerose667@gmail.com',
 'milinddoshi1@gmail.com',
 'millerk@ronto.com',
 'mjulesf@gmail.com',
 'mlaramd@gmail.com',
 'mmaloof@gmail.com',
 'mmonteverde522@gmail.com',
 'mriordan.014@gmail.com',
 'muttleybag-online@yahoo.co.uk',
 'theracer06@gmail.com',
 'mynameisbossi@gmail.com',
 'mysticnight13@gmail.com',
 'lwildenburg@gmail.com',
 'namisop35@hotmail.com',
 'nathangroup.az@gmail.com',
 'nathanpeters777@hotmail.com',
 'anitawalsh23@gmail.com',
 'nemms@web.de',
 'bgcope76@gmail.com',
 'neveen.moussa@gmail.com',
 'newmica11118@aol.com',
 'niall_behan@yahoo.co.uk',
 'nicholas.jewitt@tdsecurities.com',
 'nick@mihaljevic.org',
 'nikola@mickic.com',
 'nikolaynkolev@gmail.com',
 'majkanolan@gmail.com',
 'Normcoryell@gmail.com',
 'ntshipman@gmail.com',
 'nunolopesvieira@hotmail.com',
 'oanaglavan@yahoo.com',
 'oceannasofia@gmail.com',
 'okuenzl1@hs-koblenz.de',
 'oldpablo@comcast.net',
 'olgatomberg@gmail.com',
 'oliverk302@gmail.com',
 'ondrejroba@gmail.com',
 'oneclassicom@gmail.com',
 'onetwoqi.om@gmail.com',
 'oukayaoglu@gmail.com',
 'p.herzmann11@eabjm.org',
 'p.toulouse99@gmail.com',
 'p33ling@comcast.net',
 'pam@bluebetween.com',
 'pamm32792@Yahoo.com',
 'paco@ancla.net',
 'paulette.walther@gmail.com',
 'Pcdizzy@gmail.com',
 'pcimino12@nyc.rr.com',
 'peanutndb@gmail.com',
 'peter@kudlicka.eu',
 'petkoxray@gmail.com',
 'ltp@asu.edu',
 'philgeorge@charter.net',
 'phredder2002@yahoo.com',
 'pieni.haapasalo@gmail.com',
 'pjakobs@gmail.com',
 'pjwns1@naver.com',
 'plamen.gj@gmail.com',
 'pmpics@gmail.com',
 'pokercam@gmail.com',
 'poly2art@gmail.com',
 'preidkennedy@gmail.com',
 'purchase007@mac.com',
 'pwalt@comcast.net',
 'r.a.bishop@rogers.com',
 'rachuis@hotmail.com',
 'rahul.chhibber@gmail.com',
 'randolph.james@yahoo.com',
 'ravenousarche@gmail.com',
 'rebecca.banua@gmail.com',
 'rebekcah@hotmail.com',
 'reddins@sbcglobal.net',
 'retail308@gmail.com',
 'reustmd@gmail.com',
 'reversenutrition@hotmail.com',
 'reville@gmail.com',
 'riceball.tohru@gmail.com',
 'rick84uk@hotmail.com',
 'ckron79@gmail.com',
 'bebopbecca@gmail.com',
 'rmaloul@gmail.com',
 'robbyd@u20.org',
 'Robin14159@gmail.com',
 'robmoser@gmail.com',
 'bandrup@hotmail.com',
 'rogdubois@verizon.net',
 'rohan@neon-flux.com',
 'greengrlsmith@gmail.com',
 'rscottkatz@gmail.com',
 'rtmcwill@gmail.com',
 'harvjag@gmail.com',
 'rukusx7@gmail.com',
 'russelllong@me.com',
 'ruzica.dimova@gmail.com',
 'rxxxxr@gmail.com',
 'r.w.anderson92@gmail.com',
 'rzucker@ivc.edu',
 's_mccloudaz@hotmail.com',
 's.campbell129@gmail.com',
 'sabrina-mclaughlin@charter.net',
 'euro2512@yahoo.com',
 'samsonalin@email.it',
 'sandytravis100@gmail.com',
 'sarenayram@gmail.com',
 'sca6@cox.net',
 'chrisdsusi@gmail.com',
 'scott.brovsky@gmail.com',
 'scray21@gmail.com',
 'scream63@gmx.com',
 'sdantilio@comcast.net',
 'sdscbsm@gmail.com',
 'sebastiansalazarvamos@gmail.com',
 'sergio.h.pastor@gmail.com',
 'sgiles31@hotmail.com',
 'sha6785@gmail.com',
 'shaneaheres@gmail.com',
 'sharcat2002@yahoo.com',
 'sharonandianfrench@gmail.com',
 'shaunban@gmail.com',
 'sherrykinavey@yahoo.com',
 'shrank@gmail.com',
 'simon.adriansson@gmail.com',
 'simonkuipers@me.com',
 'matthewgolan@yahoo.com',
 'skakoon@mail.ru',
 'skneepkens@gmail.com',
 'sop@olivr.net',
 'spaincxl2010@gmail.com',
 'spc828@gmail.com',
 'Spektrolyte@gmx.com',
 'stacytillotson@gmail.com',
 'starbuxian@gmail.com',
 'stevulovabaska@hotmail.com',
 'stew2030@yahoo.com',
 'rodirtor@hotmail.com',
 'striker.js@gmail.com',
 'suelamprop@aol.com',
 'suelamprop@gmail.com',
 'sunflowercastle@gmail.com',
 'sunnyharalson@hotmail.com',
 'supertonic@gmail.com',
 'susan_ellis@hotmail.com',
 'susan.chalmers@me.com',
 'suzanne.birkett0008@live.co.uk',
 'svelotas@me.com',
 'swedishrose7@gmail.com',
 'Sweetytart87@aol.com',
 'Sweetytrt87@aol.com',
 'trevida_t@hotmail.com',
 'tainap@gmail.com',
 'tang0tang0@yahoo.com',
 'tannyh@aol.com',
 'tatiana1@gmail.com',
 'taylor.holcomb@gmail.com',
 'tbednarick@hotmail.com',
 'teamfritch@gmail.com',
 'ted.c.hu@gmail.com',
 'twee916@gmail.com',
 'testdecodifytest1@gmail.com',
 'tfg120@madisoncounty.net',
 'tfval@hotmail.com',
 'mhaugsted@gmail.com',
 'the.ali.abdulla@gmail.com',
 'billydavis360@gmail.com',
 'thom.k.j@icloud.com',
 'trs707@gmail.com',
 'tvg.hardstyle@gmail.com',
 'obrien.thomond@gmail.com',
 'tiffanymckelvey@gmail.com',
 'timfromnepal@yahoo.co.uk',
 'timgolly@t-online.de',
 'timholt1@gmail.com',
 'timm27@icloud.com',
 'tina007@gmail.com',
 'tomas.pagac@gmail.com',
 'tymbarkmichalski@gmail.com',
 'tpung@msn.com',
 'tracielipson@gmail.com',
 'tresbeau52@gmail.com',
 'trrobinson82@gmail.com',
 'turquoise555@gmail.com',
 'tanya@tanyavarga.com',
 'twilly@vomer.com',
 'twistedtreea@live.com',
 'umer_javed21@hotmail.com',
 'bmgbeglag@aol.com',
 'phiferc@cox.net',
 'valleymount123@gmail.com',
 'vanessascotto@gmail.com',
 'varvara.janovska@gmail.com',
 'naturalmystic@gmx.net',
 'vazaver@gmail.com',
 'vdjbortz@gmail.com',
 'veganchile368@gmail.com',
 'victoriana77@gmail.com',
 'virginialfarr@gmail.com',
 'Vitadoc767@aol.com',
 'Vitadoc7672001@yahoo.com',
 'vdisch@yahoo.com',
 'vlad.test@mail.ee',
 'w.staten.evans@gmail.com',
 'wanda.reed0@gmail.com',
 'wedowfam718@gmail.com',
 'wellsj@wellsj.com',
 'wendy.c.blackwood@gmail.com',
 'wikingskan@gmail.com',
 'william.j.hanlon@gmail.com',
 'williambhusian@fastmail.com.au',
 'marthasky@juno.com',
 'wintersnasha@gmail.com',
 'woodjustin06@gmail.com',
 'wtaddeo@gmail.com',
 'wwotw1369@gmail.com',
 'xjordont@gmail.com',
 'test@xpansa.com',
 'xspmhghm@gmail.com',
 'xtaforster@gmail.com',
 'yaosheng@gmail.com',
 'yzmina@gmail.com',
 'zenren@gmail.com',
'idrisulas@yahoo.com',
'phantasmagoria143@yahoo.com',
'Kaspars7777@gmail.com',
'zach.h.sherman@gmail.com',
'kissawaymydts@gmail.com',
'julieannelovell@hotmail.com',
'dchamizo@hotmail.com',
'wendyfischer@hotmail.com',
'brider1119@gmail.com',
'yellowdogralph@yahoo.com']

