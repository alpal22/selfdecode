import csv

import numpy
import pandas
from django.core.management import BaseCommand
from django.db import transaction
from django.utils.text import slugify

from decodify.helpers import remaining_time
from genome.models import Snp, SnpStudy, DiseaseTrait, Gene, SnpGenes


def add_diseasetrait(row, study, item, is_mapped):
    item = item.strip()
    disease_trait = DiseaseTrait.objects.filter(name__iexact=item).first()
    if not disease_trait:
        disease_trait = DiseaseTrait.objects.create(
            name=item,
            slug=slugify(item),
            is_mapped=is_mapped
        )
    else:
        disease_trait.is_mapped = True
        disease_trait.save()
    study.diseasestraits.add(disease_trait)


class Command(BaseCommand):
    help = """
    Import all data from external file
    ./manage.py gwas_import --file /www/gwas_catalog_v1.0.1-associations_e88_r2017-04-03.tsv --ensembl /www/ensembl.alleles.csv
    """

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)
        parser.add_argument('--ensembl', type=str)

    def unassign_loc_genes(self):
        genes = Gene.objects.filter(name__istartswith="loc").all()
        count = genes.count()
        print("Unassign %s genes" % count)
        for gene in genes:
            remaining_time(count)
            gene.snps.clear()

    def assign_genes(self):
        print("Assign Genes...")

        data = self.data.copy()

        mapped_gene = data.mapped_gene.str.split(";|,| - | x ").apply(pandas.Series, 1).stack().map(lambda v: v.strip())
        mapped_gene.index = mapped_gene.index.droplevel(-1)
        mapped_gene.name = 'mapped_genes'
        data = data.join(mapped_gene)
        del mapped_gene

        reported_gene = data.reported_gene.str.split(",| x ").apply(pandas.Series, 1).stack().map(lambda v: v.strip())
        reported_gene.index = reported_gene.index.droplevel(-1)
        reported_gene.name = 'reported_genes'
        data = data.join(reported_gene)
        del reported_gene

        data = data.groupby("rsid")

        count = len(data)
        for rsid, rows in data:
            remaining_time(count)
            snp = Snp.objects.filter(rsid=rsid).first()

            if not snp:
                continue

            for index, row in rows.iterrows():
                total_genes = row.mapped_genes
                bool_genes_with_loc = row.mapped_genes.str.lower().str.startswith("loc")
                genes_with_loc = row.mapped_genes[bool_genes_with_loc]
                genes = []
                if total_genes.count() > genes_with_loc.count():
                    genes = row.mapped_genes[bool_genes_with_loc == False]
                elif total_genes.count() == genes_with_loc.count():
                    if row.reported_genes.count() > 0 and row.reported_genes.str.lower().str.startswith(
                            "intergenic").count() == 0:
                        genes = row.reported_genes
                    else:
                        genes = row.mapped_genes

                genes_objs = []
                for gene in genes:
                    obj = Gene.objects.filter(name__iexact=gene).first()
                    if obj:
                        genes_objs.append(obj)

                if not genes_objs:
                    continue

                for gene in genes_objs:
                    if not SnpGenes.objects.filter(snp=snp, gene=gene).exists():
                        SnpGenes.objects.create(
                            snp=snp,
                            gene=gene,
                            excluded=False
                        )

                # snp.genes.add(*genes_objs)

    def update_snps(self, options):
        print("Loading Ensembl csv...")
        endf = pandas.read_csv(options.get('ensembl'), header=0, delimiter=',', quoting=csv.QUOTE_ALL)
        endf.maf = endf.maf.str.replace(r'[^0-9.]', '').str.strip()
        endf = endf.where(pandas.notnull(endf), None)

        endf.allels = endf.allels.replace(r'^\/$', '', regex=True)

        print("Update SNPs...")
        count = len(endf)
        for index, row in endf.iterrows():
            remaining_time(count)
            snp = Snp.objects.filter(rsid=row.rsid).first()

            if not snp:
                Snp.objects.create(
                    rsid=row.rsid,
                    minor_allele=row.maf_sym if row.maf_sym else None,
                    minor_allele_frequency=row.maf if row.maf else None,
                    ambiguity_code=row.ambiguity if row.ambiguity else None,
                    alleles=row.allels if row.allels else None
                )
            else:
                snp.minor_allele = row.maf_sym if row.maf_sym and not snp.minor_allele else None
                snp.minor_allele_frequency = row.maf if row.maf and not snp.minor_allele_frequency else None
                snp.ambiguity_code = row.ambiguity if row.ambiguity and not snp.ambiguity_code else None
                snp.alleles = row.allels if row.allels and not snp.alleles else None
                snp.save()

    def import_snps(self):
        print("Create SNPs...")
        data = self.data.copy()

        data.snp_gene_ids = data.snp_gene_ids.str.split(";|,| - | x ")

        data = data.groupby("rsid")

        count = len(data)

        for rsid, rows in data:
            remaining_time(count)
            snp = Snp.objects.filter(rsid=rsid).first()
            snp_row = rows.iloc[0]

            gene_ids = ','.join(list(snp_row.snp_gene_ids))

            if not snp:
                Snp.objects.create(
                    rsid=rsid,
                    name=rsid,
                    code=rsid,
                    heterozygous_color="yellow",
                    homozygous_major_color="green",
                    homozygous_minor_color="red",
                    red_flag=False,
                    chr_id=snp_row.chrom_id if snp_row.chrom_id else None,
                    chr_pos=snp_row.chrom_pos if snp_row.chrom_pos else None,
                    gene_ids=gene_ids,
                    upstream_gene_id=snp_row.upstream_gene_id if snp_row.upstream_gene_id else None,
                    downstream_gene_id=snp_row.downstream_gene_id if snp_row.downstream_gene_id else None,
                    upstream_gene_distance=snp_row.upstream_gene_dist if snp_row.upstream_gene_dist else None,
                    downstream_gene_distance=snp_row.downstream_gene_dist if snp_row.downstream_gene_dist else None,
                    merged=snp_row.merged,
                    snp_id_current=snp_row.snp_id_current if snp_row.snp_id_current else None,
                )
            else:
                snp.chr_id = snp_row.chrom_id if snp_row.chrom_id else None
                snp.chr_pos = snp_row.chrom_pos if snp_row.chrom_pos else None
                snp.gene_ids = gene_ids
                snp.upstream_gene_id = snp_row.upstream_gene_id if snp_row.upstream_gene_id else None
                snp.downstream_gene_id = snp_row.downstream_gene_id if snp_row.downstream_gene_id else None
                snp.upstream_gene_distance = snp_row.upstream_gene_dist if snp_row.upstream_gene_dist else None
                snp.downstream_gene_distance = snp_row.downstream_gene_dist if snp_row.downstream_gene_dist else None
                snp.merged = snp_row.merged if snp_row.merged else None
                snp.snp_id_current = snp_row.snp_id_current if snp_row.snp_id_current else None
                snp.save()

    def import_studies(self):
        print("Create Studies...")
        data = self.data.copy()

        context = data.context.str.split(';').apply(pandas.Series, 1).stack()
        context = context.index.droplevel(-1)
        context.name = "cleaned_context"

        data = data.join(context)

        data = data.groupby("rsid")

        count = len(data)
        for rsid, rows in data:
            remaining_time(count)
            snp = Snp.objects.filter(rsid=rsid).first()

            if not snp:
                continue

            for index, row in rows.iterrows():
                study = SnpStudy.objects.filter(
                    snp=snp,
                    ci_text=row.ci,
                ).first()

                if not study:
                    study = SnpStudy.objects.create(
                        snp=snp,
                        pubmed_id=row.pubmedid if row.pubmedid else None,
                        first_author=row.author,
                        odds_ratio=row.beta if row.beta else None,
                        risk_allele=row.risk_allele.upper() if row.risk_allele and len(
                            row.risk_allele) == 1 and row.risk_allele != "?" else None,
                        risk_allele_frequency=row.risk_allele_frequency if row.risk_allele_frequency else None,
                        ci_text=row.ci,
                        p_value=row.p if row.p else None,
                        p_value_mlog=row.p_mlog if row.p_mlog else None,
                        context=row.cleaned_context,
                        study=row.study,
                        initial_sample_size=row.initial_sample_size,
                        replication_sample_size=row.replication_sample_size,
                        journal=row.journal,
                        journal_published_at=row.date if row.date else None,
                        accession=row.study_accession,
                        p_value_text=row.p_text,
                        region=row.region,
                        intergenic=row.intergenic if row.intergenic else None,
                        strongest_snp_risk_allele=row.strongest_snp_allele,
                        platform_qc=row.platform,
                        cnv=row.cnv,
                    )
                else:
                    study.pubmed_id = row.pubmedid if row.pubmedid else None
                    study.first_author = row.author
                    study.odds_ratio = row.beta if row.beta else None
                    study.risk_allele = row.risk_allele.upper() if row.risk_allele and len(row.risk_allele) == 1 and row.risk_allele != "?" else None
                    study.risk_allele_frequency = row.risk_allele_frequency if row.risk_allele_frequency else None
                    study.ci_text = row.ci
                    study.p_value = row.p if row.p else None
                    study.p_value_mlog = row.p_mlog if row.p_mlog else None
                    study.context = row.cleaned_context
                    study.study = row.study
                    study.initial_sample_size = row.initial_sample_size
                    study.replication_sample_size = row.replication_sample_size
                    study.journal = row.journal
                    study.journal_published_at = row.date if row.date else None
                    study.accession = row.study_accession
                    study.p_value_text = row.p_text
                    study.region = row.region
                    study.intergenic = row.intergenic if row.intergenic else None
                    study.strongest_snp_risk_allele = row.strongest_snp_allele
                    study.platform_qc = row.platform
                    study.cnv = row.cnv
                    study.save()

    def assign_diseases(self):
        print("Assign Diseases...")
        data = self.data.copy()

        data = data.groupby("rsid")

        count = len(data)
        for rsid, rows in data:
            remaining_time(count)
            snp = Snp.objects.filter(rsid=rsid).first()

            if not snp:
                continue

            for index, row in rows.iterrows():
                study = SnpStudy.objects.filter(
                    snp=snp,
                    ci_text=row.ci,
                ).first()

                if not study:
                    continue

                for item in row.mapped_trait.split(','):
                    add_diseasetrait(row, study, item, is_mapped=True)

                for item in row.disease_trait.split(','):
                    add_diseasetrait(row, study, item, is_mapped=False)

    def handle(self, *args, **options):
        print("Loading GWAS csv...")
        data = pandas.read_csv(options.get('file'), header=0, delimiter="\t", quoting=csv.QUOTE_ALL)

        data.columns = [
            "date_added", "pubmedid", "author", "date", "journal", "link", "study", "disease_trait",
            "initial_sample_size",
            "replication_sample_size", "region", "chr_id", "chr_pos", "reported_gene", "mapped_gene",
            "upstream_gene_id",
            "downstream_gene_id", "snp_gene_ids", "upstream_gene_dist", "downstream_gene_dist", "strongest_snp_allele",
            "snps", "merged", "snp_id_current", "context", "intergenic", "risk_allele_frequency", "p", "p_mlog",
            "p_text",
            "beta", "ci", "platform", "cnv", "mapped_trait", "mapped_trait_uri", "study_accession"
        ]

        snps = data.strongest_snp_allele.str.split(';| x ').apply(pandas.Series, 1).stack().map(lambda v: v.strip())
        snps.name = 'snp_allele'
        chr_ids = data.chr_id.str.split(";").apply(pandas.Series).stack()
        chr_ids.name = "chrom_id"
        chr_pos = data.chr_pos.str.split(";").apply(pandas.Series).stack()
        chr_pos.name = "chrom_pos"

        plain_data = pandas.DataFrame(snps)
        plain_data = plain_data.join(chr_ids)
        plain_data = plain_data.join(chr_pos)

        plain_data = plain_data.reset_index(level=1, drop=True)

        data = data.join(plain_data)

        data = data[["snp_allele", "pubmedid", "author", "date", "journal", "link", "study", "initial_sample_size",
                     "replication_sample_size", "reported_gene", "mapped_gene",
                     "context", "risk_allele_frequency", "p", "p_mlog", "beta", "ci",
                     "platform", "mapped_trait", "study_accession", "disease_trait",
                     "strongest_snp_allele", "snp_gene_ids", "p_text", "cnv", "intergenic", "region",
                     "snp_id_current", "merged", "downstream_gene_dist", "upstream_gene_dist",
                     "downstream_gene_id", "upstream_gene_id", "chrom_pos", "chrom_id"]]

        data = data.replace(numpy.nan, "", regex=True)
        data = data.replace("NR", "", regex=True)

        data.beta = data.beta.str.replace(r'.*([0-9]+\.[0-9]+).*', '\\1').str.strip()
        data.risk_allele_frequency = data.risk_allele_frequency.str.replace(r'.*([0-9]+\.[0-9]+).*', '\\1').str.strip()
        data.p = data.p.str.replace(r'.*([0-9]+\.[0-9]+).*', '\\1').str.strip()
        # data.p_mlog = data.p_mlog.str.replace(r'.*([0-9]+\.[0-9]+).*', '\\1').str.strip()
        data.snp_allele = data.snp_allele.str.replace(r'[-]{2,}', '-')

        rsid = data.snp_allele.str.split("-").apply(pandas.Series, 1).stack().map(lambda v: v.strip())
        rsid.index = rsid.index.droplevel(-1)
        rsid.name = 'rsid'

        # Filter rows that don't starts with "rs*"
        data = data[data.snp_allele.str.startswith("rs")]

        rsid_allele = data.snp_allele.str.split("-").apply(pandas.Series, 2)
        rsid_allele.columns = ["rsid", "risk_allele"]

        data = data.join(rsid_allele)

        data = data.replace(numpy.nan, "", regex=True)
        data = data.replace("NR", "", regex=True)

        self.data = data

        with transaction.atomic():
            self.import_snps()
            self.update_snps(options)
            # self.import_studies()
            # self.assign_genes()
            # raise Exception("Stop script")
            # self.assign_diseases()
        print("Done")
