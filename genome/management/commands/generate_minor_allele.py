from django.core.management import BaseCommand
from django.db.models import Q

from decodify.helpers import remaining_time
from genome.models import Snp


class Command(BaseCommand):

    def handle(self, *args, **options):
        snps = Snp.objects.filter(Q(minor_allele='') | Q(minor_allele__isnull=True)).prefetch_related("population_genomes")
        count = snps.count()
        for snp in snps:
            remaining_time(count)
            population = snp.population_genomes.filter(abbr="ALL").first()
            if not population or not population.group:
                continue
            groups = population.group.split(';')
            alleles = {}
            for group in groups:
                print(group)
                allele, frequency = group.split(':')
                alleles[allele] = float(frequency)

            minor_allele = min(alleles, key=lambda k: alleles[k])
            minor_allele_frequency = alleles[minor_allele]

            if len(alleles) > 2:
                del alleles[minor_allele]
                minor_allele = min(alleles, key=lambda k: alleles[k])
                minor_allele_frequency = alleles[minor_allele]

            snp.minor_allele = minor_allele
            snp.minor_allele_frequency = minor_allele_frequency
            snp.save()
