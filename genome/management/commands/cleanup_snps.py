from django.core.management import BaseCommand
from django_pandas.io import read_frame

from decodify.helpers import remaining_time
from genome.models import Snp


class Command(BaseCommand):

    def handle(self, *args, **options):
        data = read_frame(Snp.objects.filter(description_advanced__iregex=r'<h2>Rs.*?is associated with the following:</h2>.*'))
        data.description_advanced = data.description_advanced.str.replace(r'<h2>Rs.*?is associated with the following:</h2>[\W\w]+', '')
        count = len(data)
        for index, item in data.iterrows():
            remaining_time(count)
            Snp.objects.filter(pk=item.id).update(description_advanced=item.description_advanced)
