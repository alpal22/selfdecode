import re

from django.core.management import BaseCommand
from django.db import transaction

from decodify.helpers import remaining_time
from genome.models import Gene, Snp, SnpAllele


def process_field(data):
    if data:
        return re.sub(r'<a[\w\"= ]+href=\"\/index\.php[\/\w?&;=\(\)%\'#]+\"[\w\"\=\ \(\)\%\'\#&]+>([\w;\ \(\)\%\'\#&]+)<\/a>+', '\g<1>', data)
    else:
        return data


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--model', type=str)

    def process_genes(self):
        genes = Gene.objects.all()
        cnt = genes.count()
        with transaction.atomic():
            for gene in genes:
                remaining_time(cnt)
                gene.description_advanced = process_field(gene.description_advanced)
                gene.description_simple = process_field(gene.description_simple)
                gene.fix_advanced = process_field(gene.fix_advanced)
                gene.fix_simple = process_field(gene.fix_simple)
                gene.uniprot_pharmacological_use = process_field(gene.uniprot_pharmacological_use)
                gene.ghr_function = process_field(gene.ghr_function)
                gene.save()
            print("Finished processing Genes")

    def process_snps(self):
        snps = Snp.objects.all()
        cnt = snps.count()
        with transaction.atomic():
            for snp in snps:
                remaining_time(cnt)
                snp.description_advanced = process_field(snp.description_advanced)
                snp.description_simple = process_field(snp.description_simple)
                snp.save()
            print("Finished processing SNPs")

    def process_snp_alleles(self):
        alleles = SnpAllele.objects.all()
        cnt = alleles.count()
        with transaction.atomic():
            for allele in alleles:
                remaining_time(cnt)
                allele.description_minor = process_field(allele.description_minor)
                allele.description_hetero = process_field(allele.description_hetero)
                allele.description_major = process_field(allele.description_major)
                allele.fix_minor = process_field(allele.fix_minor)
                allele.fix_major = process_field(allele.fix_major)
                allele.fix_hetero = process_field(allele.fix_hetero)
                allele.save()
            print("Finished processing SNP Alleles")

    def handle(self, *args, **options):
        associations = {
            "gene": self.process_genes,
            "snp": self.process_snps,
            "snp_allele": self.process_snp_alleles
        }
        associations.get(options.get("model"))()

