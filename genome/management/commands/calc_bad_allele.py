from django.core.management import BaseCommand

from decodify.helpers import remaining_time
from genome.models import Snp


class Command(BaseCommand):
    def handle(self, *args, **options):
        snps = Snp.objects.filter(studies__isnull=False).prefetch_related(
            "studies"
        ).all()

        count = snps.count()

        for snp in snps:
            remaining_time(count)
            alleles = list(snp.studies.values_list("risk_allele", flat=True))
            alleles += [snp.minor_allele]
            # Filter out any None or empty values
            alleles = list(filter(lambda v: v, alleles))
            # Get more common risk allele
            if len(alleles) > 0:
                bad_allele = max(set(alleles), key=alleles.count)
                min_allele = min(set(alleles), key=alleles.count)
                if bad_allele == min_allele:
                    bad_allele = alleles[0]
            else:
                bad_allele = None

            snp.bad_allele = bad_allele
            snp.save()
