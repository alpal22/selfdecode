import csv

import pandas
from django.core.management.base import BaseCommand
from django.utils.text import slugify

from genome.models import Gene, TranscriptionFactor


class Command(BaseCommand):
    help = "Import Transcription from external file"

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)

    def handle(self, *args, **options):
        data = pandas.read_csv(options.get('file'), header=0, quoting=csv.QUOTE_ALL, delimiter=',')
        data.columns = ['gene', 'url', 'image', 'factors']
        i = 0
        factors = data.factors.str.split(',').to_frame()

        data = data.join(factors, rsuffix="_list")
        for index, item in data.iterrows():
            if str(item.factors_list) == "nan":
                continue
            gene = Gene.objects.filter(name=item.gene).first()
            if not gene:
                continue
            for factor in item.factors_list:
                factor = factor.strip()
                slug = slugify(factor)
                transcription = TranscriptionFactor.objects.filter(slug=slug).first()
                if not transcription:
                    transcription = TranscriptionFactor.objects.create(
                        name=factor,
                        slug=slug,
                        ref_url=item.url.strip()
                    )
                    transcription.save()
                gene.transcription_factors.add(transcription.pk)
            i += 1
            print("%s: Update gene '%s'" % (i, gene,))
            gene.save()
        print("Done import")
