import glob
import os

import pandas
import re

from django.utils.text import slugify
from django_pandas.io import read_frame
# from lxml import objectify

from django.core.management import BaseCommand

from genome.models import Gene, GeneKeyword, UniprotCategory, UniprotData

cleaner = re.compile(r'^[PF]\:')


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def purify_attr(value):
    return cleaner.sub('', str(value.get("value")))


class Command(BaseCommand):
    help = "Import all data from external file"

    def add_arguments(self, parser):
        parser.add_argument('--dir', type=str)

    def handle(self, *args, **options):
        os.chdir(options.get('dir'))
        genes = read_frame(Gene.objects.values("id", "name"))
        genes.columns = ["gene_pk", "gene"]

        molecular_function_obj, _ = UniprotCategory.objects.get_or_create(name="Molecular function", slug=slugify("Molecular function"))
        biological_processes_obj, _ = UniprotCategory.objects.get_or_create(name="Biological processes", slug=slugify("Biological processes"))
        drug_bank_obj, _ = UniprotCategory.objects.get_or_create(name="Drug bank", slug=slugify("Drug bank"))
        i = 0
        for files in chunks(glob.glob("*.xml"), 10):
            rows = []
            for file in files:
                i += 1
                print("%s: %s" % (i, file,))
                with open(file) as f:
                    xml = objectify.parse(f)
                    root = xml.getroot()
                    if not hasattr(root, "gene"):
                        continue

                    gene_name = str(root.gene.name).lower().strip()
                    function = root.cssselect('comment[type="function"] text')
                    molecular = [purify_attr(v) for v in root.cssselect('property[value^="F:"]')]
                    biological = [purify_attr(v) for v in root.cssselect('property[value^="P:"]')]
                    keywords = [v.text for v in root.findall('keyword')]
                    path_ways = [purify_attr(v) for v in root.cssselect('dbReference[type="Reactome"] property')]
                    alt_names = [alt_name.fullName for alt_name in root.protein.findall('alternativeName')]
                    subcellular = root.cssselect('comment[type="subcellular location"]')
                    drug_bank = [v.get("value") for v in root.cssselect('dbReference[type="DrugBank"] property')]
                    expression_tissue = [v for v in root.cssselect('comment[type="tissue specificity"] text')]
                    expression_induction = [v for v in root.cssselect('comment[type="induction"] text')]
                    miscellaneous = [v for v in root.cssselect('comment[type="miscellaneous"] text')]
                    developmental = [v for v in root.cssselect('comment[type="developmental stage"] text')]
                    caution = [v for v in root.cssselect('comment[type="caution"] text')]
                    enzyme_regulation = [v for v in root.cssselect('comment[type="enzyme regulation"] text')]
                    cofactor = [v for v in root.cssselect('comment[type="cofactor"] text')]

                    function = function.pop().text if len(function) > 0 else ""

                    subcellular = subcellular.pop().findall('subcellularLocation') if len(subcellular) > 0 else []

                    rows.append([
                        gene_name, function, molecular, biological, keywords, path_ways,
                        alt_names, ["; ".join([str(sub.location if hasattr(sub, "location") else ""),
                                               str(sub.topology if hasattr(sub, "topology") else "")]
                                              ) for sub in subcellular], drug_bank, expression_tissue,
                        expression_induction, miscellaneous, developmental, caution, enzyme_regulation, cofactor
                    ])

            df = pandas.DataFrame(rows, columns=(
                'gene', 'function', 'molecular_function', 'biological_processes', 'keywords', 'pathways', 'alt_names',
                'subcellular', 'drug_bank', 'expression_tissue', 'expression_induction', "miscellaneous",
                "developmental", "caution", "enzyme_regulation", "cofactor",
            ))
            df = df.merge(genes, on="gene", how="left")
            # Get fields with gene_pk only
            df = df[pandas.notnull(df['gene_pk'])]

            for index, row in df.iterrows():
                gene = Gene.objects.get(pk=row.gene_pk)
                gene.function = row.function

                for keyword in row.keywords:
                    kw, _ = GeneKeyword.objects.get_or_create(name=keyword, slug=slugify(keyword))
                    gene.keywords.add(kw.pk)

                for value in row.molecular_function:
                    UniprotData.objects.get_or_create(category_id=molecular_function_obj.pk, gene=gene, value=value)

                for value in row.biological_processes:
                    UniprotData.objects.get_or_create(category_id=biological_processes_obj.pk, gene=gene, value=value)

                for value in row.drug_bank:
                    UniprotData.objects.get_or_create(category_id=drug_bank_obj.pk, gene=gene, value=value)

                for value in row.expression_tissue:
                    gene.tissue_specificity = value

                for value in row.expression_induction:
                    gene.induction = value

                for value in row.miscellaneous:
                    gene.miscellaneous = value

                for value in row.developmental:
                    gene.developmental_stage = value

                for value in row.caution:
                    gene.caution = value

                for value in row.enzyme_regulation:
                    gene.enzyme_regulation = value

                for value in row.cofactor:
                    gene.cofactor = value

                gene.save()
