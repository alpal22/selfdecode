import csv

import pandas
from django.core.management import BaseCommand
from django_pandas.io import read_frame

from genome.models import Snp, PopulationGenome


class Command(BaseCommand):
    help = "Import all data from external file"

    def add_arguments(self, parser):
        parser.add_argument('--file-allele', type=str)
        parser.add_argument('--file-genetic', type=str)

    def _import_alleles(self, options):
        print("Start update alleles")
        allele = pandas.read_csv(options.get('file_allele'), header=0, delimiter=',', quoting=csv.QUOTE_ALL)
        allele.columns = ["id", "rsid", "url", "ancestral", "maf"]

        genetic = pandas.read_csv(options.get('file_genetic'), header=0, delimiter=',', quoting=csv.QUOTE_ALL)
        genetic.columns = ["id", "rsid", "url", "title", "abbr", "group", "group2"]

        allele.maf = allele.maf.str.replace('[^0-9.]', '').str.strip()
        allele.ancestral = allele.ancestral.str.strip().str.replace('"', '')
        allele.rsid = allele.rsid.str.strip()

        snps = Snp.objects.all()
        index = 0
        for snp in snps:
            rows = allele.ix[allele.rsid == snp.rsid]
            for i, row in rows.iterrows():
                snp.ancestral_allele = row.ancestral
                snp.minor_allele_frequency = row.maf
                snp.save()
                index += 1
                print("%s: %s" % (index, snp.rsid,))

    def handle(self, *args, **options):
        self._import_alleles(options)
        print("Start update genomes")

        genetic = pandas.read_csv(options.get('file_genetic'), header=0, delimiter=',', quoting=csv.QUOTE_ALL)
        genetic.columns = ["id", "rsid", "url", "title", "abbr", "group", "group2"]

        snps = read_frame(Snp.objects.values_list("rsid", "id"))
        genetic = genetic.merge(snps, on="rsid", how="left")

        genetic.title = genetic.title.str.replace('"', '')
        genetic.abbr = genetic.abbr.str.replace('"', '')
        genetic.group = genetic.group.str.replace('"', '')
        genetic.group2 = genetic.group2.str.replace('"', '')

        groups = genetic.groupby('rsid').groups
        to_create = []
        i = 0
        for rsid, indexes in groups.items():
            for index in indexes:
                row = genetic.ix[index]
                to_create.append(PopulationGenome(
                    snp_id=row.id_y,
                    title=row.title,
                    abbr=row.abbr,
                    group=row.group,
                    group2=row.group2
                ))
            i += 1
            print("%s: %s" % (i, rsid,))

        PopulationGenome.objects.bulk_create(to_create)

        print("Done")
