import pandas

from django.core.management import BaseCommand
from decodify.helpers import remaining_time

from genome.models import DiseaseTrait


class Command(BaseCommand):
    """
    To run the command, type: ./manage.py import_EBI_data --file=data5.csv
    """
    help = "Import EBI data from data5.xlsx to DiseaseTrait model"

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)

    def handle(self, *args, **options):
        data = pandas.read_csv(options.get('file'), sep='\t')
        data.columns = data.columns.str.lower()

        diseases = data.mapped_trait.str.split(',').apply(pandas.Series, 1).stack().map(lambda v: v.strip()).str.lower()
        diseases.index = diseases.index.droplevel(-1)
        diseases.name = 'diseases'

        urls = data.uris.str.split(',').apply(pandas.Series, 1).stack().map(lambda v: v.strip())
        urls.index = urls.index.droplevel(-1)
        urls.name = 'urls'

        data = data.join(diseases)
        data = data.drop(['mapped_trait', 'uris'], axis=1)
        dis_urls = pandas.concat([diseases.reset_index(drop=True), urls.reset_index(drop=True)], axis=1)
        res = pandas.merge(data, dis_urls, on='diseases').drop_duplicates()
        res = res.fillna(value='')

        count = len(res)

        for index, row in res.iterrows():
            remaining_time(count)
            try:
                obj = DiseaseTrait.objects.get(name=row['diseases'])
            except Exception as e:
                print(row['diseases'], '  DOES NOT EXISTS  --> ', e)
                continue
            if not obj.definition:
                obj.definition = row['definitions']
            obj.ref_url = row['urls']
            obj.save()

        print('Done!')
