import re

from django.core.management import BaseCommand
from django.db.models import Q

from genome.models import Gene, Snp


class Command(BaseCommand):
    def handle(self, *args, **options):
        domain = 'snpedia.com'
        regexp = re.compile(r'\<a[^<]*href="https?:\/\/((?:www.)?' + domain + ').+?\>(.+?)\<\/a\>')

        classes = {
            Gene: ('description_advanced', 'description_simple', 'fix_advanced', 'fix_simple',),
            Snp: ('',)
        }

        for model, fields in classes.items():
            qs = Q()
            for field in fields:
                qs |= Q(**{
                    field + '__icontains': domain
                })
            queryset = model.objects.filter(qs).all()

            for obj in queryset:
                print(obj.pk, obj)
                import pdb
                pdb.set_trace()
