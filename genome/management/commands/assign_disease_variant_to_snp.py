from django.core.management import BaseCommand
from django.db import transaction
from django.db.models import Max

from decodify.helpers import remaining_time
from genome.models import Snp, DiseaseVariant


class Command(BaseCommand):
    help = "Generate importance field for SNPs"

    def handle(self, *args, **options):
        snps = Snp.objects.all()
        count = snps.count()
        with transaction.atomic():
            for snp in snps:
                remaining_time(count)
                disease_variants = DiseaseVariant.objects.filter(rsid=snp.rsid)
                if disease_variants.exists():
                    snp.disease_variant.add(*list(disease_variants.all()))
        print("Done")
