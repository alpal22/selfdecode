from django.core.management import BaseCommand
from decodify.helpers import remaining_time

from genome.models import PopulationGenome


class Command(BaseCommand):
    help = "Split group2 table in PopulationGenome mode"

    def handle(self, *args, **options):
        all_items = PopulationGenome.objects.select_related().all()
        cnt = len(all_items)
        for item in all_items:
            remaining_time(cnt)
            group2 = item.group2.split(';')

            if item.snp.major_allele or item.snp.minor_allele:
                for group in group2:
                    splited_group = group.split(':')
                    if len(group2) > 3:
                        continue
                    if item.snp.minor_allele and not item.snp.major_allele:
                        if group.count(item.snp.minor_allele) == 2:
                            try:
                                item.homozygous_minor = splited_group[0]
                                item.homozygous_minor_freq = splited_group[1]
                            except IndexError:
                                item.homozygous_minor = ''
                                item.homozygous_minor_freq = 0
                        elif group.count(item.snp.minor_allele) == 1:
                            try:
                                item.heterozygous = splited_group[0]
                                item.heterozygous_freq = splited_group[1]
                            except IndexError:
                                item.heterozygous = ''
                                item.heterozygous_freq = 0
                        elif group.count(item.snp.minor_allele) == 0:
                            try:
                                item.homozygous_major = splited_group[0]
                                item.homozygous_major_freq = splited_group[1]
                            except IndexError:
                                item.homozygous_major = ''
                                item.homozygous_major_freq = 0

                    elif item.snp.major_allele and not item.snp.minor_allele:
                        if group.count(item.snp.major_allele) == 2:
                            try:
                                item.homozygous_major = splited_group[0]
                                item.homozygous_major_freq = splited_group[1]
                            except IndexError:
                                item.homozygous_minor = ''
                                item.homozygous_minor_freq = 0
                        elif group.count(item.snp.major_allele) == 1:
                            try:
                                item.heterozygous = splited_group[0]
                                item.heterozygous_freq = splited_group[1]
                            except IndexError:
                                item.heterozygous = ''
                                item.heterozygous_freq = 0
                        elif group.count(item.snp.major_allele) == 0:
                            try:
                                item.homozygous_minor = splited_group[0]
                                item.homozygous_minor_freq = splited_group[1]
                            except IndexError:
                                item.homozygous_minor = ''
                                item.homozygous_minor_freq = 0
                    else:
                        if group.count(item.snp.minor_allele) == 2:
                            try:
                                item.homozygous_minor = splited_group[0]
                                item.homozygous_minor_freq = splited_group[1]
                            except IndexError:
                                item.homozygous_minor = ''
                                item.homozygous_minor_freq = 0
                        elif group.count(item.snp.major_allele) == 2:
                            try:
                                item.homozygous_major = splited_group[0]
                                item.homozygous_major_freq = splited_group[1]
                            except IndexError:
                                item.homozygous_major = ''
                                item.homozygous_major_freq = 0
                        elif group.count(item.snp.major_allele) == 1 or group.count(item.snp.minor_allele) == 1:
                            try:
                                item.heterozygous = splited_group[0]
                                item.heterozygous_freq = splited_group[1]
                            except IndexError:
                                item.heterozygous = ''
                                item.heterozygous_freq = 0
                item.save()
