from django.core.management import BaseCommand
from django_pandas.io import read_frame

from decodify.helpers import remaining_time
from genome.models import Snp, GeneOntology, UniprotData


class Command(BaseCommand):

    def handle(self, *args, **options):
        gene_ontologys = dict(GeneOntology.objects.values_list("go_name", "id"))
        uniprot_data = UniprotData.objects.filter(category_id__in=(1, 2)).prefetch_related("gene").all()
        count = uniprot_data.count()
        for row in uniprot_data:
            remaining_time(count)
            go = gene_ontologys.get(row.value)
            if go:
                row.gene.go.add(go)
            else:
                go = GeneOntology.objects.create(
                    go_name=row.value,
                    type=row.category.slug
                )
                gene_ontologys[go.go_name] = go.id
                row.gene.go.add(go)
