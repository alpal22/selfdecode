import pandas
from django.core.management import BaseCommand

from decodify.helpers import remaining_time
from genome.models import DiseaseTraitCategory, Gene, DiseaseTrait, GeneDiseaseTrait


class Command(BaseCommand):
    help = "Import all data from external file all_gene_disease_associations.tsv"

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)

    def _parse_file(self):
        self.data = pandas.read_csv(self.options.get("file"), comment='#', header=0, delimiter="\t")
        self.data.geneName = self.data.geneName.str.strip().str.lower()
        self.data.diseaseName = self.data.diseaseName.str.strip().str.lower()

    def _import_diseases(self):
        print("Start import genes diseases associations")

        disease_category = DiseaseTraitCategory.objects.get(name="Disease")
        count = len(self.data)

        genes = {gene.name.lower(): gene for gene in Gene.objects.all()}
        diseases = {disease.name.lower(): disease for disease in DiseaseTrait.objects.all()}

        for index, row in self.data.iterrows():
            remaining_time(count)
            # gene = Gene.objects.filter(name__iexact=row.geneName).first()
            gene = genes.get(row.geneName.lower())
            # disease = DiseaseTrait.objects.filter(name__iexact=row.diseaseName).first()
            disease = diseases.get(row.diseaseName.lower())
            if not gene:
                continue
            if not disease:
                disease = DiseaseTrait.objects.create(name=row.diseaseName, category=disease_category)
                diseases[disease.name.lower()] = disease
            gene.disgenet_geneid = row.geneId
            disease.disgenet_diseaseid = row.diseaseId
            gene.save()
            disease.save()
            genedisease = GeneDiseaseTrait.objects.filter(
                gene=gene,
                diseasetrait=disease
            ).first()
            if not genedisease:
                GeneDiseaseTrait.objects.create(
                    gene=gene,
                    diseasetrait=disease,
                    description=row.description,
                    sources=row.sources,
                    score=row.score,
                    nofpmids=row.NofPmids,
                    nofsnps=row.NofSnps
                )
            else:
                genedisease.description = row.description
                genedisease.sources = row.sources
                genedisease.score = row.score
                genedisease.nofpmids = row.NofPmids
                genedisease.nofsnps = row.NofSnps
                genedisease.save()

        print("Done")

    def handle(self, *args, **options):
        self.options = options

        self._parse_file()
        self._import_diseases()
