from django.core.management import BaseCommand

from chemical.models import Chemical
from decodify.helpers import remaining_time
from genome.models import UniprotData


class Command(BaseCommand):
    def handle(self, *args, **options):
        items = UniprotData.objects.filter(category__slug="drug-bank").all()
        chemicals = {c.name.lower(): c for c in Chemical.objects.all()}
        count = items.count()
        for item in items:
            remaining_time(count)
            chemical = chemicals.get(item.value.lower())
            if not chemical:
                continue
            item.chemical = chemical
            item.save()
