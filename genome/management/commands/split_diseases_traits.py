import pandas
from django.core.management.base import BaseCommand

from genome.models import DiseaseTrait, DiseaseTraitCategory


class Command(BaseCommand):
    help = "Split diseases/traits (CTD_diseases.csv.gz)"

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)

    def handle(self, *args, **options):
        data = pandas.read_csv(options.get('file'), header=0, delimiter=',')
        data.DiseaseName = data.DiseaseName.str.lower().str.strip()
        synonyms = data.Synonyms.str.split("\|").apply(pandas.Series, 1).stack().map(lambda v: v.lower().strip())
        synonyms.index = synonyms.index.droplevel(-1)
        synonyms.name = 'synonym'
        data = data.join(synonyms)

        disease_cat = DiseaseTraitCategory.objects.filter(name="Disease").first()
        trait_cat = DiseaseTraitCategory.objects.filter(name="Trait").first()
        measurement_cat = DiseaseTraitCategory.objects.filter(name="Measurement").first()

        diseases_traits = DiseaseTrait.objects.all()
        i = 0
        cnt = diseases_traits.count()
        for dt in diseases_traits:
            i += 1
            if not data[data.DiseaseName == dt.name].empty or not data[data.synonym == dt.name].empty:
                print("%s/%s: Disease: %s" % (i, cnt, dt.name,))
                dt.category = disease_cat
            elif "measurement" in dt.name:
                print("%s/%s: Trait: %s" % (i, cnt, dt.name,))
                dt.category = measurement_cat
            else:
                print("%s/%s: Trait: %s" % (i, cnt, dt.name,))
                dt.category = trait_cat
            dt.save()
        print("Done.")
