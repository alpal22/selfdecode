import re, copy

from django.core.management import BaseCommand
from django.db import transaction

from decodify.helpers import remaining_time
from genome.models import Gene, Snp, SnpAllele


def process_field(data):
    if data:
        return re.sub(r'\<a[^<]*href=\"https?:\/\/(?:www\.)?amazon\.com.+?>(.+?)<\/a>', '\g<1>', data)
    else:
        return data


def instance_different(first, last, fields):
    # result = True
    for field in fields:
        if getattr(first, field) != getattr(last, field):
            return True
    return False


def process_instance(instance, fields):
    for field in fields:
        setattr(instance, field, process_field(getattr(instance, field)))
    return instance


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--model', type=str)

    def process_genes(self):
        genes = Gene.objects.all()
        cnt = genes.count()
        fields = ("description_advanced", "description_simple", "fix_advanced", "fix_simple", "uniprot_pharmacological_use", "ghr_function")
        changed = []
        with transaction.atomic():
            for gene in genes:
                checker = copy.copy(gene)
                remaining_time(cnt)
                process_instance(gene, fields)
                gene.save()
                if instance_different(checker, gene, fields):
                    changed.append(gene.id)
            print("Finished processing Genes")
            print(changed)

    def process_snps(self):
        snps = Snp.objects.all()
        cnt = snps.count()
        fields = ("description_advanced", "description_simple")
        changed = []
        with transaction.atomic():
            for snp in snps:
                checker = copy.copy(snp)
                remaining_time(cnt)
                process_instance(snp, fields)
                snp.save()
                if instance_different(checker, snp, fields):
                    changed.append(snp.id)
            print("Finished processing SNPs")
            print(changed)

    def process_snp_alleles(self):
        alleles = SnpAllele.objects.all()
        cnt = alleles.count()
        fields = ("description_minor", "description_hetero", "description_major", "fix_minor", "fix_major", "fix_hetero")
        changed = []
        with transaction.atomic():
            for allele in alleles:
                checker = copy.copy(allele)
                remaining_time(cnt)
                allele.save()
                process_instance(allele, fields)
                if instance_different(checker, allele, fields):
                    changed.append(allele.id)
            print("Finished processing SNP Alleles")
            print(changed)

    def handle(self, *args, **options):
        associations = {
            "gene": self.process_genes,
            "snp": self.process_snps,
            "snp_allele": self.process_snp_alleles
        }
        associations.get(options.get("model"))()

