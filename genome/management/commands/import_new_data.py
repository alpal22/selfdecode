import csv
import re

import numpy
import pandas
import numpy as np
from django.core.management import BaseCommand
from django.db import transaction

from phpserialize import dict_to_list, loads, load
from six import StringIO

from decodify.helpers import remaining_time
from genome.models import DiseaseTrait, DiseaseTraitCategory, Gene, Pathology, GeneOntology, DiseaseSymptom, \
    DiseaseSymptomInteraction, DiseaseVariant

go_objects = dict(GeneOntology.objects.values_list("go_id", "id"))


def parse_go_item(item):
    item = item[:-1]
    item_list = item.split("[")
    return {"go_id": item_list[1], "go_name": item_list[0].strip()}


def parse_go_row(data_str):
    data_list = data_str.split(";")
    data_dict = [parse_go_item(record) for record in data_list]
    return data_dict


def assign_gene_go(gene, go_row, go_type):
    go_parsed = parse_go_row(go_row)
    for go in go_parsed:
        go_obj = go_objects.get(go["go_id"])
        if go_obj:
            gene.go.add(go_obj)
        else:
            go_obj = GeneOntology.objects.create(
                go_name=go["go_name"],
                go_id=go["go_id"],
                type=go_type
            )
            gene.go.add(go_obj)
            go_objects[go["go_id"]] = go_obj.id


def update_existing_gene(row):
    gene = Gene.objects.filter(id=row["id"]).first()
    if not gene:
        return

    gene.ctd_uniprot_ids = "|".join([gene.ctd_uniprot_ids, row["uniprot_id"]])

    gene.protein_names = row["protein_names"]
    gene.gene_encoded_by = row["encodedon"]
    gene.natural_variant = row["natural_variant"]
    gene.length = row["length"]
    gene.polymorphysm = row["uniprot_polymorphism"]
    gene.ec_number = row["ec_number"]
    gene.catalytic_activity = row["uniprot_cataylsis"]
    gene.kinetics = row["uniprot_kinetics"]
    gene.active_site = row["uniprot_active_site"]
    gene.binding_site = row["uniprot_binding_site"]
    gene.metal_binding = row["uniprot_metalbinding"]
    gene.dna_binding = row["uniprot_dnabinding"]
    gene.nucleotide_binding = row["uniprot_nucleotidebinding"]
    gene.site = row["uniprot_site"]
    gene.subunit_structure = row["uniprot_subunit_structure"]
    gene.pathology_allergenic = row["pathology_allergenic"]
    gene.uniprot_biotechnological_use = row["uniprot_biotechnological_use"]
    gene.uniprot_disruption_phenotype = row["uniprot_disruption_phenotype"]
    gene.uniprot_disease = row["uniprot_disease"]
    gene.uniprot_pharmacological_use = row["uniprot_pharmacological_use"]
    gene.uniprot_toxic_dose = row["uniprot_toxic_dose"]
    gene.uniprot_subcellular_location = row["uniprot_subcellular_location"]
    gene.uniprot_peptide = row["uniprot_peptide"]
    gene.uniprot_lipidation = row["uniprot_lipidation"]
    gene.uniprot_disulfide_bond = row["uniprot_disulfide_bond"]
    gene.uniprot_zinc_finger = row["uniprot_zinc_finger"]
    gene.uniprot_protein_families = row["uniprot_protein_families"]
    gene.uniprot_domain = row["uniprot_domain"]
    gene.uniprot_motif = row["uniprot_motif"]
    gene.accessions_ensembl = row["accessions_ensembl"]
    gene.accessions_pdb = row["accessions_pdb"]
    gene.accession_disgenet = row["accession_disgenet"]
    gene.accession_kegg = row["accession_kegg"]
    gene.accession_ctd = row["accession_ctd"]
    gene.accession_intac = row["accession_intac"]
    gene.accession_genewiki = row["accession_genewiki"]

    gene.save()
    if row["go_biological_process"]:
        assign_gene_go(gene, row["go_biological_process"], "biological-process")
    if row["go_molecular_function"]:
        assign_gene_go(gene, row["go_molecular_function"], "molecular-function")
    if row["go_cellular_component"]:
        assign_gene_go(gene, row["go_cellular_component"], "cellular-component")


def create_new_disease(row):
    return ''


def udpate_existing_disease(disease, row):
    # print(row['name'])
    # print(row['annotation_score'])
    disease.medlineplus_summary = row['medlineplus_summary']
    disease.nih_rare_diseases_summary = row['nih_rare_diseases_summary']
    disease.malacards_summary = row['malacards_summary']
    disease.uniprotkb_summary = row['uniprotkb_summary']
    disease.disease_ontology_summary = row['disease_ontology_summary']
    disease.cdc_summary = row['cdc_summary']
    disease.wikipedia_summary = row['wikipedia_summary']
    disease.ghr_summary = row['ghr_summary']
    disease.omim_summary = row['omim_summary']
    disease.annotation_score = row['annotation_score'] if row['annotation_score'] else 0
    disease.accession_malacards = row['accession_malacards']
    disease.disease_ontology = row['disease_ontology']
    disease.icd10 = row['icd10']
    disease.icd9cm = row['icd9cm']
    disease.snomed_ct = row['snomed_ct']
    disease.mesh = row['mesh']
    disease.ncit = row['ncit']
    disease.umsl = row['umsl']
    disease.omim = row['omim']
    disease.orphanet = row['orphanet']
    disease.icd10_via_orphanet = row['icd10_via_orphanet']
    disease.mesh_via_orphanet = row['mesh_via_orphanet']
    disease.umls_via_orphanet = row['umls_via_orphanet']
    disease.medgen = row['medgen']
    disease.save()
    categories = []
    for category_name in row.categories:
        category = DiseaseTraitCategory.objects.filter(name__iexact=category_name).first()
        if not category:
            category = DiseaseTraitCategory.objects.create(
                name=category_name
            )
        categories.append(category)
    disease.categories.add(*categories)


class Command(BaseCommand):
    """
    To run the command, type: ./manage.py import_EBI_data --file=data5.csv
    """
    help = "Import EBI data from data5.xlsx to DiseaseTrait model"

    def add_arguments(self, parser):
        parser.add_argument('--malacards-file', type=str)

    def import_malacards_diseases(self, file_path):
        data = pandas.read_csv(file_path, delimiter=",", header=0)
        data.categories = data.categories.str.split("\|")
        data.synonyms = data.synonyms.str.split("\|")

        data = data.replace(numpy.nan, "", regex=True)

        count = len(data)
        skipped = 0
        total = 0
        got_in_cycle = 0
        with transaction.atomic():
            for index, row in data.iterrows():
                remaining_time(count)
                meshes = ['MESH:' + record for record in re.split('\|| |,', row["mesh"])]
                diseases = DiseaseTrait.objects.filter(ctd_id__in=meshes)
                total += len(diseases)
                if not diseases.exists():
                    diseases = DiseaseTrait.objects.filter(name__iexact=row['name'])
                    total += len(diseases)
                    if not diseases.exists():
                        diseases = DiseaseTrait.objects.filter(ctd_synonyms__name__iexact=row['name'])
                        total += len(diseases)
                        if not diseases.exists():
                            skipped += 1
                            # create_new_disease(row)
                            continue

                for disease in diseases:
                    got_in_cycle += 1
                    udpate_existing_disease(disease, row)


        print("Done. Skipped %s. Found %s of %s. Total found %s:%s" % (skipped, count - skipped, count, total, got_in_cycle,))

    def import_ghr_genes(self, file_path):
        data = pandas.read_csv(file_path, delimiter="\t", header=0)
        data = data.replace(np.nan, '', regex=True)
        data = data.to_dict('records')
        cnt = len(data)
        with transaction.atomic():
            for row in data:
                remaining_time(cnt)
                gene = Gene.objects.filter(id=row["id"]).first()
                if not gene:
                    continue
                gene.ghr_function = row["ghr_function"]
                gene.save()
                # if row["related_disease_summary"]:
                #     pathologies = dict_to_list(loads(row["related_disease_summary"].encode(), decode_strings=True))
                #     Pathology.objects.bulk_create(
                #         [Pathology(
                #             gene=gene,
                #             name=record["name"],
                #             summary=record["summary"]
                #         ) for record in pathologies]
                #     )

    def import_uniprot_kb_genes(self, file_path):
        data = pandas.read_csv(file_path, delimiter="\t", header=0)
        data = data.sort(["id"], ascending=[1])
        data = data.replace(np.nan, '', regex=True)
        data = data.to_dict('records')
        cnt = len(data)
        skipped = 0
        with transaction.atomic():
            for row in data:
                # import pdb
                # pdb.set_trace()

                remaining_time(cnt)
                if row["id"] != "?" and row["id"] != "GENE ID":
                    update_existing_gene(row)
                else:
                    skipped += 1
                    # create_new_gene(row)
        print(skipped)

    def import_disease_symptom(self, file_path):
        data = pandas.read_csv(file_path, delimiter="\t", header=0)
        data = data.replace(np.nan, '', regex=True)
        data = data.to_dict('records')
        DiseaseSymptom.objects.bulk_create([
            DiseaseSymptom(
                name=record["name"],
                hpo_id=record["hpo_id"]
            ) for record in data
        ])

    def import_disease_symptom_interaction(self, file_path):
        data = pandas.read_csv(file_path, delimiter="\t", header=0)
        data = data.replace(np.nan, '', regex=True)
        data = data.to_dict('records')
        diseases = dict(DiseaseTrait.objects.filter(accession_malacards__isnull=False).values_list("accession_malacards", "id"))
        symptoms = dict(DiseaseSymptom.objects.values_list("slug", "id"))
        interactions_to_save = []
        cnt = len(data)
        skipped = 0
        with transaction.atomic():
            for row in data:
                remaining_time(cnt)
                disease_id = diseases.get(row["disease"])
                symptom_id = symptoms.get(row["symptom"].replace("_", '-'))
                if disease_id and symptom_id:
                    interactions_to_save.append(DiseaseSymptomInteraction(
                        disease_id=disease_id,
                        symptom_id=symptom_id,
                        frequency=row["frequency"]
                    ))
                else:
                    skipped += 1
            DiseaseSymptomInteraction.objects.bulk_create(interactions_to_save)
            print("Skipped %s of %s" % (skipped, cnt, ))

    def import_disease_variant_data(self, file_path):
        data = pandas.read_csv(file_path, delimiter="\t", header=0)
        data = data.replace(np.nan, '', regex=True)
        data = data.to_dict('records')
        diseases = dict(
            DiseaseTrait.objects.filter(accession_malacards__isnull=False).values_list("accession_malacards", "id"))
        genes = dict((Gene.objects.values_list("name", "id")))
        interactions_to_save = []
        cnt = len(data)
        skipped = 0
        with transaction.atomic():
            for row in data:
                remaining_time(cnt)
                disease_id = diseases.get(row["mcid"])
                gene_id = genes.get(row["gene"].lower())
                if disease_id:
                    interactions_to_save.append(DiseaseVariant(
                        disease_id=disease_id,
                        rsid=row["rsid"],
                        gene_symbol=row["gene"],
                        gene_id=gene_id,
                        variant=row["variant"],
                        significance=row["significance"],
                        risk_allele=row["risk_allele"],
                        assembly=row["assembly"]
                    ))
                else:
                    skipped += 1
            DiseaseVariant.objects.bulk_create(interactions_to_save)
            print("Skipped %s of %s" % (skipped, cnt,))

    def handle(self, *args, **options):
        self.import_disease_symptom_interaction(options.get('malacards_file'))
