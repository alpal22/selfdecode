from django.core.management.base import BaseCommand

from genome.models import Gene


class Command(BaseCommand):

    def handle(self, *args, **options):
        genes = Gene.objects.all()
        for gene in genes:
            gene.slug = gene.name
            gene.save()
