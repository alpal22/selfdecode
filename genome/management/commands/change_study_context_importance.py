from django.core.management import BaseCommand
from django.db import transaction

from decodify.helpers import remaining_time
from genome.models import Snp


DEFAULT_IMPORTANCE_VALUE = 5
DEFAULT_CONTEXT_ITEMS = (
    'missense_variant',
    'regulatory_region_variant',
    '3_prime_UTR_variant',
    '5_prime_UTR_variant'
)


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
                    '-i',
                    action='store',
                    dest='importance',
                    default=DEFAULT_IMPORTANCE_VALUE,
                    help='Importance for specified contexts; default is {0}'.format(
                        DEFAULT_IMPORTANCE_VALUE),
        )

        parser.add_argument(
            '-l',
            action='store',
            dest='list',
            nargs="+",
            default=DEFAULT_CONTEXT_ITEMS,
            help='List of contexts; default is {0}'.format(DEFAULT_CONTEXT_ITEMS),
        )

    def handle(self, *args, **options):
        importance = options.get('importance')
        context_items = options.get('list')

        query_set = Snp.objects.filter(studies__context__in=context_items)

        with transaction.atomic():
            for chunk in chunks(query_set, 1000):
                for item in chunk:
                    Snp.objects.filter(pk=item.pk).update(importance=importance)
