from django.core.management import BaseCommand

from decodify.helpers import remaining_time
from genome.models import Snp, SnpGenes


class Command(BaseCommand):

    def handle(self, *args, **options):
        snps = Snp.objects.all()
        count = snps.count()
        for snp in snps:
            remaining_time(count)
            genes = snp.related_genes.filter(gene__name__istartswith="loc")
            if genes.count() < snp.related_genes.count():
                genes.update(excluded=True)
        print("Mark all relations for ~no-gene")
        SnpGenes.objects.filter(gene__name__iexact="~no-gene").update(excluded=True)
        print("Done.")
