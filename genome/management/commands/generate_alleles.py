from django.core.management import BaseCommand

from decodify.helpers import remaining_time
from genome.models import Snp


class Command(BaseCommand):
    def handle(self, *args, **options):
        snps = Snp.objects.all().prefetch_related("population_genomes")
        count = snps.count()
        for snp in snps:
            remaining_time(count)
            population = snp.population_genomes.filter(abbr="ALL").first()
            major_allele_frequency = None
            minor_allele_frequency = None
            major_allele = None
            minor_allele = None
            alleles = str(snp.alleles).split("/")
            if population is None:
                if len(alleles) < 2:
                    continue
                if alleles[0] == snp.minor_allele or alleles[1] == snp.major_allele:
                    major_allele = alleles[1]
                    minor_allele = alleles[0]
                if alleles[1] == snp.minor_allele or alleles[0] == snp.major_allele:
                    major_allele = alleles[0]
                    minor_allele = alleles[1]
            else:
                group = population.group.split(";")
                if len(group) == 2:
                    group = dict([group[0].split(":"), group[1].split(":")])
                    major_allele = max(group, key=group.get)
                    minor_allele = min(group, key=group.get)
                    major_allele_frequency = group.get(major_allele)
                    minor_allele_frequency = group.get(minor_allele)
                else:
                    if len(alleles) < 2:
                        continue
                    if alleles[0] == snp.minor_allele or alleles[1] == snp.major_allele:
                        major_allele = alleles[1]
                        minor_allele = alleles[0]
                    if alleles[1] == snp.minor_allele or alleles[0] == snp.major_allele:
                        major_allele = alleles[0]
                        minor_allele = alleles[1]

            snp.major_allele = major_allele
            snp.minor_allele = minor_allele
            snp.major_allele_frequency = major_allele_frequency
            snp.minor_allele_frequency = minor_allele_frequency
            snp.save()
