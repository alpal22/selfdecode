import csv
import re

import pandas
from django.core.management import BaseCommand
from django.db import transaction

from decodify.helpers import remaining_time
from genome.models import SnpStudy

alleles_code = {
    "A": "T",
    "T": "A",
    "G": "C",
    "C": "G",
}

ALLELE_PRECISION = 0.2


class Command(BaseCommand):
    help = "Import all data from external file ./manage.py convert_risk_alleles --file ./gwas_catalog_v1.0.1-associations_e86_r2016-10-09.csv --ensemble ./ensembl.alleles.csv"

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)
        parser.add_argument('--ensemble', type=str)

    def handle(self, *args, **options):
        headers = {
            "DATE ADDED TO CATALOG": str, "PUBMEDID": str, "FIRST AUTHOR": str, "DATE": str, "JOURNAL": str,
            "LINK": str, "STUDY": str, "DISEASE/TRAIT": str, "INITIAL SAMPLE SIZE": str,
            "REPLICATION SAMPLE SIZE": str, "REGION": str, "CHR_ID": str, "CHR_POS": str, "REPORTED GENE(S)": str,
            "MAPPED_GENE": str, "UPSTREAM_GENE_ID": str, "DOWNSTREAM_GENE_ID": str, "SNP_GENE_IDS": str,
            "UPSTREAM_GENE_DISTANCE": str, "DOWNSTREAM_GENE_DISTANCE": str, "STRONGEST SNP-RISK ALLELE": str,
            "SNPS": str, "MERGED": str, "SNP_ID_CURRENT": str, "CONTEXT": str, "strERGENIC": str,
            "RISK ALLELE FREQUENCY": str, "P-VALUE": str, "PVALUE_MLOG": str, "P-VALUE (TEXT)": str,
            "OR or BETA": str, "95% CI (TEXT)": str, "PLATFORM [SNPS PASSING QC]": str, "CNV": str,
            "MAPPED_TRAIT": str, "MAPPED_TRAIT_URI": str, "STUDY ACCESSION": str

        }
        data = pandas.read_csv(options.get('file'), header=0, delimiter=',', quoting=csv.QUOTE_ALL, dtype=headers)
        data.columns = [
            "date_added", "pubmedid", "author", "date", "journal", "link", "study", "trait", "initial_sample_size",
            "replication_sample_size", "region", "chr_id", "chr_pos", "reported_gene", "mapped_gene",
            "upstream_gene_id",
            "downstream_gene_id", "snp_gene_ids", "upstream_gene_dist", "downstream_gene_dist", "strongest_snp_allele",
            "snps", "merged", "snp_id_current", "context", "ergenic", "risk_allele_frequency", "p", "p_mlog", "p_text",
            "beta", "ci", "platform", "cnv", "mapped_trait", "mapped_trait_uri", "study_accession"
        ]
        print("Split rsids...")
        snps = data.strongest_snp_allele.str.split(';| x ').apply(pandas.Series, 1).stack().drop_duplicates().map(lambda v: v.strip())
        snps.index = snps.index.droplevel(-1)
        snps.name = 'rsid_allele'
        snps = data.join(snps)

        snps.risk_allele_frequency = snps.risk_allele_frequency.str.replace(r'.*([0-9]+\.[0-9]+).*', '\\1').str.strip()
        snps.rsid_allele = snps.rsid_allele.str.replace(r'[-]{2,}', '-')

        print("Load emsembl data...")
        endf = pandas.read_csv(options.get('ensemble'), header=0, delimiter=',', quoting=csv.QUOTE_ALL)
        endf.maf = endf.maf.str.replace(r'[^0-9.]', '').str.strip()

        cnt = len(snps)
        with transaction.atomic():
            for index, snp in snps.iterrows():
                remaining_time(cnt)
                if not str(snp.rsid_allele).startswith("rs") or "-" not in str(snp.rsid_allele):
                    continue
                result = "C"
                rsid, allele = snp.rsid_allele.split("-")
                allele = allele.strip()[0]
                rsid = rsid.strip()

                if allele == "?":
                    continue

                strongest_allele = allele

                ensembls = endf.ix[endf.rsid == rsid]
                for index, ensembl in ensembls.iterrows():
                    # if ensembl.ambiguity in ("R", "Y",):
                    ensembl.allels = ensembl.allels.upper()
                    if ("G" in ensembl.allels or "C" in ensembl.allels) and ("A" in ensembl.allels or "T" in ensembl.allels):
                        if allele not in ensembl.allels:
                            allele = alleles_code[allele.upper()]
                    else:
                        if str(snp.risk_allele_frequency) not in ('nan', 'NR',) \
                                and str(ensembl.maf) not in ('nan', 'NR',)\
                                and snp.risk_allele_frequency\
                                and ensembl.maf:
                            risk_allele_frequency = snp.risk_allele_frequency
                            maf = ensembl.maf
                            if abs(float(risk_allele_frequency) - float(maf)) > ALLELE_PRECISION:
                                allele = alleles_code[allele]
                    ensembl.ambiguity = "" if str(ensembl.ambiguity) == "nan" else str(ensembl.ambiguity)
                    SnpStudy.objects.filter(snp__rsid=rsid, risk_allele=strongest_allele).update(
                        risk_allele=allele.upper() if allele else allele
                    )
        print("Done")
