import pandas
import re
from django.core.management.base import BaseCommand

from genome.models import Snp

re1 = re.compile(r"e([-+\d]+)")
re2 = re.compile(r"([-+]){1}[0]{1,}")


def accurate(x):
    cropped = "%.2e" % x
    formatted = re1.sub("*10<sup>\g<1></sup>", cropped)
    cleaned = re2.sub("\g<1>", formatted)
    return cleaned

class Command(BaseCommand):
    help = "Import SNPs from external files"

    def add_arguments(self, parser):
        parser.add_argument('--rsid-file', type=str)
        parser.add_argument('--data-file', type=str)

    def handle(self, *args, **options):
        rsidf = pandas.read_csv(options.get('rsid_file'), header=0, delimiter=',')
        rsidf.columns = ['rsid']

        dataf = pandas.read_csv(options.get('data_file'), header=0, delimiter=',')
        dataf.columns = ['trait', 'rsid', 'p', 'pub', 'link']

        rsid_flat = dataf.rsid.str.split(';').apply(pandas.Series, 1).stack().map(lambda v: v.strip())
        rsid_flat.index = rsid_flat.index.droplevel(-1)
        rsid_flat.name = 'rsid'

        del dataf['rsid']
        dataf = dataf.join(rsid_flat)

        data = rsidf.merge(dataf, on='rsid', how='left').dropna()
        groups = data.groupby('rsid').groups

        # Generate html for description
        li_tpl = """<li>{trait} (P={p}) (<a href="http://{link}">R</a>)</li>"""
        template = """
                    <h2>{rsid} is associated with the following:</h2>
                    <p><!--block--></p>
                    <ul>
                        {items}
                    </ul>"""
        num = 0
        for rsid, indexes in groups.items():
            lis = ""
            num += 1
            print("%s. rsid: %s" % (num, rsid,))
            for index in indexes:
                item = data.ix[index]
                item.p = accurate(float(item.p))
                lis += li_tpl.format(trait=item.trait, p=item.p, link=item.link)
            html = template.format(rsid=rsid.title(), items=lis)
            snp = Snp.objects.filter(rsid=rsid).first()
            if snp is None:
                continue
            snp.description_advanced += html
            snp.save()

        print("Done import")
