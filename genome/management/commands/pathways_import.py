import re

import pandas
from django.core.management import BaseCommand
from django_pandas.io import read_frame

from genome.models import Gene, GenePathway


class Command(BaseCommand):
    help = "Import all data from external file"

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)
        parser.add_argument('--html', type=str)

    def handle(self, *args, **options):
        data = pandas.read_csv(options.get('file'), header=30, delimiter=',')
        data.columns = ["gene", "gene_id", "pathway_name", "pathway_id"]

        html = pandas.read_csv(options.get('html'), header=0, delimiter=',')
        html.columns = ["id", "pathway_id", "html"]

        html.pathway_id = html.pathway_id.str.split("?").apply(lambda v: v[1].replace('hsa', '').strip())

        data.gene = data.gene.str.lower()

        genes = read_frame(Gene.objects.values("id", "name"))
        genes.columns = ["pk", "gene"]

        data = data.merge(genes, on="gene", how="left")
        data = data[data.pk.notnull()]

        data = data[data.pathway_id.str.match(r'^KEGG')]

        data.pathway_id = data.pathway_id.str.split(':').apply(lambda x: x[1].strip())

        data = data.merge(html, on="pathway_id")

        import pdb
        pdb.set_trace()

        i = 0
        for index, row in data.iterrows():
            pathway = GenePathway.objects.filter(kegg_id=row.pathway_id).first()
            if not pathway:
                pathway = GenePathway.objects.create(name=row.pathway_name, kegg_id=row.pathway_id, html=row.html)
            else:
                pathway.html = row.html
                pathway.save()
            Gene.objects.filter(pk=row.pk).first().pathways.add(pathway.pk)
            i += 1
            print("%s: %s" % (i, row.pk,))

        print("Done import")
