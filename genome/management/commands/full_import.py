import csv

import pandas
from django.core.management import BaseCommand
from django_pandas.io import read_frame

from genome.models import Gene, DiseaseTrait, Snp


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


class Command(BaseCommand):
    help = "Import all data from external file"

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)

    def handle(self, *args, **options):
        headers = {
            "DATE ADDED TO CATALOG": str, "PUBMEDID": str, "FIRST AUTHOR": str, "DATE": str, "JOURNAL": str,
            "LINK": str, "STUDY": str, "DISEASE/TRAIT": str, "INITIAL SAMPLE SIZE": str,
            "REPLICATION SAMPLE SIZE": str, "REGION": str, "CHR_ID": str, "CHR_POS": str, "REPORTED GENE(S)": str,
            "MAPPED_GENE": str, "UPSTREAM_GENE_ID": str, "DOWNSTREAM_GENE_ID": str, "SNP_GENE_IDS": str,
            "UPSTREAM_GENE_DISTANCE": str, "DOWNSTREAM_GENE_DISTANCE": str, "STRONGEST SNP-RISK ALLELE": str,
            "SNPS": str, "MERGED": str, "SNP_ID_CURRENT": str, "CONTEXT": str, "strERGENIC": str,
            "RISK ALLELE FREQUENCY": str, "P-VALUE": str, "PVALUE_MLOG": str, "P-VALUE (TEXT)": str,
            "OR or BETA": str, "95% CI (TEXT)": str, "PLATFORM [SNPS PASSING QC]": str, "CNV": str,
            "MAPPED_TRAIT": str, "MAPPED_TRAIT_URI": str, "STUDY ACCESSION": str

        }
        data = pandas.read_csv(options.get('file'), header=0, delimiter=',', quoting=csv.QUOTE_ALL, dtype=headers)
        data.columns = [
            "date_added", "pubmedid", "author", "date", "journal", "link", "study", "trait", "initial_sample_size",
            "replication_sample_size", "region", "chr_id", "chr_pos", "reported_gene", "mapped_gene", "upstream_gene_id",
            "downstream_gene_id", "snp_gene_ids", "upstream_gene_dist", "downstream_gene_dist", "strongest_snp_allele",
            "snps", "merged", "snp_id_current", "context", "ergenic", "risk_allele_frequency", "p", "p_mlog", "p_text",
            "beta", "ci", "platform", "cnv", "mapped_trait", "mapped_trait_uri", "study_accession"
        ]

        # self.import_genes(data)
        self.import_traits(data)
        self.import_snps(data)

    def import_snps(self, data):
        print("Start import snps...")
        snps = data.snps.str.split(';| x ').apply(pandas.Series, 1).stack().drop_duplicates().map(lambda v: v.strip())
        snps.index = snps.index.droplevel(-1)
        snps.name = 'rsid'
        snps = data.join(snps)

        genes = read_frame(Gene.objects.values("name", "id"))
        genes.columns = ["gene_name", "gene_id"]
        traits = read_frame(DiseaseTrait.objects.values("name", "id"))
        traits.columns = ["trait_name", "trait_id"]

        print("Prepare traits...")
        mapped_trait = snps.mapped_trait.str.split(',').apply(pandas.Series, 1).stack().map(lambda v: v.lower().strip())
        mapped_trait.index = mapped_trait.index.droplevel(-1)
        mapped_trait.name = 'trait_name'

        print("Prepare genes...")
        mapped_gene = data.mapped_gene.str.split(',| - |;| x ').apply(pandas.Series, 1).stack().map(lambda v: v.strip().lower())
        mapped_gene.name = "gene_name"
        mapped_gene.index = mapped_gene.index.droplevel(-1)

        print("Join all data...")
        snps = snps.join(mapped_trait)
        snps = snps.join(mapped_gene)

        existing_snps = list(Snp.objects.values_list("rsid", flat=True))

        print("Creating new snps...")
        snps_to_create = []
        for index, snp in snps.iterrows():
            if not str(snp.rsid).startswith("rs"):
                continue
            if snp.rsid in existing_snps:
                continue
            existing_snps.append(snp.rsid)
            snps_to_create.append(Snp(
                rsid=snp.rsid,
                name=snp.rsid,
                code=snp.rsid,
                importance=1
            ))
        print("Bulk create will insert %s new snps" % len(snps_to_create))
        Snp.objects.bulk_create(snps_to_create)

        print("Assign genes/traits to snps...")
        snps_all = Snp.objects.all()
        snps = snps.merge(genes, on="gene_name", how="left")
        snps = snps.merge(traits, on="trait_name", how="left")
        i =0
        for snp in snps_all:
            rows = snps.ix[snps.rsid == snp.rsid]
            # skip if rsid not exist in db
            if rows.empty:
                continue
            for index, row in rows.iterrows():
                if str(row.trait_id) != "nan":
                    snp.diseasestraits.add(row.trait_id)
                if str(row.gene_id) != "nan":
                    snp.genes.add(row.gene_id)
                i += 1
                print("%s: %s <- %s" % (i, snp.rsid, row.trait_name,))

        print("Done importing snps\n")

    def import_traits(self, data):
        print("Start import diseases/traits...")
        traits = data[["mapped_trait", "mapped_trait_uri"]]

        traits.columns = ["trait", "mapped_trait_uri"]

        s1 = traits.trait.str.split(',').apply(pandas.Series, 1).stack().map(lambda v: v.lower().strip())
        s1.name = 'trait'

        s2 = traits.mapped_trait_uri.str.split(',').apply(pandas.Series, 1).stack().map(lambda v: v.lower().strip())
        s1.name = 'uri'

        traits = pandas.concat([s1, s2], axis=1)
        traits.columns = ["trait", "uri"]

        traits = traits.drop_duplicates(subset="trait")
        i = 0
        for index, trait in traits.iterrows():
            ref_url = trait.uri
            trait = trait.trait
            if str(trait) == "nan":
                continue
            trait = trait.strip()
            if not trait:
                continue
            dt = DiseaseTrait.objects.filter(name=trait).first()
            if not dt:
                dt = DiseaseTrait(name=trait, ref_url=ref_url)
            dt.ref_url = ref_url
            dt.save()
            i += 1
            print("%s: %s" % (i, trait,))

        print("Done importing diseases/traits\n")

    def import_genes(self, data):
        print("Start import genes...")
        reported_gene = data.reported_gene.str.split(',| - |;| x ').apply(pandas.Series, 1).stack()
        mapped_gene = data.mapped_gene.str.split(',| - |;| x ').apply(pandas.Series, 1).stack()

        unique_genes = pandas.concat([reported_gene, mapped_gene], ignore_index=True).to_frame()
        unique_genes.columns = ["gene"]
        unique_genes = unique_genes.gene.str.lower().to_frame().gene.str.strip()
        unique_genes = unique_genes.unique()

        existing_genes = Gene.objects.values_list("name", flat=True)

        genes_to_create = []
        for gene in unique_genes:
            gene = gene.strip().lower()
            if not gene:
                continue
            if gene in existing_genes:
                continue
            genes_to_create.append(Gene(name=gene))
        print("Bulk create will insert %s new genes" % len(genes_to_create))
        Gene.objects.bulk_create(genes_to_create)
        print("Done importing genes\n")
