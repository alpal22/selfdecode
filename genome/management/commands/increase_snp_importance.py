from django.db import transaction

from django.core.management import BaseCommand
from decodify.helpers import remaining_time
from genome.models import Snp


class Command(BaseCommand):
    def handle(self, *args, **options):
        snps = Snp.objects.prefetch_related("studies").all()
        cnt = snps.count()
        changed = 0
        with transaction.atomic():
            for snp in snps:
                remaining_time(cnt)
                if 2 <= snp.studies.count() < 4 and snp.importance > 3:
                    snp.importance = 3
                    snp.save()
                    changed += 1

                if snp.studies.count() >= 4 and snp.importance < 5:
                    snp.importance = 5
                    snp.save()
                    changed += 1
            print("Successfully changed %s of %s" % (changed, cnt))
