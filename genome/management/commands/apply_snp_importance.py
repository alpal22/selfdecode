from django.core.management import BaseCommand
from django.db.models import Max

from genome.models import Snp


class Command(BaseCommand):
    help = "Generate importance field for SNPs"

    def handle(self, *args, **options):
        snps = Snp.objects.filter(importance__lte=1).prefetch_related("studies").all()
        i = 0
        count = snps.count()
        for snp in snps:
            i += 1
            print("%s/%s" % (i, count,))
            p_mlog = snp.studies.aggregate(Max("p_value_mlog")).get("p_value_mlog__max")
            if not p_mlog:
                snp.importance = 1
                snp.save()
                continue
            p_mlog = float(p_mlog)
            if p_mlog > 9.8:
                snp.importance = 5
            elif p_mlog > 8:
                snp.importance = 4
            elif p_mlog > 5.15:
                snp.importance = 3
            else:
                snp.importance = 2
            snp.save()
        print("Done")
