from django.core.management.base import BaseCommand
from django.utils.text import slugify

from genome.models import DiseaseTrait


class Command(BaseCommand):

    def handle(self, *args, **options):
        diseases = DiseaseTrait.objects.all()
        for disease in diseases:
            disease.slug = slugify(disease.name)
            disease.save()
