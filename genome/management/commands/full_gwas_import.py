import numpy
import pandas
from django.core.management import BaseCommand


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)

    def handle(self, *args, **options):
        data = pandas.read_csv(options.get('file'), header=0, delimiter="\t")
        data.columns = [
            "date_added", "pubmedid", "author", "date", "journal", "link", "study", "trait", "initial_sample_size",
            "replication_sample_size", "region", "chr_id", "chr_pos", "reported_gene", "mapped_gene",
            "upstream_gene_id",
            "downstream_gene_id", "snp_gene_ids", "upstream_gene_dist", "downstream_gene_dist", "strongest_snp_allele",
            "snps", "merged", "snp_id_current", "context", "ergenic", "risk_allele_frequency", "p", "p_mlog", "p_text",
            "beta", "ci", "platform", "cnv", "mapped_trait", "mapped_trait_uri", "study_accession"
        ]

        self.import_new_snps(data)

    def import_new_snps(self, data):
        print("Start import snps...")

        snps = data.strongest_snp_allele.str.split(';| x ').apply(pandas.Series, 1).stack().drop_duplicates().map(lambda v: v.strip())
        snps.index = snps.index.droplevel(-1)
        snps.name = 'rsid_risk_allele'
        data = data.join(snps)

        data = data.replace(numpy.nan, '')

        data.rsid_risk_allele = data.rsid_risk_allele.str.split(';| x ')
        data.chr_id = data.chr_id.str.split(';')
        data.chr_pos = data.chr_pos.str.split(';')

        import pdb
        pdb.set_trace()
