import csv
import pandas
from django.core.management import BaseCommand

from genome.models import Snp


class Command(BaseCommand):
    help = "Import all data from external file"

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)

    def import_data(self, data):
        i = 0
        cnt = Snp.objects.count()
        for index, snp in data.iterrows():
            i += 1
            print("%s/%s" % (i, cnt,))
            Snp.objects.filter(rsid=snp.rsid).update(
                p_value=snp.p,
                p_value_mlog=snp.p_mlog,
                context=snp.context,
                study=snp.study,
                initial_sample_size=snp.initial_sample_size,
                replication_sample_size=snp.replication_sample_size,
                journal=snp.journal,
                journal_published_at=snp.date,
            )

    def handle(self, *args, **options):
        headers = {
            "DATE ADDED TO CATALOG": str, "PUBMEDID": str, "FIRST AUTHOR": str, "DATE": str, "JOURNAL": str,
            "LINK": str, "STUDY": str, "DISEASE/TRAIT": str, "INITIAL SAMPLE SIZE": str,
            "REPLICATION SAMPLE SIZE": str, "REGION": str, "CHR_ID": str, "CHR_POS": str, "REPORTED GENE(S)": str,
            "MAPPED_GENE": str, "UPSTREAM_GENE_ID": str, "DOWNSTREAM_GENE_ID": str, "SNP_GENE_IDS": str,
            "UPSTREAM_GENE_DISTANCE": str, "DOWNSTREAM_GENE_DISTANCE": str, "STRONGEST SNP-RISK ALLELE": str,
            "SNPS": str, "MERGED": str, "SNP_ID_CURRENT": str, "CONTEXT": str, "strERGENIC": str,
            "RISK ALLELE FREQUENCY": str, "P-VALUE": str, "PVALUE_MLOG": str, "P-VALUE (TEXT)": str,
            "OR or BETA": str, "95% CI (TEXT)": str, "PLATFORM [SNPS PASSING QC]": str, "CNV": str,
            "MAPPED_TRAIT": str, "MAPPED_TRAIT_URI": str, "STUDY ACCESSION": str

        }
        data = pandas.read_csv(options.get('file'), header=0, delimiter=',', quoting=csv.QUOTE_ALL, dtype=headers)
        data.columns = [
            "date_added", "pubmedid", "author", "date", "journal", "link", "study", "trait", "initial_sample_size",
            "replication_sample_size", "region", "chr_id", "chr_pos", "reported_gene", "mapped_gene", "upstream_gene_id",
            "downstream_gene_id", "snp_gene_ids", "upstream_gene_dist", "downstream_gene_dist", "strongest_snp_allele",
            "snps", "merged", "snp_id_current", "context", "ergenic", "risk_allele_frequency", "p", "p_mlog", "p_text",
            "beta", "ci", "platform", "cnv", "mapped_trait", "mapped_trait_uri", "study_accession"
        ]

        snps = data.snps.str.split(';| x ').apply(pandas.Series, 1).stack().drop_duplicates().map(lambda v: v.strip())
        snps.index = snps.index.droplevel(-1)
        snps.name = 'rsid'
        data = data.join(snps)

        self.import_data(data)
        print("Done")
