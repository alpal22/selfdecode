import io
import itertools
import json
import zipfile
from collections import OrderedDict
from itertools import groupby

import boto3
import stripe
from allauth.account.utils import send_email_confirmation
from boto.s3.connection import S3Connection
from boto.s3.key import Key
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.db.models import Q, Count, Max, Min, F, Func
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import redirect
from django.shortcuts import render, get_object_or_404, render_to_response
from django.template import Context
from django.template import RequestContext
from django.template.loader import get_template
from django.utils.text import slugify
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_GET
from django.views.generic import DetailView
from sqlalchemy import and_
from sqlalchemy import column
from sqlalchemy import desc
from sqlalchemy import text
from sqlalchemy.sql import label

from analysis.models import Symptom
from chemical.facades import SubstanceCategoryFactory
from chemical.models import SubstanceCategory, ChemicalDiseaseInteraction
from decodify.aggregators import ArrayAgg, JsonAgg, ArrayaAggDelimited
from genome.decorators import subscription_required, uploaded_file_required
from genome.helpers import execute_sqla_query, get_related_instance, get_gene_scores, \
    get_potentially_problematic_genes, badges_for_gene
from genome.raw_sql_queries import _gene_chemical_interactions
from log.helpers import log_recent_changes
from log.models import LogEntry
from payment.models import Transaction
from recommendation.helpers import get_recommendations
from .models import UserRsid, DiseaseTrait, GeneDiseaseTrait, Category, GeneOntology, DiseaseGOAssociations, File, DiseaseHierarchy, \
    GeneDiseasePathwayInteraction, Pathway, DiseaseSymptom, UserGeneReputation, SnpGenes, SnpStudy
from .forms import NameForm, SnpForm, GenePackForm
from .models import Condition
from .models import Gene, GenePack, Post
from .models import MarketingEvent
from .models import Page
from .models import Snp
from .models import Substance
from .models import UserProfile
from .models import UserRsid, DiseaseTrait, GeneDiseaseTrait, Category, GeneOntology, DiseaseGOAssociations, File, DiseaseHierarchy, \
    GeneDiseasePathwayInteraction, Pathway, DiseaseSymptom, UserGeneReputation, SnpGenes
from .serializers import PageObjSerializer, DiseaseAutocompleteSerializer, SnpSubDiseasesSerializer

REPORTS_PER_PAGE = 100


def ab_test_script(request, event_name):
    marketing_event_instance = MarketingEvent.objects.get(name=event_name)
    marketing_event_instance.users_shown.add(request.user)

    if marketing_event_instance.views_a < marketing_event_instance.views_b:
        MarketingEvent.objects.filter(name=event_name).update(views_a=F('views_a') + 1)
        return marketing_event_instance.script_name_a
    else:
        MarketingEvent.objects.filter(name=event_name).update(views_b=F('views_b') + 1)
        return marketing_event_instance.script_name_b


def marketing_scripts(request): #  ALWAYS CREATE IN DB BEFORE CREATING HERE
    if request.user.is_authenticated():
        if request.GET.get("event_name"):
            marketing_event_instance = MarketingEvent.objects.get(name=request.GET.get("event_name"))
            if request.GET.get("variant") == "a":
                MarketingEvent.objects.filter(name=request.GET.get("event_name")).update(conversions_a=F('conversions_a') + 1)
            else:
                MarketingEvent.objects.filter(name=request.GET.get("event_name")).update(conversions_b=F('conversions_b') + 1)
        # if MarketingEvent.objects.filter(name="consult_id00003").exists() & (request.user not in MarketingEvent.objects.get(name="consult_id00003").users_shown.all()) & (len(SiteLogin.objects.filter(user=request.user)) > 1):
        #     return render(request, "marketing/" + ab_test_script(request, "consult_id00003"))

        if MarketingEvent.objects.filter(name="exit_intent_id00002").exists() & (request.user not in MarketingEvent.objects.get(name="exit_intent_id00002").users_shown.all()):
            return render(request, "marketing/" + ab_test_script(request, "exit_intent_id00002"))
    # if request.user.is_authenticated() & MarketingEvent.objects.filter(name="consult_id00003").exists() & MarketingEvent.objects.get(name="consult_id00003").users_shown.exists() & (request.user not in MarketingEvent.objects.get(name="consult_id00003").users_shown.all()) & (SiteLogin.objects.filter(user=request.user).count()>2):
    #     MarketingEvent.objects.get(name="consult_id00003").users_shown.add(request.user)
    #     return render(request, "marketing/consult.js")
    #if request.META.HTTP_REFERER ==
    return render(request, "marketing/test.js")


def choose_user(request):
    if request.user.is_authenticated():
        if request.method == "POST":
            query_set = User.objects.get(email__iexact=request.POST["email"])
            return render(request, "genome/user_history.html", {"query_set": query_set})
        return render(request, "genome/choose_user.html")
    return HttpResponse("404")


def paginate_report(request, data_list, per_page=50):
    p = request.GET.get("page")
    paginator = Paginator(data_list, per_page)
    try:
        page_set = paginator.page(p)
    except PageNotAnInteger:
        page_set = paginator.page(1)
    except EmptyPage:
        page_set = paginator.page(paginator.num_pages)
    return paginator, page_set


def regroup_report(request, data):
    grouped_iter = itertools.groupby(data, key=lambda v: v['gene'])
    grouped = [
        {'grouper': key, 'list': list(val)}
        for key, val in grouped_iter
        ]
    return grouped


@login_required
def sign_s3(request):

    s3_bucket = settings.S3_BUCKET
    file_name = str(request.user.id) + "__" + request.GET.get('file-name')
    file_type = 'application/zip'
    s3_client = boto3.client('s3')
    presigned_post = s3_client.generate_presigned_post(
        Bucket=s3_bucket,
        Key=file_name,
        Fields={"acl": "public-read", "Content-Type": file_type},
        Conditions=[
            {"acl": "public-read"},
            {"Content-Type": file_type}
        ],
        ExpiresIn=3600
    )
    return HttpResponse(
        json.dumps({
            'data': presigned_post,
            'url': 'https://{}.s3.amazonaws.com/{}'.format(s3_bucket, file_name)
            }
        ))


def post(request, slug):
    try:
        query_set = Post.objects.get(slug=slug)
    except:
        return HttpResponse("404") #make this page
    return render(request, "genome/post.html", {"query_set":query_set})


@require_GET
def diseases_list_api(request):
    # print(request.GET)
    if request.GET.get('data[q]', ''):
        query = request.GET.get('data[q]')
        diseases = DiseaseTrait.objects.filter(name__icontains=query).annotate(text=F('name')).values('id', 'text')
    else:
        diseases = []
        query = ''
    result = {
        'q': query,
        'results': list(diseases)
    }
    return JsonResponse(result, safe=False)


@require_GET
def genes_list_api(request):
    # print(request.GET)
    if request.GET.get('data[q]', ''):
        query = request.GET.get('data[q]')
        genes = Gene.objects.filter(name__icontains=query).annotate(text=F('name')).values('id', 'text')
    else:
        genes = []
        query = ''
    result = {
        'q': query,
        'results': [{'id': item['id'], 'text': item['text'].upper()} for item in list(genes)]
    }
    return JsonResponse(result, safe=False)


@login_required
@subscription_required(redirect_url="/checkout")
@uploaded_file_required(redirect_url="/uploader")
def dashboard(request):
    user_files = request.user.file_set.exclude(
        pk=request.user.user_profile.active_file_id
    ).filter(
        deleted_at__isnull=True
    ).order_by("-created_at")
    user_files = [request.user.user_profile.active_file] + list(user_files)
    transactions = Transaction.objects.filter(user=request.user)
    paginator = Paginator(user_files, 10)
    return render(request, "genome/dashboard.html", {
        "user_files": user_files,
        "paginator": paginator,
        "transactions": transactions,
        # "recommendations": _recommendations(request),
        "X23ANDME_CLIENT_ID": settings.X23ANDME_CLIENT_ID,
    })


@login_required
@subscription_required(redirect_url="/checkout")
@uploaded_file_required(redirect_url="/uploader")
def gene_packs(request):
    paginate_by = 10

    query = GenePack.objects.annotate(total_likes=Count('likes'))\
        .exclude(pack_type='sponsored').order_by('-total_likes')

    query_set_upvoted = query.filter(total_likes__gt=0)
    query_set_unvoted = query.filter(total_likes=0)

    return render(request, "genome/gene_packs.html", {
        "query_set_installed": query_set_upvoted,
        "query_set_available": query_set_unvoted
    })


@login_required
@subscription_required(redirect_url="/checkout")
@uploaded_file_required(redirect_url="/uploader")
def create_gene_pack(request):
    if request.method == 'POST':
        form = GenePackForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            if not instance.creator:
                instance.creator = request.user.username

            instance.owner = request.user
            instance.save()
            form.save_m2m()

            log_recent_changes(request.user, instance, "New gene pack created")

            return HttpResponseRedirect(reverse('gene_packs'))
        else:
            return render(request, "genome/create-gene-pack.html", {'form': form})

    return render(request, "genome/create-gene-pack.html")


@login_required
@subscription_required(redirect_url="/checkout")
@uploaded_file_required(redirect_url="/uploader")
def edit_gene_pack(request, id):
    gene_pack = get_object_or_404(GenePack, pk=id)
    form = GenePackForm(instance=gene_pack)
    if request.method == 'POST':
        form = GenePackForm(request.POST, instance=gene_pack)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('gene_packs'))

    return render(request, "genome/edit-gene-pack.html", {'form': form, 'gene_pack': gene_pack})


@login_required
@subscription_required(redirect_url="/checkout")
@uploaded_file_required(redirect_url="/uploader")
def delete_gene_pack(request, id):
    instance = get_object_or_404(GenePack, pk=id)
    if request.user == instance.owner:
        instance.delete()
        return HttpResponseRedirect(reverse('gene_packs'))


@login_required
@subscription_required(redirect_url="/checkout")
@uploaded_file_required(redirect_url="/uploader")
def like_gene_pack_api(request, pack_id):
    GenePack.objects.get(id=pack_id).likes.add(request.user)
    return JsonResponse('OK', safe=False)


@login_required
@subscription_required(redirect_url="/checkout")
@uploaded_file_required(redirect_url="/uploader")
def dislike_gene_pack_api(request, pack_id):
    GenePack.objects.get(id=pack_id).likes.remove(request.user)
    return JsonResponse('OK', safe=False)


@login_required
@subscription_required(redirect_url="/checkout")
@uploaded_file_required(redirect_url="/uploader")
def genes_to_look_at(request):
    query_set = request.user.user_profile.active_file.genes_to_look_at.filter(

        usergenereputation__score__gte=3.7,
        usergenereputation__pk__isnull=False
    ).extra(
        select={
            'gene_score': 'get_gene_score("genome_file_genes_to_look_at"."gene_id", "genome_file_genes_to_look_at"."file_id")'
        }
    ).exclude(name="~no-gene").order_by('-gene_score')

    paginator, page = paginate_report(request, query_set, 24)

    file_id = request.user.user_profile.active_file_id

    problematic_snps_ids = get_potentially_problematic_genes(request.user, only_affected_snps=True)

    if page.number < 2:
        genes_with_affected_snps = Gene.objects.filter(
            snps__id__in=problematic_snps_ids
        ).extra(
            select={'gene_score': 'get_gene_score("genome_gene"."id", %s)'},
            select_params=(request.user.user_profile.active_file.id, )
        ).order_by('-gene_score')
    else:
        genes_with_affected_snps = Gene.objects.none()

    prepared_genes = [o.pk for o in page.object_list]
    prepared_genes += list(genes_with_affected_snps.values_list('id', flat=True))

    bad_genes = get_potentially_problematic_genes(request.user, prepared_genes)

    aldjquery = Gene.sa.query(
        Gene.sa.id, Gene.sa.name.label('gene'), Gene.sa.slug, Snp.sa.rsid, Snp.sa.minor_allele,
        Snp.sa.homozygous_minor_color, Snp.sa.homozygous_major_color, Snp.sa.heterozygous_color,
        UserRsid.sa.genotype_style, UserRsid.sa.genotype, UserGeneReputation.sa.score,
        label('tup', column('json_agg((genome_gene.name, genome_gene.slug))', is_literal=True)),
        label('gene_score', column('get_gene_score(genome_file_genes_to_look_at.gene_id, genome_file_genes_to_look_at.file_id)', is_literal=True)),
    ).filter(
        Gene.sa.id.in_(prepared_genes)
    ).join(
        SnpGenes.sa
    ).join(
        Snp.sa
    ).outerjoin(
        UserRsid.sa, and_(UserRsid.sa.rsid == Snp.sa.rsid, UserRsid.sa.file_id == file_id)
    ).outerjoin(
        UserGeneReputation.sa,
        and_(UserGeneReputation.sa.gene_id == Gene.sa.id, UserGeneReputation.sa.file_id == file_id)
    ).group_by(
        Gene.sa.id, Snp.sa.rsid, Snp.sa.minor_allele, Snp.sa.homozygous_minor_color,
        Snp.sa.homozygous_major_color, Snp.sa.homozygous_major_color, Snp.sa.heterozygous_color,
        UserRsid.sa.genotype_style, UserRsid.sa.genotype, UserGeneReputation.sa.score,
        UserGeneReputation.sa.gene_id, UserGeneReputation.sa.file_id
    ).order_by(
        desc('gene_score')
    )

    sub_query = text(
        """SELECT snp_id from genome_userprofile_bookmarked_snps
           WHERE userprofile_id = {0}""".format(request.user.user_profile.id))

    bookmarked_query = aldjquery.filter(
        Snp.sa.id.in_(sub_query)
    )

    not_bookmarked_query = aldjquery.filter(
        ~Snp.sa.id.in_(sub_query)
    )

    united_queries = bookmarked_query.union_all(not_bookmarked_query)

    query_result = execute_sqla_query(united_queries)

    result = OrderedDict()
    added_snp = OrderedDict()
    gene_set = {}

    for gene in query_result:
        gene_name = gene.gene
        gene_slug = gene.slug
        gene_id = gene.id
        gene_set[gene_id] = None

        key = '{0}|{1}|{2}|{3}'.format(gene_name, gene_slug, gene_id, round(gene.gene_score, 2))

        if key not in result:
            result[key] = []
            added_snp[key] = []

        if gene.rsid in added_snp[key]:
            continue
        result[key].append(gene)
        added_snp[key].append(gene.rsid)

    if genes_with_affected_snps.exists():
        for gene2 in genes_with_affected_snps:
            key = '{0}|{1}|{2}|{3}'.format(gene2.name, gene2.slug, gene2.id, round(gene2.gene_score, 2))
            if key in result:
                result.move_to_end(key, last=False)

    genes = Gene.objects.filter(pk__in=[item for item in gene_set.keys()])

    return render(request, "genome/genes_to_look_at.html", {
        "gene_list": result,
        "bad_genes": bad_genes,
        "page": page,
        'genes': {gene_item.name: gene_item for gene_item in genes}
    })


def info(request, id):
    query_set = Snp.objects.get(id=id)
    return render(request, "genome/info.html", {"query_set":query_set})


def fixes(request, id):
    query_set = Snp.objects.get(id=id)
    return render(request, "genome/fixes.html", {"query_set":query_set})


@csrf_exempt
@login_required
def symptoms_conditions(request):
    if request.method == "POST":
        value = str(request.POST.get("value"))
        obj_id = request.POST.get("id")
        form_type = request.POST.get("form_type")
        if (form_type == "symptom") & (value == "true"):
            if ":" in obj_id:
                instance = get_related_instance(obj_id)
                request.user.user_profile.related_objects.connect(instance)
            else:
                request.user.user_profile.symptoms.add(Symptom.objects.get(id=obj_id))
        elif (form_type == "condition") & (value == "true"):
            request.user.user_profile.conditions.add(Condition.objects.get(id=obj_id))
        elif (form_type == "symptom") & (value == "false"):
            if ":" in obj_id:
                model, id = obj_id.split(":")
                object_type = ContentType.objects.filter(model=model).first()
                instance = request.user.user_profile.related_objects.filter(object_id=id, object_type=object_type).first()
                request.user.user_profile.related_objects.remove(instance)
            else:
                request.user.user_profile.symptoms.remove(Symptom.objects.get(id=obj_id))
        elif (form_type == "condition") & (value == "false"):
            request.user.user_profile.conditions.remove(Condition.objects.get(id=obj_id))
        return HttpResponse("200")
    user_symptoms = request.user.user_profile.symptoms.all()
    user_conditions = request.user.user_profile.conditions.all()
    user_related_objects = request.user.user_profile.related_objects.values_list('object_id', flat=True)
    query_set_related_objects = list({v.object_id: v for v in Symptom.related_objects.all()}.values())
    return render(request, "genome/symptoms_conditions.html", {
        "query_set_symptoms": Symptom.objects.all().order_by("name"),
        "query_set_conditions": Condition.objects.all().order_by("name"),
        "query_set_related_objects": query_set_related_objects,
        "user_symptoms": user_symptoms,
        "user_conditions": user_conditions,
        "user_related_objects": user_related_objects,
    })


def home_page(request):
    return render(request, "genome/home_page.html", {
        "abs_url": request.build_absolute_uri("/"),
        "fb_app_id": settings.FACEBOOK_APP_ID,
        "fb_share_image": settings.FACEBOOK_SHARE_IMAGE
    })


@login_required
def profile(request):
    form = NameForm()
    if request.method == "POST":
        form = NameForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            if not form.instance.user_profile.is_email_verified() and 'email' in form.changed_data:
                send_email_confirmation(request, form.instance)
            return redirect(reverse("dashboard"))
    return render(request, "genome/profile.html", {
        "form": form,
        "STRIPE_API_KEY": settings.STRIPE_PUBLIC_KEY,
    })


@login_required
@subscription_required(redirect_url="/checkout")
@uploaded_file_required(redirect_url="/uploader")
def snp_packs(request):
    if request.method == "POST":
        gene_pack_instance = GenePack.objects.get(id=request.POST["gene_pack_id"])

        if gene_pack_instance.pack_type == "sponsored" and \
                not request.POST.get("action") == "delete":
            request.user.user_profile.gene_packs.add(GenePack.objects
                                                     .get(id=request.POST["gene_pack_id"]))

        elif not request.POST.get("action") == "delete":
            UserProfile.objects.get(user=request.user).gene_packs.add(
                GenePack.objects.get(pk=request.POST.get("gene_pack_id")))
            for item in Gene.objects.filter(related_gene_pack__id=request.POST.get(
                    "gene_pack_id")):
                UserProfile.objects.get(user=request.user).gene.add(item)
        else:
            for item in GenePack.objects.get(pk=request.POST.get("gene_pack_id")).genes.all():
                UserProfile.objects.get(user=request.user).gene.remove(item)
            UserProfile.objects.get(user=request.user).gene_packs.remove(GenePack.objects.get(
                pk=request.POST.get("gene_pack_id")))

    query_set_installed = request.user.user_profile.gene_packs.extra(
        select={
            "user_genotype_style": "genome_userrsid.genotype_style",
            "score_hetero": "genome_snpallele.score_hetero",
            "score_major": "genome_snpallele.score_major",
            "score_minor": "genome_snpallele.score_minor",
        },
        tables=["genome_userrsid", 'genome_snp', 'genome_category', 'genome_genepack_categories', 'genome_snpallele', 'genome_snp_genes'],
        where=[
            "genome_genepack.id = genome_genepack_categories.genepack_id",
            "genome_category.id = genome_genepack_categories.category_id",
            "genome_category.id = genome_snpallele.category_id",
            "genome_snpallele.snp_id = genome_snp.id",
            "genome_snp.id = genome_snp_genes.snp_id",
            "genome_snp.rsid = genome_userrsid.rsid",
            "genome_userrsid.file_id = %s" % request.user.user_profile.active_file_id,
        ])\
        .filter(pack_type="sponsored",).order_by("id")
    query_set_available = GenePack.objects.prefetch_related(
        "categories"
    ).filter(pack_type="sponsored").exclude(id__in=query_set_installed)

    return render(request, "genome/snp_packs.html", {
        "query_set_installed": query_set_installed,
        "query_set_available": query_set_available
    })


@login_required
def snp_pack(request, id, category_id=None):
    gene_pack = get_object_or_404(GenePack.objects.prefetch_related("categories"), pk=id)

    if request.user.user_profile.gene_packs.filter(pk=gene_pack.pk).exists():
        active_file_id = request.user.user_profile.active_file_id
        query_set = Category.objects.raw("""
            SELECT genome_category.id,
                    genome_category.blue_grade_label,
                    genome_category.red_grade_label,
                    genome_category.orange_grade_label,
                    genome_snpallele.description_major,
                    genome_snpallele.description_hetero,
                    genome_snpallele.description_minor,
                    genome_snpallele.score_major,
                    genome_snpallele.score_hetero,
                    genome_snpallele.score_minor,
                    genome_snp.homozygous_major_color,
                    genome_snp.heterozygous_color,
                    genome_snp.homozygous_minor_color,
                    genome_userrsid.genotype_style,
                    genome_category.name as category,
                    genome_userrsid.genotype as user_genotype,
                    genome_snp.minor_allele,
                    genome_snp.minor_allele_frequency,
                    genome_snp.rsid,
                    genome_gene.name as gene
            FROM "genome_category"
            INNER JOIN "genome_genepack_categories" ON ("genome_category"."id" = "genome_genepack_categories"."category_id")
            LEFT JOIN "genome_snpallele" ON ("genome_category"."id" = "genome_snpallele"."category_id")
            LEFT JOIN "genome_snp" ON ("genome_snpallele"."snp_id" = "genome_snp"."id")
            LEFT JOIN "genome_snp_genes" ON ("genome_snp"."id" = "genome_snp_genes"."snp_id")
            LEFT JOIN "genome_gene" ON ("genome_snp_genes"."gene_id" = "genome_gene"."id")
            LEFT JOIN "genome_userrsid" ON ("genome_userrsid"."rsid" = "genome_snp"."rsid") AND (genome_userrsid.file_id = %s)
            WHERE ("genome_genepack_categories"."genepack_id" = %s""" + (' AND "genome_category"."id" = %s' % category_id if category_id is not None else '') + """)
            ORDER BY "genome_category"."name" ASC
        """, params=(active_file_id, id,))

        result = []
        for obj in query_set:
            row = {}
            for column in query_set.columns:
                row[column] = getattr(obj, column)
            result.append(row)

        return render(request, "genome/sponsored_pack.html", {
            "query_set": result,
            "gene_pack": gene_pack,
            "page_id": id
        })
    return HttpResponse("Not Authorized")


def s3(request):
    user_id = request.user.id
    file_name = str(user_id) + "__" + request.FILES.get("gene_file").name
    with io.BytesIO() as zip_file:
        with zipfile.ZipFile(zip_file, 'a', zipfile.ZIP_DEFLATED, False) as zip:
            zip.writestr(file_name, request.FILES.get("gene_file").read())
        zip_contents = zip_file.getvalue()
    conn = S3Connection(settings.AWS_ACCESS_KEY_ID, settings.AWS_SECRET_ACCESS_KEY, host="s3-us-west-1.amazonaws.com")
    bucket = conn.get_bucket('decodify')
    k = Key(bucket)
    k.key = file_name + ".zip"
    k.set_metadata('Content-Type', 'application/zip')

    k.set_contents_from_string(zip_contents)
    return HttpResponse("200")


def snp(request, rsid):
    query_set = get_object_or_404(Snp.objects.prefetch_related(
        "studies__diseasestraits", "related_snp_allele"
    ), rsid=rsid.lower())

    user_rsid_record = None
    if request.user.is_authenticated():
        user_rsid_record = UserRsid.objects.filter(rsid=rsid, file=request.user.user_profile.active_file).first()

    return render(request, "genome/snp.html", {
        "query_set": query_set,
        "user_rsid_record": user_rsid_record,
        "genes": query_set.genes.filter(related_snps__excluded=False).all(),
        "related_snp_allele": query_set.related_snp_allele.filter(category__name="General").first(),
    })


def gene_experiment_ajax(request, gene):
    query_set = get_object_or_404(Gene, pk=gene)
    query = Q()

    if request.GET.get("type"):
        if request.GET.get("type") == "increase":
            query = Q(log2fold__gte=0)
        if request.GET.get("type") == "decrease":
            query = Q(log2fold__lte=0)
    if request.GET.get("category"):
        query &= Q(configuration__identification__factor_name__contains=request.GET.get("category"))

    analytics = query_set.analytics_set.filter(query).prefetch_related(
        "configuration", "configuration__identification", "configuration__identification__organism").extra(
        select={"ordering": "abs(log2fold)"}).order_by('-ordering')
    analytics_log2fold_points = {
        'min_positive': analytics.filter(log2fold__gte=0).aggregate(
            Min('log2fold'))["log2fold__min"],
        'max_positive': analytics.filter(log2fold__gte=0).aggregate(
            Max('log2fold'))["log2fold__max"],
        'min_negative': analytics.filter(log2fold__lte=0).aggregate(
            Min('log2fold'))["log2fold__min"],
        'max_negative': analytics.filter(log2fold__lte=0).aggregate(
            Max('log2fold'))["log2fold__max"]
    }

    page = request.GET.get("page")
    paginator = Paginator(analytics, 10)
    try:
        current_page = paginator.page(page)
    except PageNotAnInteger:
        current_page = paginator.page(1)
    except EmptyPage:
        current_page = paginator.page(paginator.num_pages)
    data_set = {
        "analytics_log2fold_points": analytics_log2fold_points,
        "analytics": current_page.object_list
    }
    template = get_template('genome/partials/_experiment_table_body.html')
    if current_page.has_next():
        has_next = True
        next_page_number = current_page.next_page_number()
    else:
        next_page_number = current_page.paginator.num_pages
        has_next = False
    context = Context({
        "data_set": data_set
    })
    return JsonResponse({
        'paginator': {
            'has_next': has_next,
            'next_page_number': next_page_number,
        },
        'data': template.render(context)
    })


def gene_disease_ajax(request, gene):
    gene_disease_interactions_raw = DiseaseTrait.objects.raw("""
        select
        genome_diseasetrait.id,
        genome_diseasetrait.slug,
        genome_diseasetrait.name,
        genome_gene.function,
        genome_gene.description_simple,
        MAX(combined_score) as sort_field,
        MAX(genome_genediseasetrait.inference_score) as max_inference_score,
        Json_Agg(distinct (chemical_chemical.slug, chemical_chemical.name)) as chemicals,
        string_to_array(array_to_string(array_agg(genome_genediseasetrait.pub_med_ids),'|'), '|') as pubmed_ids

        from genome_diseasetrait


        inner join genome_genediseasetrait on genome_diseasetrait.id = genome_genediseasetrait.diseasetrait_id
        inner join chemical_chemical on genome_genediseasetrait.inference_chemical_id = chemical_chemical.id
        inner join genome_gene on genome_gene.id = genome_genediseasetrait.gene_id

        where genome_gene.id = {0}
        group by genome_diseasetrait.id, genome_gene.function, genome_gene.description_simple
        order by sort_field desc""".format(gene))

    page = request.GET.get("page")
    paginator = Paginator(list(gene_disease_interactions_raw), 10)
    try:
        current_page = paginator.page(page)
    except PageNotAnInteger:
        current_page = paginator.page(1)
    except EmptyPage:
        current_page = paginator.page(paginator.num_pages)

    disease_interactions = current_page.object_list

    template = get_template('genome/partials/table_bodies/gene_disease_interaction.html')
    if current_page.has_next():
        has_next = True
        next_page_number = current_page.next_page_number()
    else:
        next_page_number = current_page.paginator.num_pages
        has_next = False
    context = Context({
        "disease_interactions": disease_interactions
    })

    return JsonResponse({
        'paginator': {
            'has_next': has_next,
            'next_page_number': next_page_number,
        },
        'data': template.render(context)
    })


def gene(request, gene):
    qs = Gene.objects.prefetch_related(
        "transcription_factors", "relation"
    )
    if request.user.is_authenticated():
        qs = qs.annotate(
            gene_score=Func(F("id"), request.user.user_profile.active_file.id, function='get_gene_score')
        )

    query_set = get_object_or_404(qs, slug__iexact=gene.lower())

    data_set = {
        "keywords": ", ".join(query_set.keywords.values_list("name", flat=True)),
        "molecular_function": query_set.go.exclude(go_id='').filter(type="molecular-function"),
        "biological_processes": query_set.go.exclude(go_id='').filter(type="biological-process"),
        "drug_bank": query_set.uniprot_data.filter(category__slug="drug-bank").select_related("chemical"),
        "categories": SubstanceCategory.objects.root_nodes,
    }

    per_page = 10

    extra = {
        "join": '',
        "where": ''
    }

    # increase_interactions = _gene_chemical_interactions(request, query_set, "increases", extra)
    # decrease_interactions = _gene_chemical_interactions(request, query_set, "decreases", extra)

    top_interactions = query_set.chemicalgeneinteraction_set.all().annotate(
        total=Count('actions') + F('amount') - 1,
        action=ArrayAgg('actions__action')
    ).order_by('-total').values(
        'chemical__name',
        'chemical__display_as', 'chemical__slug', 'interaction', 'total', 'action')[:per_page]

    if query_set.protein_names:
        col_style = 'col-sm-9'
    else:
        col_style = 'col-sm-6 col-sm-offset-3'

    if request.user.is_authenticated():
        gene_id = query_set.id

        contains_risk_allele, bad_genes = badges_for_gene(request.user, [gene_id])

        snpids = query_set.snps.values_list("id", flat=True)

        related_snps = Snp.sa.query(
            Snp.sa.id, Snp.sa.rsid, Snp.sa.minor_allele, Snp.sa.description_simple,
            UserRsid.sa.genotype, SnpStudy.sa.risk_allele
        ).join(
            UserRsid.sa, UserRsid.sa.rsid == Snp.sa.rsid
        ).join(
            SnpStudy.sa, and_(SnpStudy.sa.snp_id == Snp.sa.id, SnpStudy.sa.risk_allele != None), isouter=True
        ).filter(
            UserRsid.sa.file_id == request.user.user_profile.active_file.id,
            Snp.sa.id.in_(snpids)
        )

        related_snps = execute_sqla_query(related_snps)

        return render(request, "genome/gene.html", {
            "query_set": query_set,
            "related_snps": related_snps,
            "data_set": data_set,
            "bad_genes": bad_genes,
            "contains_risk_allele": contains_risk_allele,
            # "increase_interactions": increase_interactions[:per_page],
            # "increase_interactions_has_pages": len(increase_interactions) > per_page,
            # "decrease_interactions": decrease_interactions[:per_page],
            # "decrease_interactions_has_pages": len(decrease_interactions) > per_page,
            "top_interactions": top_interactions,
            "col_style": col_style,
        })

    return render(request, "genome/gene.html", {
        "query_set": query_set,
        "data_set": data_set,
        # "increase_interactions": increase_interactions[:per_page],
        # "increase_interactions_has_pages": len(increase_interactions) > per_page,
        # "decrease_interactions": decrease_interactions[:per_page],
        # "decrease_interactions_has_pages": len(decrease_interactions) > per_page,
        "top_interactions": top_interactions,
        'col_style': col_style
    })


def gene_pathway_redirect(request, slug, kegg_id):
    kegg_id = "KEGG:" + kegg_id
    return HttpResponseRedirect(reverse('gene_pathway_details', args=(slug, kegg_id)))


def gene_pathway(request, slug, kegg_id):
    gene = get_object_or_404(Gene, name__iexact=slug)
    pathway = Pathway.objects.filter(pathway_id=kegg_id).first()
    diseases = GeneDiseasePathwayInteraction.objects.filter(pathway=pathway, gene=gene, disease_id__isnull=False).all()
    if pathway.html:
        pathway.html = pathway.html.format(
            img_url="https://s3-us-west-1.amazonaws.com/decodify/pathway_images/hsa%s.png" % pathway.pathway_id[5:]
        ).replace('href="/', 'href="http://www.genome.jp/')
    return render(request, "genome/gene_pathway.html", {
        "gene": gene,
        "pathway": pathway,
        "diseases": diseases,
    })


def pathway(request, pathway_id):
    query_set = get_object_or_404(Pathway, pathway_id=pathway_id)
    chemical_list = query_set.chemicalpathway_set.all()
    if query_set.html:
        query_set.html = query_set.html.format(
            img_url="https://s3-us-west-1.amazonaws.com/decodify/pathway_images/hsa%s.png" % query_set.pathway_id[5:]
        ).replace('href="/', 'href="http://www.genome.jp/')
    return render(request, "genome/pathway.html", {
        "pathway": query_set,
        "chemical_list": chemical_list[:20]
    })


def genediseasetrait_list(request, gene): # !!!
    query_set = GeneDiseaseTrait.objects.filter(gene__name__iexact=gene).select_related(
        "gene", "diseasetrait"
    ).order_by("-score")

    paginator, page = paginate_report(request, query_set)

    return render(request, "genome/genediseasetraits.html", {
        "page": page,
    })


def disease_chemicals_ajax(request, slug):
    query_set = get_object_or_404(DiseaseTrait.objects.prefetch_related(
        "studies", "studies__snp"
    ), slug=slug)
    disease_chemical_interactions = ChemicalDiseaseInteraction.objects.filter(disease=query_set).values(
        "chemical__name", "chemical__slug", "chemical__display_as"
    ).annotate(
        chemicals=ArrayAgg("chemical__slug"),
        max_inference_score=Max("inference_score"),
        genes=JsonAgg("inference_gene__name", "inference_gene__name", distinct=True),
        pubmed_ids=ArrayaAggDelimited("pub_med_ids", delimiter="|", distinct=True)
    ).order_by("-inference_score")

    page = request.GET.get("page")
    paginator = Paginator(disease_chemical_interactions, 10)
    try:
        current_page = paginator.page(page)
    except PageNotAnInteger:
        current_page = paginator.page(1)
    except EmptyPage:
        current_page = paginator.page(paginator.num_pages)

    chemical_interactions = current_page.object_list

    template = get_template('genome/partials/table_bodies/disease_chemical_interaction.html')
    if current_page.has_next():
        has_next = True
        next_page_number = current_page.next_page_number()
    else:
        next_page_number = current_page.paginator.num_pages
        has_next = False
    context = Context({
        "chemical_interactions": chemical_interactions
    })

    return JsonResponse({
        'paginator': {
            'has_next': has_next,
            'next_page_number': next_page_number,
        },
        'data': template.render(context)
    })


def disease_genes_ajax(request, slug):
    query_set = get_object_or_404(DiseaseTrait, slug=slug)
    gene_disease_interactions = GeneDiseaseTrait.objects.filter(diseasetrait=query_set).values(
        "gene__name", "gene__slug", "gene__id"
    ).annotate(
        max_inference_score=Max("inference_score"),
        sort_field=Max("combined_score")
    ).order_by("-sort_field", "-inference_score")

    page = request.GET.get("page")
    paginator = Paginator(gene_disease_interactions, 10)
    try:
        current_page = paginator.page(page)
    except PageNotAnInteger:
        current_page = paginator.page(1)
    except EmptyPage:
        current_page = paginator.page(paginator.num_pages)

    gene_interactions = current_page.object_list
    gene_ids = [item['gene__id'] for item in list(gene_interactions)]
    gene_scores = []
    bad_genes = []
    contains_risk_allele = []
    if request.user.is_authenticated():
        contains_risk_allele, bad_genes = badges_for_gene(request.user, gene_ids)

    template = get_template('genome/partials/table_bodies/disease_gene_interaction.html')
    if current_page.has_next():
        has_next = True
        next_page_number = current_page.next_page_number()
    else:
        has_next = False
        next_page_number = current_page.paginator.num_pages
    context = Context({
        "genes": gene_interactions,
        "bad_genes": bad_genes,
        "contains_risk_allele": contains_risk_allele,
        'gene_scores': gene_scores,
        'AJAX_NOINFO_MESSAGE': settings.AJAX_NOINFO_MESSAGE,
        'page': page,
    })

    return JsonResponse({
        'paginator': {
            'has_next': has_next,
            'next_page_number': next_page_number,
        },
        'data': template.render(context),
    })


def diseasetrait_redirect(request, slug):
    obj = get_object_or_404(DiseaseTrait, slug=slug)
    cat = slugify(obj.category.name)
    return redirect(reverse(cat, args=[slug]), permanent=True)


def gene_ontology_redirect(request, go_id):
    obj = get_object_or_404(GeneOntology, go_id=go_id)
    type = obj.type
    return redirect(reverse(type, args=[go_id]), permanent=True)


def disease_risk_color_index(genotype, risk_alleles, minor_allele):
    grouped_alleles = set(risk_alleles)
    if minor_allele is None:
        minor_allele = ""

    index = -10

    if len(grouped_alleles) == 1:
        allele = grouped_alleles.pop()
        if genotype.count(allele) == 2:
            index = 2
        elif genotype.count(allele) == 1:
            index = 1
        else:
            index = 0
    elif len(grouped_alleles) == 2:
        allele1 = grouped_alleles.pop()
        allele2 = grouped_alleles.pop()
        if genotype.count(allele1) or genotype.count(allele2):
            index = 1
        else:
            index = 0

    index2 = 0
    cnt = genotype.count(minor_allele)
    if cnt == 2:
        index2 = 2
    elif cnt == 1:
        index2 = 1

    return index, index2


@login_required
def bad_genes_api(request):
    bad_genes_list = get_potentially_problematic_genes(request.user)
    bad_genes_list = Gene.objects.filter(snps__id__in=bad_genes_list).annotate(gene_cnt=Count('id')).values_list('id', flat=True)
    return JsonResponse(list(bad_genes_list), safe=False)


def diseasetrait(request, slug):
    query_set = get_object_or_404(DiseaseTrait.objects.prefetch_related(
        "studies", "studies__snp", "relation"
    ), slug=slug)

    per_page = 10
    # data_available = GeneDiseaseTrait.objects.filter(diseasetrait__slug=slug).count() > 0
    disease_in_hierarchy = DiseaseHierarchy.objects.filter(disease=query_set).first()
    if request.user.is_authenticated() and request.user.user_profile.active_file is not None:
        user_genotypes = Snp.objects.filter(
            studies__diseasestraits=query_set
        ).extra(
            select={
                'user_genotype': 'genome_userrsid.genotype',
                'genotype_style': 'genome_userrsid.genotype_style',
            },
            tables=['genome_userrsid'],
            where=["genome_userrsid.rsid = genome_snp.rsid", "genome_userrsid.file_id = {}".format(request.user.user_profile.active_file.pk)],
        ).annotate(
            Count("id"),
            risk_alleles=ArrayAgg("studies__risk_allele"),
        ).prefetch_related("genes")
        if disease_in_hierarchy:
            user_genotypes_sub_diseases = Snp.objects.filter(
                studies__diseasestraits__diseasehierarchy__in=disease_in_hierarchy.get_descendants()
            ).extra(
                select={
                    'user_genotype': 'genome_userrsid.genotype',
                    'genotype_style': 'genome_userrsid.genotype_style',
                },
                tables=['genome_userrsid'],
                where=["genome_userrsid.rsid = genome_snp.rsid",
                       "genome_userrsid.file_id = {}".format(request.user.user_profile.active_file.pk)],
            ).annotate(
                Count("id"),
                risk_alleles=ArrayAgg("studies__risk_allele"),
                disease=F('studies__diseasestraits__name')
            ).prefetch_related("genes")
        else:
            user_genotypes_sub_diseases = []

        file_exists = request.user.is_authenticated() and request.user.user_profile.active_file is not None
    else:
        user_genotypes = []
        user_genotypes_sub_diseases = []
        file_exists = None

    page = request.GET.get('page')
    page_type = request.GET.get('page_type')

    # Sorting by Alleles for My SNPs tab
    user_genotypes = sorted(
        list(user_genotypes),
        key=lambda o: disease_risk_color_index(o.user_genotype, o.risk_alleles, o.minor_allele),
        reverse=True
    )
    # Sorting by Alleles for My SNPs Sub Diseases
    user_genotypes_sub_diseases = list(sorted(
        list(user_genotypes_sub_diseases),
        key=lambda o: disease_risk_color_index(o.user_genotype, o.risk_alleles, o.minor_allele),
        reverse=True)
    )
    # Trims Snps without Risk Alleles (My SNPs)
    bad_user_genes_for_disease = filter(
        lambda item: disease_risk_color_index(
            item.user_genotype,
            item.risk_alleles,
            item.genotype_style
        )[0] > 0,
        user_genotypes
    )

    # Genes ids for Snps with Risk Alleles (My Snps tab)
    b_genes = Gene.objects.filter(snps__in=bad_user_genes_for_disease).values_list('id', flat=True)

    # Grouping for Sub Diseases by Rsid and disease Subcategory (list of rows)
    sub_diseases_snps = user_genotypes_sub_diseases
    user_genotypes_sub_diseases = [list(j) for i, j in groupby(user_genotypes_sub_diseases)]

    beneficial_cat = SubstanceCategoryFactory.get(slug="beneficial-substances")
    toxins_cat = SubstanceCategoryFactory.get(slug="toxins")

    beneficial_subs_recomends = get_recommendations(b_genes, beneficial_cat)

    toxins_recomends = get_recommendations(b_genes, toxins_cat)

    molecular_functions = DiseaseGOAssociations.objects.filter(
        disease=query_set,
        gene_ontology__type="molecular-function"
    )
    biological_process = DiseaseGOAssociations.objects.filter(
        disease=query_set,
        gene_ontology__type="biological-process"
    )

    if request.is_ajax():
        contains_risk_allele = []
        bad_genes = []

        cross_genes = set(b_genes)
        # Gene ids for Snps with Risk Alleles (My Snp with sub diseases tab)
        if page_type == 'my_snp_sub_diseases':
            b_sub_diseases_genes = Gene.objects.filter(
                snps__in=sub_diseases_snps
            ).values_list('id', flat=True)
            cross_genes.update(b_sub_diseases_genes)

        if request.user.is_authenticated() and request.user.user_profile.active_file:
            contains_risk_allele, bad_genes = badges_for_gene(request.user, cross_genes)

        if page_type == 'my_snp':
            page_type = user_genotypes
        elif page_type == 'my_snp_sub_diseases':
            page_type = user_genotypes_sub_diseases
        else:
            all_snps = Snp.objects.filter(
                studies__diseasestraits=query_set
            ).annotate(
                Count("id"),
                risk_alleles=ArrayAgg("studies__risk_allele")
            ).prefetch_related("genes", "studies")
            page_type = all_snps

        paginator = Paginator(page_type, per_page)

        try:
            current_page = paginator.page(page)
        except PageNotAnInteger:
            current_page = paginator.page(2)
        except EmptyPage:
            current_page = paginator.page(paginator.num_pages)

        current_page.bad_genes = bad_genes
        current_page.contains_risk_allele = contains_risk_allele

        # My Snp with sub diseases tab
        if request.GET.get('page_type') == 'my_snp_sub_diseases':
            return JsonResponse(current_page, encoder=SnpSubDiseasesSerializer, safe=False)
        # My SNPs tab
        return JsonResponse(current_page, encoder=PageObjSerializer, safe=False)

    return render(request, "genome/diseasetrait.html", {
        "query_set": query_set,
        "file_exists": file_exists,
        # "user_genotypes": user_genotypes[:per_page],
        # "user_genotypes_sub_diseases": user_genotypes_sub_diseases[:per_page],
        # "user_genotypes_has_pages": len(user_genotypes) > per_page,
        # "user_genotypes_sub_diseases_has_pages": len(user_genotypes_sub_diseases) > per_page,
        "beneficial_subs_recomends": beneficial_subs_recomends,
        "toxins_recomends": toxins_recomends,
        "disease_slug": slug,
        # "data_available": data_available,
        "molecular_functions": molecular_functions[:per_page],
        "biological_process": biological_process[:per_page],
        # "disease_pathways": disease_pathways[:per_page],
        # "disease_pathways_has_pages": len(disease_pathways) > per_page,

        # "bad_genes": bad_genes,
        # "contains_risk_allele": contains_risk_allele,
    })


def gene_ontology(request, go_id):
    query_set = get_object_or_404(GeneOntology, go_id=go_id)

    interactions = DiseaseGOAssociations.objects.filter(gene_ontology=query_set).order_by("-inference_gene_qty")
    bad_genes = []
    contains_risk_allele = []
    gene_scores = []

    if request.user.is_authenticated():
        gene_ids = [item.pk for item in query_set.gene_set.all()]

        if gene_ids:
            contains_risk_allele, bad_genes = badges_for_gene(request.user, gene_ids)

    return render(request, "genome/gene_ontology.html", {
        "query_set": query_set,
        "interactions": interactions,
        "bad_genes": bad_genes,
        'contains_risk_allele': contains_risk_allele,
        'gene_scores': gene_scores
    })


def page(request, slug):
    try:
        query_set = Page.objects.get(slug=slug)
    except:
        return HttpResponse("404") #make this page
    return render(request, "genome/page.html", {
        "query_set": query_set,
        "abs_url": request.build_absolute_uri("/"),
        "fb_app_id": settings.FACEBOOK_APP_ID,
    })


def description(request, gene):
    query_set = Gene.objects.filter(name=gene).first()

    data_set = {
        "keywords": ", ".join(query_set.keywords.values_list("name", flat=True)),
        "molecular_function": ", ".join(
            query_set.uniprot_data.filter(category__slug="molecular-function").values_list("value", flat=True)
        ),
        "biological_processes": ", ".join(
            query_set.uniprot_data.filter(category__slug="biological-processes").values_list("value", flat=True)
        ),
        "drug_bank": ", ".join(
            query_set.uniprot_data.filter(category__slug="drug-bank").values_list("value", flat=True)
        ),
        "categories": SubstanceCategory.objects.root_nodes,
    }

    return render(request, "genome/description.html", {
        "query_set": query_set,
        "data_set": data_set,
    })


def start(request):
    if request.user.user_profile.active_file is not None:
        return redirect("/dashboard")
    return render(request, "genome/start.html")


def get_disease_trait_modal_body(request, rsid, user_name):
    user_instance = User.objects.get(username=user_name)
    item = get_object_or_404(Snp, rsid=rsid)
    user_rsid_record = UserRsid.objects.get(rsid=rsid, file=user_instance.user_profile.active_file)
    return render(request, "genome/partials/modal_bodies/disease_trait_modal_body.html", {
        "item": item,
        "user_rsid_record": user_rsid_record,
    })


def get_substance_interaction_modal_body(request, gene):
    gene_obj = Gene.objects.filter(name=gene).first()
    exception_status = gene_obj.exception_status if gene_obj.exception_status else None

    def correspond_string(first_arg, comprasion, second_arg=''):
        return first_arg if exception_status == comprasion else second_arg

    decrease_tab_active = correspond_string('active', 'decreased')
    increase_tab_active = 'active' if not decrease_tab_active else ''

    increase_tab_class = ''
    decrease_tab_class = ''

    if exception_status is not None:
        increase_tab_class = correspond_string('tab-blue', 'increased', 'tab-red')
        decrease_tab_class = correspond_string('tab-blue', 'decreased', 'tab-red')

    return render(request, "genome/partials/modal_bodies/substance_interaction_modal_body.html", {
        "item2": {
            "grouper": gene,
        },
        "gene": gene_obj,
        "increase_tab_active": increase_tab_active,
        "decrease_tab_active": decrease_tab_active,
        "increase_tab_class": increase_tab_class,
        "decrease_tab_class": decrease_tab_class,
    })


def fix(request, gene):
    query_set = Gene.objects.filter(name=gene)[0]
    return render(request, "genome/fix.html", {"query_set":query_set})


def quick_refund(request):
    if request.user.is_staff:
        stripe.api_key = "sk_live_vt7pYA86jtaknphGhpvtIUyn"
        email = request.GET["email"]
        r = stripe.Charge.list(customer=UserProfile.objects.get(user__email=email).stripe_customer_id)
        charge_id = r.data[0].id
        re = stripe.Refund.create(charge=charge_id)
        return HttpResponse(request.GET["email"] + " was refunded" + str(re))


def substance(request, substance):
    query_set = Substance.objects.get(slug=substance)
    return render(request, "genome/substance.html", {"query_set":query_set})


def admin_gene(request, gene):
    gene_id = Gene.objects.get(name=gene).id
    return redirect("/joes_office/genome/gene/" + str(gene_id) + "/change/")


@login_required
@subscription_required(redirect_url="/checkout")
@uploaded_file_required(redirect_url="/uploader")
def my_favorable_options(request):
    query_set = Gene.objects.filter(
        id__in=request.user.user_profile.active_file.genes_to_look_at.all(),
        fix_simple__gt=0
    )
    return render(request, "genome/my_favorable_options.html", {
        "query_set": query_set
    })


@login_required
def add_bookmark_snp(request):
    if not request.is_ajax():
        return HttpResponseBadRequest()

    if request.GET.get("snp_id"):
        request.user.user_profile.bookmarked_snps.add(Snp.objects.get(pk=request.GET["snp_id"]))
    elif request.GET.get("snp_ids[]"):
        request.user.user_profile.bookmarked_snps.add(*Snp.objects.filter(pk__in=request.GET.getlist("snp_ids[]")))
    elif request.GET.get("gene_id"):
        gene = Gene.objects.filter(pk=request.GET["gene_id"]).first()
        if gene:
            request.user.user_profile.bookmarked_snps.add(*gene.snps.all())
    return JsonResponse({
        "result": None,
        "error": None,
    })


@login_required
def blood_markers(request):
    form = SnpForm()
    return render(request, "genome/blood_markers.html", {"form": form})


def snp_autocomplete(request):
    qs = Snp.objects.all()
    if request.GET:
        qs = qs.filter(rsid__icontains=request.GET["q"])
    return qs


def handler403(request):
    response = render_to_response('error/403.html', context_instance=RequestContext(request))
    response.status_code = 403
    return response


def handler404(request):
    response = render_to_response('error/404.html', context_instance=RequestContext(request))
    # if request.user.is_anonymous():
    #     user = None
    # else:
    #     user = request.user
    # entry = LogEntry(
    #     error='404',
    #     user=user,
    #     url=request.build_absolute_uri(),
    #     debug_info=''
    # )
    # entry.save()
    response.status_code = 404
    return response


def handler500(request):
    response = render_to_response('error/500.html', context_instance=RequestContext(request))
    response.status_code = 500
    return response


def disease_tree(request):
    root_nodes = DiseaseHierarchy.objects.filter(parent__isnull=True)
    return render(request, 'genome/disease_tree.html', {"root_nodes": root_nodes})


def disease_tree_ajax(request, node_id):
    node = DiseaseHierarchy.objects.filter(id=node_id).first()
    child_nodes = node.get_children()
    template = get_template('genome/partials/disease_tree_nodes.html')
    context = Context({
        "nodes": child_nodes
    })
    return JsonResponse({
        "data": template.render(context)
    })


class SymptomDetailView(DetailView):
    template_name = 'genome/symptom.html'
    model = DiseaseSymptom


def autocomplete_diseases(request):
    query_set = DiseaseTrait.objects.all()
    if request.GET.get("query"):
        query_set = query_set.filter(name__icontains=request.GET.get("query"))
    else:
        query_set = query_set[:15]
    return JsonResponse(query_set, encoder=DiseaseAutocompleteSerializer, safe=False)


def try_demo(request):
    demo_user = authenticate(email='demo@demo.com', password='demodemo', username='demo')
    login(request, demo_user)
    return HttpResponseRedirect(reverse('dashboard'))


@login_required
def resend_confirmation_email(request):
    if not request.user.user_profile.is_email_verified():
        send_email_confirmation(request, request.user)

    messages.info(request, "Confirmation email was re-send. Check your inbox.")

    return HttpResponseRedirect(request.META.get("HTTP_REFERER", ""))