# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-14 02:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('genome', '0007_auto_20160514_0233'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='active_file',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='genome.File'),
        ),
    ]
