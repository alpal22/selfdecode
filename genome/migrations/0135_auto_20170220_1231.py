# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-02-20 12:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('genome', '0134_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='snp',
            name='disease_variant',
            field=models.ManyToManyField(to='genome.DiseaseVariant'),
        ),
    ]
