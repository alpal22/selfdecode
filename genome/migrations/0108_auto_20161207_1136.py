# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-12-07 11:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('genome', '0106_snpstudy_alleles'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='snp',
            name='all_hetero_allele',
        ),
        migrations.RemoveField(
            model_name='snp',
            name='all_hetero_frequency',
        ),
        migrations.RemoveField(
            model_name='snp',
            name='all_major_allele',
        ),
        migrations.RemoveField(
            model_name='snp',
            name='all_major_frequency',
        ),
        migrations.RemoveField(
            model_name='snp',
            name='all_minor_allele',
        ),
        migrations.RemoveField(
            model_name='snp',
            name='all_minor_frequency',
        ),
        migrations.RemoveField(
            model_name='snp',
            name='alleles',
        ),
        migrations.RemoveField(
            model_name='snp',
            name='ambiguity_code',
        ),
        migrations.RemoveField(
            model_name='snp',
            name='eur_hetero_allele',
        ),
        migrations.RemoveField(
            model_name='snp',
            name='eur_hetero_frequency',
        ),
        migrations.RemoveField(
            model_name='snp',
            name='eur_major_allele',
        ),
        migrations.RemoveField(
            model_name='snp',
            name='eur_major_frequency',
        ),
        migrations.RemoveField(
            model_name='snp',
            name='eur_minor_allele',
        ),
        migrations.RemoveField(
            model_name='snp',
            name='eur_minor_frequency',
        ),
        migrations.RemoveField(
            model_name='snp',
            name='risk_allele',
        ),
        migrations.AddField(
            model_name='snp',
            name='minor_allele',
            field=models.CharField(blank=True, max_length=1, null=True, verbose_name='Minor Allele'),
        ),
        migrations.RemoveField(
            model_name='snp',
            name='minor_allele_frequency',
        ),
        migrations.AddField(
            model_name='snp',
            name='minor_allele_frequency',
            field=models.FloatField(blank=True, null=True, verbose_name='Minor Allele Frequency'),
        ),
        migrations.AlterField(
            model_name='snpstudy',
            name='alleles',
            field=models.CharField(blank=True, max_length=16, null=True),
        ),
    ]
