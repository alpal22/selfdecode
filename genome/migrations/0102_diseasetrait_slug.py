# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-12-05 12:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('genome', '0101_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='diseasetrait',
            name='slug',
            field=models.SlugField(blank=True, max_length=255, null=True),
        ),
    ]
