# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-03-15 15:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('genome', '0138_auto_20170314_0751'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='file_uploads_count',
            field=models.PositiveIntegerField(default=0, null=True),
        ),
    ]
