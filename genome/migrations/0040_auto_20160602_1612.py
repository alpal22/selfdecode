# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-02 16:12
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('genome', '0039_auto_20160602_1601'),
    ]

    operations = [
        migrations.AlterField(
            model_name='snp',
            name='gene',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='snp', to='genome.Gene'),
        ),
    ]
