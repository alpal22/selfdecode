# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-03-16 10:37
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('genome', '0141_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='diseasetrait',
            name='definition',
            field=ckeditor.fields.RichTextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='snp',
            name='disease_variant',
            field=models.ManyToManyField(blank=True, to='genome.DiseaseVariant'),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='affiliated_from',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='affiliates', to='affiliate.AffiliateProfile'),
        ),
    ]
