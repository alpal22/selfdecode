# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-11-11 12:35
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('genome', '0094_auto_20161104_1205'),
        ('genome', '0083_auto_20161027_0823'),
    ]

    operations = [
    ]
