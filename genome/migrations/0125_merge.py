# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-01-26 13:41
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('genome', '0123_auto_20170123_1307'),
        ('genome', '0124_auto_20170123_1039'),
        ('genome', '0123_uniprotdata_chemical'),
    ]

    operations = [
    ]
