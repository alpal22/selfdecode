# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-02-07 13:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('genome', '0132_diseasesymptom_diseasesymptominteraction'),
    ]

    operations = [
        migrations.CreateModel(
            name='DiseaseVariant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rsid', models.CharField(blank=True, max_length=255, null=True)),
                ('gene_symbol', models.CharField(blank=True, max_length=255, null=True)),
                ('variant', models.CharField(blank=True, max_length=255, null=True)),
                ('significance', models.CharField(blank=True, max_length=512, null=True)),
                ('risk_allele', models.CharField(blank=True, max_length=255, null=True)),
                ('assembly', models.CharField(blank=True, max_length=255, null=True)),
                ('disease', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='genome.DiseaseTrait')),
                ('gene', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='genome.Gene')),
            ],
        ),
    ]
