# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-02-03 12:49
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('genome', '0127_diseasepathway_html'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='DiseasePathwayInteraction',
            new_name='GeneDiseasePathwayInteraction',
        ),
        migrations.RenameModel(
            old_name='DiseasePathway',
            new_name='Pathway',
        ),
    ]
