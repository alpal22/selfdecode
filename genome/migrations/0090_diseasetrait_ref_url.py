# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-10-26 14:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('genome', '0089_auto_20161026_1236'),
    ]

    operations = [
        migrations.AddField(
            model_name='diseasetrait',
            name='ref_url',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
