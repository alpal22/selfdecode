# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-14 22:30
from __future__ import unicode_literals

from django.db import migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('genome', '0048_file_genes_to_look_at'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gene',
            name='fix_simple',
            field=tinymce.models.HTMLField(blank=True, default='', null=True),
        ),
    ]
