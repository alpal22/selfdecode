# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-09-16 07:57
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('genome', '0080_bloodmarker'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bloodmarker',
            name='tags',
        ),
    ]
