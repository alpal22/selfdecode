from functools import wraps

from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect
from django.utils.decorators import available_attrs
from django.conf import settings


def subscription_required(redirect_url=None):
    def decorator(fn):
        @wraps(fn, assigned=available_attrs(fn))
        def wrapper(request, *args, **kwargs):
            if not request.user.user_profile.has_subscription() and not request.user.user_profile.active_file.is_demofile:
                if redirect_url is None:
                    raise PermissionDenied
                else:
                    return redirect(redirect_url)
            return fn(request, *args, **kwargs)
        return wrapper
    return decorator


def uploaded_file_required(redirect_url=None):
    def decorator(fn):
        @wraps(fn, assigned=available_attrs(fn))
        def wrapper(request, *args, **kwargs):
            if request.user.user_profile.active_file is None:
                if redirect_url is None:
                    raise PermissionDenied
                else:
                    return redirect(redirect_url)
            return fn(request, *args, **kwargs)
        return wrapper
    return decorator
