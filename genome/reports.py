from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import PageNotAnInteger
from django.core.paginator import Paginator, EmptyPage
from django.db.models import F
from django.db.models import Q, Count
from django.http import FileResponse
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.template.loader import get_template
from django.views.generic import DetailView
from sqlalchemy import alias
from sqlalchemy import asc
from sqlalchemy import column
from sqlalchemy import desc
from sqlalchemy import func
from sqlalchemy import literal
from sqlalchemy import or_
from sqlalchemy import select
from sqlalchemy import text
from sqlalchemy import union_all
from sqlalchemy.orm import Query
from sqlalchemy.sql import label
from sqlalchemy.sql.functions import coalesce

from sqlalchemy import and_

from sqlalchemy import column
from sqlalchemy import desc
from sqlalchemy import or_
from sqlalchemy.sql import label
from sqlalchemy.sql.functions import func

from chemical.facades import SubstanceCategoryFactory
from chemical.models import SubstanceCategory, ChemicalGeneInteraction, Chemical
from decodify.aggregators import JsonAgg, ArrayAgg
from decodify.sqla_helpers import has_minor_alleles
from genome.decorators import subscription_required
from genome.decorators import uploaded_file_required
from genome.models import UserRsid, UserProfile, Snp, DiseaseTrait, GenePack, Gene, \
    DiseaseHierarchy, SnpGenes, SnpStudy
from social.models import AccessRequest
from genome.helpers import query_count, paginate_sqla_statement
from genome.views import get_potentially_problematic_genes
from recommendation.helpers import get_recommendations, get_bookmarked_recommendations, add_scores, \
    _get_chemicals_by_gene_direction


def paginate_report(request, data_list, per_page=50):
    p = request.GET.get("page")
    paginator = Paginator(data_list, per_page)
    try:
        page_set = paginator.page(p)
    except PageNotAnInteger:
        page_set = paginator.page(1)
    except EmptyPage:
        page_set = paginator.page(paginator.num_pages)
    return paginator, page_set


def _snps_has_info(snps, active_file):
    data_set = []
    items = {snp.rsid: snp for snp in Snp.objects.filter(pk__in=[snp['pk'] for snp in snps])}
    userrsid = {snp.rsid: snp for snp in UserRsid.objects.filter(rsid__in=[snp['rsid'] for snp in snps], file=active_file)}

    for item in snps:
        rsid_obj = items.get(item.get('rsid'))
        user_rsid_record = userrsid.get(item.get('rsid'))

        if rsid_obj is None or user_rsid_record is None:
            continue

        studies = rsid_obj.studies.filter(ci_text__isnull=False, risk_allele__isnull=False). \
            exclude(ci_text='').exclude(risk_allele='')

        related_info = rsid_obj.related_snp_allele.first()

        diseases_count = 0
        # we may have studies, but we may not have diseases attached to them
        for study in studies:
            diseases_count += study.diseasestraits.count()

        has_risks = diseases_count != 0
        has_info = False

        if related_info is not None:
            gen_style = user_rsid_record.genotype_style

            if gen_style == 'heterozygous':
                has_info = related_info.description_hetero != '' and \
                           'No info...' not in related_info.description_hetero
            elif gen_style == 'homozygous_major':
                has_info = related_info.description_major != '' and \
                           'No info...' not in related_info.description_major
            else:
                has_info = related_info.description_minor != '' and \
                           'No info...' not in related_info.description_minor

        item['available_info'] = has_risks or has_info
        # if item.get('available_info'):
        data_set.append(item)

    return data_set


def _get_bookmarked_ids(user, snp_ids=None):
    snps = user.user_profile.bookmarked_snps
    if snp_ids:
        snps = snps.filter(pk__in=snp_ids)
    return snps.values_list("id", flat=True)


def _mark_bookmarked_snps(snps, user):
    bookmarked_ids = _get_bookmarked_ids(user, [snp['pk'] for snp in snps])

    # 'snps' is a list of mutable objects, so we can change it by reference
    for item in snps:
        item['is_bookmarked'] = item['pk'] in bookmarked_ids

    return snps


def _get_snps_genes(snps):
    """
    Get list of genes for given SNPs
    :type snps: list[Snp]
    :rtype: dict[str] = Gene
    """
    genes = Gene.objects.filter(snps__pk__in=[snp['pk'] for snp in snps]).all()
    return {gene.name: gene for gene in genes}


def get_bookmarked_report_data(request):
    result = request.user.user_profile.bookmarked_snps.prefetch_related(
        "studies", "genes"
    ).filter(
        related_genes__excluded=False,
        genes__name__isnull=False
    ).extra(
        select={
            "user_genotype": "genome_userrsid.genotype",
            "user_genotype_style": "genome_userrsid.genotype_style",
            # "gene_name": "genome_gene.name",
        },
        tables=["genome_userrsid"],
        where=[
            "genome_snp.rsid = genome_userrsid.rsid",
            "genome_userrsid.file_id = %s" % request.user.user_profile.active_file_id,
        ]
    ).annotate(
        snpstudies=JsonAgg(
            "studies__risk_allele", "studies__risk_allele_frequency",
            "studies__diseasestraits__name", "studies__p_value",
            distinct=True
        ),
        genes_list=ArrayAgg("genes__name", distinct=True)
    ).order_by("genes_list").all()

    return result.values(
        "pk", "rsid", "user_genotype", "user_genotype_style", "genes_list", "snpstudies",
        "importance", "minor_allele", "minor_allele_frequency", "red_flag",
    )


def get_snps_report_data(request):
    disease_name = request.GET.get("disease")
    result = Snp.objects.filter(
        importance__gte=4,
        related_genes__excluded=False,
        genes__name__isnull=False
    ).prefetch_related(
        "studies", "genes"
    ).extra(
        select={
            "user_genotype": "genome_userrsid.genotype",
            "user_genotype_style": "genome_userrsid.genotype_style",
            # "gene_name": "genome_gene.name",
        },
        tables=["genome_userrsid"],
        where=[
            "genome_snp.rsid = genome_userrsid.rsid",
            "genome_userrsid.file_id = %s" % request.user.user_profile.active_file_id,
            "genome_userrsid.genotype_style='homozygous_minor' OR genome_userrsid.genotype_style='minor'",
        ]
    ).annotate(
        snpstudies=JsonAgg(
            "studies__risk_allele", "studies__risk_allele_frequency",
            "studies__diseasestraits__name", "studies__p_value",
            distinct=True
        ),
        genes_list=ArrayAgg("genes__name", distinct=True)
    ).order_by("genes_list").all()

    if disease_name:
        diseases = DiseaseTrait.objects.filter(name__istartswith=disease_name).values_list("id", flat=True)

        hierarchy = DiseaseHierarchy.objects.filter(
            Q(disease__name__istartswith=disease_name)
        ).order_by("level").first()

        if hierarchy:
            diseases = list(hierarchy.get_descendants().values_list("disease_id", flat=True)) + list(diseases)
        result = result.filter(studies__diseasestraits__pk__in=diseases)
    return result.values(
        "pk", "rsid", "user_genotype", "user_genotype_style", "genes_list", "snpstudies",
        "importance", "minor_allele", "minor_allele_frequency", "red_flag",
    )


def get_important_report_data(request):
    result = Snp.objects.filter(
        importance__gte=5,
        related_genes__excluded=False,
        genes__name__isnull=False
    ).prefetch_related(
        "studies", "genes"
    ).extra(
        select={
            "user_genotype": "genome_userrsid.genotype",
            "user_genotype_style": "genome_userrsid.genotype_style",
            # "gene_name": "genome_gene.name",
        },
        tables=["genome_userrsid"],
        where=[
            "genome_snp.rsid = genome_userrsid.rsid",
            "genome_userrsid.file_id = %s" % request.user.user_profile.active_file_id,
        ]
    ).annotate(
        snpstudies=JsonAgg(
            "studies__risk_allele", "studies__risk_allele_frequency",
            "studies__diseasestraits__name", "studies__p_value",
            distinct=True
        ),
        genes_list=ArrayAgg("genes__name", distinct=True)
    ).order_by("genes_list").all()
    return result.values(
        "pk", "rsid", "user_genotype", "user_genotype_style", "genes_list", "snpstudies",
        "importance", "minor_allele", "minor_allele_frequency", "red_flag",
    )


def get_variance_report_data(request):
    query = request.GET.get("query")
    # maf = float(request.GET.get("maf") or '1.0')
    # imp = float(request.GET.get("importance") or '2.0')
    # rep = request.GET.get("reputation", 'all')
    sort_by = request.GET.get("sort_by")
    has_description = request.GET.get("has_description")
    sbmt = request.GET.get("sbmt")
    search_object = request.GET.get("search_object")

    if query and not search_object:
        query.upper()
        or_set = or_(func.upper(Gene.sa.fix_simple).contains(query),
                     func.upper(Gene.sa.fix_advanced).contains(query),
                     func.upper(Snp.sa.description_simple).contains(query),
                     func.upper(Snp.sa.rsid).contains(query),
                     func.upper(Snp.sa.description_advanced).contains(query),
                     func.upper(Snp.sa.name).contains(query),
                     func.upper(Gene.sa.name).contains(query)
                     )
    else:
        or_set = or_(True)

    aljquery = Snp.sa.query(
        Snp.sa.id.label('pk'), Snp.sa.rsid, Snp.sa.importance, Snp.sa.minor_allele,
        Snp.sa.minor_allele_frequency, Snp.sa.red_flag,
        UserRsid.sa.genotype.label('user_genotype'),
        UserRsid.sa.genotype_style.label('user_genotype_style'),
        label(
            'genes_list',
            column("""ARRAY_AGG(DISTINCT "genome_gene"."name")""", is_literal=True)),
        label(
            'snpstudies',
            column("""
                    JSON_AGG(DISTINCT
                        ("genome_snpstudy"."risk_allele",
                        "genome_snpstudy"."risk_allele_frequency",
                        "genome_diseasetrait"."name",
                        "genome_snpstudy"."p_value"))""", is_literal=True)),
        label(
            'rflag',
            column("""
                    red_flag_algo(
                        minor_allele,
                        JSON_AGG(DISTINCT
                            ("genome_snpstudy"."risk_allele",
                            "genome_snpstudy"."risk_allele_frequency",
                            "genome_diseasetrait"."name",
                            "genome_snpstudy"."p_value")
                        ),
                        genome_userrsid.genotype
                    )""", is_literal=True))).filter(
        # and_(Snp.sa.importance > imp),
        # and_(Snp.sa.minor_allele_frequency < maf),
        or_set,
        Gene.sa.name != None,
        SnpGenes.sa.excluded == False,
        UserRsid.sa.file_id == request.user.user_profile.active_file_id,
        Snp.sa.rsid == UserRsid.sa.rsid
    ).join(SnpGenes.sa).join(Gene.sa).outerjoin(SnpStudy.sa).join(
        UserRsid.sa, UserRsid.sa.rsid == Snp.sa.rsid
    ).outerjoin(SnpStudy.sa.diseasestraits).group_by(Snp.sa.id, UserRsid.sa.genotype,
                                                     UserRsid.sa.genotype_style)

    # .join(ChemicalGeneInteraction.sa).join(
    #     Chemical.sa
    # )

    if sbmt and has_description == 'on':
        aljquery = aljquery.filter(and_(Snp.sa.description_advanced != ''))

    if not sort_by:
        aljquery = aljquery.order_by('genes_list')

    elif sort_by == 'important_snps':
        aljquery = aljquery.order_by(desc(Snp.sa.importance))

    elif sort_by == 'red_flagged':
        aljquery = aljquery.having(
            column("""
                        red_flag_algo(
                            minor_allele,
                            JSON_AGG(DISTINCT
                                ("genome_snpstudy"."risk_allele",
                                "genome_snpstudy"."risk_allele_frequency",
                                "genome_diseasetrait"."name",
                                "genome_snpstudy"."p_value")
                            ),
                            genome_userrsid.genotype
                        )""", is_literal=True) == True)

    elif sort_by == 'most_rare':
        aljquery = aljquery.filter(and_(or_(UserRsid.sa.genotype_style == 'homozygous_minor',
                                            UserRsid.sa.genotype_style == 'minor')))

    if search_object == 'gene' and query:
        gene = Gene.objects.filter(name__icontains=query).first()
        aljquery = aljquery.filter(Gene.sa.id == gene.id)

    elif search_object == 'chemical' and query:
        chemical = Chemical.objects.filter(name__contains=query).exclude(
            recommendation_status='disallow_everywhere',
            categories__name__icontains='Obscure Chemicals'
        ).first()
        aljquery = aljquery.join(
            ChemicalGeneInteraction.sa
        ).join(Chemical.sa).filter(Chemical.sa.id == chemical.id)

    elif search_object == 'disease' and query:
        disease = DiseaseTrait.objects.filter(name__contains=query).first()
        aljquery = aljquery.filter(DiseaseTrait.sa.id == disease.id)

    return aljquery


def get_rare_report_data(request):
    result = Snp.objects.filter(
        importance__gte=1,
        minor_allele_frequency__lt=0.15,
        minor_allele_frequency__gt=0,
        related_genes__excluded=False,
        genes__name__isnull=False
    ).prefetch_related(
        "studies", "genes"
    ).extra(
        select={
            "user_genotype": "genome_userrsid.genotype",
            "user_genotype_style": "genome_userrsid.genotype_style",
            # "gene_name": "genome_gene.name",
        },
        tables=["genome_userrsid"],
        where=[
            "genome_snp.rsid = genome_userrsid.rsid",
            "genome_userrsid.file_id = %s" % request.user.user_profile.active_file_id,
            "genome_userrsid.genotype_style = 'homozygous_minor'",
        ]
    ).annotate(
        snpstudies=JsonAgg(
            "studies__risk_allele", "studies__risk_allele_frequency",
            "studies__diseasestraits__name", "studies__p_value",
            distinct=True
        ),
        genes_list=ArrayAgg("genes__name", distinct=True)
    ).order_by("genes_list").all()
    print(result.query)
    return result.values(
        "pk", "rsid", "user_genotype", "user_genotype_style", "genes_list", "snpstudies",
        "importance", "minor_allele", "minor_allele_frequency", "red_flag",
    )


def get_list_gene_pack_data(request, gene_pack=None):
    if gene_pack is None:
        gene_pack = GenePack.objects.prefetch_related(
            "genes"
        ).get(id=request.GET.get('entity_pk'))

    result = Snp.objects.filter(
        importance__gte=1,
        minor_allele_frequency__lte=1.0,
        genes__name__isnull=False,
        genes__in=list(gene_pack.genes.all())
    ).prefetch_related(
        "studies", "genes"
    ).extra(
        select={
            "user_genotype": "genome_userrsid.genotype",
            "user_genotype_style": "genome_userrsid.genotype_style",
            # "gene_name": "genome_gene.name",
        },
        tables=["genome_userrsid"],
        where=[
            "genome_snp.rsid = genome_userrsid.rsid",
            "genome_userrsid.file_id = %s" % request.user.user_profile.active_file_id,
        ]
    ).annotate(
        snpstudies=JsonAgg(
            "studies__risk_allele", "studies__risk_allele_frequency",
            "studies__diseasestraits__name", "studies__p_value",
            distinct=True
        ),
        genes_list=ArrayAgg("genes__name", distinct=True)
    ).order_by("genes_list").all()

    return result.values(
        "pk", "rsid", "user_genotype", "user_genotype_style", "genes_list", "snpstudies",
        "importance", "minor_allele", "minor_allele_frequency", "red_flag",
    )


@login_required
def list_gene_pack(request, id):
    gene_pack = get_object_or_404(GenePack.objects.prefetch_related("genes"), pk=id)
    query_set = get_list_gene_pack_data(request, gene_pack)
    paginator, page_set = paginate_report(request, query_set)

    problematic_genes = get_potentially_problematic_genes(request.user)
    problematic_genes = Gene.objects.filter(snps__id__in=problematic_genes).annotate(gene_cnt=Count('id')).values_list('id', flat=True)
    problematic_gene_pack_genes = gene_pack.genes.filter(id__in=problematic_genes).values_list("id", flat=True)

    interact_chem = ChemicalGeneInteraction.objects.filter(
        gene_id__in=list(problematic_gene_pack_genes),
        chemical__recommendation_status__in=["default"]
    ).annotate(
        total=Count('actions') + F('amount') - 1
    ).order_by("-total")
    beneficial_cat = SubstanceCategoryFactory.get(slug="beneficial-substances")

    _, chem_dict = _get_chemicals_by_gene_direction(problematic_gene_pack_genes)
    targeted_recommendations = get_recommendations(problematic_gene_pack_genes, beneficial_cat, values=('name', 'slug', 'id',))

    beneficial_sub_recommendations = Chemical.objects.exclude(
        recommendation_status='disallow_everywhere'
    ).filter(
        id__in=list(interact_chem.filter(
            chemical__categories__in=list(beneficial_cat.get_family())
        ).values_list('chemical_id', flat=True)[:5])
    ).annotate(Count("id"))

    beneficial_chem_list = list(
        beneficial_sub_recommendations.values('name', 'slug', 'id')
    )

    add_scores(beneficial_chem_list, chem_dict)

    data_set = _mark_bookmarked_snps(page_set.object_list, request.user)

    data_set = _snps_has_info(data_set, request.user.user_profile.active_file)

    genes = _get_snps_genes(data_set)

    return render(request, "genome/reports/list_gene_pack.html", {
        "query_set": data_set,
        "page_set": page_set,
        "query_set_count": paginator.count,
        "name": gene_pack.name,
        "report_state": "list_gene_pack",
        "entity_pk": id,
        "chem_recommendations": beneficial_chem_list,
        "targeted_recommendations": targeted_recommendations,
        "genes": genes,
    })


@login_required
@subscription_required(redirect_url="/checkout")
@uploaded_file_required(redirect_url="/uploader")
def my_rare_snps(request):
    query_set = get_rare_report_data(request)
    paginator, page_set = paginate_report(request, query_set)

    data_set = _snps_has_info(page_set.object_list, request.user.user_profile.active_file)

    data_set = _mark_bookmarked_snps(data_set, request.user)

    genes = _get_snps_genes(data_set)

    return render(request, "genome/reports/my_rare_snps.html", {
        "query_set": data_set,
        "page_set": page_set,
        "query_set_count": paginator.count,
        "genes": genes,
    })


@login_required
@subscription_required(redirect_url="/checkout")
@uploaded_file_required(redirect_url="/uploader")
def my_important_snps(request):
    query_set = get_important_report_data(request)
    paginator, page_set = paginate_report(request, query_set)

    data_set = _snps_has_info(page_set.object_list, request.user.user_profile.active_file)

    data_set = _mark_bookmarked_snps(data_set, request.user)

    genes = _get_snps_genes(data_set)

    return render(request, "genome/reports/my_important_snps.html", {
        "query_set": data_set,
        "page_set": page_set,
        "query_set_count": paginator.count,
        "genes": genes,
    })


@login_required
@subscription_required(redirect_url="/checkout")
@uploaded_file_required(redirect_url="/uploader")
def bookmarked_snps(request):
    if request.is_ajax() and request.method == "GET":
        UserProfile.objects.get(user=request.user).bookmarked_snps.remove(Snp.objects.get(id=request.GET["snp_id"]))
        return JsonResponse({
            "result": None,
            "error": None,
        })

    beneficial_cat = SubstanceCategoryFactory.get(slug="beneficial-substances")

    b_genes = tuple(request.user.user_profile.active_file.genes_to_look_at.values_list("id", flat=True))[:50]
    _, chem_dict = _get_chemicals_by_gene_direction(b_genes)

    bookmarked_recommendations = get_bookmarked_recommendations(request.user, beneficial_cat)

    add_scores(bookmarked_recommendations, chem_dict, reverse_ordering=True)

    query_set = get_bookmarked_report_data(request)

    paginator, page_set = paginate_report(request, query_set)

    data_set = _snps_has_info(page_set.object_list, request.user.user_profile.active_file)

    genes = _get_snps_genes(data_set)

    return render(request, "genome/reports/bookmarked_snps.html", {
        "query_set": data_set,
        "page_set": page_set,
        "query_set_count": paginator.count,
        "bookmarked_recomendations": bookmarked_recommendations,
        "genes": genes,
        "bookmarked_page": True,
    })


@login_required
@subscription_required(redirect_url="/checkout")
@uploaded_file_required(redirect_url="/uploader")
def snps_to_look_at(request):
    query_set = get_snps_report_data(request)
    paginator, page_set = paginate_report(request, query_set)

    data_set = _snps_has_info(page_set.object_list, request.user.user_profile.active_file)

    data_set = _mark_bookmarked_snps(data_set, request.user)

    genes = _get_snps_genes(data_set)

    return render(request, "genome/reports/snps_to_look_at.html", {
        "query_set": data_set,
        "page_set": page_set,
        "query_set_count": paginator.count,
        "genes": genes,
    })


@login_required
@subscription_required(redirect_url="/checkout")
@uploaded_file_required(redirect_url="/uploader")
def variance_report(request):
    query_set = get_variance_report_data(request)
    #
    # paginator, page_set = paginate_report(request, query_set)

    data_set, page_info = paginate_sqla_statement(query_set, request.GET.get('page'))

    data_set = [obj.__dict__ for obj in data_set]
    data_set = _snps_has_info(data_set, request.user.user_profile.active_file)

    data_set = _mark_bookmarked_snps(data_set, request.user)

    genes = _get_snps_genes(data_set)

    return render(request, "genome/reports/variance_report.html", {
        "query_set": data_set,
        "page_set": page_info,
        "query_set_count": page_info.total_count,
        "genes": genes,
    })


@login_required
@uploaded_file_required(redirect_url="/uploader")
def get_report_pdf_page(request):
    report_state = request.GET.get("report")
    get_report_params = {
        "snps_to_look_at": {"function": get_snps_report_data, "filename": "My Bad SNPs report"},
        "my_important_snps": {"function": get_important_report_data, "filename": "My Important SNPs report"},
        "my_rare_snps": {"function": get_rare_report_data, "filename": "My Bad Rare SNPs report"},
        "bookmarked_snps": {"function": get_bookmarked_report_data, "filename": "My Bookmarked SNPs report"},
        "variance_report": {"function": get_variance_report_data, "filename": "My Custom SNPs report"},
        "list_gene_pack": {"function": get_list_gene_pack_data, "filename": "My Gene Pack SNPs report"},
    }
    query_set = get_report_params[report_state]['function'](request)
    template = get_template('genome/reports/pdf_report.html')

    paginator, page_set = paginate_report(request, query_set)

    data_set = [obj._asdict() for obj in page_set.object_list]
    data_set = _snps_has_info(data_set, request.user.user_profile.active_file)

    genes = _get_snps_genes(data_set)

    context = {
        "query_set": data_set,
        "query_set_count": paginator.count,
        "request": request,
        "genes": genes,
        "user": request.user,
    }

    html = template.render(context)

    import pdfkit

    out_path = '/tmp/%s-%s.pdf' % (report_state, request.user.pk,)

    options = {
        'page-size': 'A4',
        'encoding': "UTF-8",
        'margin-top': 15,
        'margin-bottom': 15,
        'zoom': 1
    }

    pdfkit.from_string(html, out_path, options=options)
    response = FileResponse(open(out_path, 'rb'), content_type='application/octet-stream')
    response['Content-Disposition'] = 'attachment; filename="' + get_report_params[report_state]['filename'] + '.pdf"'

    return response


class CompareReports(LoginRequiredMixin, DetailView):
    template_name = 'genome/variance_report_comparison.html'
    slug_url_kwarg = 'comparator'
    per_page = 50
    _current_user_table_alias = 't1'
    _comparator_user_table_alias = 't2'

    _subquery_fields = [
        Snp.sa.id.label('pk'),
        Snp.sa.rsid.label('rsid'),
        Snp.sa.importance.label('importance'), Snp.sa.minor_allele.label('minor_allele'),
        Snp.sa.minor_allele_frequency.label('minor_allele_frequency'),
        Snp.sa.red_flag.label('red_flag'),
        UserRsid.sa.genotype.label('user_genotype'),
        UserRsid.sa.genotype_style.label('user_genotype_style'),
        label('genes_list', column(
            """ARRAY_AGG(DISTINCT "genome_gene"."name")""", is_literal=True)),
        label('snpstudies', column("""JSON_AGG(DISTINCT
                                  ("genome_snpstudy"."risk_allele",
                                  "genome_snpstudy"."risk_allele_frequency",
                                  "genome_diseasetrait"."name",
                                  "genome_snpstudy"."p_value"))""", is_literal=True)),
    ]

    def _build_query(self, fields, file_id):
        """
        Builds query with specific fields
        :param fields: list of SqlAlchemy fields
        :param file_id: User file id

        :return: SqlAlchemy Query
        """

        query = Snp.sa.query(*fields).filter(
            Gene.sa.name != None,
            SnpGenes.sa.excluded == False,
            Snp.sa.rsid == UserRsid.sa.rsid,
            UserRsid.sa.file_id == file_id,
            UserRsid.sa.genotype != '--'
        ).join(SnpGenes.sa).join(Gene.sa).outerjoin(SnpStudy.sa).join(
            UserRsid.sa, UserRsid.sa.rsid == Snp.sa.rsid
        ).outerjoin(
            SnpStudy.sa.diseasestraits
        ).group_by(
            Snp.sa.id,
            UserRsid.sa.genotype,
            UserRsid.sa.genotype_style
        ).filter(
            Snp.sa.importance >= 1,
            has_minor_alleles(count=2)
        )

        if self.kwargs.get('compare_type') == 'bad_snps':
            query = query.filter(
                or_(
                    UserRsid.sa.genotype_style == 'homozygous_minor',
                    UserRsid.sa.genotype_style == 'minor'
                ),
            )
        elif self.kwargs.get('compare_type') == 'rare_snps':
            query = query.filter(
                Snp.sa.minor_allele_frequency <= 0.15,
                Snp.sa.minor_allele_frequency > 0.0,
                UserRsid.sa.genotype_style == 'homozygous_minor',
            )

        return query

    def get_queryset(self):
        comparator_username = self.kwargs.get('comparator')
        access_request = AccessRequest.objects.filter(hash=comparator_username).first()

        comparator_instance = access_request.sender.user_profile

        current_user_query = alias(
            self._build_query(
                self._subquery_fields,
                access_request.receiver.user_profile.active_file_id
            ),
            name=self._current_user_table_alias
        )
        comparator_user_query = alias(
            self._build_query(
                self._subquery_fields,
                comparator_instance.active_file_id
            ),
            name=self._comparator_user_table_alias
        )

        cur_user = current_user_query.c
        comp_user = comparator_user_query.c

        fields = [
            label('pk', coalesce(cur_user.pk, comp_user.pk)),
            label('rsid', coalesce(cur_user.rsid, comp_user.rsid)),
            label('importance', coalesce(cur_user.importance, comp_user.importance)),
            label('minor_allele', coalesce(cur_user.minor_allele, comp_user.minor_allele)),
            label('minor_allele_frequency', coalesce(
                cur_user.minor_allele_frequency,
                comp_user.minor_allele_frequency
            )),
            label('red_flag', coalesce(cur_user.red_flag, comp_user.red_flag)),
            label('genes_list', coalesce(cur_user.genes_list, comp_user.genes_list)),
            label('snpstudies', coalesce(cur_user.snpstudies, comp_user.snpstudies)),
            current_user_query.c.user_genotype.label('t1_user_genotype'),
            current_user_query.c.user_genotype_style.label('t1_user_genotype_style'),
            comparator_user_query.c.user_genotype.label('t2_user_genotype'),
            comparator_user_query.c.user_genotype_style.label('t2_user_genotype_style'),
        ]

        result_query = Query(fields).select_from(
            current_user_query
        ).join(
            comparator_user_query,
            comparator_user_query.c.pk == current_user_query.c.pk,
            full=True
        ).order_by(
            asc(coalesce(current_user_query.c.genes_list, comparator_user_query.c.genes_list))
        ).filter(comparator_user_query.c.user_genotype == current_user_query.c.user_genotype,)

        return result_query

    def paginate_query(self):
        page_num = self.request.GET.get('page')
        return paginate_sqla_statement(self.query, page_num, self.per_page)

    def get_context_data(self, **kwargs):
        comparator_username = self.kwargs.get('comparator')
        access_request = AccessRequest.objects.filter(hash=comparator_username).first()
        self.query = self.get_queryset()
        object_list, pagination_info = self.paginate_query()
        title = "Bad and Rare SNPs" if self.kwargs.get('compare_type') == 'rare_snps' else "Bad genes"

        context = {
            'object_list': object_list,
            'pagination': pagination_info,
            'current_user_name': access_request.receiver.username,
            'comparator_user_name': access_request.sender.username,
            'page_title': title
        }

        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()

        return self.render_to_response(context=context)
