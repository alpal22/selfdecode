from allauth.account.signals import user_signed_up

from django.dispatch import receiver

from .models import GenePack, UserProfile


@receiver(user_signed_up)
def auto_install_snp_packs(request, user, **wkargs):
    available_packs = GenePack.objects.filter(pack_type="sponsored")
    UserProfile.objects.get(user=user).gene_packs.add(*available_packs)
