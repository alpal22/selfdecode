from django.contrib import admin

# Register your models here.
from django.contrib.contenttypes.admin import GenericTabularInline
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe

from analysis.forms import SystemAdminForm, SymptomAdminInlineForm, ForeignObjectForm
from analysis.models import Symptom, System, SystemSymptoms


class SymptomAdminInline(admin.TabularInline):
    model = SystemSymptoms
    form = SymptomAdminInlineForm
    extra = 0


@admin.register(Symptom)
class SymptomAdmin(admin.ModelAdmin):
    form = ForeignObjectForm
    list_display = ("id", "slug", "name",)
    list_display_links = ("id", "slug",)
    search_fields = ("name", "slug",)
    readonly_fields = ("slug",)


@admin.register(System)
class SystemAdmin(admin.ModelAdmin):
    form = SystemAdminForm
    list_display = ("id", "slug", "name", "threshold", "symptoms_callable",)
    list_display_links = ("id", "slug",)
    search_fields = ("name", "slug",)
    readonly_fields = ("slug",)

    inlines = (SymptomAdminInline,)

    def symptoms_callable(self, inst):
        return mark_safe(", ".join(
            [('<a href="%s">%s</a>' % (reverse("admin:analysis_symptom_change", args=(o.pk,)), o.name,)) for o in inst.symptoms.all()]
        ))

    symptoms_callable.short_description = "Symptoms"
