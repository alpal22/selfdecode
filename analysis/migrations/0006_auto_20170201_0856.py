# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-02-01 08:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0005_auto_20170130_1327'),
    ]

    operations = [
        migrations.AddField(
            model_name='system',
            name='threshold_symptoms',
            field=models.ManyToManyField(blank=True, related_name='threshold_systems', to='analysis.Symptom'),
        ),
    ]
