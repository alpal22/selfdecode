# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-02-16 12:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0008_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='symptom',
            name='selfhacked_link',
            field=models.CharField(blank=True, max_length=255),
        ),
    ]
