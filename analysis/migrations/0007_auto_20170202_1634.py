# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-02-02 16:34
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0006_auto_20170201_0856'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='system',
            name='threshold_symptoms',
        ),
    ]
