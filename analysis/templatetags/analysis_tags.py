from django import template

register = template.Library()


@register.assignment_tag()
def exclude_similar_symptom(item, query_set_symptoms):
    """
    Removes similar related objects from symptoms list
    :param item: RelatesObject
    :param query_set_symptoms: QuerySet
    :return: bool
    """
    symptoms_lower = [symptom.lower() for symptom in query_set_symptoms.values_list('name', flat=True)]
    related_lower = item.object.name.lower()
    return related_lower not in symptoms_lower


@register.simple_tag
def progress_status(page):
    """
    Calc progress on the personalized health report page
    :param page: PageObject
    :return: int
    """
    return (100 * page.number) // page.paginator.num_pages
