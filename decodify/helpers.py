import time

import datetime

import math

from django.contrib.gis.geoip2 import GeoIP2
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from ipware.ip import get_ip


def get_geo_location(request):
    try:
        ip = get_ip(request)
        g = GeoIP2()
        location = g.country(ip)["country_code"]
        if location != "US":
            location = "international"
    except:
        location = "international"

    return location


def paginate(objects_list, page, per_page=25):
    paginator = Paginator(objects_list, per_page)

    try:
        result_page = paginator.page(page)
    except PageNotAnInteger:
        result_page = paginator.page(1)
    except EmptyPage:
        result_page = paginator.page(paginator.num_pages)

    return result_page


def remaining_time(count, end="\n"):
    remaining_time.i += 1
    cur_time = time.monotonic()
    if cur_time - remaining_time.start_time >= 1 or remaining_time.i >= count:
        remaining_time.start_time = cur_time
        i_per_sec = remaining_time.i - remaining_time.last_i
        remaining_time.last_i = remaining_time.i
        time_remaining = (count - remaining_time.i) / i_per_sec
        time_remaining = datetime.timedelta(seconds=math.floor(time_remaining))
        print("Time left %s" % time_remaining, end=" | ")
        print("%s rows per second" % i_per_sec, end=" | ")
        print("%s/%s" % (remaining_time.i, count,), end=end)
        if remaining_time.i >= count:
            remaining_time.start_time = 0
            remaining_time.last_i = 0
            remaining_time.i = 0
remaining_time.start_time = 0
remaining_time.last_i = 0
remaining_time.i = 0
