from django.db.models import Aggregate, Func
from django.db.models.expressions import Expression
from django.db.models.fields import CharField


class ArrayAgg(Aggregate):
    function = 'ARRAY_AGG'
    name = 'ArrayAgg'
    template = '%(function)s(%(distinct)s%(expressions)s)'

    def __init__(self, expression, distinct=False, **extra):
        super(ArrayAgg, self).__init__(
            expression, distinct='DISTINCT ' if distinct else '', **extra)

    def convert_value(self, value, expression, connection, context):
        return list(filter(lambda v: v, value))


class GenotypeColorIndex(Expression):

    def as_sql(self, compiler, connection):
        return "case genotype_style when 'major' then '2' when 'minor' then '0' when 'heterozygous' then '1' when 'homozygous_minor' then '0' when 'homozygous_major' then '2' else genotype_style end", []


class JsonAgg(Aggregate):
    function = "JSON_AGG"
    name = "JsonAgg"
    template = '%(function)s(%(distinct)s(%(expressions)s))'

    def __init__(self, *expressions, distinct=False, **extra):
        super(JsonAgg, self).__init__(
            *expressions, distinct='DISTINCT ' if distinct else '', output_field=CharField(), **extra)

    def convert_value(self, value, expression, connection, context):
        return list(filter(lambda v: v, value))


class ArrayaAggDelimited(Aggregate):
    function = ""
    name = "ArrayaAggDelimited"
    template = "string_to_array(array_to_string(array_agg(%(expressions)s),'%(delimiter)s'), '%(delimiter)s')"

    def __init__(self, *expressions, distinct=False, **extra):
        super(ArrayaAggDelimited, self).__init__(
            *expressions, **extra)
        self.distinct = distinct

    def convert_value(self, value, expression, connection, context):
        res = list(filter(lambda v: v, value))
        if self.distinct:
            res = list(set(res))
        return res


class Position(Func):
    function = 'POSITION'
    name = 'Position'
    template = '%(function)s(%(expressions)s)'


class ArrayaToString(Aggregate):
    function = ""
    name = "ArrayaToString"
    template = "array_to_string(array_agg(%(distinct)s(%(expressions)s)),'%(delimiter)s')"

    def __init__(self, *expressions, distinct=False, **extra):
        print(extra, expressions)
        super(ArrayaToString, self).__init__(
            *expressions, distinct='DISTINCT ' if distinct else '', output_field=CharField(), **extra)
        self.distinct = distinct
