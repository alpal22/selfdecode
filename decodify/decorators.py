from django.core.exceptions import PermissionDenied


def ajax_required(fn):
    def wrapper(request, *args, **kwargs):
        if not request.is_ajax():
            raise PermissionDenied
        return fn(request, *args, **kwargs)
    return wrapper


def disallow_demouser(fn):
    def wrapper(request, *args, **kwargs):
        if request.user.user_profile.active_file.is_demofile and not request.user.user_profile.has_subscription():
            raise PermissionDenied
        return fn(request, *args, **kwargs)
    return wrapper
