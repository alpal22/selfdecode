

class BaseFactory(object):
    _cache = {}

    @classmethod
    def get(cls, slug):
        instance = cls._cache.get(slug, None)
        if instance is None:
            instance = cls.model.objects.filter(slug=slug).first()
            cls._cache.update({slug: instance})

        return instance
