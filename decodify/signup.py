from datetime import timedelta

from allauth.account.views import SignupView
from django.utils import timezone

from payment.constants import PROVIDER_STRIPE
from payment.models import Transaction


class SignupFreeView(SignupView):
    template_name = "account/signup_free.html"

    def form_valid(self, form):
        result = super(SignupFreeView, self).form_valid(form=form)
        days = 365*3
        Transaction.objects.create(
            user=self.user,
            duration_in_days=days,
            transaction_type="a_signup",
            provider=PROVIDER_STRIPE,
            canceled=False,
            date_start=timezone.now(),
            date_expires=timezone.now() + timedelta(days=days)
        )
        return result
