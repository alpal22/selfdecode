from sqlalchemy import column


def has_minor_alleles(count=2):
    """
    SqlAlchemy Column to get count minor alleles in user genotype
    :param count: minor alleles count in genotype. Default: 2
    :return: SqlAlchemy column
    """
    return column("""
        (
            CHAR_LENGTH(genome_userrsid.genotype) - CHAR_LENGTH(
                REPLACE(genome_userrsid.genotype, genome_snp.minor_allele, '')
            )
        ) = %s
        """ % count, is_literal=True)
