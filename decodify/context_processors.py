from django.conf import settings


def settings_to_templates(request):
    return {
        "config": {
            "AJAX_NOINFO_MESSAGE": settings.AJAX_NOINFO_MESSAGE,
        }
    }
