"""decodify URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import debug_toolbar
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include
from django.conf import settings
from django.conf.urls.static import static

from decodify.signup import SignupFreeView
from rest_framework.documentation import include_docs_urls

urlpatterns = [
    url(r'^joes_office/', admin.site.urls),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^accounts/signup_free/', SignupFreeView.as_view(), name="account_signup_free"),
    url(r'^hijack/', include('hijack.urls')),
    url(r'^paypal/', include('paypal.standard.ipn.urls')),
    url(r'', include("genome.urls")),
    url(r'', include("chemical.urls", namespace="chemical")),
    url(r'', include("payment.urls", namespace="payment")),
    url(r'', include("guide.urls", namespace="guide")),
    url(r'', include("analysis.urls", namespace="analysis")),
    url(r'^fileapi/', include("fileapi.urls", namespace="fileapi")),
    url(r'^forum/', include("forum.urls", namespace="forum")),
    url(r'', include("experiment.urls", namespace="experiment")),
    url(r'', include("search.urls", namespace="search")),
    url(r'', include("database.urls", namespace="database")),
    url(r'', include('recommendation.urls', namespace="recommendation")),
    url(r'', include("affiliate.urls", namespace="affiliate")),
    url(r'^search/', include('haystack.urls')),
    url(r'', include("rating.urls", namespace="rating")),
    url(r'^social/', include('social.urls', namespace="social")),
    url(r'^api/v1/', include("api.urls", namespace="api")),
    url(r'^blog/', include('blog.urls', namespace="blog"))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    urlpatterns += [
        url(r'^api-docs/', include_docs_urls(title="Api docs")),
    ]

urlpatterns += [
    url(r'__debug__/', include(debug_toolbar.urls)),
]

handler500 = 'genome.views.handler500'
handler404 = 'genome.views.handler404'
handler403 = 'genome.views.handler403'
