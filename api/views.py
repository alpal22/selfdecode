from django.db.models import Case, CharField
from django.db.models import Count, Max
from django.db.models import F
from django.db.models import Value
from django.db.models import When
from rest_framework.decorators import detail_route
from rest_framework.viewsets import GenericViewSet

from api.serializers import ChemicalPathwaySerializer, ChemicalGeneInteractionSerializer, ChemicalOrganismSerializer, \
    ChemicalPreparationSerializer
from chemical.models import Chemical
from decodify.aggregators import ArrayAgg
from genome.helpers import badges_for_gene


class ChemicalView(GenericViewSet):
    """
    get:
        Get chemical related data
    """

    queryset = Chemical.objects.all()

    @detail_route(url_path="related-pathways")
    def related_pathways(self, request, pk):
        instance = self.get_object()
        pathways = instance.pathways.all()
        pathways = self.paginate_queryset(pathways)

        return self.get_paginated_response(
            ChemicalPathwaySerializer(pathways, many=True).data
        )

    @detail_route(url_path="genes-interactions")
    def genes_interactions(self, request, pk):
        instance = self.get_object()

        interactions = instance.chemicalgeneinteraction_set.all().annotate(
            total=Count('actions') + F('amount') - 1,
            action=ArrayAgg('actions__action')
        ).order_by('-total').prefetch_related(
            "actions", "actions__interaction_type"
        ).select_related("gene")

        interactions = self.paginate_queryset(interactions)

        all_interact_genes = {interaction.gene.id for interaction in interactions}

        if request.user.is_authenticated():
            contains_risk_allele, bad_genes = badges_for_gene(request.user, all_interact_genes)
            for interaction in interactions:
                setattr(interaction.gene, 'bad_gene', (str(interaction.gene.pk) in bad_genes))
                setattr(interaction.gene, 'contains_risk_allele', (str(interaction.gene.pk) in contains_risk_allele))

        return self.get_paginated_response(
            ChemicalGeneInteractionSerializer(interactions, many=True).data
        )

    @detail_route(url_path="top-organisms")
    def top_organisms(self, request, pk):
        instance = self.get_object()

        organisms = instance.concentrations.exclude(unified_concentration__isnull=True).exclude(organism__slug__isnull=True).annotate(
            slug=F("organism__slug"),
            name=Case(
                When(organism__english_name__isnull=False, then=F("organism__english_name")),
                default=F("organism__latin_name"),
                output_field=CharField(),
            )).values("slug", "name", "organism__english_name", "organism__slug", "conc_unit", "conc_min", "conc_max").annotate(
            max_conc=Max("unified_concentration")).filter(rel_type="organism").order_by("-max_conc").all()

        organisms = self.paginate_queryset(organisms)
        return self.get_paginated_response(
            ChemicalOrganismSerializer(organisms, many=True).data
        )

    @detail_route(url_path="top-foods")
    def top_foods(self, request, pk):
        instance = self.get_object()

        foods = instance.concentrations.exclude(unified_concentration__isnull=True).values("preparation__name", "conc_unit", "conc_min", "conc_max").annotate(
            max_conc=Max("unified_concentration")).filter(rel_type="preparation").order_by("-max_conc").all()

        foods = self.paginate_queryset(foods)

        return self.get_paginated_response(
            ChemicalPreparationSerializer(foods, many=True).data
        )
