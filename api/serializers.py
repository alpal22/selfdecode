from django.core.urlresolvers import reverse
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer, Serializer
from rest_framework import serializers

from chemical.models import ChemicalPathway, ChemicalGeneInteraction, Organism, ChemicalGeneInteractionAction, \
    ChemicalGeneInteractionType, ChemicalConcentration
from genome.models import Gene


class ChemicalPathwaySerializer(ModelSerializer):
    details_page_url = SerializerMethodField()

    def get_details_page_url(self, instance):
        return reverse('pathway_details', args=(instance.pathway_id,))

    class Meta:
        model = ChemicalPathway
        fields = ('pathway_id', 'pathway_name', 'details_page_url',)


class OrganismSerializer(ModelSerializer):
    name = SerializerMethodField()

    def get_name(self, instance):
        return instance.english_name.title()

    class Meta:
        model = Organism
        fields = ('id', 'name',)


class GeneSerializer(ModelSerializer):
    name = SerializerMethodField()
    bad_gene = SerializerMethodField()
    contains_risk_allele = SerializerMethodField()
    details_page_url = SerializerMethodField()

    def get_name(self, instance):
        return instance.name.upper()

    def get_bad_gene(self, instance):
        return getattr(instance, 'bad_gene', None)

    def get_contains_risk_allele(self, instance):
        return getattr(instance, 'contains_risk_allele', None)

    def get_details_page_url(self, instance):
        return reverse('gene', args=(instance.slug,))

    class Meta:
        model = Gene
        fields = ('id', 'name', 'slug', 'description_simple', 'function', 'bad_gene', 'contains_risk_allele', 'details_page_url',)


class ChemicalGeneInteractionTypeSerializer(ModelSerializer):
    class Meta:
        model = ChemicalGeneInteractionType
        fields = ('id', 'name',)


class ChemicalGeneInteractionActionSerializer(ModelSerializer):
    interaction_type = SerializerMethodField()

    def get_interaction_type(self, instance):
        return ChemicalGeneInteractionTypeSerializer(instance.interaction_type).data

    class Meta:
        model = ChemicalGeneInteractionAction
        fields = ('id', 'action', 'interaction_type',)


class ChemicalGeneInteractionSerializer(ModelSerializer):
    gene = SerializerMethodField()
    organism = SerializerMethodField()
    actions = SerializerMethodField()
    pubmed_urls = SerializerMethodField()

    def get_gene(self, instance):
        return GeneSerializer(instance.gene).data

    def get_organism(self, instance):
        return OrganismSerializer(instance.organism).data

    def get_actions(self, instance):
        return ChemicalGeneInteractionActionSerializer(instance.actions, many=True).data

    def get_pubmed_urls(self, instance):
        return instance.pub_med_ids_as_list()

    class Meta:
        model = ChemicalGeneInteraction
        fields = ('id', 'pubmed_urls', 'gene', 'organism', 'actions',)


class ChemicalOrganismSerializer(Serializer):
    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    name = serializers.CharField(max_length=250)
    organism__slug = serializers.CharField(max_length=250)
    conc_unit = serializers.CharField(max_length=250)
    max_conc = serializers.FloatField()
    conc_min = serializers.FloatField()
    conc_max = serializers.FloatField()
    details_page_url = SerializerMethodField()

    def get_details_page_url(self, instance):
        return reverse('chemical:organism', args=(instance.get("organism__slug"),))


class ChemicalPreparationSerializer(Serializer):
    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    preparation__name = serializers.CharField(max_length=250)
    conc_unit = serializers.CharField(max_length=250)
    max_conc = serializers.FloatField()
