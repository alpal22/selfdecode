from api import views

from rest_framework import routers

router = routers.SimpleRouter()

router.register(r'chemical', views.ChemicalView, "chemical")

urlpatterns = router.urls
