from django.contrib import admin

# Register your models here.
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe

from experiment.models import Identification


@admin.register(Identification)
class IdentificationAdmin(admin.ModelAdmin):
    list_display = ("pk", "slug", "name", "title_display", "factor_name", "factor_type", "view_page",)
    search_fields = ("name", "slug", "title",)
    list_display_links = ("pk", "slug",)

    def title_display(self, inst):
        max_length = 70
        return mark_safe('<span title="%s">%s</span>' % (
            inst.title,
            inst.title[:max_length] + ("..." if len(inst.title) > max_length else ""),
        ))

    def view_page(self, inst):
        return mark_safe('<a href="%s" target="_blank">View Page</a>' % (
            reverse("experiment:details", args=(inst.slug,)),
        ))

    view_page.short_description = ""

    title_display.short_description = "Title"
    title_display.admin_order_field = "title"
