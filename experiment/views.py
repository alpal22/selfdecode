from django.core.paginator import PageNotAnInteger
from django.core.paginator import Paginator, EmptyPage
from django.db.models import Count
from django.db.models import F
from django.db.models import Max, Min
from django.db.models import QuerySet
from django.http import JsonResponse
from django.http import response
from django.shortcuts import render
from django.template import Context
from django.template.loader import get_template
from django.views.generic import DetailView
from django.views.generic import ListView

from decodify.aggregators import JsonAgg, ArrayAgg
from experiment.models import Identification, Analytics
from genome.mixins import AutocompleteSlugMixin


class ExperimentDetails(DetailView):
    model = Identification
    template_name = "experiment/details.html"

    def get_queryset(self):
        query_set = super(ExperimentDetails, self).get_queryset().prefetch_related(
            "configuration_set", "configuration_set__analytics_set"
        )
        return query_set

    def get_context_data(self, **kwargs):
        context = super(ExperimentDetails, self).get_context_data(**kwargs)
        context['min_positive'] = self.queryset.filter(configuration_set__analytics_set__log2fold__gte=0).aggregate(Min('configuration_set__analytics_set__log2fold'))
        context['max_positive'] = self.queryset.filter(configuration_set__analytics_set__log2fold__gte=0).aggregate(Max('configuration_set__analytics_set__log2fold'))
        context['min_negative'] = self.queryset.filter(configuration_set__analytics_set__log2fold__lte=0).aggregate(Min('configuration_set__analytics_set__log2fold'))
        context['max_negative'] = self.queryset.filter(configuration_set__analytics_set__log2fold__lte=0).aggregate(Max('configuration_set__analytics_set__log2fold'))
        return context


def experiment_ajax(request, slug):
    query_set = Identification.objects.filter(slug=slug)
    analytics = Analytics.objects.filter(configuration__identification=query_set).values(
        "gene_name", "gene__name"
    ).annotate(
        log2folds=JsonAgg("configuration_id", "log2fold"),
    ).extra(
        select={"ordering": "abs(log2fold)"}).order_by('-ordering')
    context = {
        'min_positive': query_set.filter(configuration__analytics__log2fold__gte=0).aggregate(
            Min('configuration__analytics__log2fold'))["configuration__analytics__log2fold__min"],
        'max_positive': query_set.filter(configuration__analytics__log2fold__gte=0).aggregate(
            Max('configuration__analytics__log2fold'))["configuration__analytics__log2fold__max"],
        'min_negative': query_set.filter(configuration__analytics__log2fold__lte=0).aggregate(
            Min('configuration__analytics__log2fold'))["configuration__analytics__log2fold__min"],
        'max_negative': query_set.filter(configuration__analytics__log2fold__lte=0).aggregate(
            Max('configuration__analytics__log2fold'))["configuration__analytics__log2fold__max"],
        'slicer': ":%s" % query_set.get().configuration_set.count(),
        'configurations': query_set.get().configuration_set.values("id"),
    }
    page = request.GET.get("page")
    paginator = Paginator(analytics, 10)
    template = get_template('experiment/partials/_details_table_body.html')

    try:
        current_page = paginator.page(page)
    except PageNotAnInteger:
        current_page = paginator.page(1)
    except EmptyPage:
        current_page = paginator.page(paginator.num_pages)

    if current_page.has_next():
        has_next = True
        next_page_number = current_page.next_page_number()
    else:
        next_page_number = current_page.paginator.num_pages
        has_next = False
    context = Context({
        "analytics": current_page.object_list,
        "context": context
    })
    return JsonResponse({
        'paginator': {
            'has_next': has_next,
            'next_page_number': next_page_number,
        },
        'data': template.render(context)
    })


def experiment_details(request, slug):
    query_set = Identification.objects.prefetch_related(
        "configuration_set"
    ).filter(slug=slug)

    analytics = Analytics.objects.filter(configuration__identification=query_set).values(
        "gene_name", "gene__name"
    ).annotate(
        log2folds=JsonAgg("configuration_id", "log2fold"),
    ).extra(
        select={"ordering": "abs(log2fold)"}).order_by('-ordering')
    context = {
        'min_positive': query_set.filter(configuration__analytics__log2fold__gte=0).aggregate(
            Min('configuration__analytics__log2fold'))["configuration__analytics__log2fold__min"],
        'max_positive': query_set.filter(configuration__analytics__log2fold__gte=0).aggregate(
            Max('configuration__analytics__log2fold'))["configuration__analytics__log2fold__max"],
        'min_negative': query_set.filter(configuration__analytics__log2fold__lte=0).aggregate(
            Min('configuration__analytics__log2fold'))["configuration__analytics__log2fold__min"],
        'max_negative': query_set.filter(configuration__analytics__log2fold__lte=0).aggregate(
            Max('configuration__analytics__log2fold'))["configuration__analytics__log2fold__max"],
        'slicer': ":%s" % query_set.get().configuration_set.count(),
        'configurations': query_set.get().configuration_set.values("id"),
    }
    return render(request, "experiment/details.html", {
        "object": query_set.get(),
        "context": context,
        "analytics": analytics[:10]
    })
