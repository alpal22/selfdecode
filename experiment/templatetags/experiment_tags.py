from colour import Color
from django import template
from django.utils.safestring import mark_safe

register = template.Library()


@register.simple_tag
def colour_saturation(color, min, max, value):
    color = Color(color)
    if abs(float(max)) - abs(float(min)) != 0:
        saturation = (abs(float(value)) - abs(float(min))) / (abs(float(max)) - abs(float(min)))
    else:
        saturation = 1
    color.set_saturation(saturation if saturation <= 1 else 1)
    return color


@register.filter
def max_log2fold(agg_list):
    if len(agg_list) > 0:
        agg_list = [record["f2"] for record in agg_list]
        return max(agg_list)
    else:
        return ''


@register.simple_tag
def log2folds_tds(log2folds, context):
    response = ''
    log2folds = log2folds_to_dict(log2folds)
    for conf in context["configurations"]:
        if log2folds.get(conf["id"]):
            if log2folds.get(conf["id"]) >= 0:
                color = colour_saturation("red", context["min_positive"], context["max_positive"], log2folds.get(conf["id"]))
            else:
                color = colour_saturation("blue", context["max_negative"], context["min_negative"], log2folds.get(conf["id"]))

            response += '<td style="background:%s"><div class="gxaShowCell">%s</div></td>' % (color, log2folds.get(conf["id"]))
        else:
            response += '<td></td>'
    return mark_safe(response)



@register.filter
def log2folds_to_dict(agg_list):
    if len(agg_list) > 0:
        return {record["f1"]: record["f2"] for record in agg_list}
    else:
        return {}
