from django.conf.urls import url

from experiment.views import experiment_details, experiment_ajax

urlpatterns = [
    url(r'^experiment_ajax/(?P<slug>.+)/', experiment_ajax, name="experiment_ajax"),
    url(r'^experiment_ajax/(?P<slug>.+)/', experiment_ajax, name="experiment_ajax"),
    url(r'^experiment/(?P<slug>.*)/$', experiment_details, name="details"),
]
