import os
from django.db import transaction
import xml.etree.ElementTree as ET

import pandas
from django.core.management import BaseCommand


from decodify.helpers import remaining_time
from experiment.models import Identification, Configuration


class Command(BaseCommand):
    help = "./manage.py UploadAndAssignChemicalMOAS --file moas.csv"

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)

    def handle(self, *args, **options):
        files = os.listdir(options.get('file'))
        count = len(files)
        with transaction.atomic():
            experiments_to_save = []
            for file in files:
                remaining_time(count)
                print(file)
                ident = Identification.objects.filter(name=file[:-18]).first()
                if not ident:
                    continue

                tree = ET.parse(options.get('file') + file)  # Initiates the tree Ex: <user-agents>
                root = tree.getroot()  # Starts the root of the tree Ex: <user-agent>

                confs = []
                for contrast in root.iter('contrast'):
                    conf = {}
                    # for i, sub in enumerate(contrast):
                    conf["group"] = contrast.attrib["id"]
                    for j, sub_sub in enumerate(contrast):
                        conf[sub_sub.tag] = sub_sub.text
                    confs.append(conf)
                print(confs)
                for conf in confs:
                    experiments_to_save.append(Configuration(
                        identification=ident,
                        contrast=conf["group"],
                        name=conf["name"]
                    ))
            Configuration.objects.bulk_create(experiments_to_save)
                # data = pandas.read_table(options.get('file') + file, sep='\t', header=None, index_col=0, names=list(range(50)))
                # data = data.transpose()
                # experiment = Identification(
                #     name=file.split('.')[0],
                #     title=', '.join(data["Investigation Title"].dropna().values) if "Investigation Title" in data else '',
                #     factor_name=', '.join(data["Experimental Factor Name"].dropna().values) if "Experimental Factor Name" in data else '',
                #     factor_type=', '.join(data["Experimental Factor Type"].dropna().values) if "Experimental Factor Type" in data else '',
                #     pub_med_id=', '.join(data["PubMed ID"].dropna().values) if "PubMed ID" in data else '',
                #     publication_title=', '.join(data["Publication Title"].dropna().values) if "Publication Title" in data else '',
                #     publication_authors=', '.join(data["Publication Author List"].dropna().values) if "Publication Author List" in data else '',
                #     protocol_name=', '.join(data["Protocol Name"].dropna().values) if "Protocol Name" in data else '',
                #     protocol_description=', '.join(data["Protocol Description"].dropna().values) if "Protocol Description" in data else ''
                # )
                # experiments_to_save.append(experiment)
                # except IndexError:
                #     print('No organization match for {}'.format(file))
                # except UnicodeDecodeError:
                #     print('No organization match for {}'.format(file))
                # Identification.objects.bulk_create(experiments_to_save)
