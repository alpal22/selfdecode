from django.core.management import BaseCommand
from django.db import transaction
from decodify.helpers import remaining_time
from experiment.models import Configuration


class Command(BaseCommand):

    def handle(self, *args, **options):
        configurations = Configuration.objects.all()
        cnt = configurations.count()
        with transaction.atomic():
            for conf in configurations:
                remaining_time(cnt)
                if conf.condition:
                    # conf.condition = conf.condition.replace("\'", "")
                    # import pdb
                    # pdb.set_trace()                # if "' vs '" in conf.name:
                    cond, comparison = conf.name.split(" vs ")
                    conf.condition = cond.replace("\'", "")
                    conf.comparison = comparison.replace("\'", "")
                    conf.save()
                else:
                    continue
