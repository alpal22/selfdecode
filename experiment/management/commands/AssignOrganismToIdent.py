from django.core.management import BaseCommand
from django.db import transaction

from chemical.models import *
from decodify.helpers import remaining_time
from experiment.models import Analytics, Identification
from genome.models import Synonym


class Command(BaseCommand):

    def handle(self, *args, **options):
        existing_organisms = {record.latin_name: record.id for record in Organism.objects.all()}
        existing_idents = Identification.objects.all()
        cnt = existing_idents.count()
        with transaction.atomic():
            for ident in existing_idents:
                remaining_time(cnt)
                organism = existing_organisms.get(ident.organism_name)
                if organism:
                    ident.organism_id = organism
                    ident.save()
