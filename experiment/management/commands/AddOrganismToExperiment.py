import os
from django.db import transaction

import pandas
from django.core.management import BaseCommand


from decodify.helpers import remaining_time
from experiment.models import Identification


class Command(BaseCommand):
    help = "./manage.py UploadAndAssignChemicalMOAS --file moas.csv"

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)

    def handle(self, *args, **options):
        files = os.listdir(options.get('file'))
        count = len(files)
        with transaction.atomic():
            for file in files:
                remaining_time(count)
                print(file)
                ident = Identification.objects.filter(name=file[:-9]).first()
                if not ident:
                    continue
                data = pandas.read_table(options.get('file') + file, sep='\t')
                organism = data["Characteristics[organism]"][0]
                if not organism:
                    continue

                ident.organism_name = organism
                ident.save()

