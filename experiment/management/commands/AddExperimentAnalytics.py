import os
from django.db import transaction
import xml.etree.ElementTree as ET

import pandas
from django.core.management import BaseCommand


from decodify.helpers import remaining_time
from experiment.models import Identification, Configuration, Analytics


class Command(BaseCommand):
    help = "./manage.py UploadAndAssignChemicalMOAS --file moas.csv"

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)

    def handle(self, *args, **options):
        files = os.listdir(options.get('file'))
        count = len(files)
        i = 0
        with transaction.atomic():
            for file in files:
                i += 1
                print("file %s of %s" % (i, count,))
                experiments_to_save = []
                ident = Identification.objects.filter(name=(file.split('_')[0])).first()
                if not ident:
                    continue

                configurations = Configuration.objects.filter(identification=ident)
                if not configurations.exists():
                    continue

                data = pandas.read_csv(options.get('file') + file, delimiter='\t', header=0)
                data = data.dropna()
                data = data.to_dict('records')
                for row in data:
                    remaining_time(len(data))
                    for configuration in configurations.all():
                        if not configuration.contrast + '.p-value' in row.keys():
                            continue
                        experiments_to_save.append(Analytics(
                            gene_name=row["Gene Name"],
                            design_element=row["Design Element"],
                            configuration=configuration,
                            p_value=row[configuration.contrast + '.p-value'],
                            t_statistic=row[configuration.contrast + '.t-statistic'],
                            log2fold=row[configuration.contrast + '.log2foldchange']
                        ))
                Analytics.objects.bulk_create(experiments_to_save)
