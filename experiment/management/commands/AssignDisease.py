from django.core.management import BaseCommand
from django.db import transaction
import re
from chemical.models import *
from decodify.helpers import remaining_time
from experiment.models import Analytics, Identification, Configuration
from genome.models import Synonym, DiseaseTrait


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, l.count(), n):
        yield l[i:i + n]


class Command(BaseCommand):

    def handle(self, *args, **options):
        idfs = Identification.objects.filter(factor_name__contains="disease").all()
        confs = Configuration.objects.filter(identification__in=idfs)
        cnt = confs.count()
        with transaction.atomic():
            for conf in confs:
                remaining_time(cnt)
                print(conf.name)
                disease_name = re.split(r'[\'\";]', conf.name)[1]
                disease = DiseaseTrait.objects.filter(name__icontains=disease_name).first()
                conf.disease_name = disease_name
                if disease:
                    conf.disease = disease
                conf.save()