from django.core.management import BaseCommand
from django.db import transaction

from chemical.models import *
from decodify.helpers import remaining_time
from experiment.models import Analytics
from genome.models import Synonym


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, l.count(), n):
        yield l[i:i + n]


class Command(BaseCommand):

    def handle(self, *args, **options):
        existing_genes = {record.name.lower().strip(): record.id for record in Gene.objects.all()}
        # synonym_genes = {record.name.lower().strip(): record.gene_set.first().id for record in Synonym.objects.all()}
        # import pdb
        # pdb.set_trace()
        # existing_genes += synonym_genes
        existing_analytics = Analytics.objects.all()
        cnt = existing_analytics.count()
        with transaction.atomic():
            for analytics in chunks(existing_analytics, 200000):
                for analytic in analytics:
                    remaining_time(cnt)
                    gene = existing_genes.get(analytic.gene_name.lower().strip())
                    if gene:
                        analytic.gene_id = gene
                        analytic.save()
