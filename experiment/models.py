from django.db import models

# Create your models here.
from django_extensions.db.fields import AutoSlugField

from chemical.models import Organism
from genome.models import Gene, DiseaseTrait


class Identification(models.Model):
    name = models.CharField(max_length=512, null=True, blank=True)
    slug = AutoSlugField(populate_from='name')
    title = models.CharField(max_length=512, null=True, blank=True)
    factor_name = models.CharField(max_length=512, null=True, blank=True)
    factor_type = models.CharField(max_length=512, null=True, blank=True)
    pub_med_id = models.CharField(max_length=512, null=True, blank=True)
    publication_title = models.TextField(default='', null=True, blank=True)
    publication_authors = models.TextField(default='', null=True, blank=True)
    protocol_name = models.TextField(default='', null=True, blank=True)
    protocol_description = models.TextField(default='', null=True, blank=True)
    organism_name = models.CharField(max_length=500, null=True, blank=True)
    organism = models.ForeignKey(Organism, null=True, blank=True)

    def __str__(self):
        return self.name


class Configuration(models.Model):
    identification = models.ForeignKey(Identification)
    contrast = models.CharField(max_length=500)
    name = models.CharField(max_length=500)
    disease_name = models.CharField(max_length=500, null=True, blank=True)
    disease = models.ForeignKey(DiseaseTrait, null=True, blank=True)
    condition = models.CharField(max_length=512, null=True, blank=True)
    comparison = models.CharField(max_length=512, null=True, blank=True)


class Analytics(models.Model):
    gene_name = models.CharField(max_length=512)
    gene = models.ForeignKey(Gene, null=True, blank=True)
    design_element = models.CharField(max_length=512)
    configuration = models.ForeignKey(Configuration)
    p_value = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=10)
    t_statistic = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=10)
    log2fold = models.DecimalField(null=True, blank=True, max_digits=15, decimal_places=10)


