from django.contrib import admin
from django.forms import Form
from django.http import HttpResponse
from import_export.admin import ExportMixin, ExportActionModelAdmin
from import_export.formats import base_formats
from datetime import timedelta
from django.utils import timezone
from .models import LogEntry


@admin.register(LogEntry)
class AuthorAdmin(ExportActionModelAdmin):
    date_hierarchy = 'time'
    list_display = ('error', 'user', 'time', 'url')
    search_fields = ['user__email', 'url']
    readonly_fields = ('error', 'user', 'time', 'url', 'debug_info', 'debug_info_hash')

    def export_last24_admin_action(self, request, queryset=None):
        queryset = LogEntry.objects.filter(time__gte=timezone.now()-timedelta(days=1))
        formats = self.get_export_formats()
        file_format = formats[0]()
        export_data = self.get_export_data(file_format, queryset)
        content_type = file_format.get_content_type()
        try:
            response = HttpResponse(export_data, content_type=content_type)
        except TypeError:
            response = HttpResponse(export_data, mimetype=content_type)
        response['Content-Disposition'] = 'attachment; filename=%s' % (
            self.get_export_filename(file_format),
        )
        return response

    export_last24_admin_action.short_description = (
        'Export %(verbose_name_plural)s for last 24 hours')
    export_last24_admin_action.acts_on_all = True
    actions = [export_last24_admin_action]

    def changelist_view(self, request, extra_context=None):
        try:
            action = self.get_actions(request)[request.POST['action']][0]
            action_acts_on_all = action.acts_on_all
        except (KeyError, AttributeError):
            action_acts_on_all = False

        if action_acts_on_all:
            post = request.POST.copy()
            post.setlist(admin.helpers.ACTION_CHECKBOX_NAME,
                         self.model.objects.values_list('id', flat=True))
            request.POST = post

        return admin.ModelAdmin.changelist_view(self, request, extra_context)

    def has_add_permission(self, request):
        return False
