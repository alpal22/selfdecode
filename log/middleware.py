import hashlib
import traceback
from datetime import timedelta
from django.utils import timezone

import sys
from django.core.mail import send_mail

from decodify import settings
from .models import LogEntry
from django.core.urlresolvers import RegexURLResolver
from django.http import Http404


def resolver(request):
    """
    Returns a RegexURLResolver for the request's urlconf.

    If the request does not have a urlconf object, then the default of
    settings.ROOT_URLCONF is used.
    """
    from django.conf import settings
    urlconf = getattr(request, "urlconf", settings.ROOT_URLCONF)
    return RegexURLResolver(r'^/', urlconf)


def get_user(request):
    if request.user.is_anonymous():
        user = None
    else:
        user = request.user

    return user


class CustomExceptionLogger:

    def process_exception(self, request, exception):
        if isinstance(exception, Http404):
            return self.handle_404(request, exception)
        else:
            return self.handle_500(request, exception)

    def handle_500(self, request, exception):
        exc_info = sys.exc_info()

        hashed = hashlib.sha1()
        hashed.update(traceback.format_exc().encode())
        hashed = hashed.hexdigest()

        exist_error = LogEntry.objects.filter(debug_info_hash=hashed, time__gte=timezone.now() - timedelta(days=1)).exists()

        entry = LogEntry(
            error='500',
            user=get_user(request),
            url=request.build_absolute_uri(),
            debug_info=traceback.format_exc(),
            debug_info_hash=hashed
        )
        entry.save()
        if settings.ERROR_EMAIL:
            if not exist_error:
                send_mail("500 error",
                          """We have the following error :
                                %
                                From %
                                At %
                                Time %
                                """ % (entry.debug_info, entry.user.email, entry.url, entry.time,),
                          "noreplay@selfdecode.com",
                          [settings.ERROR_EMAIL])
        if settings.DEBUG:
            return self.debug_500_response(request, exception, exc_info)
        else:
            return self.production_500_response(request, exception, exc_info)

    def handle_404(self, request, exception):
        entry = LogEntry(
            error='404',
            user=get_user(request),
            url=request.build_absolute_uri(),
            debug_info=traceback.format_exc()
        )
        entry.save()

        if settings.DEBUG:
            from django.views import debug
            return debug.technical_404_response(request, exception)
        else:
            callback, param_dict = resolver(request).resolve404()
            return callback(request, **param_dict)

    def debug_500_response(self, request, exception, exc_info):
        from django.views import debug
        return debug.technical_500_response(request, *exc_info)

    def production_500_response(self, request, exception, exc_info):
        callback, param_dict = resolver(request).resolve500()
        return callback(request, **param_dict)
