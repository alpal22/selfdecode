from django.contrib.admin.models import LogEntry, CHANGE
from django.utils.encoding import force_text
from django.contrib.admin.options import get_content_type_for_model

from genome.models import ModelFieldChanges


def log_recent_changes(user, object, message, changed_fields=None):
    entry = LogEntry.objects.create(
        user=user,
        content_type_id=get_content_type_for_model(object).pk,
        object_id=object.pk,
        object_repr=force_text(object),
        action_flag=CHANGE,
        change_message=message,
    )

    if changed_fields:
        ModelFieldChanges.objects.create(log_entry=entry, changed_fields=changed_fields)
