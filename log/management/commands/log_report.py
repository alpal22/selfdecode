from django.core.management import BaseCommand
from django.db import transaction
from django.utils.datastructures import OrderedSet

from log.models import LogEntry


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


class Command(BaseCommand):

    def handle(self, *args, **options):
        query_set = LogEntry.objects.all().order_by('-id')
        urls = OrderedSet()
        with transaction.atomic(), open('output.txt', 'w') as f:
            for chunk in chunks(query_set, 1000):
                for item in chunk:
                    if item.error == '500':
                        urls.add('{0}\t{1}\t{2}\n'.format(item.id, item.url, item.error))
            for it in urls:
                f.write(it)
