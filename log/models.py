from django.db import models
from django.contrib.auth.models import User


class LogEntry(models.Model):
    user = models.ForeignKey(User, related_name='logs', blank=True, null=True)
    error = models.CharField(max_length=100, blank=True, null=True)
    time = models.DateTimeField(auto_now_add=True)
    url = models.CharField(max_length=500, blank=True, null=True)
    debug_info = models.TextField(blank=True, null=True)
    debug_info_hash = models.CharField(max_length=512, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'entries'

    def __str__(self):
        return '{} error, {}'.format(self.error, self.time.strftime("%d %b %H:%M"))
