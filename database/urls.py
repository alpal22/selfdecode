from django.conf.urls import url

from database import views

urlpatterns = [
    url(r'^database/', views.DatabaseView.as_view(), name="main"),
    url(r'^list-genes/letter/(?P<letter>[a-z])/page/(?P<page>\d+)', views.AllGenesByLetter.as_view(), name='all-genes-by-letter-with-page'),
    url(r'^list-genes/letter/(?P<letter>[a-z0-9])', views.AllGenesByLetter.as_view(), name='all-genes-by-letter'),
    url(r'^list-genes/page/(?P<page>\d+)', views.AllGenes.as_view(), name="all-genes"),
    url(r'^list-genes/', views.AllGenes.as_view(), name='list-genes'),

    url(r'^list-diseases/letter/(?P<letter>[a-z])/page/(?P<page>\d+)', views.AllDiseasesByLetter.as_view(), name='all-diseases-by-letter-with-page'),
    url(r'^list-diseases/letter/(?P<letter>[a-z0-9])', views.AllDiseasesByLetter.as_view(), name='all-diseases-by-letter'),
    url(r'^list-diseases/page/(?P<page>\d+)', views.AllDiseases.as_view(), name="all-diseases"),
    url(r'^list-diseases/', views.AllDiseases.as_view(), name='list-diseases'),

    url(r'^list-traits/letter/(?P<letter>[a-z])/page/(?P<page>\d+)', views.AllTraitsByLetter.as_view(), name='all-traits-by-letter-with-page'),
    url(r'^list-traits/letter/(?P<letter>[a-z0-9])', views.AllTraitsByLetter.as_view(), name='all-traits-by-letter'),
    url(r'^list-traits/page/(?P<page>\d+)', views.AllTraits.as_view(), name="all-traits"),
    url(r'^list-traits/', views.AllTraits.as_view(), name='list-traits'),

    url(r'^list-measurements/letter/(?P<letter>[a-z])/page/(?P<page>\d+)', views.AllMeasurementsByLetter.as_view(), name='all-measurements-by-letter-with-page'),
    url(r'^list-measurements/letter/(?P<letter>[a-z0-9])', views.AllMeasurementsByLetter.as_view(), name='all-measurements-by-letter'),
    url(r'^list-measurements/page/(?P<page>\d+)', views.AllMeasurements.as_view(), name="all-measurements"),
    url(r'^list-measurements/', views.AllMeasurements.as_view(), name='list-measurements'),

    url(r'^list-biological-process/page/(?P<page>\d+)', views.AllBiologicalProcesses.as_view(), name="all-biological-process"),
    url(r'^list-biological-process/', views.AllBiologicalProcesses.as_view(), name='list-biological-process'),

    url(r'^list-molecular-function/page/(?P<page>\d+)', views.AllMolecularFunctions.as_view(), name="all-molecular-function"),
    url(r'^list-molecular-function/', views.AllMolecularFunctions.as_view(), name='list-molecular-function'),

    url(r'^list-snps/page/(?P<page>\d+)', views.AllSnps.as_view(), name="all-snps"),
    url(r'^list-snps/', views.AllSnps.as_view(), name='list-snps'),

    url(r'^list-health-effects/letter/(?P<letter>[a-z])/page/(?P<page>\d+)', views.AllHealthEffectsByLetter.as_view(), name='all-health-effects-by-letter-with-page'),
    url(r'^list-health-effects/letter/(?P<letter>[a-z0-9])', views.AllHealthEffectsByLetter.as_view(), name='all-health-effects-by-letter'),
    url(r'^list-health-effects/page/(?P<page>\d+)/?$', views.AllHealthEffects.as_view(), name='all-health-effects'),
    url(r'^list-health-effects/?$', views.AllHealthEffects.as_view(), name='list-health_effects'),

    url(r'^list-chemicals/page/(?P<page>\d+)/?$', views.AllChemicals.as_view(), name='all-chemicals'),
    url(r'^list-chemicals/?$', views.AllChemicals.as_view(), name='list-chemicals'),

    url(r'^list-chemicals-categorized/(?P<category>natural-treatments|beneficial-substances|important-natural-compounds|popular-drugs|toxins|drug|obscure_chemicals)/page/(?P<page>\d+)/?$', views.AllChemicalsByCategory.as_view(), name='all-chemicals-categorized'),
    url(r'^list-chemicals-categorized/(?P<category>natural-treatments|beneficial-substances|important-natural-compounds|popular-drugs|toxins|drug|obscure_chemicals)/$', views.AllChemicalsByCategory.as_view(), name='list-chemicals-categorized'),

    url(r'^list-experiments/page/(?P<page>\d+)/$', views.ExperimentList.as_view(), name="list-experiments"),
    url(r'^list-experiments/$', views.ExperimentList.as_view(), name="list-experiments"),
]
