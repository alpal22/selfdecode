import string

from django.db.models import Count
from django.http import JsonResponse
from django.views.generic import ListView
from django.views.generic import TemplateView
from haystack.backends import SQ
from haystack.query import SearchQuerySet

from chemical.models import HealthEffect, Chemical
from chemical.models import SubstanceCategory
from experiment.models import Identification
from genome.mixins import AutocompleteSlugCatMixin, AutocompleteSlugTypeMixin, AutocompleteSlugMixin
from genome.models import Gene, Snp, DiseaseTrait, GeneOntology


class DatabaseView(TemplateView):
    template_name = 'database/database.html'


# Base classes for extending


class AllEnteries(ListView):
    paginate_by = 80
    allow_empty = False
    page_title = ''
    slug = ''

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['letters'] = [letter for letter in string.ascii_uppercase]
        context['title'] = self.page_title
        return context

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            query = request.GET.get('query', '')
            queryset = SearchQuerySet().models(self.model).filter(
                SQ(text__exact=query) | SQ(text__contains=query)
            )[:10]
            results = [item.text for item in queryset]
            data = {
                'query': query,
                'type': self.slug,
                "suggestions": results
            }
            return JsonResponse(data, safe=False)
        return super().get(request, *args, **kwargs)


class AllEnteriesByLetter(AllEnteries):

    def get_queryset(self, **kwargs):
        queryset = super().get_queryset(**kwargs)
        letter = self.kwargs['letter']
        if letter.isdigit():
            return queryset.filter(name__regex=r'^\d.+')
        return queryset.filter(name__startswith=letter)


class AllChemicals(AutocompleteSlugMixin, ListView):
    model = Chemical
    template_name = "database/list/chemical_list.html"
    paginate_by = 100
    context_object_name = 'chemicals'
    slug = 'chemical'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'All Chemicals'
        return context

    def get_queryset(self, **kwargs):
        queryset = super(AllChemicals, self).get_queryset(**kwargs)
        self.category = SubstanceCategory.objects.filter(slug='obscure_chemicals').first()
        return queryset.exclude(categories__in=list(self.category.get_family()))


class AllChemicalsByCategory(AllChemicals):
    template_name = "database/list/chemical_categorized_list.html"

    def get_context_data(self, **kwargs):
        context = super(AllChemicalsByCategory, self).get_context_data(**kwargs)
        context['category'] = self.category.slug
        context['title'] = self.category.name + ' Chemicals'
        return context

    def get_queryset(self, **kwargs):
        if self.kwargs['category'] == "obscure_chemicals":
            queryset = super(AllChemicals, self).get_queryset(**kwargs)
        else:
            queryset = super(AllChemicalsByCategory, self).get_queryset(**kwargs)
        self.category = SubstanceCategory.objects.filter(slug=self.kwargs['category']).first()
        qs = queryset.filter(categories__in=list(self.category.get_family())).annotate(cnt=Count('pk')).values('name', 'slug', 'display_as')
        return qs


class AllGeneOntology(AutocompleteSlugTypeMixin, AllEnteries):
    model = GeneOntology
    type = 'biological-process'
    context_object_name = 'biological_processes'
    page_title = 'All Biological Processes'

    def get_queryset(self, **kwargs):
        queryset = super().get_queryset(**kwargs)
        return queryset.filter(type=self.type)


# All database list


class AllGenes(AllEnteries):
    model = Gene
    template_name = 'database/list/genes.html'
    context_object_name = 'genes'
    page_title = 'All Genes'
    slug = 'gene'


class AllGenesByLetter(AllEnteriesByLetter):
    model = Gene
    template_name = 'database/list/genes_list_pages.html'
    context_object_name = 'genes'
    page_title = 'All Genes'
    slug = 'gene'


class AllSnps(AllEnteries):
    model = Snp
    template_name = "database/list/snps.html"
    context_object_name = 'snp_packs'
    page_title = 'All SNP Packs'
    slug = 'snp'


class AllTraits(AutocompleteSlugCatMixin, AllEnteries):
    model = DiseaseTrait
    template_name = 'database/list/traits.html'
    category = 'Trait'
    context_object_name = 'traits'
    page_title = 'All Traits'
    slug = 'trait'

    def get_queryset(self, **kwargs):
        queryset = super().get_queryset(**kwargs)
        return queryset.filter(category__name=self.category)


class AllTraitsByLetter(AllTraits):
    model = DiseaseTrait
    template_name = 'database/list/traits_list_pages.html'
    category = 'Trait'
    context_object_name = 'traits'
    slug = 'trait'

    def get_queryset(self, **kwargs):
        queryset = super().get_queryset(**kwargs)
        letter = self.kwargs['letter']
        if letter.isdigit():
            return queryset.filter(name__regex=r'^\d.+', category__name=self.category)
        return queryset.filter(name__startswith=letter, category__name=self.category)


class AllMeasurements(AllTraits):
    template_name = 'database/list/measurements.html'
    category = 'Measurement'
    context_object_name = 'measurements'
    page_title = 'All Measurements'
    slug = 'measurement'


class AllMeasurementsByLetter(AllTraitsByLetter):
    template_name = 'database/list/measurements_list_pages.html'
    category = 'Measurement'
    context_object_name = 'measurements'
    page_title = 'All Measurements'
    slug = 'measurement'


class AllDiseases(AllTraits):
    template_name = 'database/list/diseases.html'
    category = 'Disease'
    context_object_name = 'diseases'
    page_title = 'All Diseases'
    slug = 'disease'


class AllDiseasesByLetter(AllTraitsByLetter):
    template_name = 'database/list/diseases_list_pages.html'
    category = 'Disease'
    context_object_name = 'diseases'
    page_title = 'All Diseases'


class AllBiologicalProcesses(AllGeneOntology):
    template_name = 'database/list/biological_process_list.html'
    type = 'biological-process'
    context_object_name = 'biological_processes'
    page_title = 'All Biological Processes'


class AllMolecularFunctions(AllGeneOntology):
    template_name = 'database/list/molecular_function_list.html'
    type = 'molecular-function'
    context_object_name = 'molecular_function'
    page_title = 'All Molecular Functions'


class AllHealthEffects(AutocompleteSlugMixin, ListView):
    model = HealthEffect
    template_name = "database/list/healtheffect_list.html"
    paginate_by = 100
    allow_empty = False
    context_object_name = 'health_effects'
    slug = 'health-effect'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['letters'] = [letter for letter in string.ascii_uppercase]
        context['title'] = 'All Health Effects'
        return context


class AllHealthEffectsByLetter(AllHealthEffects):
    template_name = 'database/list/healtheffect_list_pages.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['letters'] = [letter for letter in string.ascii_uppercase]
        context['title'] = 'All Health Effects'
        return context

    def get_queryset(self, **kwargs):
        queryset = super().get_queryset(**kwargs)
        letter = self.kwargs['letter']
        if letter.isdigit():
            return queryset.filter(name__regex=r'^\d.+')
        return queryset.filter(name__startswith=letter)


class ExperimentList(AutocompleteSlugMixin, ListView):
    model = Identification
    template_name = "database/list/experiments.html"
    paginate_by = 80
    page_title = "All Experiments"
    slug = 'experiment'

    def get_context_data(self, **kwargs):
        context = super(ExperimentList, self).get_context_data(**kwargs)
        context['title'] = self.page_title
        return context
