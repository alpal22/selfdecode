from django.contrib import admin

from blog.forms import CommentAdminForm
from blog.models import Article, Tag, Comment


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    class Media:
        js = ('blog/js/wysiwyg-toolbar.js',)


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    form = CommentAdminForm
