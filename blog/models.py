from django.db import models
from django.contrib.auth.models import User

from ckeditor.fields import RichTextField


class TimeStampedModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    class Meta:
        ordering = ['-updated_at']
        abstract = True


class Tag(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255)

    def __str__(self):
        return self.name


class Article(TimeStampedModel):
    article_name = models.CharField(max_length=140, blank=False)
    slug = models.SlugField(max_length=255)
    article_body = RichTextField()
    tags = models.ManyToManyField(Tag, related_name='articles', blank=True)

    def __str__(self):
        return self.article_name


class Comment(TimeStampedModel):
    author = models.ForeignKey(User, related_name='blog_comments')
    article = models.ForeignKey(Article, related_name='comments')
    body = RichTextField()

    def __str__(self):
        return '{}, {}'.format(self.author, self.article)

