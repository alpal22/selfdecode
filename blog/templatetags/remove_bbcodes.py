import re

from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter
@stringfilter
def remove_bbcodes(value):
    value = re.sub(r"\[.+\]", '', value)
    return value