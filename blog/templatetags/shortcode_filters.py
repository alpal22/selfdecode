from blog.parser import parser
from django import template
register = template.Library()


@register.simple_tag(name='shortcode', takes_context=True)
def shortcodes_replace(context, value):
    return parser.parse(value, context=context)

