from django.conf.urls import url

from blog import views

urlpatterns = [
    url(r'^article/(?P<article_name>[a-z][a-z\-]*[a-z])/article_page/(?P<article_page>\d+)', views.ArticleDetailView.as_view(), name='article-paginated'),
    url(r'^article/(?P<article_name>[a-z][a-z\-]*[a-z])', views.ArticleDetailView.as_view(), name='article'),
    url(r'^tag/(?P<tag>\w+)/page/(?P<page>\d+)', views.AllArticlesByTagView.as_view(), name='all-articles-by-tag-paginated'),
    url(r'^tag/(?P<tag>\w+)', views.AllArticlesByTagView.as_view(), name='all-articles-by-tag'),
    url(r'^page/(?P<page>\d+)$', views.AllArticlesView.as_view(), name='all-articles-paginated'),
    url(r'^$', views.AllArticlesView.as_view(), name='all-articles'),
]
