from genome.helpers import execute_sqla_query
from genome.views import paginate_report
from genome.reports import get_variance_report_data, _snps_has_info, _get_snps_genes
from django.template import loader
from blog.parser.parser import register_shortcode


@register_shortcode
def variance_report(kwargs, context=None, template_name='genome/partials/part_report.html'):

    request = context.get('request')
    user = context.get('request').user
    request.GET = request.GET.copy()
    request.GET.update(kwargs)
    query_set = get_variance_report_data(request)
    query_set = execute_sqla_query(query_set)
    paginator, page_set = paginate_report(request, query_set, per_page=10)
    data_set = page_set.object_list


    # data_set = _snps_has_info(page_set.object_list, request.user.user_profile.active_file)
    # genes = _get_snps_genes(data_set)

    context = {
        'query_set': data_set,
        'page_set': page_set,
        'query_set_count': paginator.count,
        'user': user,
        'report_state': 'variance_report',
        # "genes": genes,
        'request': request,
    }

    template = loader.get_template(template_name)

    return template.render(context)
