import re
from html import parser

REGISTERED_SHORTCODES = {}


def register_shortcode(func):
    print(func.__name__)
    REGISTERED_SHORTCODES[func.__name__] = func
    return func


def parse(value, context=None):
    ex = re.compile(r'\[(.*?)\]')
    groups = ex.findall(value)
    parsed = value

    for item in groups:
        args = __parse_args__(item)
        name = item.partition(' ')[0]
        item = re.escape(item)
        print(REGISTERED_SHORTCODES)
        try:
            function = REGISTERED_SHORTCODES.get(name)
            result = function(args, context=context)
            parsed = re.sub(r'\[' + item + r'\]', result, parsed)
        except ImportError:
            pass

    return parsed


def __parse_args__(value):
    ex = re.compile(r'[ ]*(\w+)=([^" ]+|"[^"]*")[ ]*(?: |$)')
    value = parser.unescape(value)
    groups = ex.findall(value)
    kwargs = {}
    print(value)
    for group in groups:
        print(group)
        if group.__len__() == 2:

            item_key = group[0]
            item_value = group[1]

            if item_value.startswith('"'):
                if item_value.endswith('"'):
                    item_value = item_value[1:]
                    item_value = item_value[:item_value.__len__() - 1]

            kwargs[item_key] = item_value

    return kwargs


