import importlib

from django.apps import AppConfig


class BlogConfig(AppConfig):
    name = 'blog'

    def ready(self):
        from os.path import dirname, basename, isfile
        import glob
        modules = glob.glob(dirname(__file__) + "/parser/shortcodes/*.py")

        file_names = [basename(f)[:-3] for f in modules if isfile(f) and
                      basename(f) != '__init__.py']

        for i in file_names:
            importlib.import_module('blog.parser.shortcodes.{0}'.format(i))
