from django.db.models import Count
from blog.models import Tag


class SidebarMixin:

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tags'] = Tag.objects.annotate(count=Count('articles')).order_by('-count')
        return context
