from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import DetailView, ListView

from blog.forms import CommentForm
from blog.models import Article, Tag, Comment
from blog.helpers import SidebarMixin
from forum.models import UserPermissions

PAGINATE_ARTICLES_BY = 15


class ArticleDetailView(SidebarMixin, DetailView):
    model = Article
    template_name = 'blog/article-detail.html'
    slug_url_kwarg = 'article_name'
    slug_field = 'slug'

    def post(self, *args, **kwargs):
        form = CommentForm(self.request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.author = self.request.user
            form.article = self.get_object()
            form.save()
        if self.request.POST.get('new-username'):
            UserPermissions.objects.update_or_create(user=self.request.user, username_changed=True)
            u = self.request.user
            u.username = self.request.POST['new-username']
            u.save()
        return HttpResponseRedirect(reverse('blog:article', kwargs={'article_name': self.kwargs['article_name']}))

    def _paginate_comments(self, data_list, per_page=PAGINATE_ARTICLES_BY):
        p = self.kwargs.get("article_page")
        paginator = Paginator(data_list, per_page)
        try:
            page_set = paginator.page(p)
        except PageNotAnInteger:
            page_set = paginator.page(1)
        except EmptyPage:
            page_set = paginator.page(paginator.num_pages)
        return page_set

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        all_comments = Comment.objects.filter(article=self.object)
        context['comments'] = self._paginate_comments(all_comments)
        context['form'] = CommentForm()
        return context


class AllArticlesView(SidebarMixin, ListView):
    model = Article
    template_name = 'blog/all-articles.html'
    context_object_name = 'articles'
    paginate_by = PAGINATE_ARTICLES_BY


class AllArticlesByTagView(SidebarMixin, ListView):
    model = Article
    template_name = 'blog/all-articles-by-tag.html'
    context_object_name = 'articles'
    paginate_by = PAGINATE_ARTICLES_BY

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['tag'] = Tag.objects.filter(slug=self.kwargs.get('tag')).first()
        return context

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset(*args, **kwargs)
        return queryset.filter(tags__slug=self.kwargs.get('tag'))
