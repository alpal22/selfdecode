$(document).ready(function () {

    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader("X-CSRFToken", $.cookie.get('csrftoken'));
        }
    });

    function set_rating(instance, total_rating, user_rating) {


        var $score_root = instance.find('.score');

        if(total_rating > 0) {
            $score_root.text('+{rating}'.replace('{rating}', total_rating));
        }
        else if(total_rating < 0) {
            $score_root.text(total_rating);
        }
        else {
            $score_root.text('');
        }

        if(user_rating == 1) {
            instance.find('.up').addClass('green-pointer');
        }
        else if (user_rating == -1) {
            instance.find('.down').addClass('red-pointer');
        }
        else {
            instance.find('.up').removeClass('green-pointer');
            instance.find('.down').removeClass('red-pointer');
        }

    }

    function process_entities(data, arr) {
        $(data).each(function (index, value) {
            var com_id = arr[index];
            var item = $('[data-action="voting"][data-content-type="{0}"][data-object-id="{1}"]'.format(
                value.content_type,
                com_id
            ));

            set_rating(item, value.response.total_rating, value.response.user_rating);

        });
    }

    var comment_ids = [];
    var post_ids = [];

    $('[data-action="voting"][data-content-type="comment"]').each(function (index, value) {
       comment_ids.push($(value).attr('data-object-id'))
    });

    $('[data-action="voting"][data-content-type="article"]').each(function (index, value) {
       post_ids.push($(value).attr('data-object-id'))
    });

    var request_data = [
        {'blog.comment': comment_ids},
        {'blog.article': post_ids}
    ];

    $.post('/single/rating/', JSON.stringify({'objects': request_data})).done(function(data) {


        process_entities(data.response.comment, comment_ids);

        process_entities(data.response.article, post_ids);
    });

    function post_rating(rating) {
        var obj_id = this.data('object-id');
        var request_data = {
            'obj': this.data('content-type'),
            'obj_label': this.data('label'),
            'obj_id': obj_id,
            'user_rating': rating
        };
        var item = $(this);

        $.post('/single/vote/', request_data).done(function (data) {
            set_rating(item, data.total_rating, data.user_rating);
        });
    }


    $('[data-action="voting"]').each(function () {
        var voting = $(this);
        var $up = $(this).find('.up');
        var $down = $(this).find('.down');

        $up.click(function () {
            $down.removeClass('red-pointer');

            if ($(this).hasClass('green-pointer')) {
                post_rating.call(voting, 0);
                $up.removeClass('green-pointer');
            }
            else {
                post_rating.call(voting, 1);
                $up.addClass('green-pointer');
            }
        });

        $down.click(function () {
            $up.removeClass('green-pointer');

            if ($(this).hasClass('red-pointer')) {
                post_rating.call(voting, 0);

                $down.removeClass('red-pointer');
            }
            else {
                post_rating.call(voting, -1);

                $down.addClass('red-pointer');
            }
        });
    });

});