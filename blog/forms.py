from ckeditor.widgets import CKEditorWidget
from django import forms

from blog.models import Comment


class CommentAdminForm(forms.ModelForm):
    body = forms.CharField(widget=CKEditorWidget(config_name='forum_admin'))

    class Meta:
        fields = ['author', 'article', 'body']
        model = Comment


class CommentForm(forms.ModelForm):
    body = forms.CharField(widget=CKEditorWidget(config_name='forum_post'))

    class Meta:
        fields = ['body']
        model = Comment
