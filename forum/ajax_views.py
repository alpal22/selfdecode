from datetime import datetime

from django.core.exceptions import PermissionDenied
from django.db.models import F
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.conf import settings
from django.views.decorators.http import require_POST, require_GET

from decodify.decorators import ajax_required
from haystack.backends import SQ
from haystack.query import SearchQuerySet
from forum.models import Tag, Post, LastView, Comment


@ajax_required
def clear_notifications(request):
    """
    Remove all forum notifications for user
    """
    LastView.objects.filter(user=request.user, post__comments__time__gt=F("last_view")).update(last_view=datetime.now())
    return JsonResponse("OK", safe=False)


@ajax_required
def get_comment_body(request):
    id = request.GET.get('id', '')
    comment = get_object_or_404(Comment, pk=id)
    if request.user == comment.author:
        return JsonResponse({
            'body': comment.body.lstrip().rstrip(),
            'options': settings.CKEDITOR_CONFIGS.get('forum_post')
        })
    else:
        raise PermissionDenied()


@require_POST
def save_comment_body(request):
    comment = get_object_or_404(
        Comment,
        pk=request.POST.get('id'),
        author=request.user
    )
    comment.body = request.POST.get('body', '[deleted]').lstrip().rstrip()
    comment.save()
    return redirect(request.META.get('HTTP_REFERER'))


@require_GET
def get_tags_by_query(request):
    query = request.GET.get("query", "")
    result = {}
    if query:
        sq = SQ()
        list_tags = query.split(',')
        for tag in list_tags:
            sq.add(SQ(text__contains=tag.lower()), SQ.OR)
        tags_queryset = SearchQuerySet().models(Tag).filter(sq)
        posts_queryset = SearchQuerySet().models(Post).filter(sq)
        result = {
            'tags': [dict(name=item.text, slug=item.slug) for item in tags_queryset],
            'posts': [dict(name=item.text, slug=item.slug) for item in posts_queryset][:10],
        }

    return JsonResponse(result, safe=False)
