import random

from django.shortcuts import render, HttpResponseRedirect, get_object_or_404
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.utils.text import slugify
from django.db.models import Count

from log.helpers import log_recent_changes
from .models import Category, Comment, Post, Tag, LastView, UserPermissions
from .forms import CommentForm, PostForm
from decodify.helpers import paginate


@login_required
def index(request):
    categories = Category.objects.annotate(p=Count('posts')).order_by('-p')
    posts = Post.objects.all()
    tags = Tag.objects.annotate(p=Count('posts')).order_by('-p')
    context = {
        'categories': categories,
        'posts': posts,
        'tags': tags,
    }
    return render(request, 'main.html', context)


@login_required
def new_post(request):
    categories = Category.objects.annotate(p=Count('posts')).order_by('-p')
    tags = Tag.objects.annotate(p=Count('posts')).order_by('-p')
    form = PostForm()

    if request.method == "POST":
        form = PostForm(request.POST)

        if form.is_valid():
            instance = form.save(commit=False)
            slug = slugify(instance.title)
            if Post.objects.filter(slug=slug).exists():
                slug += '-{}'.format(random.randint(1, 10000))
            instance.slug = slug
            instance.author = request.user
            instance.save()
            tags = request.POST.get('tags').split(',')
            tags_ = [tag.strip() for tag in tags if tag]

            log_recent_changes(request.user, instance, "New forum post created")

            for tag in tags_:
                item = Tag.objects.get_or_create(name=tag.capitalize(), slug=slugify(tag))
                instance.tag.add(item[0])
            return HttpResponseRedirect(reverse('forum:post', kwargs={'slug': slug}))

    context = {
        'categories': categories,
        'tags': tags,
        'form': form
    }
    return render(request, 'new-post.html', context)


@login_required
def post(request, slug=None, page=None):
    post = Post.objects.get(slug=slug)

    if request.user.is_authenticated():
        LastView.objects.update_or_create(
            post=post,
            user=request.user
        )

    if request.method == "POST":
        form = CommentForm(request.POST)
        form = form.save(commit=False)
        form.author = request.user
        form.post = post
        form.save()
        if request.POST.get('new-username'):
            UserPermissions.objects.update_or_create(user=request.user, username_changed=True)
            u = request.user
            u.username = request.POST['new-username']
            u.save()

    categories = Category.objects.annotate(p=Count('posts')).order_by('-p')
    tags = Tag.objects.annotate(p=Count('posts')).order_by('-p')
    comments = Comment.objects.filter(post__slug=slug)
    form = CommentForm()

    comments = paginate(comments, page, 20)

    context = {
        'categories': categories,
        'post': post,
        'comments': comments,
        'tags': tags,
        'form': form
    }
    return render(request, 'post.html', context)


class SidebarMixin:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.annotate(p=Count('posts')).order_by('-p')
        context['tags'] = Tag.objects.annotate(p=Count('posts')).order_by('-p')
        return context


class CategoryDetail(LoginRequiredMixin, SidebarMixin, DetailView):
    model = Category
    context_object_name = 'category'
    template_name = 'category-posts.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        posts = Post.objects.filter(category__slug=self.kwargs.get('slug'))
        posts = sorted(posts, key=lambda x: x.last_comment_time, reverse=True)

        context['posts'] = paginate(posts, self.kwargs.get('page'), 10)

        return context


class TagDetail(LoginRequiredMixin, SidebarMixin, DetailView):
    model = Tag
    context_object_name = 'tag'
    template_name = 'tag-posts.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        posts = Post.objects.filter(tag__slug=self.kwargs.get('slug'))
        posts = sorted(posts, key=lambda x: x.last_comment_time, reverse=True)

        context['posts'] = paginate(posts, self.kwargs.get('page'), 10)

        return context


class UserPosts(LoginRequiredMixin, SidebarMixin, ListView):
    model = Post
    template_name = 'my-posts.html'
    paginate_by = 10
    context_object_name = 'posts'

    def get_queryset(self, **kwargs):
        queryset = super().get_queryset(**kwargs)
        user_posts = queryset.filter(author=self.request.user)
        posts = sorted(user_posts, key=lambda x: x.last_comment_time, reverse=True)
        return posts


class AllTags(LoginRequiredMixin, SidebarMixin, ListView):
    model = Tag
    template_name = 'all-tags.html'
    context_object_name = 'all_tags'


class RecentPostsView(LoginRequiredMixin, SidebarMixin, ListView):
    model = Post
    template_name = 'recent-posts.html'
    paginate_by = 10
    context_object_name = 'posts'

    def get_queryset(self, **kwargs):
        queryset = super().get_queryset(**kwargs)
        return sorted(queryset, key=lambda x: x.last_comment_time, reverse=True)[:50]


@login_required
def edit_post(request, id):
    categories = Category.objects.annotate(p=Count('posts')).order_by('-p')
    tags = Tag.objects.annotate(p=Count('posts')).order_by('-p')
    post_tags = Tag.objects.filter(posts__id=id)
    current_post = get_object_or_404(Post, pk=id)
    form = PostForm(instance=current_post)

    if request.method == "POST":
        form = PostForm(request.POST, instance=current_post)
        if form.is_valid():
            form.save()
            tags = request.POST.get('tags').split(',')
            tags_ = [tag.strip() for tag in tags if tag]

            log_recent_changes(request.user, current_post, "Post {} has been changed".format(current_post.title))

            for tag in tags_:
                item = Tag.objects.get_or_create(name=tag.capitalize(), slug=slugify(tag))
                current_post.tag.add(item[0])
            return HttpResponseRedirect(reverse('forum:post', kwargs={'slug': current_post.slug}))

    context = {
        'categories': categories,
        'tags': tags,
        'form': form,
        'post_tags': post_tags,
    }
    return render(request, 'edit-post.html', context)
