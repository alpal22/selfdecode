from django.contrib import admin

from forum.forms import CommentAdminForm, PostAdminForm
from .models import Category, Post, Comment, Tag, UserPermissions


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass


@admin.register(UserPermissions)
class UserPermissionsAdmin(admin.ModelAdmin):
    search_fields = ['user']


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    form = PostAdminForm
    list_display = ['title', 'author', 'category', 'time']
    list_filter = ['category']
    search_fields = ['title']
    date_hierarchy = 'time'


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    form = CommentAdminForm
    list_display = ['author', 'time', 'post']
    date_hierarchy = 'time'
    search_fields = ['author', 'body']
