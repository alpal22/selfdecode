from django.conf.urls import url

from . import views
from . import ajax_views

urlpatterns = [
    url(r'^new-post/$', views.new_post, name="new-post"),
    url(r'^edit-post/(?P<id>\d+)/$', views.edit_post, name="edit-post"),
    url(r'^category/(?P<slug>[A-Za-z0-9-_]+)/page/(?P<page>\d+)', views.CategoryDetail.as_view(), name="category-posts-paginated"),
    url(r'^category/(?P<slug>[A-Za-z0-9-_]+)/$', views.CategoryDetail.as_view(), name="category-posts"),
    url(r'^tag/(?P<slug>[A-Za-z0-9-_]+)/page/(?P<page>\d+)', views.TagDetail.as_view(), name="tag-posts-paginated"),
    url(r'^tag/(?P<slug>[A-Za-z0-9-_]+)/$', views.TagDetail.as_view(), name="tag-posts"),
    url(r'^post/(?P<slug>[A-Za-z0-9-_]+)/page/(?P<page>\d+)$', views.post, name="post-paginated"),
    url(r'^post/(?P<slug>[A-Za-z0-9-_]+)/$', views.post, name="post"),
    url(r'^my-posts/page/(?P<page>\d+)$', views.UserPosts.as_view(), name="my-posts-paginated"),
    url(r'^my-posts/$', views.UserPosts.as_view(), name="my-posts"),
    url(r'^recent-posts/page/(?P<page>\d+)/$', views.RecentPostsView.as_view(), name="recent-posts-paginated"),
    url(r'^recent-posts/$', views.RecentPostsView.as_view(), name="recent-posts"),
    url(r'^all-tags/$', views.AllTags.as_view(), name="all-tags"),
    url(r'^$', views.index, name="main"),
    # Ajax views
    url(r'^clear-notifications/$', ajax_views.clear_notifications, name="clear-notifications"),
    url(r'^get-comment-body/$', ajax_views.get_comment_body, name="get-comment-body"),
    url(r'^save-comment-body/$', ajax_views.save_comment_body, name="save-comment-body"),

    url(r'^get-tags-by-query/$', ajax_views.get_tags_by_query, name="get-tags-by-query"),
]
