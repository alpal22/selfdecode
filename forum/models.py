from django.db import models

from django.contrib.auth.models import User
from django.utils import timezone
from django.db.models import Max

from ckeditor.fields import RichTextField


class Category(models.Model):
    name = models.CharField(max_length=100, default="All")
    slug = models.SlugField(max_length=100, default='all')

    class Meta:
        verbose_name_plural = 'Categories'

    def get_top_posts(self, posts=3):
        all_posts = self.posts.all()
        top_posts = sorted(all_posts, key=lambda x: x.last_comment_time, reverse=True)
        return top_posts[:posts]

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255)

    def __str__(self):
        return self.name


class Post(models.Model):
    title = models.CharField(max_length=500)
    category = models.ForeignKey(Category, related_name='posts', null=True, on_delete=models.SET_NULL)
    tag = models.ManyToManyField(Tag, related_name='posts', blank=True)
    author = models.ForeignKey(User)
    time = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    slug = models.SlugField(max_length=500)
    body = RichTextField()

    class Meta:
        ordering = ['time']

    def __str__(self):
        return self.title

    @property
    def last_comment_time(self):
        return self.comments.aggregate(Max('time'))['time__max'] or self.time


class Comment(models.Model):
    author = models.ForeignKey(User)
    post = models.ForeignKey(Post, related_name='comments', default=1)
    fake_username = models.CharField(max_length=100)
    time = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    body = models.TextField()

    class Meta:
        ordering = ['-time']

    def __str__(self):
        return '{}, {}'.format(self.author, self.time)


class LastView(models.Model):
    user = models.ForeignKey(User, related_name='views')
    post = models.ForeignKey(Post, related_name='views')
    last_view = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}  {}'.format(self.user.username, self.post.title)


class UserPermissions(models.Model):
    user = models.ForeignKey(User, related_name='forum_permissions')
    is_banned = models.BooleanField(blank=True, default=False)
    username_changed = models.BooleanField(blank=True, default=False)

    class Meta:
        verbose_name_plural = 'UserPermissions'

    def __str__(self):
        return self.user.username
