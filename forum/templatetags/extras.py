import hashlib
import re

from django import template
from django.db.models import Count
from django.db.models import F
from django.shortcuts import render

from decodify.aggregators import ArrayAgg
from forum.models import LastView
from django.template.defaultfilters import stringfilter, date

from social.models import AccessRequest

register = template.Library()


@register.assignment_tag
def unread_comments(user):
    forum_notifications = LastView.objects.filter(
        user=user,
        post__comments__time__gt=F("last_view")
    ).exclude(
        post__comments__author=user
    ).annotate(
        Count("pk")
    ).select_related("post").count()
    if not user.user_profile.active_file.is_demofile:
        access_notifications = AccessRequest.objects.filter(
            receiver=user,
            status='pending'
        ).count()
    else:
        access_notifications = 0
    return forum_notifications + access_notifications


@register.assignment_tag
def notifications(request, user):
    forum_notifications = LastView.objects.filter(
        user=user,
        post__comments__time__gt=F("last_view")
    ).exclude(
        post__comments__author=user
    ).annotate(
        Count("post"),
        author_name=ArrayAgg("post__comments__author__username", distinct=True)
    ).select_related("post").prefetch_related(
        "post__comments",
        "post__comments__author"
    )
    if not user.user_profile.active_file.is_demofile:
        access_notifications = AccessRequest.objects.filter(
            receiver=user,
            status='pending'
        )
    else:
        access_notifications = AccessRequest.objects.none()

    return render(request, "notifications.html", {
        'forum_notifications': forum_notifications,
        'access_notifications': access_notifications,
    }).content


@register.filter
def to_md5(value):
    return hashlib.md5(value.encode()).hexdigest()


@register.filter
@stringfilter
def repair_bb_links(value):
    value = re.sub(r"URL='(.+)'", lambda x: 'URL=%s' % x.group(1), value)
    value = re.sub(r"\[SIZE.+\]", '', value)
    value = re.sub(r"\[\/SIZE\]", '', value)
    value = re.sub(r"\[USER.+\]", '', value)
    value = re.sub(r"\[MEDIA.+\]", '', value)
    return value
