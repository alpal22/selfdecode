from ckeditor.widgets import CKEditorWidget
from django import forms

from .models import Comment, Post


class CommentForm(forms.ModelForm):
    body = forms.CharField(widget=CKEditorWidget(config_name='forum_post'))

    class Meta:
        model = Comment
        fields = ['body']


class PostForm(forms.ModelForm):
    body = forms.CharField(widget=CKEditorWidget(config_name='forum_post'))

    class Meta:
        model = Post
        fields = ['title', 'category', 'tag', 'body']


class CommentAdminForm(forms.ModelForm):
    body = forms.CharField(widget=CKEditorWidget(config_name='forum_admin'))

    class Meta:
        fields = ['author', 'post', 'time', 'body']
        model = Comment


class PostAdminForm(forms.ModelForm):
    body = forms.CharField(widget=CKEditorWidget(config_name='forum_admin'))

    class Meta:
        fields = '__all__'
        model = Post
