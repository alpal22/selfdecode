import pandas
import phpserialize
import datetime

from django.core.management.base import BaseCommand
from django.utils.text import slugify
from django.contrib.auth.models import User
from forum.models import Category, Post, Comment, Tag
from django.db.utils import IntegrityError
from django.core.mail import send_mail

# from genome.models import DiseaseTrait


class Command(BaseCommand):
    help = """
    Import data to the forum from the selfhacked's forum dump.
    ./manage.py import_forum_data --xf_node xf_node.csv --xf_post xf_post.csv --xf_tag xf_tag.csv --xf_thread xf_thread.csv --xf_user xf_user.csv
    """

    def add_arguments(self, parser):
        parser.add_argument('--xf_node', type=str)
        parser.add_argument('--xf_post', type=str)
        parser.add_argument('--xf_tag', type=str)
        parser.add_argument('--xf_thread', type=str)
        parser.add_argument('--xf_user', type=str)

    def handle(self, *args, **options):
        xf_node = pandas.read_table(options.get('xf_node'))
        xf_post = pandas.read_table(options.get('xf_post'))
        xf_tag = pandas.read_table(options.get('xf_tag'))
        xf_thread = pandas.read_table(options.get('xf_thread'))
        xf_user = pandas.read_table(options.get('xf_user'))

        # Clear data from trash
        xf_post.drop(['username', 'warning_message', 'edit_count', 'last_edit_date', 'last_edit_user_id', 'warning_id', 'position', 'likes', 'like_users', 'attach_count', 'ip_id'], axis=1, inplace=True)
        xf_thread.drop(['reply_count', 'view_count', 'username', 'sticky', 'discussion_state', 'discussion_open', 'discussion_type', 'first_post_likes', 'first_post_likes', 'last_post_date', 'last_post_id', 'last_post_user_id', 'last_post_username', 'prefix_id'], axis=1, inplace=True)
        xf_user.drop(['visible', 'is_admin', 'activity_visible', 'gender', 'user_group_id', 'secondary_group_ids', 'display_style_group_id', 'permission_combination_id', 'message_count', 'conversations_unread', 'trophy_points', 'alerts_unread', 'avatar_date', 'avatar_width', 'avatar_height', 'gravatar', 'is_moderator', 'like_count', 'warning_points', 'is_staff'], axis=1, inplace=True)
        xf_node.drop(['description', 'parent_node_id', 'display_order', 'display_in_list', 'lft', 'rgt', 'style_id', 'effective_style_id', 'breadcrumb_data'], axis=1, inplace=True)

        xf_node.rename(columns={'title': 'category_title'}, inplace=True)
        xf_post.rename(columns={'post_date': 'comment_pub_date'}, inplace=True)
        xf_thread.rename(columns={'title': 'post_title', 'post_date': 'post_pub_date'}, inplace=True)

        xf_thread.tags = xf_thread.tags.map(lambda x: phpserialize.loads(x.encode(), decode_strings=True), na_action='ignore')

        for row in xf_node.itertuples():
            if row.depth >= 1:
                obj, created = Category.objects.get_or_create(name=row.category_title)
                if created:
                    obj.slug = slugify(obj.name)
                    obj.save()
                    self.stdout.write(self.style.SUCCESS('Category {} has been created.'.format(obj.name)))

        old_users = []
        created_users = []
        for row in xf_user.itertuples():
            if not row.is_banned:
                u = User.objects.filter(email=row.email).first()
                if u:
                    old_users.append(u)
                else:
                    password = User.objects.make_random_password()
                    obj = User.objects.create(username=row.username, password=password, email=row.email)
                    obj.save()
                    self.stdout.write(self.style.SUCCESS('User with email: {} and username: {} password: {} has been created'.format(obj.email, obj.username, password)))
                    created_users.append((obj, password))

        print("New users", [(user.email, password) for user, password in created_users])
        print("Old users", [user.email for user in old_users])

        for row in xf_tag.itertuples():
            obj, created = Tag.objects.get_or_create(name=row.tag)
            if created:
                obj.slug = slugify(obj.name)
                obj.save()
                self.stdout.write(self.style.SUCCESS('Tag {} has been created.'.format(obj.name)))

        posts = xf_thread.merge(xf_user, on='user_id')
        posts = posts.merge(xf_post, left_on='first_post_id', right_on='post_id')
        posts = posts.merge(xf_node, on='node_id')
        for row in posts.itertuples():
            if row.message_state == 'visible':
                _user = User.objects.filter(email=row.email).first()
                _cat = Category.objects.filter(name=row.category_title).first()
                if _user and _cat:
                    obj, created = Post.objects.get_or_create(title=row.post_title, author=_user, category=_cat)
                    if created:
                        obj.slug = slugify(obj.title)
                        obj.body = row.message
                        obj.time = datetime.datetime.fromtimestamp(row.post_pub_date)
                        obj.save()
                        # self.stdout.write(self.style.SUCCESS('Post with title: {} was successfully created'.format(row.post_title)))
                        if row.tags:
                            for item in row.tags.values():
                                tag = Tag.objects.filter(name=item['tag']).first()
                                obj.tag.add(tag)
                else:
                    # self.stdout.write(self.style.ERROR('Author with email: {} or category {} are missing in database. Cannot create post'.format(row.email, row.category_title)))
                    continue

        xf_post = xf_post.merge(xf_thread, on="thread_id")
        comments = xf_post[xf_post['post_id'] != xf_post['first_post_id']]
        comments = comments.merge(xf_user, left_on='user_id_x', right_on='user_id')
        for row in comments.itertuples():
            _user = User.objects.filter(email=row.email).first()
            _post = Post.objects.filter(title=row.post_title).first()
            print(_user, _post, row.message)
            if _user and _post:
                obj = Comment.objects.create(post=_post, author=_user)
                obj.time = datetime.datetime.fromtimestamp(row.comment_pub_date)
                obj.body = row.message
                obj.save()
                # self.stdout.write(self.style.SUCCESS('Comment with body: {} was successfully created'.format(row.message)))
            else:
                # self.stdout.write(self.style.ERROR('Author with email: {} or post {} are missing in database. Cannot create comment'.format(row.email, row.post_title)))
                continue

        # old_users = ['sun-shade@ya.ru']
        # created_users = [('khokhlov86@gmail.com', '322')]
        # for email, password in created_users:
        #     print('Sending email to new user with email {} and password {}'.format(email, password))
        #     created_user_email_body = '''
        #     You are new one
        #     Email: {email}
        #     Password: {password}
        #     '''
        #     status = send_mail(
        #         'Forum migrate',
        #         created_user_email_body.format(email=email, password=password),
        #         'support@selfdecode.com',
        #         [email],
        #         fail_silently=False,
        #     )
        #     print("Mail sending status: ", status)

        # for email in old_users:
        #     print('Sending email to old user with email: {}'.format(email))
        #     created_user_email_body = '''
        #     You are old user
        #     Email: {email}
        #     '''
        #     status = send_mail(
        #         'Forum migrate',
        #         created_user_email_body.format(email=email),
        #         'support@selfdecode.com',
        #         [email],
        #         fail_silently=False,
        #     )
        #     print("Mail sending status: ", status)

        # self.stdout.write(self.style.SUCCESS('Done'))
