import uuid

from rating.models import AnonymousUser


def _get_user_intance(request):
    """
    Real user or Anonymous for rating
    :param request: request object
    :return: User instance
    """
    is_anonymous = request.user.is_anonymous()
    if not is_anonymous:
        user_instance = request.user
    else:
        user_instance = _get_anonymous_user(request)

    return user_instance


def _get_anonymous_user(request):
    """
    Get or create anonymous user
    :param request:
    :return:
    """
    uid = request.get_signed_cookie('hash', None)
    if uid:
        anonymous_user = AnonymousUser.objects.filter(hash=uid).first()
    else:
        anonymous_user = None

    if not anonymous_user:
        uid = str(uuid.uuid4())
        ip = request.META.get("REMOTE_ADDR", '')
        user_agent = request.META.get("HTTP_USER_AGENT", '')
        anonymous_user = AnonymousUser.objects.create(hash=uid, ip=ip, user_agent=user_agent)

    return anonymous_user
