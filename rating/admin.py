from django.contrib import admin

from rating.models import AnonymousUser, Vote

admin.site.register(Vote)
admin.site.register(AnonymousUser)
