from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models


class AnonymousUser(models.Model):
    hash = models.CharField(max_length=255)
    ip = models.GenericIPAddressField(blank=True, null=True)
    user_agent = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.hash

    def is_anonymous(self):
        return True


class Vote(models.Model):
    user = models.ForeignKey(User, blank=True, null=True, related_name='votes')
    anonymous_user = models.ForeignKey(AnonymousUser, blank=True, null=True)
    rating = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    def __str__(self):
        return '{} voted for {} with id: {}. Rating: {}'.format(
            self.user or self.anonymous_user,
            self.content_type,
            self.object_id,
            self.rating
        )
