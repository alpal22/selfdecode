from django.conf.urls import url

from rating import ajax_views

urlpatterns = [
    url(r'^stars/rating/', ajax_views.get_stars_rating, name="stars-rating"),
    url(r'^stars/vote/', ajax_views.stars_vote, name="stars-vote"),
    url(r'^single/rating/', ajax_views.single_rating, name="stars-rating"),
    url(r'^single/vote/', ajax_views.single_vote, name="stars-vote"),
]
