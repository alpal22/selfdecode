  $(document).ready(function () {

            var stars = $('#rating-stars').rating({
                theme: 'krajee-fa',
                showClear: false,
                step: 1,
                size: 'xs',
                hoverChangeStars: false,
                starCaptionClasses: function () {
                    return 'label label-danger'
                }
            });

            $.ajaxSetup({
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("X-CSRFToken", $.cookie.get('csrftoken'));
                }
            });

            var obj_data = {
                obj: stars.data('obj'),
                obj_label: stars.data('label'),
                obj_id: stars.data('obj-id')
            };

            $.post('/stars/rating/', obj_data).done(function (data) {
                var user_rating = data.user_rating;
                var avg_rating = data.total_rating;
                stars
                    .rating("update", data.total_rating)
                    .rating("refresh", {starCaptions: function(){return data.votes_count + ' Votes'}});

                function init_handlers() {
                    stars.on('rating.change', function (ent, value) {
                        user_rating = value;
                        var request_body = $.extend(obj_data, {user_rating: user_rating});
                        $.post('/stars/vote/', request_body).done(function (data) {
                            avg_rating = data.total_rating;
                            stars.rating("refresh", {starCaptions: function(){return data.votes_count + ' Votes'}});
                            init_handlers();
                        });
                    });

                    stars.on('rating.hover', function () {
                        stars.rating('update', user_rating);
                    });

                    stars.on('rating.hoverleave', function () {
                        stars.rating('update', avg_rating);
                    });
                }
                init_handlers();
            });

        });