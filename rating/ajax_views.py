import json

from django.contrib.contenttypes.models import ContentType
from django.db.models import IntegerField, Avg
from django.db.models import Sum
from django.http import JsonResponse

from rating.models import Vote
from rating.helpers import _get_user_intance


def get_stars_rating(request):
    """
    Gets rating and average score when loading rating stars
    :param request:
    :return:
    """
    obj = request.POST.get('obj')
    obj_id = request.POST.get('obj_id')
    obj_label = request.POST.get('obj_label')

    user_instance = _get_user_intance(request)

    handler = StarsRatingHandler(obj, obj_id, obj_label, user_instance)

    return JsonResponse(handler.response)


def stars_vote(request):
    """
    Receives votes from the rating stars
    :param request:
    :return:
    """
    obj = request.POST.get('obj')
    obj_id = request.POST.get('obj_id')
    obj_label = request.POST.get('obj_label')
    user_rating = int(request.POST.get('user_rating', 0))
    response = JsonResponse({})

    user_instance = _get_user_intance(request)

    if user_instance.is_anonymous():
        response.set_signed_cookie('hash', user_instance.hash)

    handler = StarsRatingHandler(obj, obj_id, obj_label, user_instance, user_rating)

    response.content = json.dumps(handler.response)

    return response


def single_rating(request):
    objects = json.loads(request.body.decode('utf-8')).get('objects')

    user_instance = _get_user_intance(request)
    final_response = {}
    response = []
    for item in objects:
        key, value = item.popitem()
        label, content_type = key.split('.')
        for item_id in value:
            if content_type == 'gene':
                handler = IncDecSingleRatingHandler(content_type, item_id, label, user_instance)
            else:
                handler = SingleRatingHandler(content_type, item_id, label, user_instance)
            result = handler.response
            response.append({
                'content_type': content_type,
                'label': label,
                'response': result
            })
        final_response[content_type] = response
        response = []

    return JsonResponse({'response': final_response})


def single_vote(request):
    obj = request.POST.get('obj')
    obj_id = request.POST.get('obj_id')
    obj_label = request.POST.get('obj_label')
    user_rating = request.POST.get('user_rating')
    response = JsonResponse({})

    user_instance = _get_user_intance(request)

    if user_instance.is_anonymous():
        response.set_signed_cookie('hash', user_instance.hash)

    if obj == 'gene':
        handler = IncDecSingleRatingHandler(obj, obj_id, obj_label, user_instance, user_rating)
    else:
        handler = SingleRatingHandler(obj, obj_id, obj_label, user_instance, user_rating)

    response.content = json.dumps(handler.response)

    return response


class RatingHandler(object):
    def __init__(self, content_type, obj_id, obj_label, user, rating=None):
        self._vote_objects = Vote.objects.filter(content_type__model=content_type,
                                                 content_type__app_label=obj_label,
                                                 object_id=obj_id)
        if not user.is_anonymous():
            self._user_vote_object = self._vote_objects.filter(user_id=user.pk).first()
        else:
            self._user_vote_object = self._vote_objects.filter(anonymous_user=user).first()

        if self._user_vote_object and rating is not None:
            self.user_rating = self._validate_vote_value(rating)
        elif not self._user_vote_object and rating is not None:
            self._create_vote_instance(content_type, obj_id, obj_label, user, rating)

    @property
    def votes_count(self):
        return self._vote_objects.count()

    @property
    def total_rating(self):
        # TODO OTDEBAJIT
        total = self._vote_objects.aggregate(
            Sum('rating', output_field=IntegerField())
        ).get('rating__sum')

        return total if total else 0

    @property
    def user_rating(self):
        return 0 if not self._user_vote_object else self._user_vote_object.rating

    @user_rating.setter
    def user_rating(self, value):
        validated = self._validate_vote_value(value)
        self._user_vote_object.rating = validated
        self._user_vote_object.save()

    @property
    def user_vote_object(self):
        return self._user_vote_object

    def _validate_vote_value(self, value):
        return int(value)

    def _create_vote_instance(self, content_type, obj_id, label, user, rating):
        extra_fields = {}
        if user.is_anonymous():
            extra_fields['anonymous_user'] = user
        else:
            extra_fields['user'] = user

        obj_contenttype = ContentType.objects.get(model=content_type, app_label=label)
        self._user_vote_object = Vote.objects.create(
            content_type=obj_contenttype,
            object_id=obj_id,
            rating=self._validate_vote_value(rating),
            **extra_fields
        )

    @property
    def response(self):
        return {
            'votes_count': self.votes_count,
            'user_rating': self.user_rating,
            'total_rating': self.total_rating
        }


class StarsRatingHandler(RatingHandler):
    def __init__(self, content_type, obj_id, obj_label, user, rating=None):
        super().__init__(content_type, obj_id, obj_label, user, rating)

    def _validate_vote_value(self, value):
        validated = super()._validate_vote_value(value)
        return validated if 1 <= validated <= 5 else 5  # from 1 to 5 stars

    @property
    def total_rating(self):
        avg_rating = self._vote_objects.aggregate(Avg('rating', output_field=IntegerField()))
        return avg_rating.get('rating__avg')


class SingleRatingHandler(RatingHandler):
    def __init__(self, content_type, obj_id, obj_label, user_id, rating=None):
        super().__init__(content_type, obj_id, obj_label, user_id, rating)

    def _validate_vote_value(self, value):
        validated = super()._validate_vote_value(value)
        return validated if -1 <= validated <= 1 else 1


class IncDecSingleRatingHandler(SingleRatingHandler):
    def __init__(self, content_type, obj_id, obj_label, user_id, rating=None):
        super().__init__(content_type, obj_id, obj_label, user_id, rating)

    @property
    def inc_count(self):
        return self._vote_objects.filter(rating=1).count()

    @property
    def dec_count(self):
        return self._vote_objects.filter(rating=-1).count()

    @property
    def response(self):
        return {
            'inc_count': self.inc_count,
            'dec_count': self.dec_count,
            'user_rating': self.user_rating,
        }
