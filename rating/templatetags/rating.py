from django import template

register = template.Library()


@register.inclusion_tag('rating/rating_tmpl.html', takes_context=True)
def rating(context, label, obj, obj_id):
    return {
        'request': context['request'],
        'obj': obj,
        'obj_id': obj_id,
        'obj_label': label
    }