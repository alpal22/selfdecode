from django.apps import AppConfig


class ModelRelationExistanceConfig(AppConfig):
    name = 'model_relation_existance'
