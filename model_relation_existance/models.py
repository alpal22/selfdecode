from django.db import models

# Create your models here.
from chemical.models import Chemical
from genome.models import Gene, DiseaseTrait, Snp, Pathway


class GeneRelations(models.Model):
    gene = models.OneToOneField(Gene, related_name='relation')
    chemicals_increase = models.BooleanField(default=True)
    chemicals_decrease = models.BooleanField(default=True)
    snps = models.BooleanField(default=True)
    diseases = models.BooleanField(default=True)
    gene_packs = models.BooleanField(default=True)
    pathways = models.BooleanField(default=True)
    experiments_increase = models.BooleanField(default=True)
    experiments_decrease = models.BooleanField(default=True)
    molecular_functions = models.BooleanField(default=True)
    biological_processes = models.BooleanField(default=True)
    transcription_factors = models.BooleanField(default=True)
    drug_bank = models.BooleanField(default=True)
    synonyms = models.BooleanField(default=True)


class ChemicalRelations(models.Model):
    chemical = models.OneToOneField(Chemical, related_name='relation')
    health_effects = models.BooleanField(default=True)
    t3db_data = models.BooleanField(default=True)
    moas = models.BooleanField(default=True)
    targets = models.BooleanField(default=True)
    genes = models.BooleanField(default=True)
    organisms = models.BooleanField(default=True)
    preparations = models.BooleanField(default=True)
    diseases = models.BooleanField(default=True)
    pathways = models.BooleanField(default=True)


class DiseaseTraitsRelations(models.Model):
    disease = models.OneToOneField(DiseaseTrait, related_name='relation')
    genes = models.BooleanField(default=True)
    hierarchy = models.BooleanField(default=True)
    chemicals = models.BooleanField(default=True)
    pathways = models.BooleanField(default=True)
    experiments = models.BooleanField(default=True)
    molecular_functions = models.BooleanField(default=True)
    biological_processes = models.BooleanField(default=True)
    symptoms = models.BooleanField(default=True)


class SnpRelations(models.Model):
    snp = models.OneToOneField(Snp, related_name='relation')
    genes = models.BooleanField(default=True)
    diseases = models.BooleanField(default=True)


class PathwayRelations(models.Model):
    pathway = models.OneToOneField(Pathway, related_name='relation')
    diseases = models.BooleanField(default=True)
    genes = models.BooleanField(default=True)
    chemicals = models.BooleanField(default=True)
