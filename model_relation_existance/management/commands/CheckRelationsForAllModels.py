from django.core.management import BaseCommand
from django.db import transaction

from chemical.models import *
from decodify.helpers import remaining_time
from genome.models import *
from model_relation_existance.models import *


class Command(BaseCommand):
    help = "Make Chemcial slug"

    def process_chemicals(self):
        chemicals = Chemical.objects.all()
        relations_to_create = []
        cnt = chemicals.count()
        with transaction.atomic():
            for chemical in chemicals:
                remaining_time(cnt)
                relations_to_create.append(ChemicalRelations(
                    chemical=chemical,
                    health_effects=chemical.health_effects.exists(),
                    t3db_data=chemical.t3db_data.exists(),
                    moas=chemical.moas.exists(),
                    # targets=chemical,
                    genes=chemical.chemicalgeneinteraction_set.exists(),
                    organisms=chemical.concentrations.filter(rel_type="organism").exists(),
                    preparations=chemical.concentrations.filter(rel_type="preparation").exists(),
                    diseases=chemical.chemicaldiseaseinteraction_set.exists(),
                    pathways=chemical.pathways.exists()
                ))
            ChemicalRelations.objects.bulk_create(relations_to_create)
            print("Finished processing Chemicals")

    def process_genes(self):
        genes = Gene.objects.all()
        relations_to_create = []
        cnt = genes.count()
        with transaction.atomic():
            for gene in genes:
                remaining_time(cnt)
                relations_to_create.append(GeneRelations(
                    gene=gene,
                    chemicals_increase=gene.chemicalgeneinteraction_set.filter(actions__action='increases').exists(),
                    chemicals_decrease=gene.chemicalgeneinteraction_set.filter(actions__action='decreases').exists(),
                    snps=gene.snps.exists(),
                    diseases=gene.diseasetraits.exists(),
                    gene_packs=gene.related_gene_pack.exists(),
                    pathways=gene.global_pathways.exists(),
                    experiments_increase=gene.analytics_set.filter(log2fold__gte=0).exists(),
                    experiments_decrease=gene.analytics_set.filter(log2fold__lte=0).exists(),
                    molecular_functions=gene.go.filter(
                        type="molecular-function").exists(),
                    biological_processes=gene.go.filter(
                        type="biological-process").exists(),
                    transcription_factors=gene.transcription_factors.exists(),
                    drug_bank=gene.uniprot_data.filter(category__slug="drug-bank").exists(),
                    synonyms=gene.synonyms.exists(),

                ))
            GeneRelations.objects.bulk_create(relations_to_create)
            print("Finished processing Genes")

    def process_diseases(self):
        diseases = DiseaseTrait.objects.all()
        relations_to_create = []
        cnt = diseases.count()
        with transaction.atomic():
            for disease in diseases:
                remaining_time(cnt)
                relations_to_create.append(DiseaseTraitsRelations(
                    disease=disease,
                    genes=disease.genediseasetrait_set.exists(),
                    hierarchy=disease.diseasehierarchy_set.exists(),
                    chemicals=disease.chemicaldiseaseinteraction_set.exists(),
                    pathways=disease.genediseasepathwayinteraction_set.exists(),
                    experiments=disease.configuration_set.exists(),
                    molecular_functions=disease.diseasegoassociations_set.filter(gene_ontology__type="molecular-function").exists(),
                    biological_processes=disease.diseasegoassociations_set.filter(gene_ontology__type="biological-process").exists(),
                    symptoms=disease.diseasesymptominteraction_set.exists()
                ))
            DiseaseTraitsRelations.objects.bulk_create(relations_to_create)
            print("Finished processing Disease Traits")

    def process_snps(self):
        snps = Snp.objects.all()
        relations_to_create = []
        cnt = snps.count()
        with transaction.atomic():
            for snp in snps:
                remaining_time(cnt)
                relations_to_create.append(
                    SnpRelations(
                        snp=snp,
                        genes=snp.genes.exists(),
                        diseases=snp.disease_variant.exists()
                    )
                )
            SnpRelations.objects.bulk_create(relations_to_create)
            print("Finished processing SNPs")

    def process_pathways(self):
        pathways = Pathway.objects.all()
        relations_to_create = []
        cnt = pathways.count()
        with transaction.atomic():
            for pathway in pathways:
                remaining_time(cnt)
                relations_to_create.append(
                    PathwayRelations(
                        pathway=pathway,
                        diseases=pathway.genediseasepathwayinteraction_set.exists(),
                        genes=pathway.genes.exists(),
                        chemicals=pathway.chemicalpathway_set.exists()
                    )
                )
            PathwayRelations.objects.bulk_create(relations_to_create)
            print("Finished processing Pathways")

    def handle(self, *args, **options):
        self.process_chemicals()
        self.process_genes()
        self.process_diseases()
        self.process_snps()
        self.process_pathways()
